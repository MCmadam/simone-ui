#!/usr/bin/env bash
# Remove the front-end
usage() {
    echo "$0 STACK_ENV TARGET, e.g. $0 nonprod develop"
}

STACK_ENV=$1
TARGET=$2
BUILD_MODE=$3

if [ "$TARGET" == "" ]; then
    usage
    exit 1
fi

echo Nothing necessary.
