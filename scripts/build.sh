#!/usr/bin/env bash

validate_args() {
  if [ "${STACK_ENV}" == "" ]; then
    echo "Missing argument 'STACK_ENV'"
    echo "This should either be 'alpha' or 'prod'"
    exit 1
  elif [ "${STACK_ENV}" != "alpha" ] && [ "${STACK_ENV}" != "prod" ]; then
    echo "Invalid argument 'STACK_ENV'"
    echo "This should either be 'alpha' or 'prod'"
    exit 1
  fi

  if [ "${TARGET}" == "" ]; then
    echo "Missing argument 'TARGET'"
    echo "e.g. 'develop', 'test', 'acc'"
    exit 1
  fi

  if [ "${BUILD_MODE}" != "production" ] && [ "${BUILD_MODE}" != "development" ]; then
    echo "Invalid argument 'BUILD_MODE'"
    echo "Should either be 'production' or 'development'"
    exit 1
  fi

  echo "=== Build arguments valid"
  echo "STACK_ENV=${STACK_ENV}"
  echo "TARGET=${TARGET}"
  echo "BUILD_MODE=${BUILD_MODE}"
}

STACK_ENV=$1
TARGET=$2
BUILD_MODE=$3

if [ "${BUILD_MODE}" == "" ]; then
  BUILD_MODE="production"
fi

validate_args

echo ""
echo "=== fetching dependencies"
yarn

source scripts/environment.sh ${STACK_ENV} ${TARGET}

yarn

echo ""
echo "=== building mode=${BUILD_MODE} env=${TARGET}"
echo "=== using features"
echo "=== domain_branding=${VUE_APP_FEATURE_DOMAIN_BRANDING}"
npx vue-cli-service build --mode ${BUILD_MODE} --env ${TARGET}
