#!/usr/bin/env bash
set -eou pipefail

validate_args() {
  if [ "${AWS_PROFILE}" == "" ]; then
    echo ""
    echo "Make sure your AWS_PROFILE is set"
    exit 1
  fi

  if [ "${STACK_ENV}" == "" ]; then
    echo ""
    echo "Missing argument 'STACK_ENV'"
    echo "This should either be 'alpha' or 'prod'"
    exit 1
  elif [ "${STACK_ENV}" != "alpha" ] && [ "${STACK_ENV}" != "prod" ]; then
    echo ""
    echo "Invalid argument 'STACK_ENV'"
    echo "This should either be 'alpha' or 'prod'"
    exit 1
  fi

  if [ "${TARGET}" == "" ]; then
    echo ""
    echo "Missing argument 'TARGET'"
    echo "e.g. 'develop', 'test', 'acc', 'prod'"
    exit 1
  fi

  echo "=== Environment arguments valid"
  echo "STACK_ENV=${STACK_ENV}"
  echo "TARGET=${TARGET}"
  echo "==="
}

STACK_ENV=$1
TARGET=$2

validate_args

DOMAIN="part-up.com"
if [ "${STACK_ENV}" != "prod" ]; then
    DOMAIN="part-up.co"
fi

if [ -z ${VUE_APP_FEATURE_DOMAIN_BRANDING+x} ]; then
  if [ "${TARGET}" != "prod" ] && [ "${TARGET}" != "acc" ]; then
    VUE_APP_FEATURE_DOMAIN_BRANDING="false"
  else
    VUE_APP_FEATURE_DOMAIN_BRANDING="true"
  fi
fi

if [ -z ${VUE_APP_GTAG_ID+x} ]; then
  if [ "${TARGET}" == "prod" ]; then
    VUE_APP_GTAG_ID="UA-55573564-8"
  else
    VUE_APP_GTAG_ID="UA-55573564-7"
  fi
fi

UserPoolId=$(aws cognito-idp list-user-pools --max-results 60 | jq --raw-output '.UserPools[] | select(.Name == "UserPool-'${STACK_ENV}'") | .Id')
UserPoolRegion=$(echo ${UserPoolId} | cut -d _ -f 1)
UserPoolClientId=$(aws cognito-idp list-user-pool-clients --user-pool-id ${UserPoolId} | jq --raw-output '.UserPoolClients[] | select(.ClientName == "FrontEnd") | .ClientId')
WssEndpoint=$(aws apigatewayv2 get-apis  | jq --raw-output '.Items[] | select(.Name == "'${STACK_ENV}'-'${TARGET}'-partup-api-websockets") | .ApiEndpoint')

echo Loading the following environment arguments
echo
echo === API
echo UserPoolId: ${UserPoolId}
echo UserPoolRegion: ${UserPoolRegion}
echo UserPoolClientId: ${UserPoolClientId}
echo WssEndpoint: ${WssEndpoint}
echo ===
echo
echo === Services
echo GtagId: ${VUE_APP_GTAG_ID}
echo ===

export VUE_APP_AWS_COGNITO_REGION=${UserPoolRegion}
export VUE_APP_AWS_COGNITO_USER_POOL_ID=${UserPoolId}
export VUE_APP_AWS_COGNITO_USER_POOL_APP_CLIENT_ID=${UserPoolClientId}
export VUE_APP_COGNITO_USE_COOKIES="true"
export VUE_APP_AWS_COGNITO_COOKIE_PATH="/"
export VUE_APP_API_ENDPOINT_URL="https://${TARGET}-api.${STACK_ENV}.${DOMAIN}"
export VUE_APP_API_REGION="eu-west-1"
export VUE_APP_WSS_ENDPOINT_URL="${WssEndpoint}/${STACK_ENV}-${TARGET}"

# Analytics
export VUE_APP_GTAG_ID=${VUE_APP_GTAG_ID}

export VUE_APP_TERMS_OF_USE_URL="https://terms.part-up.com"
export VUE_APP_PRIVACY_STATEMENT_URL="https://privacy.part-up.com"

# Features
export VUE_APP_FEATURE_DOMAIN_BRANDING=${VUE_APP_FEATURE_DOMAIN_BRANDING}
