#!/bin/bash

set -e

run_tests() {
  test=$1

  echo "\nRunning ${test} tests\n"
  yarn "test:${test}"
  if [ $? -eq 0 ];
  then
    echo "\n${test} tests passed\n"
  else
    echo "\nu${test} tests failed, bailing out!\n"
    exit 1
  fi
}

for test in "unit" "component" "snapshot"
do
  run_tests $test
done
