#!/usr/bin/env bash

ENV=$1
BUILD_MODE=$2

if [ "$ENV" == "" ]; then
  ENV="develop"
fi

if [ "$ENV" != "local" ]; then
  source scripts/environment.sh alpha ${ENV}
fi

if [ "$BUILD_MODE" == "" ]; then
  BUILD_MODE="development"
fi

npx vue-cli-service serve --env ${ENV} --mode ${BUILD_MODE}
