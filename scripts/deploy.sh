#!/usr/bin/env bash
# Update the front-end

validate_args() {
  echo "=== Validating deploy arguments"

  if [ "$STACK_ENV" == "" ]; then
    echo "Missing argument 'STACK_ENV'"
    echo "This should either be 'alpha' or 'prod'"
    exit 1
  elif [ "$STACK_ENV" != "alpha" ] && [ "$STACK_ENV" != "prod" ]; then
    echo "Invalid argument 'STACK_ENV'"
    echo "This should either be 'alpha' or 'prod'"
    exit 1
  fi

  if [ "$TARGET" == "" ]; then
    echo "Missing argument 'TARGET'"
    echo "e.g. 'develop', 'test', 'acc'"
    exit 1
  fi

}

STACK_ENV=$1
TARGET=$2
BUILD_MODE=$3

if [ "$TARGET" == "develop" ]; then
    BUILD_MODE="development"
fi

validate_args

DOMAIN="part-up.com"
if [ "$STACK_ENV" != "prod" ]; then
    DOMAIN="part-up.co"
fi

scripts/build.sh ${STACK_ENV} ${TARGET} ${BUILD_MODE} || exit 1

FrontEndUploadRegion="eu-west-1"
DistributionId=$(aws cloudfront list-distributions | jq --raw-output '.DistributionList.Items[] | select(.Aliases.Items[] == "'${TARGET}'.'${STACK_ENV}'.'${DOMAIN}'") | .Id')
if [ "${DistributionId}" == "" ]; then
    echo - looking for wildcard distribution
    DistributionId=$(aws cloudfront list-distributions | jq --raw-output '.DistributionList.Items[] | select(.Aliases.Items[] == "'${STACK_ENV}'.'${DOMAIN}'") | .Id')

    if [ "${DistributionId}" == "" ]; then
        echo Could not find DistributionId.
        exit 1
    fi
fi

echo ""
echo "=== Synchronising with FrontEnd bucket"
aws --region ${FrontEndUploadRegion} s3 sync dist s3://${TARGET}.${STACK_ENV}.${DOMAIN}

echo ""
echo "=== Invalidating CloudFront index"
aws cloudfront create-invalidation --distribution-id ${DistributionId} --paths "/index.html" "/service-worker.js" "/nomodule.js"
