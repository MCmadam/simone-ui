import Vue from 'vue'

// #region plugins
import '@/plugins/globals'
import '@/plugins/logger'
import '@/plugins/router'
import '@/plugins/store'
import '@/plugins/ability'
import '@/plugins/i18n'
import '@/plugins/modal'
import '@/plugins/dialog'
import '@/plugins/websocket'
import '@/plugins/gtag'
// #endregion

import '@/api/config'

// #region app deps
import router from '@/router'
import store from '@/store'
import i18n from '@/util/lang/i18n'
// #endregion

import '@/assets/scss/main.scss'
import '@/components/_global-components'
import '@/filters/_global-filters'

import boot from './boot'
import app from './components/app.vue'

Vue.config.productionTip = false

boot(() => {
  new Vue({
    i18n,
    router,
    store,
    render: (h) => h(app),
  }).$mount('#root')
})
