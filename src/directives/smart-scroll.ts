import debounce from 'lodash/debounce'
import throttle from 'lodash/throttle'

const directive = (el) => {
  let prev = null
  let current = null

  return throttle((e) => {
    prev = current
    current = e.target.scrollingElement.scrollTop

    if (current > 60) {
      el.classList.add('scrolled-60-down')
    } else {
      el.classList.remove('scrolled-60-down')
    }

    if (current <= 10) {
      el.classList.remove('scrolling-down')
      el.classList.remove('scrolling-up')
    } else if (current > prev) {
      el.classList.remove('scrolling-up')
      el.classList.add('scrolling-down')
    } else if (prev > current) {
      el.classList.remove('scrolling-down')
      el.classList.add('scrolling-up')
    }
  }, 250)
}

const inserted = (el) => {
  const handler = directive(el)

  window.document.addEventListener('scroll', handler, { passive: true })

  el._smartScrollHandler = handler
}

const unbind = (el) => {
  window.document.removeEventListener('scroll', el._smartScrollHandler)
}

export default {
  inserted,
  unbind,
}
