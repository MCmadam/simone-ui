import get from 'lodash/get'
import noop from 'lodash/noop'
import pick from 'lodash/pick'
import hasOwn from '@/util/object/has-own'
import { swipeDown, swipeLeft, swipeRight, swipeUp } from '@/util/gestures'

const invokeSafe = (obj, path, ...args) => {
  return get(obj, path, noop).apply(obj, args)
}

export const handleGesture = (wrapper) => {
  const { touchstartX, touchendX, touchstartY, touchendY } = wrapper

  Object.assign(wrapper, {
    offsetX: touchendX - touchstartX,
    offsetY: touchendY - touchstartY,
  })

  if (swipeLeft(wrapper)) {
    invokeSafe(wrapper, 'left', wrapper)
  } else if (swipeRight(wrapper)) {
    invokeSafe(wrapper, 'right', wrapper)
  } else if (swipeUp(wrapper)) {
    invokeSafe(wrapper, 'up', wrapper)
  } else if (swipeDown(wrapper)) {
    invokeSafe(wrapper, 'down', wrapper)
  }
}

export const touchstart = (event, wrapper) => {
  const touch = event.changedTouches[0]

  Object.assign(wrapper, {
    touchstartX: touch.clientX,
    touchstartY: touch.clientY,
    originalEvent: event,
  })

  invokeSafe(wrapper, 'start', wrapper)
}

export const touchend = (event, wrapper) => {
  const touch = event.changedTouches[0]

  Object.assign(wrapper, {
    touchendX: touch.clientX,
    touchendY: touch.clientY,
    originalEvent: event,
  })

  invokeSafe(wrapper, 'end', wrapper)
  handleGesture(wrapper)
}

export const touchmove = (event, wrapper) => {
  const touch = event.changedTouches[0]

  Object.assign(wrapper, {
    touchmoveX: touch.clientX,
    touchmoveY: touch.clientY,
    originalEvent: event,
  })

  invokeSafe(wrapper, 'move', wrapper)
}

export const createHandlers = (handlers) => {
  const wrapper = {
    touchstartX: 0,
    touchendX: 0,
    touchstartY: 0,
    touchendY: 0,
    touchmoveX: 0,
    touchmoveY: 0,
    offsetX: 0,
    offsetY: 0,
  }

  Object.assign(
    wrapper,
    pick(handlers, ['left', 'right', 'up', 'down', 'start', 'move', 'end'])
  )

  return {
    touchstart: (e) => touchstart(e, wrapper),
    touchend: (e) => touchend(e, wrapper),
    touchmove: (e) => touchmove(e, wrapper),
  }
}

export default {
  inserted(el, binding, vnode) {
    const value = binding.value || {}

    const { target, parent, options = { passive: true } } = value

    const targetEl = target ? target() : parent ? el.parentElement : el

    const handlers = createHandlers(value)

    targetEl._touchHandlers = Object(target._touchHandlers)
    targetEl._touchHandlers[vnode.context._uid] = handlers

    for (const key in handlers) {
      if (hasOwn(handlers, key)) {
        targetEl.addEventListener(key, handlers[key], options)
      }
    }
  },
  unbind(el, binding, vnode) {
    const value = binding.value || {}
    const { target, parent } = value

    const targetEl = target ? target() : parent ? el.parentElement : el

    if (!targetEl || !targetEl._touchHandlers) {
      return
    }

    const handlers = targetEl._touchHandlers[vnode.context._uid]

    for (const key in handlers) {
      if (hasOwn(handlers, key)) {
        targetEl.removeEventListener(key, handlers[key])
      }
    }
  },
}
