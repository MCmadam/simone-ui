import isFunction from 'lodash/isFunction'
import { clickedInElements } from '@/util/html'

/**
 * directive
 * @param {BrowserEvent} event Native click event
 * @param {HTMLElement} el native html element
 * @param {Function} callback cb
 * @param {Object} options see next
 *
 * options = {
 *  include: [] -> The elements that will not trigger the click-outside
 *  condition: () => true/false -> Will only callback when evaluates to true
 * }
 */
const directive = (event, el, callback, options = {}) => {
  const { include, condition = () => false } = options

  if (!el || condition(el, event) === false) {
    return
  }

  const { clientX, clientY, isTrusted, pointerType } = event

  if (
    ('isTrusted' in event && !isTrusted) ||
    ('pointerType' in event && !pointerType) ||
    (clientX === 0 && clientY === 0) // pressing enter on input field which triggers a click on submit button
  ) {
    return
  }

  const elements = Array.isArray(include)
    ? include
    : isFunction(include)
    ? include()
    : []

  elements.push(el)

  if (!clickedInElements(elements, clientX, clientY)) {
    callback(event)
  }
}

/**
 * Click outside directive
 *
 * value: {
 *  conition: Function
 *  handler: Function
 *  include: [HTMLDomElements]
 * }
 */
export default {
  inserted(el, binding, vnode) {
    let callback
    let options

    if (isFunction(binding.value)) {
      callback = binding.value
    } else {
      callback = binding.value.handler
      options = binding.value
    }

    if (!isFunction(callback)) {
      return
    }

    const onClick = (e) => directive(e, el, callback, options)

    document.body.addEventListener('click', onClick, { passive: true })
    // eslint-disable-next-line no-param-reassign
    el._clickOutside = onClick
  },
  unbind(el) {
    document.body.removeEventListener('click', el._clickOutside)
    // eslint-disable-next-line no-param-reassign
    delete el._clickOutside
  },
}
