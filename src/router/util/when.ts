import { NavigationGuard, Route, RawLocation } from 'vue-router'

const when = (asserter: Function, guard: NavigationGuard) => {
  return async (
    to: Route,
    from: Route,
    next: (to?: RawLocation | false | Function | void) => void
  ) => {
    if (await asserter(to, from)) {
      await guard(to, from, next)
    } else {
      next()
    }
  }
}

export default when
