import { Route, RouteRecord } from 'vue-router'

export const makeRouteRecord = (
  route: Route,
  recordProps?: Partial<Pick<RouteRecord, 'parent'>>
) => {
  const record: RouteRecord = {
    path: route.path,
    components: {
      default: {},
    },
    regex: new RegExp(route.path),
    props: {},
    name: route.name,
    meta: route.meta,
    instances: {},
  }

  if (recordProps) {
    if ('parent' in recordProps) {
      record.parent = recordProps.parent
    }
  }

  return record
}

export const makeRoute = (
  name: string,
  path: string,
  routeProps?: Partial<Pick<Route, 'meta' | 'params' | 'query'>>,
  parent?: RouteRecord
) => {
  const fullPath = parent ? parent.path + path : path

  const route: Route = {
    fullPath,
    hash: fullPath,
    matched: [],
    meta: {},
    name,
    path,
    params: {},
    query: {},
  }

  const matched: RouteRecord[] = []
  if (parent) {
    matched.push(parent)
    matched.push(makeRouteRecord(route, { parent }))
  } else {
    matched.push(makeRouteRecord(route))
  }
  route.matched = matched

  if (routeProps) {
    if ('meta' in routeProps) {
      route.meta = routeProps.meta
    }

    if ('params' in routeProps) {
      route.params = routeProps.params
    }

    if ('query' in routeProps) {
      route.query = routeProps.query
    }
  }

  return route
}
