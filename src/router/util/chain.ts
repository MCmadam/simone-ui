import { NavigationGuard, Route, RawLocation } from 'vue-router'

const chain = (...guards: Array<NavigationGuard>) => {
  return async (
    to: Route,
    from: Route,
    next: (to?: RawLocation | false | Function | void) => void
  ) => {
    let didNext = false

    const wrapped = (...args: Array<any>) => {
      if (args.length > 0) {
        didNext = true
        next(...args)
      }
    }

    for (const guard of guards) {
      await guard(to, from, wrapped)

      if (didNext) {
        return
      }
    }

    next()
  }
}

export default chain
