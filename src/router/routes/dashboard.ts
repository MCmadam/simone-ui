import chain from '@/router/util/chain'
import ensureLogin from '@/router/guards/ensure-login'
import layout from '@/router/guards/layout'

export default [
  {
    name: 'dashboard',
    path: '/dashboard',
    components: {
      default: () =>
        import(
          /* webpackChunkName: "view-dashboard" */ '@/components/views/dashboard/content/content.vue'
        ),
      header: () =>
        import(
          /* webpackChunkName: "view-dashboard-header" */ '@/components/views/dashboard/header/header.vue'
        ),
    },
    beforeEnter: chain(
      ensureLogin,
      layout('default', {
        defaultConfig: {
          header: {
            expanded: false,
          },
        },
        view: 'dashboard',
      })
    ),
  },
]
