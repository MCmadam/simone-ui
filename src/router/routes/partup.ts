import chain from '@/router/util/chain'
import ensureLogin from '@/router/guards/ensure-login'
import layout from '@/router/guards/layout'

export default [
  {
    name: 'partup-create',
    path: '/partups/aanmaken',
    components: {
      header: () =>
        import(
          /* webpackChunkName: "view-partup-header" */ '@/components/views/partup/header/header.vue'
        ),
    },
    beforeEnter: chain(
      ensureLogin,
      layout('default', {
        defaultConfig: {
          header: {
            expanded: true,
          },
        },
        view: 'partupCreate',
      })
    ),
  },

  /**
   * single partup view
   * @description this route does not render a component directly,
   * the last child route which acts as a whildcard renders the component instead
   */
  {
    path: '/partups/:partupSlug',
    components: {
      default: () =>
        import(
          /* webpackChunkName: "view-partup" */ '@/components/views/partup/partup/content/content.vue'
        ),
      header: () =>
        import(
          /* webpackChunkName: "view-partup-header" */ '@/components/views/partup/header/header.vue'
        ),
      toolbar: () =>
        import(
          /* webpackChunkName: "view-partup-toolbar" */ '@/components/views/partup/partup/toolbar/toolbar.vue'
        ),
      aside: () =>
        import(
          /* webpackChunkName: "view-partup-aside" */ '@/components/views/partup/partup/aside/aside.vue'
        ),
    },
    beforeEnter: chain(
      ensureLogin,
      layout('default', {
        defaultConfig: {
          header: {
            expanded: true,
          },
          aside: {
            expanded: true,
          },
        },
        depends: true,
        view: 'partup',
      })
    ),

    children: [
      {
        name: 'partup-activity',
        path: 'activiteiten/:activityUuid',
        component: () =>
          import(
            '@/components/views/partup/partup/content/activity/activity.vue'
          ),
      },
      {
        name: 'partup-board',
        path: 'board',
        component: () =>
          import('@/components/views/partup/partup/content/board/board.vue'),
      },
      {
        name: 'partup-partners',
        path: 'partners',
        component: () =>
          import(
            '@/components/views/partup/partup/content/partners/partners.vue'
          ),
      },
      {
        name: 'partup',
        path: '',
        component: () =>
          import(
            '@/components/views/partup/partup/content/activities/activities.vue'
          ),
      },
    ],
  },
]
