import chain from '@/router/util/chain'
import ensureLogin from '@/router/guards/ensure-login'
import layout from '@/router/guards/layout'

export default [
  {
    path: '/werk',
    components: {
      header: () =>
        import('@/components/views/find-awesome-work/header/header.vue'),
      default: () =>
        import('@/components/views/find-awesome-work/content/content.vue'),
    },
    beforeEnter: chain(
      ensureLogin,
      layout('default', {
        defaultConfig: {
          header: {
            expanded: false,
          },
          aside: {
            expanded: false,
          },
        },
        view: 'find-awesome-work',
      })
    ),
    children: [
      {
        name: 'find-awesome-work-journey',
        path: 'ontdekken',
        component: () =>
          import(
            '@/components/views/find-awesome-work/content/journey/journey.vue'
          ),
      },
      {
        name: 'find-awesome-work',
        path: '',
        component: () =>
          import(
            '@/components/views/find-awesome-work/content/overview/overview.vue'
          ),
      },
    ],
  },
]
