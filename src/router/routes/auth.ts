import chain from '@/router/util/chain'
import ensureLogout from '@/router/guards/ensure-logout'
import layout from '@/router/guards/layout'

const component = () =>
  import(
    /* webpackChunkName: "view-auth" */ '@/components/views/auth/view-auth.vue'
  )

export default [
  // #region login
  {
    name: 'login',
    path: '/inloggen',
    component,
    beforeEnter: chain(ensureLogout, layout('modal')),
  },
  // #endregion
  // #region register
  {
    name: 'register-confirm',
    path: '/registreren/bevestigen/:cognitoUserId',
    component,
    beforeEnter: chain(ensureLogout, layout('modal')),
  },
  {
    name: 'register-resend',
    path: '/registreren/verificatie-aanvragen',
    component,
    beforeEnter: chain(ensureLogout, layout('modal')),
  },
  {
    name: 'register',
    path: '/registreren',
    component,
    beforeEnter: chain(ensureLogout, layout('modal')),
  },
  // #endregion
  // #region forgot-password
  {
    name: 'forgot-password-submit',
    path: '/wachtwoord-vergeten/bevestigen',
    component,
    beforeEnter: chain(layout('modal')),
  },
  {
    name: 'forgot-password',
    path: '/wachtwoord-vergeten',
    component,
    beforeEnter: chain(layout('modal')),
  },
  // #endregion
]
