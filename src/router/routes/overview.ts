import chain from '@/router/util/chain'
import layout from '@/router/guards/layout'
import ensureLogin from '../guards/ensure-login'

export default [
  {
    name: 'overview-user',
    path: '/mensen',
    components: {
      default: () =>
        import(
          /* webpackChunkName: "view-overview-user-content" */ '@/components/views/overview/user/content/content.vue'
        ),
      header: () =>
        import(
          /* webpackChunkName: "view-overview-user-header" */ '@/components/views/overview/user/header/header.vue'
        ),
    },
    beforeEnter: chain(
      ensureLogin,
      layout('default', {
        defaultConfig: {
          header: {
            expanded: false,
          },
        },
        view: 'overviewUser',
      })
    ),
  },
  // {
  //   name: 'overview-partup',
  //   path: '/partups',
  //   components: {
  //     default: () => import(/* webpackChunkName: "view-overview-partup-content" */ '@/components/views/overview/partup/content/content.vue'),
  //     header: () => import(/* webpackChunkName: "view-overview-partup-header" */ '@/components/views/overview/partup/header/header.vue'),
  //   },
  //   beforeEnter: chain(
  //     ensureLogin,
  //     layout('default', {
  //       defaultConfig: {
  //         header: {
  //           expanded: false,
  //         },
  //       },
  //       view: 'overviewPartup',
  //     }),
  //   ),
  // },
]
