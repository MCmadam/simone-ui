import chain from '@/router/util/chain'
import layout from '@/router/guards/layout'
import ensureGroup from '@/router/guards/ensure-group'
import redirect from '@/router/guards/redirect'

export default [
  {
    name: 'admin',
    path: '/admin',
    components: {
      default: () =>
        import(
          /* webpackChunkName: "view-admin" */ '@/components/views/admin/content/content.vue'
        ),
      aside: () =>
        import(
          /* webpackChunkName: "view-admin-aside" */ '@/components/views/admin/aside/aside.vue'
        ),
    },
    beforeEnter: chain(
      ensureGroup('super_admins', 'dashboard'),
      layout('default', {
        defaultConfig: {
          aside: {
            expanded: true,
          },
        },
        view: 'admin',
      }),
      redirect('admin-organization-overview', (to) => to.name === 'admin')
    ),
    children: [
      {
        name: 'admin-organization-form',
        path: 'organisatie/:organizationUuid?',
        component: () =>
          import(
            /* webpackChunkName: "view-admin-organization-form" */ '@/components/views/admin/content/organization/form/content-admin-organization-form.vue'
          ),
      },
      {
        name: 'admin-organization-overview',
        path: 'organisaties',
        component: () =>
          import(
            /* webpackChunkName: "view-admin-organization-overview" */ '@/components/views/admin/content/organization/overview/content.vue'
          ),
      },
    ],
  },
]
