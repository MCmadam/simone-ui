import chain from '@/router/util/chain'
import ensureLogout from '@/router/guards/ensure-logout'
import layout from '@/router/guards/layout'

export default [
  {
    name: 'home',
    path: '/',
    component: () => import('@/components/views/auth/view-auth.vue'),
    beforeEnter: chain(ensureLogout, layout('modal')),
  },
  {
    name: 'not-found',
    path: '*',
    component: {
      render: () => '<div>page not found</div>',
    },
  },
]
