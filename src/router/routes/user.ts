import chain from '@/router/util/chain'
import ensureLogin from '@/router/guards/ensure-login'
import layout from '@/router/guards/layout'

export default [
  {
    name: 'user-profile',
    path: '/mensen/:userSlug',
    components: {
      default: () =>
        import(
          /* webpackChunkName: "view-user-profile-content" */ '@/components/views/user/profile/content/content.vue'
        ),
      header: () =>
        import(
          /* webpackChunkName: "view-user-profile-header" */ '@/components/views/user/profile/header/header.vue'
        ),
    },
    beforeEnter: chain(
      ensureLogin,
      layout('default', {
        defaultConfig: {
          header: {
            expanded: false,
          },
        },
        view: 'userProfile',
      })
    ),
  },
  {
    name: 'user-settings',
    path: '/instellingen',
    components: {
      default: () =>
        import(
          /* webpackChunkName: "view-user-settings-content" */ '@/components/views/user/settings/content/content.vue'
        ),
      header: () =>
        import(
          /* webpackChunkName: "view-user-settings-header" */ '@/components/views/user/settings/header/header.vue'
        ),
    },
    beforeEnter: chain(
      ensureLogin,
      layout('default', {
        defaultConfig: {
          header: {
            expanded: false,
          },
        },
        view: 'userSettings',
      })
    ),
  },
]
