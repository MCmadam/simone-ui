import chain from '@/router/util/chain'
import ensureLogin from '@/router/guards/ensure-login'
import layout from '@/router/guards/layout'

export default [
  {
    name: 'create-work',
    path: '/maak-werk-aan',
    components: {
      aside: () =>
        import('@/components/views/create-work/aside/aside-create-work.vue'),
      header: () =>
        import('@/components/views/create-work/header/header-create-work.vue'),
      default: () =>
        import(
          '@/components/views/create-work/content/content-create-work.vue'
        ),
    },
    beforeEnter: chain(
      ensureLogin,
      layout('default', {
        defaultConfig: {
          header: {
            expanded: false,
          },
          aside: {
            expanded: true,
          },
        },
        view: 'create-work',
      })
    ),
  },
]
