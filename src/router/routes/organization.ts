import chain from '@/router/util/chain'
import layout from '@/router/guards/layout'
import ensureLogin from '@/router/guards/ensure-login'

export default [
  {
    name: 'organization',
    path: '/organisaties/:organizationSlug',
    components: {
      default: () =>
        import(
          /* webpackChunkName: "view-organization-content" */ '@/components/views/organization/content/content.vue'
        ),
      header: () =>
        import(
          /* webpackChunkName: "view-organization-content" */ '@/components/views/organization/header/header.vue'
        ),
    },
    beforeEnter: chain(
      ensureLogin,
      layout('default', {
        defaultConfig: {
          header: {
            expanded: false,
          },
        },
        depends: true,
        view: 'organization',
      })
    ),
  },
]
