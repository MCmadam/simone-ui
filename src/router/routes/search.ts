import chain from '@/router/util/chain'
import layout from '@/router/guards/layout'
import ensureLogin from '../guards/ensure-login'

export default [
  {
    name: 'search',
    path: '/zoeken',
    components: {
      default: () =>
        import(
          /* webpackChunkName: "view-search-content" */ '@/components/views/search/content/content.vue'
        ),
      header: () =>
        import(
          /* webpackChunkName: "view-search-header" */ '@/components/views/search/header/header.vue'
        ),
    },
    beforeEnter: chain(
      ensureLogin,
      layout('default', {
        defaultConfig: {
          header: {
            expanded: true,
          },
        },
        view: 'search',
      })
    ),
  },
]
