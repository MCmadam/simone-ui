import Router from 'vue-router'
import autoLogout from './guards/auto-logout'

const routes: any[] = []
const routesContext = require.context('./routes', false, /\.ts$/)
routesContext.keys().forEach((fileName) => {
  const resolved = routesContext(fileName)
  const route = resolved.default
  routes.push(...route)
})

const router = new Router({
  mode: 'history',
  linkActiveClass: 'route-active',
  linkExactActiveClass: 'route-active-exact',
  routes,
})
;[autoLogout].forEach(router.beforeEach, router)

export default router
