import when from '@/router/util/when'
import asserter from '@/util/function/asserter'
import store from '@/store'
import { redirect } from '@/util/router'

/**
 * ensures a viewer is not loggedIn,
 * when a viewer is loggedIn it will redirect to dashboard.
 */
export default when(
  asserter(() => store.getters['loggedIn'], true),
  redirect('dashboard')
)
