export default (routeName: string, condition?: (to, from) => boolean) => {
  return (to, from, next) => {
    if (condition) {
      if (condition(to, from)) {
        next({
          name: routeName,
        })
      }
    } else {
      next({
        name: routeName,
      })
    }
  }
}
