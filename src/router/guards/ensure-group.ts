import when from '@/router/util/when'
import isUserInGroup from '@/data/auth/is-user-in-group'
import { redirect } from '@/util/router'

export default (group: string, redirectRouteName: string) =>
  when(
    async () => !(await isUserInGroup(group)),
    redirect(redirectRouteName, false)
  )
