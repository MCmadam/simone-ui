import store from '@/store'
import ensureLogin from './ensure-login'
import { makeRoute } from '../util/test-helpers'
jest.mock('@/store')

describe('when I want to expose a route only to loggedIn users', () => {
  const nextMock = jest.fn()

  const toRouteMock = makeRoute('toRoute', '/toRoute')
  const fromRouteMock = makeRoute('fromRoute', '/fromRoute')

  beforeEach(() => {
    nextMock.mockClear()
  })

  describe('and the user is loggedIn', () => {
    beforeEach(() => {
      store.getters['loggedIn'] = true
    })

    it('will not redirect', async () => {
      expect.assertions(1)

      await ensureLogin(toRouteMock, fromRouteMock, nextMock)

      expect(nextMock).toHaveBeenCalledWith()
    })
  })

  describe('and the user is not loggedIn', () => {
    beforeEach(() => {
      store.getters['loggedIn'] = false
    })

    it('will redirect to login', async () => {
      expect.assertions(1)

      await ensureLogin(toRouteMock, fromRouteMock, nextMock)

      expect(nextMock).toHaveBeenCalledWith(
        expect.objectContaining({
          name: 'login',
        })
      )
    })
  })
})
