import isUserInGroup from '@/data/auth/is-user-in-group'
import ensureGroup from './ensure-group'

import { makeRoute, makeRouteRecord } from '../util/test-helpers'
jest.mock('@/data/auth/is-user-in-group', () => jest.fn())

describe('when I want to expose a route only to a certain userGroup', () => {
  const nextMock = jest.fn()
  const isUserInGroupMock = isUserInGroup as jest.Mock<any>

  const toRouteMock = makeRoute('toRoute', '/toRoute')
  const fromRouteMock = makeRoute('fromRoute', '/fromRoute')

  beforeAll(() => {
    isUserInGroupMock.mockImplementation(async (group) => group === 'usergroup')
  })

  beforeEach(() => {
    nextMock.mockClear()
    isUserInGroupMock.mockClear()
  })

  describe('and the user is in the given group', () => {
    it('will call next without parameters', async () => {
      expect.assertions(1)

      const expectedUserGroup = 'usergroup'

      await ensureGroup(expectedUserGroup, 'home')(
        toRouteMock,
        fromRouteMock,
        nextMock
      )

      expect(nextMock).toHaveBeenCalledWith()
    })
  })

  describe('and the user is not in the given group', () => {
    it('will redirect the user to the given route name', async () => {
      expect.assertions(1)

      const unexpectedUserGroup = 'unexpected'

      await ensureGroup(unexpectedUserGroup, 'home')(
        toRouteMock,
        fromRouteMock,
        nextMock
      )

      expect(nextMock).toHaveBeenLastCalledWith(
        expect.objectContaining({
          name: 'home',
        })
      )
    })
  })
})
