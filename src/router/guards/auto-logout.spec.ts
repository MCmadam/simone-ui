import currentAuthenticatedUser from '@/data/auth/current-authenticated-user'
import store from '@/store'
import autoLogout from './auto-logout'
jest.mock('@/data/auth/current-authenticated-user', () => jest.fn())
jest.mock('@/store')

describe('when I want to sync the loggedIn state of the store with localStorage, the autoLogout guard', () => {
  const currentAuthenticatedUserMock = currentAuthenticatedUser as jest.Mock<
    any
  >
  const storeMock = store as jest.Mocked<typeof store>

  const nextMock = jest.fn()

  const toRouteMock = {
    name: 'toRoute',
  }

  const fromRouteMock = {
    name: 'fromRoute',
  }

  beforeEach(() => {
    currentAuthenticatedUserMock.mockClear()
    storeMock.dispatch.mockClear()
  })

  describe('when authentication information is found in localStorage', () => {
    beforeAll(() => {
      currentAuthenticatedUserMock.mockImplementation(async () => true)
    })

    it("will not dispatch the store action 'auth/logout'", () => void 0)
  })

  describe('when authentication information is not found in localStorage', () => {
    beforeAll(() => {
      currentAuthenticatedUserMock.mockImplementation(async () => false)
    })

    describe("and the store's state has the user loggedIn", () => {
      beforeAll(() => {
        store.getters['loggedIn'] = true
      })

      it("will dispatch the store action 'auth/logout'", async () => {
        expect.assertions(2)

        await autoLogout(toRouteMock, fromRouteMock, nextMock)

        expect(store.dispatch).toHaveBeenLastCalledWith('auth/logout')
        expect(nextMock).toHaveBeenCalledWith()
      })
    })

    describe("and the store's state has the user not loggedIn", () => {
      beforeAll(() => {
        store.getters['loggedIn'] = false
      })

      it("will not dispatch the store action 'auth/logout'", async () => {
        expect.assertions(2)

        await autoLogout(toRouteMock, fromRouteMock, nextMock)

        expect(store.dispatch).not.toHaveBeenCalled()
        expect(nextMock).toHaveBeenCalledWith()
      })
    })
  })
})
