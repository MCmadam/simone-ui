import getCurrentAuthenticatedUser from '@/data/auth/current-authenticated-user'
import store from '@/store'

export default async (to, from, next) => {
  let userInStorage = false

  try {
    const cognitoUser = await getCurrentAuthenticatedUser()
    if (cognitoUser) {
      userInStorage = true
    }
  } catch (error) {
    // since this is a guard the parent doesn't catch
  }

  if (!userInStorage) {
    if (store.getters['loggedIn']) {
      await store.dispatch('auth/logout')
    }

    if (store.getters['currentUser']) {
      store.dispatch('user/setUserLocal', null)
    }
  }
  next()
}
