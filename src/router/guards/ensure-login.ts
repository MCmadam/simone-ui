import when from '@/router/util/when'
import asserter from '@/util/function/asserter'
import store from '@/store'
import { redirect } from '@/util/router'

export default when(
  asserter(() => store.getters['loggedIn'], false),
  redirect('login', true)
)
