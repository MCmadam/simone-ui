import store from '@/store'
import layoutGuard from './layout'
jest.mock('@/store')

describe('when I want to define the layout for a view', () => {
  const mockStore = store as jest.Mocked<typeof store>

  beforeEach(() => {
    mockStore.dispatch.mockClear()
  })

  it('can be done by calling the layout guard', (done) => {
    expect.assertions(1)

    const layout = 'anything'

    const guard = layoutGuard(layout)

    guard(null, null, () => void 0)

    // timeout should be fired after Vue.nextTick
    setTimeout(() => {
      expect(store.dispatch).toHaveBeenCalledWith('layout/setLayout', layout)
      done()
    }, 0)
  })
})
