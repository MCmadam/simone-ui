import store from '@/store'
import ensureLogout from './ensure-logout'
import { makeRoute } from '../util/test-helpers'
jest.mock('@/store')

describe('when I want to expose a route only to users not loggedIn', () => {
  const nextMock = jest.fn()

  const toRouteMock = makeRoute('toRoute', '/toRoute')
  const fromRouteMock = makeRoute('fromRoute', '/fromRoute')

  beforeEach(() => {
    nextMock.mockClear()
  })

  describe('and the user is loggedIn', () => {
    beforeEach(() => {
      store.getters['loggedIn'] = true
    })

    it('will redirect to dashboard', async () => {
      expect.assertions(1)

      await ensureLogout(toRouteMock, fromRouteMock, nextMock)

      expect(nextMock).toHaveBeenCalledWith(
        expect.objectContaining({
          name: 'dashboard',
        })
      )
    })
  })

  describe('and the user is not loggedIn', () => {
    beforeEach(() => {
      store.getters['loggedIn'] = false
    })

    it('will not redirect', async () => {
      expect.assertions(1)

      await ensureLogout(toRouteMock, fromRouteMock, nextMock)

      expect(nextMock).toHaveBeenCalledWith()
    })
  })
})
