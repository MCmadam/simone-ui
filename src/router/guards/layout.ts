import store from '@/store'
import Vue from 'vue'
import { Route } from 'vue-router'

type LayoutGuardOptions = {
  view: string
  defaultConfig: any
  depends?: boolean
}

export default (layout, options?: LayoutGuardOptions) => {
  if (options && options.defaultConfig) {
    store.dispatch('layout/setDefaults', {
      view: options.view,
      config: options.defaultConfig,
    })
  }

  return (to: Route, from: Route, next: Function) => {
    if (store.getters['layout'] !== layout) {
      // use nextTick prevent double rendering!
      Vue.nextTick(() => {
        store.dispatch('layout/setLayout', layout)
      })
    }

    if (options && !options.depends) {
      Vue.nextTick(() => {
        store.dispatch('layout/loadPreferences', { view: options.view })
      })
    }

    next()
  }
}
