export default (value, maxLen = 20) => {
  if (!value) {
    return
  } else if (value.length <= maxLen) {
    return value
  }

  try {
    const url = new URL(value)

    if (url.hostname.length >= maxLen) {
      return url.hostname.slice(0, maxLen)
    }

    const parsed = `${url.hostname}${url.pathname}`.slice(0, maxLen)
    return parsed + '...'
  } catch (error) {
    return value.slice(0, maxLen) + '...'
  }
}
