//  TRUNCATE STRING
//  filter to limit a given input string:
//  - at a given number of characters
//  - at a given number of words, while also
//    shortening exceedingly long words
//
//  so as to be able to be flexible in the final presentation
//
//  param value @string: the string to be truncated
//  param style @string: the style of truncation ('word' || 'char')
//  param limit @number: the number of words or characters to keep in result
//

export default (value, style = 'word', limit = 10) => {
  // No input string or empty string, then return empty string
  if (!value) {
    // console.log('Value is undefined')
    return
  }
  if (value === '') {
    // console.log('Value is empty')
    return value
  }

  // Force hyperlinks to be treated as style char with limit 20
  if (value.indexOf('http') === 0) {
    const url = new URL(value)
    value = url.hostname + url.pathname

    style = 'char'
    limit = 25
  }

  // We only accept two options for the way of truncating
  if (style !== 'word' && style !== 'char') {
    // console.log('Style of truncation is not set correctly')
    return
  }

  // Parse the parameter limit to test whether it is a non-zero positive integer
  const checkStr = limit.toString()
  const checkInt = parseInt(checkStr, 10)
  if (checkInt !== limit) {
    // console.log('Limit is not an integer')
    return
  }
  if (limit <= 0) {
    return
  }

  // We passed all checks, so now parse the string
  if (style === 'word') {
    try {
      // Chop up the value in separate words
      const chunks = value.split(' ')

      for (const chunkid in chunks) {
        if (chunks[chunkid].length > 15) {
          // shorten long chunk to a length that will fit in message
          chunks[chunkid] = chunks[chunkid].substr(0, 5) + '...' + chunks[chunkid].substr(-5)
        }
      }

      let result = ''
      const l = chunks.length > limit ? limit : chunks.length
      for (let i = 0; i < l; i++) {
        result += chunks[i]
        result += ' '
      }

      // Only return an ellipsis if we actually left out chunks, otherwise trim off the trailing whitespace
      return l === chunks.length ? result.trim() : result + '...'
    } catch (error) {
      // console.log(error)
      // No idea what to do in case of an error
    }
  } else {
    return value.slice(0, limit) + '...'
  }
}
