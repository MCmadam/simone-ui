import truncateUrl from './truncateUrl'

describe('truncate url filter', () => {
  it('returns value if below max length', () => {
    expect.assertions(1)

    const url = 'https://part-up.com'
    const result = truncateUrl(url)

    expect(result).toMatch(url)
  })
})
