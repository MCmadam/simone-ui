import Vue from 'vue'
import get from 'lodash/get'

/* eslint-env node */
const filterContext = require.context('.', true, /\.js/)
filterContext.keys().forEach((fileName) => {
  const resolved = filterContext(fileName)
  const filter = get(resolved, 'default', resolved)
  Vue.filter(fileName, filter)
})

/* eslint-env browser */
