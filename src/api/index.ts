import PartupAPI from './api'

let api: PartupAPI
if (!api) {
  api = new PartupAPI()
}

export default api
