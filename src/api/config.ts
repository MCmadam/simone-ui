import Auth from '@aws-amplify/auth'
import api from '@/api'
import logger from '@/util/logger'
import parseDomainFromLocation from '@/util/url/parse-domain-from-location'
import parseBrandingFromLocation from '@/util/url/parse-branding-from-location'
import { toBool } from '@/util/string'

if (toBool(process.env.VUE_APP_COGNITO_USE_COOKIES)) {
  const cookieDomain = parseDomainFromLocation()
  const cookieSecure = cookieDomain !== 'localhost'
  Auth.configure({
    region: process.env.VUE_APP_AWS_COGNITO_REGION,
    userPoolId: process.env.VUE_APP_AWS_COGNITO_USER_POOL_ID,
    userPoolWebClientId:
      process.env.VUE_APP_AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
    cookieStorage: {
      domain: cookieDomain,
      path: process.env.VUE_APP_AWS_COGNITO_COOKIE_PATH,
      expires: 365,
      secure: cookieSecure,
    },
  })
} else {
  Auth.configure({
    region: process.env.VUE_APP_AWS_COGNITO_REGION,
    userPoolId: process.env.VUE_APP_AWS_COGNITO_USER_POOL_ID,
    userPoolWebClientId:
      process.env.VUE_APP_AWS_COGNITO_USER_POOL_APP_CLIENT_ID,
  })
}

api.configure({
  endpoint: process.env.VUE_APP_API_ENDPOINT_URL,
  region: process.env.VUE_APP_API_REGION,
  service: 'execute-api',
  customHeader: async () => {
    const headers: any = {}

    try {
      const session = await Auth.currentSession()
      if (session) {
        headers.Authorization = session.getIdToken().getJwtToken()
      }
    } catch (error) {
      logger.warn("can't add authorization to headers", error)
    }

    headers['X-Pu-Branding'] = parseBrandingFromLocation()

    return headers
  },
})
