/**
 * JSON parse response data
 * response must be used as reference
 * @param response axios response
 */
export default (responseData: string) => {
  return JSON.parse(responseData)
}
