/**
 * JSON parse response data
 * response must be used as reference
 * @param response axios response
 */
export default (requestData: object) => {
  return JSON.stringify(requestData)
}
