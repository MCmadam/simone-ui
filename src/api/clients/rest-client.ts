import jsonParseTransformer from './response-transformers/json'
import jsonStringifyTransformer from './request-transformers/json'
import axios, { AxiosRequestConfig } from 'axios'
import ApiError from '@/util/error/api-error'
import logger from '@/util/logger'
import {
  ApiClient,
  ApiOptions,
  ApiResponse,
  CredentialManager,
  Credentials,
  RequestConfig,
  RequestMethod,
  RequestSigner,
  RequestServiceInfo,
} from '@/types'

export default class PuRestClient implements ApiClient {
  private _customHeader: Function
  private _endpoint: string
  private _service: string
  private _region: string
  private _credentialManager: CredentialManager
  private _requestSigner: RequestSigner

  constructor(
    credentialManager: CredentialManager,
    requestSigner: RequestSigner,
    options: ApiOptions
  ) {
    this._credentialManager = credentialManager
    this._requestSigner = requestSigner
    this._customHeader = options.customHeader
    this._endpoint = options.endpoint
    this._service = options.service
    this._region = options.region
  }

  /**
   *
   * @param path url path
   * @param method request method
   * @param init request config
   */
  public async ajax<T>(
    path: string,
    method: RequestMethod,
    init: RequestConfig = {}
  ) {
    const config: AxiosRequestConfig = {
      method,
      url: this._endpoint + path,
      params: init.query,
      headers: {},
      cancelToken: init.cancelToken,
    }

    if ('transformRequest' in init) {
      config.transformRequest = []
      if (Array.isArray(init.transformRequest)) {
        config.transformRequest.push(...init.transformRequest)
      } else {
        config.transformRequest.push(init.transformRequest)
      }
      config.transformRequest.push(jsonStringifyTransformer)
    }

    if ('transformResponse' in init) {
      config.transformResponse = []
      config.transformResponse.push(jsonParseTransformer)
      if (Array.isArray(init.transformResponse)) {
        config.transformResponse.push(...init.transformResponse)
      } else {
        config.transformResponse.push(init.transformResponse)
      }
    }

    if (init.data) {
      config.headers['Content-Type'] = 'application/json; charset=UTF-8'
      config.data = init.data
    }

    if (this._customHeader) {
      const customHeader = await this._customHeader()
      Object.assign(config.headers, customHeader)
    }

    if (init.headers) {
      Object.assign(config.headers, init.headers)
    }

    if (config.headers.Authorization) {
      return this._request<T>(config)
    }

    try {
      const credentials = await this._credentialManager.get()
      const signedConfig = this._sign(config, credentials)
      return this._request<T>(signedConfig)
    } catch (error) {
      return this._request<T>(config)
    }
  }

  public del<T>(path: string, config?: RequestConfig) {
    return this.ajax<T>(path, 'DELETE', config)
  }

  public head<T>(path: string, config?: RequestConfig) {
    return this.ajax<T>(path, 'HEAD', config)
  }

  public get<T>(path: string, config?: RequestConfig) {
    return this.ajax<T>(path, 'GET', config)
  }

  public patch<T>(path: string, config?: RequestConfig) {
    return this.ajax<T>(path, 'PATCH', config)
  }

  public post<T>(path: string, config?: RequestConfig) {
    return this.ajax<T>(path, 'POST', config)
  }

  public put<T>(path: string, config?: RequestConfig) {
    return this.ajax<T>(path, 'PUT', config)
  }

  private _sign(config: RequestConfig, credentials: Credentials) {
    // thanks amplify for using different cases between modules.
    /* eslint-disable */
    const creds = {
      access_key: credentials.accessKeyId,
      secret_key: credentials.secretAccessKey,
      session_token: credentials.sessionToken,
    }
    /* eslint-enable */

    const signerServiceInfo: RequestServiceInfo = {
      region: this._region,
      service: this._service,
    }

    // avoid mutating the config directly, if the signer fails it may have already mutated the config.
    const configCopy: RequestConfig = Object.assign({}, config)

    return this._requestSigner.sign(configCopy, creds, signerServiceInfo)
  }

  private async _request<T>(config: RequestConfig) {
    try {
      const { data } = await axios.request<ApiResponse<T>>(config)
      return data
    } catch (error) {
      logger.debug(error)
      throw new ApiError(error)
    }
  }
}
