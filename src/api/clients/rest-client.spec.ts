import axios from 'axios'
import PuRestClient from './rest-client'
import {
  ApiOptions,
  CredentialManager,
  RequestSigner,
  RequestConfig,
  RequestServiceInfo,
} from '@/types'
jest.mock('axios')

describe('rest client', () => {
  const axiosMock = axios as jest.Mocked<typeof axios>
  axiosMock.request.mockResolvedValue({ data: 1 } as any)

  const credentialManager = ({
    get: jest.fn(async () => {
      return {}
    }),
  } as unknown) as jest.Mocked<CredentialManager>

  const requestSigner = ({
    sign: jest.fn(
      (config: any, x: any, y: RequestServiceInfo) =>
        config as RequestConfig<any, any, any>
    ),
    signUrl: jest.fn(),
  } as unknown) as jest.Mocked<RequestSigner>

  let defaultServiceInfo: RequestServiceInfo
  let defaultOptions: ApiOptions
  let defaultPath: string
  let defaultConfig: RequestConfig
  let sut: PuRestClient

  beforeEach(() => {
    defaultServiceInfo = {
      region: 'region',
      service: 'service',
    }

    defaultOptions = {
      endpoint: 'endpoint',
      ...defaultServiceInfo,
    }

    defaultPath = '/path'

    defaultConfig = {
      data: {
        test: 1,
      },
    }

    sut = new PuRestClient(credentialManager, requestSigner, defaultOptions)

    credentialManager.get.mockClear()
    requestSigner.sign.mockClear()
    requestSigner.signUrl.mockClear()
  })

  describe('ajax', () => {
    it('composes an url of endpoint + path', async () => {
      expect.assertions(1)

      await sut.ajax('/path', 'GET')
      expect(axiosMock.request).toBeCalledWith(
        expect.objectContaining({
          url: `${defaultOptions.endpoint}${defaultPath}`,
        })
      )
    })

    it("doesn't use the credential manager to sign the request if the header includes the 'Authorization' key", async () => {
      expect.assertions(2)

      const header = {
        Authorization: '123',
      }

      await sut.ajax(defaultPath, 'get', {
        headers: header,
      })
      expect(credentialManager.get).not.toHaveBeenCalled()
      expect(axiosMock.request).toHaveBeenCalledWith(
        expect.objectContaining({
          headers: header,
        })
      )
    })

    describe('when I want to use a custom header', () => {
      it('adds a custom header to the request configuration', async () => {
        expect.assertions(2)

        const header = {
          test: 123,
        }

        const customHeader = jest.fn(async () => header)

        sut = new PuRestClient(credentialManager, requestSigner, {
          ...defaultOptions,
          customHeader,
        })

        await sut.ajax('/path', 'get')
        expect(customHeader).toHaveBeenCalled()
        expect(axiosMock.request).toHaveBeenCalledWith(
          expect.objectContaining({
            headers: header,
          })
        )
      })
    })

    describe("when I don't use a custom header", () => {
      it('uses the credentialManager and requestSigner to sign the request', async () => {
        expect.assertions(3)

        await sut.ajax(defaultPath, 'get')
        expect(credentialManager.get).toHaveBeenCalled()
        expect(requestSigner.sign).toHaveBeenCalled()
        expect(axios.request).toHaveBeenCalled()
      })

      describe('when the credentialManager cannot find credentials', () => {
        it('sends an unsigned request without credentials', async () => {
          expect.assertions(2)

          credentialManager.get.mockImplementationOnce((() =>
            Promise.reject(new Error())) as any)

          await sut.ajax(defaultPath, 'get')
          expect(requestSigner.sign).not.toHaveBeenCalled()
          expect(axios.request).toHaveBeenCalled()
        })
      })
    })
  })

  describe('request methods', () => {
    beforeEach(() => {
      sut.ajax = jest.fn()
    })

    describe('del', () => {
      it("uses ajax with 'DELETE' to invoke the request", () => {
        expect.assertions(1)

        sut.del(defaultPath, defaultConfig)
        expect(sut.ajax).toHaveBeenCalledWith(
          defaultPath,
          'DELETE',
          defaultConfig
        )
      })
    })

    describe('head', () => {
      it("uses ajax with 'HEAD' to invoke the request", () => {
        expect.assertions(1)

        sut.head(defaultPath, defaultConfig)
        expect(sut.ajax).toHaveBeenCalledWith(
          defaultPath,
          'HEAD',
          defaultConfig
        )
      })
    })

    describe('get', () => {
      it("uses ajax with 'GET' to invoke the request", () => {
        expect.assertions(1)

        sut.get(defaultPath, defaultConfig)
        expect(sut.ajax).toHaveBeenCalledWith(defaultPath, 'GET', defaultConfig)
      })
    })

    describe('patch', () => {
      it("uses ajax with 'PATCH' to invoke the request", () => {
        expect.assertions(1)

        sut.patch(defaultPath, defaultConfig)
        expect(sut.ajax).toHaveBeenCalledWith(
          defaultPath,
          'PATCH',
          defaultConfig
        )
      })
    })

    describe('post', () => {
      it("uses ajax with 'POST' to invoke the request", () => {
        expect.assertions(1)

        sut.post(defaultPath, defaultConfig)
        expect(sut.ajax).toHaveBeenCalledWith(
          defaultPath,
          'POST',
          defaultConfig
        )
      })
    })

    describe('put', () => {
      it("uses ajax with 'PUT' to invoke the request", () => {
        expect.assertions(1)

        sut.put(defaultPath, defaultConfig)
        expect(sut.ajax).toHaveBeenCalledWith(defaultPath, 'PUT', defaultConfig)
      })
    })
  })
})
