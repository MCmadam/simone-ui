import axios from 'axios'
import { DispatcherRequest, REQUEST_STATUS } from '@/types'

export default class Dispatcher<T extends Function> {
  private _executor: T
  private _id = 1
  private _queue: DispatcherRequest[] = []

  constructor(executor: T) {
    this._executor = executor
  }

  public cancel() {
    for (const request of this._queue) {
      if (request.status === REQUEST_STATUS.sent) {
        request.cancelSource.cancel()
        request.status = REQUEST_STATUS.canceled
      }
    }

    return this
  }

  // ignore ReturnType<T> as async must return a promise and
  // I don't know how to infer that properly.
  // @ts-ignore
  public async exec(...args: Parameters<T>): ReturnType<T> {
    // @ts-check

    const request: DispatcherRequest = {
      cancelSource: axios.CancelToken.source(),
      id: this._nextID(),
      status: REQUEST_STATUS.initialized,
    }
    this._applyArgs(request, args)

    this._enqueue(request)

    if (request.status === REQUEST_STATUS.canceled) {
      return
    }

    try {
      request.status = REQUEST_STATUS.sent
      const response = await this._executor(...args)
      request.status = REQUEST_STATUS.received
      return response
    } finally {
      this._dequeue(request)
    }
  }

  private _applyArgs(request: DispatcherRequest, args: any[]) {
    const [params, init] = args

    if (!params) {
      return
    }

    if (!init) {
      Object.assign(params, {
        cancelToken: request.cancelSource.token,
      })
      request.init = params
    } else {
      Object.assign(init, {
        cancelToken: request.cancelSource.token,
      })
      request.params = params
      request.init = init
    }
  }

  private _nextID() {
    return this._id++
  }

  private _enqueue(request) {
    this._queue.unshift(request)
  }

  private _dequeue(request?) {
    if (request) {
      const index = this._queue.findIndex(({ id }) => id === request.id)
      if (index > -1) {
        this._queue.splice(index, 1)
      }
    } else {
      const last = this._queue[this._queue.length - 1]
      if (
        last &&
        last.status !== REQUEST_STATUS.initialized &&
        last.status !== REQUEST_STATUS.sent
      ) {
        this._queue.pop()
      }
    }
  }
}
