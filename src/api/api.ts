import { Credentials, Signer } from '@aws-amplify/core'
import PuRestClient from '@/api/clients/rest-client'
import { ApiClient, ApiOptions, RequestConfig } from '@/types'

export default class PartupAPI {
  private _client: ApiClient

  constructor(options?: ApiOptions) {
    if (options) {
      this.configure(options)
    }
  }

  configure(options: ApiOptions) {
    if (this._client) {
      this._client = undefined
    }
    this._client = new PuRestClient(Credentials, Signer, options)
  }

  public get<T>(path: string, config: RequestConfig) {
    this._ensureClient()
    return this._client.get<T>(path, config)
  }

  public del<T>(path: string, config: RequestConfig) {
    this._ensureClient()
    return this._client.del<T>(path, config)
  }

  public head<T>(path: string, config: RequestConfig) {
    this._ensureClient()
    return this._client.head<T>(path, config)
  }

  public patch<T>(path: string, config: RequestConfig) {
    this._ensureClient()
    return this._client.patch<T>(path, config)
  }

  public post<T>(path: string, config: RequestConfig) {
    this._ensureClient()
    return this._client.post<T>(path, config)
  }

  public put<T>(path: string, config: RequestConfig) {
    this._ensureClient()
    return this._client.put<T>(path, config)
  }

  private _ensureClient() {
    if (!this._client) {
      throw new Error('PartupAPI is not configured')
    }
  }
}
