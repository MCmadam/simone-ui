import { shallowMount } from '@vue/test-utils'
import colorable from './colorable'

describe('colorable mixin', () => {
  let sut

  beforeEach(() => {
    sut = shallowMount({
      mixins: [colorable],
      template: '<div />',
    })
  })

  it('provides a color property to a component', () => {
    expect.assertions(1)

    sut.setProps({
      color: 'primary',
    })
    expect(sut.props('color')).toBe('primary')
  })

  describe('setBackgroundColor', () => {
    it('always returns a valid class object', () => {
      expect.assertions(1)

      expect(sut.vm.setBackgroundColor()).toMatchObject({})
    })

    it('applies a custom color as class', () => {
      expect.assertions(1)

      const expected = {
        primary: true,
      }
      expect(sut.vm.setBackgroundColor(null, 'primary')).toMatchObject(expected)
    })

    it('merges with given classes object', () => {
      expect.assertions(1)

      const classes = {
        primary: false,
        secondary: true,
      }
      const expected = {
        primary: true,
        secondary: true,
      }
      expect(sut.vm.setBackgroundColor(classes, 'primary')).toMatchObject(
        expected
      )
    })

    it('does not modify the given classes object but returns a new one instead', () => {
      expect.assertions(3)

      const classes = {
        primary: true,
      }
      const copy = classes
      const resolved = sut.vm.setBackgroundColor(classes, 'secondary')

      expect(copy).toBe(classes)
      expect(classes).toMatchObject({
        primary: true,
      })
      expect(resolved).toMatchObject({
        primary: true,
        secondary: true,
      })
    })

    it("does not add a class when 'param, color prop and defaultColor' are all falsy", () => {
      expect.assertions(1)

      const expected = {
        testclass: true,
      }

      expect(sut.vm.setBackgroundColor(expected)).toMatchObject(expected)
    })

    it('takes defaultColor when no color param and no color prop are present', () => {
      expect.assertions(1)

      sut.setData({
        defaultColor: 'primary',
      })

      const expected = {
        primary: true,
      }

      expect(sut.vm.setBackgroundColor()).toMatchObject(expected)
    })

    it('takes color prop when no color param is present', () => {
      expect.assertions(1)

      sut.setProps({
        color: 'primary',
      })

      const expected = {
        primary: true,
      }

      expect(sut.vm.setBackgroundColor()).toMatchObject(expected)
    })

    it('prioritizes color param above color prop', () => {
      expect.assertions(1)

      sut.setProps({
        color: 'secondary',
      })

      const expected = {
        primary: true,
      }

      expect(sut.vm.setBackgroundColor(null, 'primary')).toMatchObject(expected)
    })
  })

  describe('setTextColor', () => {
    it('always returns a valid class object', () => {
      expect(sut.vm.setTextColor()).toMatchObject({})
    })

    it('applies a custom color as class', () => {
      const expected = {
        'primary--text': true,
      }
      expect(sut.vm.setTextColor(null, 'primary')).toMatchObject(expected)
    })

    it('merges with given classes object', () => {
      expect.assertions(1)

      const classes = {
        test: true,
        'primary--text': false,
      }
      const expected = {
        test: true,
        'primary--text': true,
      }

      expect(sut.vm.setTextColor(classes, 'primary')).toMatchObject(expected)
    })

    it('does not modify the given classes object but returns a new one instead', () => {
      expect.assertions(3)

      const classes = {
        test: true,
      }
      const resolved = sut.vm.setTextColor(classes, 'secondary')

      expect(classes).not.toBe(resolved)
      expect(classes).toMatchObject({
        test: true,
      })
      expect(resolved).toMatchObject({
        test: true,
        'secondary--text': true,
      })
    })

    it("does not add a class when 'param, color prop and defaultColor' are all falsy", () => {
      expect.assertions(1)

      const expected = {
        test: true,
      }

      expect(sut.vm.setTextColor(expected)).toMatchObject(expected)
    })

    it('takes defaultColor when no color param and no color prop are present', () => {
      sut.setData({
        defaultColor: 'primary',
      })

      const expected = {
        'primary--text': true,
      }

      expect(sut.vm.setTextColor()).toMatchObject(expected)
    })

    it('takes color prop when no color param is present', () => {
      expect.assertions(1)

      sut.setProps({
        color: 'primary',
      })

      const expected = {
        'primary--text': true,
      }

      expect(sut.vm.setTextColor()).toMatchObject(expected)
    })

    it('prioritizes color param above color prop', () => {
      expect.assertions(1)

      sut.setProps({
        color: 'secondary',
      })

      const expected = {
        'primary--text': true,
      }

      expect(sut.vm.setTextColor(null, 'primary')).toMatchObject(expected)
    })
  })
})
