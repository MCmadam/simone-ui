import Vue from 'vue'

export default Vue.extend({
  name: 'disposable',

  data() {
    return {
      disposed: false,
    }
  },
})
