import Vue from 'vue'

export default Vue.extend({
  name: 'transitionable',

  props: {
    mode: {
      type: String,
    },
    transition: {
      type: String,
    },
  },
})
