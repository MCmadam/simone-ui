import Vue from 'vue'
import rulesFor from '@/lib/authorization/ability/rules-for'

// for tests @see ~/tests/abiltiy

export default ({ attribute }: any = {}) => {
  const mixin: any = {
    name: 'mixin-ability',
    ability: true,
  }

  if (attribute) {
    Object.assign(mixin, {
      watch: {
        [attribute]: {
          handler(newVal) {
            if (!newVal && this.$parent.$ability) {
              this.$ability.update(this.$parent.$ability.rules)
            } else {
              this.$ability.update(rulesFor(newVal))
            }
          },
          deep: true,
          immediate: true,
        },
      },
    })
  }

  return Vue.extend(mixin)
}
