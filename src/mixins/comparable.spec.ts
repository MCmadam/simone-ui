/* eslint-env jest */

import { shallowMount } from '@vue/test-utils'
import isEqual from 'lodash/isEqual'
import comparable from './comparable'

// const makeSubject = () => shallowMount(
//   Object.assign({
//     mixins: [
//       comparable,
//     ],
//     render: () => void 0,
//   })
// )

describe('comparable mixin', () => {
  let sut

  beforeEach(() => {
    sut = shallowMount({
      mixins: [comparable],
      template: '<div />',
    })
  })

  it("defaults to 'isEqual'", () => {
    expect.assertions(1)

    expect(sut.props('valueComparator')).toBe(isEqual)
  })

  it('can be used to compare values', () => {
    expect.assertions(1)

    const comparator = jest.fn()

    sut.setProps({
      valueComparator: comparator,
    })

    sut.vm.valueComparator()

    expect(sut.vm.valueComparator).toBeCalledTimes(1)
  })
})
