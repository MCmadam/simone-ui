export const valuableFactory = (value = 'value') => {
  /**
   * @mixin
   */
  return {
    name: 'valuable',

    props: {
      falseValue: {
        required: false,
        default: void 0,
      },
      trueValue: {
        required: false,
        default: void 0,
      },
      [value]: {
        required: false,
        default: void 0,
      },
    },

    computed: {
      computedTrueValue() {
        return this.trueValue || this[value]
      },
      hasTrueValue() {
        return typeof this.trueValue !== 'undefined' && this.falseValue !== null
      },
      hasFalseValue() {
        return (
          typeof this.falseValue !== 'undefined' && this.falseValue !== null
        )
      },
    },
  }
}

export default valuableFactory()
