/**
 * Focusable mixin
 *
 * @mixin
 */
export default {
  name: 'focusable',

  data() {
    return {
      isFocused: null,
    }
  },

  methods: {
    /**
     * Default blur handler
     *
     * @method onBlur
     * @virtual
     * @param {BlurEvent} e blur event
     */
    onBlur(e) {
      this.isFocused = false
      this.$emit('blur', e)
    },
    /**
     * Default focus handler
     *
     * @method focus
     * @virtual
     * @param {FocusEvent} e focus event
     */
    onFocus(e) {
      this.isFocused = true
      this.$emit('focus', e)
    },
  },
}
