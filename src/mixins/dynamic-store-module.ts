import Vue from 'vue'
import { Module } from 'vuex'

import store from '@/store'

/**
 * dynamicStoreModule
 * @param {string} path
 * @param {Module<S, R>} storeModule
 * @template S, R S = moduleState, R = rootState and defaults
 */
export default <S, R = unknown>(path: string, storeModule: Module<S, R>) => {
  return Vue.extend({
    beforeRouteEnter(to, from, next) {
      // hasModule is not native to vuex and implemented in `src/plugins/vuex`
      if (!store.hasModule(path)) {
        store.registerModule(path, storeModule)
      }
      next()
    },
    beforeRouteUpdate(this: Vue, to, from, next) {
      this.$store.dispatch(`${path}/reset`)
      next()
    },
    beforeDestroy() {
      if (this.$store.hasModule(path)) {
        this.$nextTick(() => {
          this.$store.unregisterModule(path)
        })
      }
    },
  })
}
