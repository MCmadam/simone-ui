export const toggleableFactory = (prop = 'value', event = 'change') => {
  return {
    model: {
      prop: prop,
      event: event,
    },

    data(vm) {
      return {
        isActive: Boolean(vm[prop]),
      }
    },

    props: {
      [prop]: {
        required: false,
      },
    },

    watch: {
      [prop]: {
        handler(newVal) {
          this.isActive = Boolean(newVal)
        },
      },
      isActive: {
        handler(newVal) {
          if (Boolean(newVal) !== this[prop]) {
            this.$emit(event, newVal)
          }
        },
      },
    },
  }
}

const toggleable = toggleableFactory()
export default toggleable
