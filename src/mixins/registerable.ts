import Vue from 'vue'
import logger from '@/util/logger'

/**
 * Used by a child to tell it want's to be defined inside a parent that exposes the same namespace
 * @description if the child cannot operate without the parent both arguments must be present
 * @param {String} namespace that get's passed from parent to child
 * @param {String} child component.name
 * @param {String} parent component.name
 * @return {any} mixin
 */
export const registerableInject = (namespace, child?, parent?) => {
  if (typeof namespace !== 'string' || namespace.length <= 0) {
    throw new Error('inject mixin cannot operate without a namespace')
  }

  let noop
  if (child && parent) {
    const msg = `${child} must be defined inside ${parent}`
    noop = {
      register: () => logger.debug(`cannot register, ${msg}`) && void 0,
      unregister: () => logger.debug(`cannot unregister, ${msg}`) && void 0,
    }
  }

  return Vue.extend({
    name: 'registerable-inject',
    inject: {
      [namespace]: {
        default: noop,
      },
    },
  })
}

/**
 * Provide to a child
 * @description provides object to child that uses the same namespace,
 * this can be extended by specifying provide in the provider itself
 * @param {String} namespace will pass namespace onto childs
 * @return {any} mixin
 */
export const registerableProvide = (namespace) => {
  return Vue.extend({
    name: 'registerable-provide',
    methods: {
      register: null,
      unregister: null,
    },
    provide() {
      return {
        [namespace]: {
          register: this.register,
          unregister: this.unregister,
        },
      }
    },
  })
}
