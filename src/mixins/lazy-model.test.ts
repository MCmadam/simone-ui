/*
 * lazy-model mixin unit tests
 *
 */

/* eslint-env jest */

import { shallowMount } from '@vue/test-utils'
import { lazyModelFactory } from './lazy-model'

describe('lazy-model mixin', () => {
  let sut

  beforeEach(() => {
    sut = shallowMount({
      mixins: [lazyModelFactory('change')],
      template: '<div />',
    })
  })

  it('updates lazyValue if value changes', async () => {
    expect.assertions(1)

    const expected = 'value'

    sut.setProps({
      value: expected,
    })

    await sut.vm.$nextTick()

    expect(sut.vm.lazyValue).toBe(expected)
  })

  it('returns lazyValue using internalValue', () => {
    expect.assertions(1)

    const expected = 'lazyvalue'

    sut.setData({
      lazyValue: expected,
    })

    expect(sut.vm.internalValue).toBe(expected)
  })

  it("set's lazyValue when internalValue is set", () => {
    expect.assertions(1)

    const expected = 'internal'

    sut.vm.internalValue = expected
    expect(sut.vm.lazyValue).toBe(expected)
  })

  it("emit's {event} when lazyValue is set", () => {
    expect.assertions(1)

    const expected = 'eventData'

    sut.vm.internalValue = expected
    expect(sut.emitted().change).toMatchObject([[expected]])
  })
})
