import isEqual from 'lodash/isEqual'

export const comparableFactory = (fn = isEqual) => {
  return {
    name: 'comparable',
    props: {
      valueComparator: {
        type: Function,
        default: fn,
      },
    },
  }
}

export default comparableFactory()
