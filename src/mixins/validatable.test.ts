/*
 * Validatable unit tests
 *
 */

/* eslint-env jest */

import validatable from './validatable'
import { shallowMount } from '@vue/test-utils'

describe('validatable mixin', () => {
  let sut

  beforeEach(() => {
    sut = shallowMount(
      Object.assign({
        mixins: [validatable],
        template: '<div />',
      })
    )
  })

  it('hasError is false when just initialized empty', () => {
    expect.assertions(1)
    expect(sut.vm.hasError).toBe(false)
  })

  it('hasError if error prop is true', () => {
    expect.assertions(1)

    sut.setProps({ error: true })
    expect(sut.vm.hasError).toBe(true)
  })

  it('hasError if error-messages is string', () => {
    expect.assertions(1)

    sut.setProps({ errorMessages: 'hello!' })
    expect(sut.vm.hasError).toBe(true)
  })

  it('hasError if error-messages is array with length', () => {
    expect.assertions(1)

    sut.setProps({ errorMessages: Array(2).fill('1, 2') })
    expect(sut.vm.hasError).toBe(true)
  })

  // it(`invokes all rules when validate is called`, () => {
  //   expect.assertions(2)

  //   const rules = [
  //     jest.fn(() => true),
  //     jest.fn(() => true),
  //   ]

  //   sut.setProps({ rules })
  //   sut.vm.validate()
  //   expect(rules[0]).toBeCalled()
  //   expect(rules[1]).toBeCalled()
  // })

  // it(`will only set valid to false if a rule returns false`, () => {
  //   expect.assertions(1)

  //   const rules = [
  //     () => true,
  //     () => false,
  //   ]

  //   sut.setProps({ rules })
  //   sut.vm.validate()
  //   expect(sut.vm.valid).toBe(false)
  // })
})
