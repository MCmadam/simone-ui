/*
 * Registerable unit tests
 *
 */

/* eslint-env jest */

import { shallowMount } from '@vue/test-utils'
import { registerableInject, registerableProvide } from './registerable'

describe('registerable mixin', () => {
  let namespace
  let injecter
  let provider

  beforeAll(() => {
    namespace = 'NAMESPACE'
  })

  beforeEach(() => {
    injecter = registerableInject(namespace)
    provider = registerableProvide(namespace)
  })

  describe('inject', () => {
    it('throws if no namespace is given', () => {
      expect(() => {
        registerableInject(undefined)
      }).toThrow()
    })
  })
})
