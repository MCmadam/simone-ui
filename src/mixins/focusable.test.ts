/*
 * focusable unit tests
 *
 */

/* eslint-env jest */

import { shallowMount } from '@vue/test-utils'
import focusable from './focusable'

describe('focusable mixin', () => {
  let sut

  beforeEach(() => {
    sut = shallowMount({
      mixins: [focusable],
      template: '<div />',
    })
  })

  it("provides data attribute 'isFocused' which defaults to null", () => {
    expect.assertions(1)
    expect(sut.vm.isFocused).toBe(null)
  })

  describe('onBlur', () => {
    it("set's isFocused to false when invoked", () => {
      expect.assertions(2)

      expect(sut.vm.isFocused).toBe(null)
      sut.vm.onBlur()
      expect(sut.vm.isFocused).toBe(false)
    })

    it("emit's a blur event with data from original event", () => {
      expect.assertions(1)

      const data = { test: 'test' }
      sut.vm.onBlur(data)
      expect(sut.emitted().blur).toMatchObject([[data]])
    })
  })

  describe('onFocus', () => {
    it("set's isFocused to true when invoked", () => {
      expect.assertions(2)

      expect(sut.vm.isFocused).toBe(null)
      sut.vm.onFocus()
      expect(sut.vm.isFocused).toBe(true)
    })

    it("emit's a focus event with data from original event", () => {
      expect.assertions(1)

      const data = { test: 'test' }
      sut.vm.onFocus(data)
      expect(sut.emitted().focus).toMatchObject([[data]])
    })
  })
})
