import filter from 'lodash/filter'
import get from 'lodash/get'
import isUndefined from 'lodash/isUndefined'
import some from 'lodash/some'
import replace from '@/util/array/replace'
import comparable from '@/mixins/comparable'
import focusable from '@/mixins/focusable'

/**
 * Selectable mixin
 *
 * Use when a component has an active/inactive state and a v-model to track the value
 * The same v-model can be shared across multiple selectable components, HOWEVER...
 *
 * each value (trueValue/falseValue/value) must be unique since the component is not aware of others
 */
export default {
  name: 'selectable',

  /**
   * this component uses the default `isEquals` comparison to evaluate change on possible complex value types
   */
  mixins: [comparable, focusable],

  /**
   * for some inputs a different model is required,
   * by defining a custom model this mixins can be used for all components
   * those who consume this mixin CAN define an own model with a different event,
   * but the prop must remain the same
   * @ignore
   * @see https://vuejs.org/v2/guide/components-custom-events.html#Customizing-Component-v-model
   */
  model: {
    prop: '_selectableValue',
    event: 'select',
  },

  data(vm) {
    return {
      /**
       * this data key is deliberatly the same as 'PuInput'
       * it's very important that these stay the same!
       * @ignore
       * @see PuInput ~/components/base/input/input.js
       */
      lazyValue: vm._selectableValue,
    }
  },

  /**
   * @todo decide if or how this functionality should be implemented in selectable
   * in order to be compatible with PuInput the _$modelEvent has to be set
   * for now it will just override the PuInput
   */
  beforeCreate() {
    this._$modelEvent = get(this, '$options.model.event', 'select')
  },

  props: {
    /**
     * model requires a property,
     * setting selectableValue from an external source does nothing
     * @name _selectableValue
     * @see model
     * @private
     */
    _selectableValue: {
      type: null,
    },

    /**
     * false value for the component, must be unique when used with a shared v-model (multiple)
     * @name falseValue
     * @type {any}
     * @default null
     * @description this is useful when the component represents an object or domnode and just a property is required as value
     * by setting this to a truthy value it will be considered. this will also prioritize before the value prop
     */
    falseValue: {
      type: null,
      default: void 0,
    },

    /**
     * true value for the component, must be unique when used with a shared v-model (multiple)
     * @name falseValue
     * @type {any}
     * @default null
     * @description this is useful when the component represents an object and just the 'id' is required as value
     * by setting this to a truthy value it will be considered. this will also prioritize before the value prop
     */
    trueValue: {
      type: null,
      default: void 0,
    },

    /**
     * explicitly tell this component that it's one of more and the model should be stored inside an array.
     * setting the model to an array results to the same behaviour (implicit)
     *
     * @name multiple
     * @type {Boolean}
     * @default false
     */
    multiple: {
      type: Boolean,
      default: null,
    },

    /**
     * the value this component represents when on the 'active' state
     * @name value
     * @type {any}
     * @description despite this mixin having a true/false value property this is used to
     * stay inline with other input components and can be used when it suffies
     */
    value: {
      required: false,
    },

    selected: {
      type: Boolean,
    },
  },

  watch: {
    /**
     * required for syncing v-model across components, do not set this from an external source!
     * updates the v-model value even when it's magically updated by vue,
     * without this watcher the v-model cannot be shared across components
     * @ignore
     */
    _selectableValue: {
      handler(newVal, oldVal) {
        this.lazyValue = newVal
      },
      // immediate: true,
    },
    value: {
      handler(newVal, oldVal) {
        if (!isUndefined(this.trueValue)) {
          // dev(
          //  `updating wrong property 'value',
          //  this component has it's trueValue set to ${this.trueValue} which should be updated instead`
          // )
          return
        }

        this.updateModel(newVal, oldVal)
      },
    },
    trueValue: {
      handler(newVal, oldVal) {
        this.updateModel(newVal, oldVal)
      },
    },
    falseValue: {
      handler(newVal, oldVal) {
        this.updateModel(newVal, oldVal, true)
      },
    },
    selected: {
      handler(newVal) {
        this.select(newVal)
      },
      immediate: true,
    },
  },

  computed: {
    /**
     * Computed value that prioritizes the trueValue if set
     * @name computedSelectableValue
     * @return {*} `trueValue` if set, `value` otherwise
     * @ignore
     */
    computedTrueValue() {
      return !isUndefined(this.trueValue) ? this.trueValue : this.value
    },

    /**
     * replica of `PuInput` property so this can be used as a standalone mixin
     * the component requiring to be an input
     * keep in sync with PuInput so the internal API remains the same
     * @see PuInput `~/components/base/input/input`
     */
    internalValue: {
      get() {
        return this.lazyValue
      },
      set(newVal) {
        this.lazyValue = newVal
        // emit an event that updates the v-model
        // this is ok since this will only be set from within this component
        this.$emit(this._$modelEvent, newVal)
      },
    },

    isActive() {
      const value = this.computedTrueValue
      const inputValue = this.internalValue

      if (this.isMultiple) {
        return Array.isArray(inputValue)
          ? some(inputValue, (item) => this.valueComparator(item, value))
          : false
      }

      return value
        ? this.valueComparator(inputValue, value)
        : Boolean(inputValue)
    },

    isMultiple() {
      return (
        this.multiple === true ||
        (this.multiple === null && Array.isArray(this.internalValue))
      )
    },

    hasFalseValue() {
      return !isUndefined(this.falseValue)
    },
  },

  methods: {
    select(val) {
      if (this.isDisabled) {
        return
      }

      const isActive = this.isActive
      if ((isActive && val === true) || (!isActive && val === false)) {
        return
      }

      const trueValue = this.computedTrueValue
      let inputValue = this.internalValue

      if (this.isMultiple) {
        if (!Array.isArray(inputValue)) {
          inputValue = []
        }

        if (isActive) {
          if (this.hasFalseValue) {
            replace(inputValue, trueValue, this.falseValue)
          } else {
            inputValue = filter(
              inputValue,
              (item) => !this.valueComparator(item, trueValue)
            )
          }
        } else {
          if (this.hasFalseValue) {
            replace(inputValue, this.falseValue, trueValue)
          } else {
            inputValue.push(trueValue)
          }
        }
      } else {
        if (isActive) {
          inputValue = this.hasFalseValue
            ? this.falseValue
            : inputValue
            ? null
            : !inputValue
        } else {
          inputValue = trueValue || !inputValue
        }
      }

      this.lazyValue = inputValue
    },
    onSelect() {
      this.select()
      this.$emit(this._$modelEvent, this.lazyValue)
      this.$emit('selected')
    },

    /**
     * update the internal v-model representation
     * whenever oneof trueValue/falseValue/value is updated from an external source
     * @private
     * @param {*} newVal
     * @param {*} oldVal
     * @param {boolean} [isFalseValue=false]
     */
    updateModel(newVal, oldVal, isFalseValue = false) {
      if (
        (this.isActive && isFalseValue) ||
        (!this.isActive && !isFalseValue)
      ) {
        return
      }

      let inputValue = this.internalValue

      if (this.isMultiple) {
        replace(inputValue, oldVal, newVal)
      } else {
        inputValue = newVal
      }

      this.lazyValue = inputValue
    },
  },
}
