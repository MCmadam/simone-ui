import assign from 'lodash/assign'
import isString from 'lodash/isString'

export default {
  name: 'colorable',

  props: {
    color: {
      type: String,
    },
  },

  data() {
    return {
      defaultColor: undefined,
    }
  },

  computed: {
    computedColor() {
      return this.color || this.defaultColor
    },
  },

  methods: {
    /**
     * Add color to classes. if color is falsy it will look for `color || defaultColor`
     * on the component instead
     *
     * @param {Object=} classes an object literal containing classnames as keys
     * @param {String=} color a word which is applied as class to classes
     * @return {Object} a `copy of classes` with color added
     */
    setBackgroundColor(classes, color) {
      const colored = assign({}, classes)
      const selectedColor = isString(color) ? color : this.computedColor

      if (selectedColor) {
        colored[selectedColor] = true
      }

      return colored
    },
    /**
     * Add color with the suffix `--text` to classes. if color is falsy it will look for the `color || defaultColor`
     * on the component instead
     *
     * @param {Object=} classes an object literal containing classnames as keys
     * @param {String=} color a word which is applied as class to classes
     * @return {Object} a `copy of classes` with color added
     */
    setTextColor(classes, color) {
      const colored = assign({}, classes)
      const selectedColor = color !== undefined ? color : this.computedColor

      if (selectedColor) {
        colored[`${selectedColor}--text`] = true
      }

      return colored
    },
  },
}
