import Vue from 'vue'

export default Vue.extend({
  name: 'taggable',

  props: {
    tag: {
      default: 'div',
    },
  },
})
