// @TODO: is this still necessary?
import pick from 'lodash/pick'

const allowedProps = {
  absolute: Boolean,
  bottom: String,
  fixed: Boolean,
  left: String,
  right: String,
  top: String,
}

export const positionableFactory = (props = []) => {
  return {
    name: 'positionable',
    props: props.length ? pick(allowedProps, props) : allowedProps,
  }
}

const positionable = positionableFactory()
export default positionable
