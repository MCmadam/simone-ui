import Vue from 'vue'

/**
 * lazy-model factory
 * @mixin
 * @param {String} event
 * @param {String} prop
 * @return {Mixin} lazy-model factory
 */
export const lazyModelFactory = (event, prop = 'value') => {
  if (!event) {
    throw new Error('lazy-model mixin must receive an event')
  }

  return Vue.extend({
    name: 'lazy-model',

    model: {
      prop,
      event,
    },

    props: {
      /**
       * model property
       * @model
       */
      [prop]: {
        required: false,
      },
    },

    data(vm) {
      return {
        lazyValue: vm[prop],
      }
    },

    computed: {
      /**
       * InternalValue
       * @prop internalValue
       * @description this acts as a proxy for automatically emitting an event when setting the value internally
       */
      internalValue: {
        get() {
          return this.lazyValue
        },
        set(val) {
          this.lazyValue = val
          /**
           * @event modelEvent
           */
          this.$emit(event, val)
        },
      },
    },

    watch: {
      // We want to update the lazyValue whenever the model prop get's updated from the outside
      [prop]: {
        handler(newVal) {
          // Since the value prop changed from the outside we do not want to emit a change event
          this.lazyValue = newVal
        },
      },
    },
  })
}

/**
 * lazy-model mixin with default param event=input, model=value
 * @mixin
 */
export default lazyModelFactory('input')
