/* eslint-env jest */

import positionable, { positionableFactory } from './positionable'
import { hasOwn } from '@/util/object'

const allowedProps = {
  absolute: Boolean,
  bottom: String,
  fixed: Boolean,
  left: String,
  right: String,
  top: String,
}

describe('positionable mixin', () => {
  it('returns the allowed props when no param given', () => {
    expect(positionable.props).toMatchObject(allowedProps)
  })

  it("set's the right props", () => {
    expect.assertions(Object.keys(allowedProps).length)

    for (const prop in allowedProps) {
      if (hasOwn(allowedProps, prop)) {
        expect(positionableFactory([prop]).props).toMatchObject({
          [prop]: allowedProps[prop],
        })
      }
    }
  })
})
