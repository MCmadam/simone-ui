import api from '@/api'
import { Uuid, Attachment, RequestCancelable } from '@/types'
import { makeFluentConfig } from '@/util/api'
import mapContextBackend from '@/lib/model/attachment/map-context-backend'

export type PostAttachmentData = {
  activity: Uuid,
  name: string,
  url: string,
}

export type PostAttachmentOptions = {
  data: PostAttachmentData,
}

export default (uuid: Uuid, options: PostAttachmentOptions & RequestCancelable) => {
  const config = makeFluentConfig(options)
    .transformResponse(mapContextBackend)
    .toConfig()

  return api.post<Attachment>(`/partup/${uuid}/attachments`, config)
}
