import api from '@/api'
import keywordPost from '@/data/keyword/post'
import { RequestCancelable, PARTUP_ALLOW_SOLICIT_REQUEST } from '@/types'
import { makeFluentConfig } from '@/util/api'

export default async (
  partup: {
    organization_uuid: string
  },
  options: {
    query?: {
      /**
       * import all outstanding backlog items from the user that is creating this partup
       */
      import_backlog?: boolean
    }
    data: {
      end_date?: string
      image_url?: string
      keywords?: any[]
      name: string
      organization_uuid?: string
      result?: string
      visibility: string
      request_option: PARTUP_ALLOW_SOLICIT_REQUEST
    }
  } & RequestCancelable
) => {
  const id = partup.organization_uuid
  const config = makeFluentConfig(options).toConfig()

  if (config.data && config.data.keywords) {
    const { data: keywords } = await keywordPost(config.data.keywords)
    config.data.keywords = keywords.map(({ uuid }) => uuid)
  }

  return await api.post<any>(`/partups/organization/${id}`, config)
}
