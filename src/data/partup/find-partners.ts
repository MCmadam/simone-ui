import api from '@/api'
import { RequestCancelable, Identifiable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (params: Identifiable, options?: RequestCancelable) => {
  const id = getIdentifier(params)
  const config = makeFluentConfig(options).toConfig()

  return api.get<[]>(`/partup/${id}/partners`, config)
}
