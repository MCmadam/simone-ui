import api from '@/api'
import keywordPost from '@/data/keyword/post'
import { Identifiable, ApiResponse, RequestCancelable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default async (
  params: Identifiable,
  options: {
    data: any
    query?: {
      validateOnly?: boolean
    }
  } & RequestCancelable
) => {
  const id = getIdentifier(params)
  const config = makeFluentConfig(options).toConfig()

  // to be replaced with new implementation below.
  if (config.data.keywords) {
    const { data: keywords } = await keywordPost(config.data.keywords)
    config.data.keywords = keywords.map(({ uuid }) => uuid)
  }

  return await api.patch<any>(`/partup/${id}`, config)
}
