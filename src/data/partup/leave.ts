import api from '@/api'
import { Identifiable, RequestCancelable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (params: Identifiable, options?: RequestCancelable) => {
  const id = getIdentifier(params)
  const config = makeFluentConfig(options).toConfig()

  return api.del(`/partup/${id}/partners`, config)
}
