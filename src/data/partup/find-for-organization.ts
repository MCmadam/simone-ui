import api from '@/api'
import { Identifiable, RequestCancelable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (
  organization: Identifiable,
  options?: {
    query?: {
      order?: string
      q?: string
    }
  } & RequestCancelable
) => {
  const id = getIdentifier(organization)
  const config = makeFluentConfig(options).toConfig()

  return api.get<any[]>(`/partups/organization/${id}`, config)
}
