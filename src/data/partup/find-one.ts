import api from '@/api'
import { Findable, RequestCancelable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (
  partup: Findable,
  options?: {
    query?: {
      solicit?: string
    }
  } & RequestCancelable
) => {
  const id = getIdentifier(partup)
  const config = makeFluentConfig(options).toConfig()

  return api.get<any>(`/partup/${id}`, config)
}
