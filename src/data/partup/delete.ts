import api from '@/api'
import { Identifiable, RequestCancelable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (partup: Identifiable, options?: RequestCancelable) => {
  const id = getIdentifier(partup)
  const config = makeFluentConfig(options).toConfig()

  return api.del<any>(`/partup/${id}`, config)
}
