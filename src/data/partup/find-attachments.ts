import api from '@/api'
import { RequestCancelable, Uuid, Attachment, BackendAttachment } from '@/types'
import { makeFluentConfig } from '@/util/api'
import mapContextBackend from '@/lib/model/attachment/map-context-backend'

export type FindAttachmentsOptions = {
  query?: {
    activity?: Uuid,
  }
} & RequestCancelable

export default (
  uuid: Uuid,
  options?: FindAttachmentsOptions,
) => {
  const config = makeFluentConfig(options)
    .transformResponse((data) => data.map(mapContextBackend))
    .toConfig()

  return api.get<Attachment[]>(`/partup/${uuid}/attachments`, config)
}
