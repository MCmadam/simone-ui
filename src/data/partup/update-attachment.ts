import api from '@/api'
import { Uuid, Attachment, RequestCancelable } from '@/types'
import { makeFluentConfig } from '@/util/api'
import mapContextBackend from '@/lib/model/attachment/map-context-backend'

export default (partupId: Uuid, attachmentId: Uuid, options: {
  data: {
    pinned: boolean,
  }
} & RequestCancelable) => {
  const config = makeFluentConfig(options)
    .transformResponse(mapContextBackend)
    .toConfig()

  return api.patch<Attachment>(`/partup/${partupId}/attachments/${attachmentId}`, config)
}
