export default (dataFn, mock, params = {}, options = {}) => {
  it('a promise will be returned that resolves to the api response', () => {
    expect.assertions(1)

    const expected = {
      uuid: '1234',
    }

    mock.mockResolvedValue(expected)

    expect(dataFn(params, options)).resolves.toBe(expected)
  })
}
