import api from '@/api'
import { Identifiable, RequestCancelable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (
  organization: Identifiable,
  options: {
    data: {
      admin: boolean
      email: string
    }
  } & RequestCancelable
) => {
  const id = getIdentifier(organization)
  const config = makeFluentConfig(options).toConfig()

  return api.post<any>(`/solicit/invite/organization/${id}`, config)
}
