import api from '@/api'
import {
  Identifiable,
  RequestCancelable,
  ORGANIZATION_VISIBILITY,
  OrganizationData,
} from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (
  organization: Identifiable,
  options: {
    data: Partial<OrganizationData>
  } & RequestCancelable
) => {
  const id = getIdentifier(organization)
  const config = makeFluentConfig(options).toConfig()

  return api.patch<any>(`/super_powers/organization/${id}`, config)
}
