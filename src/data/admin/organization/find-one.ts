import api from '@/api'
import { Identifiable, RequestCancelable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (organization: Identifiable, options?: RequestCancelable) => {
  const id = getIdentifier(organization)
  const config = makeFluentConfig(options).toConfig()

  return api.get<any>(`/super_powers/organization/${id}`, config)
}
