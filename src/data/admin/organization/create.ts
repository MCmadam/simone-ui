import api from '@/api'
import { RequestCancelable, OrganizationData } from '@/types'
import { makeFluentConfig } from '@/util/api'

export default (
  options: {
    data: OrganizationData
  } & RequestCancelable
) => {
  const config = makeFluentConfig(options).toConfig()

  return api.post<any>('/super_powers/organizations', config)
}
