import api from '@/api'
import { RequestCancelable, Identifiable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (activity: Identifiable, options?: RequestCancelable) => {
  const id = getIdentifier(activity)
  const config = makeFluentConfig(options).toConfig()

  return api.del<any>(`/activity/${id}`, config)
}
