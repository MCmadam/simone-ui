import api from '../__mocks__/api-mock'
import { Identifiable } from '@/types'
import findOne from './find-one'
import resolveApiResponse from '../__tests__/resolve-api-response'

describe('when I want to get a single activity', () => {
  beforeEach(() => {
    api.get.mockClear()
  })

  it('an Identifiable must be given', async () => {
    expect.assertions(1)

    const identifiable: Identifiable = {
      uuid: '1234',
    }

    await findOne(identifiable)

    expect(api.get).toHaveBeenCalledWith('/activity/1234', expect.anything())
  })

  resolveApiResponse(findOne, api.get)
})
