import api from '@/api'
import { Identifiable, RequestCancelable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (
  activity: Identifiable,
  options: {
    data: {
      userUuids: string[]
    }
  } & RequestCancelable
) => {
  const id = getIdentifier(activity)
  const config = makeFluentConfig(options).toConfig()

  return api.post<any>(`/activity/${id}/contributors`, config)
}
