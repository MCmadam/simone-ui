import api from '../__mocks__/api-mock'
import createForPartup from './create-for-partup'
import resolveApiResponse from '../__tests__/resolve-api-response'

describe('when I want to create an activity in a part-up', () => {
  beforeEach(() => {
    api.post.mockClear()
  })

  it('a part-up must be given as identifier', async () => {
    expect.assertions(1)

    const partup = {
      uuid: '1234',
    }

    await createForPartup(partup, {
      data: {
        name: 'activity name',
      },
    })

    expect(api.post).toHaveBeenCalledWith(
      '/activities/partup/1234',
      expect.objectContaining({
        data: {
          name: 'activity name',
        },
      })
    )
  })

  it('an activity with property name must be given as data', async () => {
    expect.assertions(1)

    const activity = {
      name: 'activity name',
    }

    await createForPartup(
      {
        uuid: '1234',
      },
      {
        data: activity,
      }
    )

    expect(api.post).toHaveBeenCalledWith(
      '/activities/partup/1234',
      expect.objectContaining({
        data: {
          name: 'activity name',
        },
      })
    )
  })

  it('a promise will be returned that resolves to the api response', () => {
    expect.assertions(1)

    const expectedResponse: any = {
      uuid: '1234',
    }

    api.post.mockResolvedValue(expectedResponse)

    const response = createForPartup(
      {
        uuid: '5678',
      },
      {
        data: {
          name: 'name',
        },
      }
    )

    expect(response).resolves.toBe(expectedResponse)
  })

  resolveApiResponse(createForPartup, api.post)
})
