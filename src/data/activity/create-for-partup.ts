import api from '@/api'
import { RequestCancelable, Identifiable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (
  partup: Identifiable,
  options: {
    data: {
      name: string
      status?: string
      end_date?: string
      description?: string
    }
  } & RequestCancelable
) => {
  const id = getIdentifier(partup)
  const config = makeFluentConfig(options).toConfig()

  return api.post<any>(`/activities/partup/${id}`, config)
}
