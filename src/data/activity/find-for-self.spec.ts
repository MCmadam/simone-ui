import api from '../__mocks__/api-mock'
import findForSelf from './find-for-self'
import resolveApiResponse from '../__tests__/resolve-api-response'

describe('when I want to get activities for myself', () => {
  beforeEach(() => {
    api.get.mockClear()
  })

  it('I do not have to pass options', async () => {
    expect.assertions(1)

    await findForSelf()

    expect(api.get).toHaveBeenCalledWith('/activities/user', expect.anything())
  })

  it('I can filter by giving a search query', async () => {
    expect.assertions(1)

    await findForSelf({
      query: {
        q: 'custom search',
      },
    })

    expect(api.get).toHaveBeenCalledWith(
      '/activities/user',
      expect.objectContaining({
        query: {
          q: 'custom search',
        },
      })
    )
  })

  resolveApiResponse(findForSelf, api.get)
})
