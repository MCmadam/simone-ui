import api from '@/api'
import { getIdentifier } from '@/util/api'
import { Identifiable } from '@/types'

export default (params: Identifiable) => {
  const id = getIdentifier(params)

  return api.post<any>(`/user/activity/bookmarks/${id}`, {})
}
