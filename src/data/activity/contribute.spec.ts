import api from '../__mocks__/api-mock'
import resolveApiResponse from '../__tests__/resolve-api-response'
import contribute from './contribute'

/* eslint-disable */

describe('when I want to contribute to an activity', () => {
  beforeEach(() => {
    api.post.mockClear()
  })

  it('an activity must be given as identifier', async () => {
    expect.assertions(1)

    const activity = {
      uuid: '1234',
    }

    await contribute(activity, {
      data: {
        userUuids: ['1'],
      },
    })

    expect(api.post).toHaveBeenCalledWith(
      '/activity/1234/contributors',
      expect.objectContaining({
        data: {
          user_uuids: ['1'],
        },
      })
    )
  })

  it('does not modify given data', async () => {
    expect.assertions(1)

    const activity = {
      uuid: '1234',
    }

    await contribute(activity, {
      data: {
        userUuids: ['1'],
      },
    })

    expect(api.post).toHaveBeenCalledWith(
      '/activity/1234/contributors',
      expect.objectContaining({
        data: {
          user_uuids: ['1'],
        },
      })
    )
  })

  resolveApiResponse(contribute, api.post)
})
