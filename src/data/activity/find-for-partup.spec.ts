import api from '../__mocks__/api-mock'
import resolveApiResponse from '../__tests__/resolve-api-response'
import findForPartup from './find-for-partup'

describe('when I want to get activities of a partup', () => {
  beforeEach(() => {
    api.get.mockClear()
  })

  it('a partup must be given as identifier', async () => {
    expect.assertions(1)

    const partup = {
      uuid: '1234',
    }

    await findForPartup(partup)

    expect(api.get).toHaveBeenCalledWith(
      '/activities/partup/1234',
      expect.anything()
    )
  })

  it('I can filter by giving a search query', async () => {
    expect.assertions(1)

    const partup = {
      uuid: '1234',
    }

    await findForPartup(partup, {
      query: {
        q: 'custom search',
      },
    })

    expect(api.get).toHaveBeenCalledWith(
      '/activities/partup/1234',
      expect.objectContaining({
        query: {
          q: 'custom search',
        },
      })
    )
  })

  resolveApiResponse(findForPartup, api.get)
})
