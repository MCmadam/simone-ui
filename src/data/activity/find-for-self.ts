import api from '@/api'
import { RequestCancelable } from '@/types'
import { makeFluentConfig } from '@/util/api'

export default (
  options?: {
    query?: {
      q: string
    }
  } & RequestCancelable
) => {
  const config = makeFluentConfig(options).toConfig()

  return api.get<any>('/activities/user', config)
}
