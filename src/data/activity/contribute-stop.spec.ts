import api from '../__mocks__/api-mock'
import resolveApiResponse from '../__tests__/resolve-api-response'
import contributeStop from './contribute-stop'

describe('when I want to stop contributing to an activity', () => {
  beforeEach(() => {
    api.del.mockClear()
  })

  it('an activity must be given as identifier', async () => {
    expect.assertions(1)

    const activity = {
      uuid: '1234',
    }

    await contributeStop(activity)

    expect(api.del).toHaveBeenCalledWith(
      '/activity/1234/contributor',
      expect.anything()
    )
  })

  resolveApiResponse(contributeStop, api.del)
})
