import api from '../__mocks__/api-mock'
import resolveApiResponse from '../__tests__/resolve-api-response'
import activityDelete from './delete'

describe('when I want to delete an activity', () => {
  beforeEach(() => {
    api.del.mockClear()
  })

  it('an activity must be given as identifier', async () => {
    expect.assertions(1)

    const activity = {
      uuid: '1234',
    }

    await activityDelete(activity)

    expect(api.del).toHaveBeenCalledWith('/activity/1234', expect.anything())
  })

  resolveApiResponse(activityDelete, api.del)
})
