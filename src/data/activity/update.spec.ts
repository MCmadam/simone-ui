import api from '../__mocks__/api-mock'
import update from './update'

describe('when I want to update a single activity', () => {
  beforeEach(() => {
    api.patch.mockClear()
  })

  it('an activity must be given as identifier', async () => {
    expect.assertions(1)

    const activity = {
      uuid: '1234',
    }

    await update(activity, {
      data: {
        description: 'description',
      },
    })

    expect(api.patch).toHaveBeenCalledWith(
      '/activity/1234',
      expect.objectContaining({
        data: {
          description: 'description',
        },
      })
    )
  })

  it('the given partial activity is not modified', async () => {
    expect.assertions(1)

    const partialActivity = {
      status: 'status',
    }

    await update(
      {
        uuid: '1234',
      },
      {
        data: partialActivity,
      }
    )

    expect(api.patch).toHaveBeenCalledWith(
      '/activity/1234',
      expect.objectContaining({
        data: partialActivity,
      })
    )
  })
})
