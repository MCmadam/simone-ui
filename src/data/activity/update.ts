import api from '@/api'
import { Identifiable, RequestCancelable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (
  activity: Identifiable,
  options: {
    data: Partial<{
      description: string
      end_date: string
      lane_order: number
      name: string
      status: string
    }>
  } & RequestCancelable
) => {
  const id = getIdentifier(activity)
  const config = makeFluentConfig(options).toConfig()

  return api.patch<any>(`/activity/${id}`, config)
}
