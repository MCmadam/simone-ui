import api from '@/api'
import { Identifiable, RequestCancelable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (activity: Identifiable, options?: RequestCancelable) => {
  const id = getIdentifier(activity)
  const config = makeFluentConfig(options).toConfig()

  return api.get<any>(`/activity/${id}`, config)
}
