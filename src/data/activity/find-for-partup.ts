import api from '@/api'
import { RequestCancelable, Identifiable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (
  params: Identifiable,
  options?: {
    query?: {
      q?: string
      solicit?: string
      contributor?: number
    }
  } & RequestCancelable
) => {
  const id = getIdentifier(params)
  const config = makeFluentConfig(options).toConfig()

  if (config.query?.status === null) {
    config.query.status = ''
  }

  return api.get<any[]>(`/activities/partup/${id}`, config)
}
