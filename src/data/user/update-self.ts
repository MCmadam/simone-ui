import api from '@/api'
import keywordPost from '@/data/keyword/post'
import reduce from 'lodash/reduce'
import { RequestCancelable } from '@/types'
import { makeFluentConfig } from '@/util/api'

export default async (
  options?: {
    data: any
    query?: {
      validateOnly?: boolean
    }
  } & RequestCancelable
) => {
  const config = makeFluentConfig(options)
    .data('data', (data) => {
      return reduce(
        data,
        (total, value, key) => {
          total[key] = value === undefined ? null : value

          return total
        },
        {}
      )
    })
    .toConfig()

  // to be replaced with implementation below, depending on back-end.
  if (config.data?.keywords) {
    let keywords

    if (config.query?.validateOnly) {
      keywords = config.data.keywords.map(({ uuid }) => uuid).filter((x) => x)
    } else {
      const { data } = await keywordPost(config.data.keywords)
      keywords = data.map(({ uuid }) => uuid)
    }

    config.data.keywords = keywords
  }

  return await api.patch<any>('/user', config)
}
