import api from '@/api'
import { SOLICIT_ENTITIES, RequestCancelable } from '@/types'
import { makeFluentConfig } from '@/util/api'

export default (
  params: {
    entity: SOLICIT_ENTITIES
    entity_uuid: string
  },
  options?: {
    query?: {
      q: string
      organization?: number
      partup?: number
    }
  } & RequestCancelable
) => {
  const config = makeFluentConfig(options).toConfig()

  return api.get<any[]>(
    `/users/${params.entity}/${params.entity_uuid}/recommendations`,
    config
  )
}
