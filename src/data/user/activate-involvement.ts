import api from '@/api'

const activateInvolvement = (id: string) => {
  return api.post(`/user/involvement/${id}/activate`, {})
}

export default activateInvolvement
