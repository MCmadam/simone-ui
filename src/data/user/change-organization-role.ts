import api from '@/api'
import { ORGANIZATION_USER_ROLES } from '@/types'
import { makeFluentConfig } from '@/util/api'

export default (
  params: {
    organizationUuid: string
    userUuid: string
  },
  options: {
    data: {
      role: ORGANIZATION_USER_ROLES
    }
  }
) => {
  const config = makeFluentConfig(options).toConfig()

  return api.patch<any>(
    `/users/organization/${params.organizationUuid}/${params.userUuid}`,
    config
  )
}
