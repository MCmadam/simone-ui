import api from '@/api'
import { Identifiable, RequestCancelable } from '@/types'
import { makeFluentConfig, getIdentifier } from '@/util/api'

export default (
  user: Identifiable,
  options?: {
    data: {
      userTo: string
    }
  } & RequestCancelable
) => {
  const id = getIdentifier(user)
  const config = makeFluentConfig(options).toConfig()

  return api.post<any>(`/user/${id}/share`, config)
}
