import api from '@/api'
import { Findable, RequestCancelable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (
  user: Findable,
  options?: {
    query?: {
      solicit?: string
    }
  } & RequestCancelable
) => {
  const id = getIdentifier(user)
  const config = makeFluentConfig(options).toConfig()

  return api.get<any>(`/user/${id}`, config)
}
