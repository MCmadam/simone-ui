import api from '@/api'

const deactivateInvolvement = (id: string) => {
  return api.post(`/user/involvement/${id}/deactivate`, {})
}

export default deactivateInvolvement
