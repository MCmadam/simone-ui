import api from '@/api'
import { Identifiable, RequestCancelable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (
  partup: Identifiable,
  options?: {
    query?: {
      solicit?: string
    }
  } & RequestCancelable
) => {
  const id = getIdentifier(partup)
  const config = makeFluentConfig(options).toConfig()

  return api.get<any[]>(`/users/partup/${id}`, config)
}
