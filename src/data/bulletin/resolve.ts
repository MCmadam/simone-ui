import api from '@/api'
import { RequestCancelable } from '@/types'
import { makeFluentConfig } from '@/util/api'

export default (uuid: string, options?: RequestCancelable) => {
  const config = makeFluentConfig(options).toConfig()

  return api.post<any>(`/user/bulletin/${uuid}/acked`, config)
}
