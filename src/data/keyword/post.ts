import api from '@/api'

export default async (keywords: any[]) => {
  const result = []

  for (const keyword of keywords) {
    if (keyword.uuid) {
      result.push(keyword)
    } else {
      const { data } = await api.post('/keywords', {
        data: keyword,
      })
      result.push(data)
    }
  }

  return {
    data: result,
  }
}
