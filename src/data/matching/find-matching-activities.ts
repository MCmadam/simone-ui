import api from '@/api'
import { MatchingActivityResponse } from '@/types'

export default () => {
  return api.get<Array<MatchingActivityResponse>>('/smatching/activities', {})
}
