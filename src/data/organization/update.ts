import api from '@/api'
import { Identifiable, RequestCancelable } from '@/types'
import { getIdentifier, makeFluentConfig } from '@/util/api'

export default (
  organization: Identifiable,
  options?: {
    data: any
  } & RequestCancelable
) => {
  const id = getIdentifier(organization)
  const config = makeFluentConfig(options).toConfig()

  return api.patch<any>(`/organization/${id}`, config)
}
