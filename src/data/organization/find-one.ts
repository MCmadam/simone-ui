import api from '@/api'
import { makeFluentConfig, getIdentifier } from '@/util/api'
import { Findable, RequestCancelable } from '@/types'

export default (
  organization: Findable,
  options?: {
    query?: {
      q: string
    }
  } & RequestCancelable
) => {
  const id = getIdentifier(organization)
  const config = makeFluentConfig(options).toConfig()

  return api.get<any>(`/organization/${id}`, config)
}
