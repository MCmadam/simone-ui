import api from '@/api'
import { ApiResponse, SOLICIT_ENTITIES, RequestCancelable } from '@/types'
import { makeFluentConfig } from '@/util/api'

type entitySolicitFindParams = {
  entity: SOLICIT_ENTITIES
  entity_uuid: string
}

type userSolicitFindParams = {
  entity: 'user'
}

type solicitFindParams = entitySolicitFindParams | userSolicitFindParams

export default (params: solicitFindParams, options?: RequestCancelable) => {
  const entity = params.entity
  const entityParams = params as entitySolicitFindParams
  const entityUuid = entityParams.entity_uuid

  const config = makeFluentConfig(options).toConfig()

  return api.get<any[]>(
    `/solicit/pending/${entity}${entityUuid ? `/${entityUuid}` : ''}`,
    config
  )
}
