import api from '@/api'
import { SOLICIT_ENTITIES, RequestCancelable } from '@/types'
import { makeFluentConfig } from '@/util/api'

export default (
  params: {
    entity: SOLICIT_ENTITIES
    entity_uuid: string
  },
  options?: RequestCancelable
) => {
  const config = makeFluentConfig(options).toConfig()

  return api.post<any>(
    `/solicit/request/${params.entity}/${params.entity_uuid}`,
    config
  )
}
