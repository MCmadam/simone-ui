import solicitAccept from './accept'
import solicitReject from './reject'
import { SOLICIT_ACTIONS, RequestCancelable } from '@/types'

export default (
  params: {
    action: SOLICIT_ACTIONS
    solicit: any
  },
  options?: {
    body?: {
      message: string
    }
  } & RequestCancelable
) => {
  switch (params.action) {
    case SOLICIT_ACTIONS.accept:
      return solicitAccept(params.solicit, options)
    case SOLICIT_ACTIONS.reject:
      return solicitReject(params.solicit, options)
  }
}
