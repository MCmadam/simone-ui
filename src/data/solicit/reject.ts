import api from '@/api'
import {
  Identifiable,
  SOLICIT_ENTITIES,
  SOLICIT_TYPES,
  RequestCancelable,
} from '@/types'
import { makeFluentConfig, getIdentifier } from '@/util/api'

export default (
  params: {
    for: SOLICIT_ENTITIES
    type: SOLICIT_TYPES
  } & Identifiable,
  options?: RequestCancelable
) => {
  const id = getIdentifier(params)
  const config = makeFluentConfig(options).toConfig()

  return api.post<any>(
    `/solicit/${params.type}/reject/${params.for}/${id}`,
    config
  )
}
