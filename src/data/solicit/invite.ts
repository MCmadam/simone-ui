import api from '@/api'
import { SOLICIT_ENTITIES, RequestCancelable } from '@/types'
import { makeFluentConfig } from '@/util/api'

export default (
  params: {
    entity: SOLICIT_ENTITIES
    entity_uuid: string
    user_uuid: string
  },
  options?: {
    data?: {
      admin: string
    }
  } & RequestCancelable
) => {
  const config = makeFluentConfig(options).toConfig()

  return api.post<any>(
    `/solicit/invite/${params.entity}/${params.entity_uuid}/${params.user_uuid}`,
    config
  )
}
