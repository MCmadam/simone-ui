import api from '@/api'
import { RequestCancelable } from '@/types'
import { makeFluentConfig } from '@/util/api'

export type SearchResponse = {
  activities: any[]
  partups: any[]
  users: any[]
  totals: {
    activities: number
    partups: number
    users: number
  }
}

export default (
  options?: {
    query?: {
      q: string
    }
  } & RequestCancelable
) => {
  const config = makeFluentConfig(options).toConfig()

  return api.get<SearchResponse>('/search', config)
}
