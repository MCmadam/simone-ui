import api from '@/api'
import { RequestCancelable } from '@/types'
import { makeFluentConfig } from '@/util/api'

export default (
  // path examples: 'user', 'organization/partups'
  path: string,
  options: {
    data: {
      image_url: string
    }
  } & RequestCancelable
) => {
  const config = makeFluentConfig(options).toConfig()

  return api.post<any>(`/${path}/image`, config)
}
