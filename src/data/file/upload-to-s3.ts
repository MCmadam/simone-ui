import axios from 'axios'
import preSign from '@/data/file/pre-sign'
import { RequestCancelable } from '@/types'

export default async (
  // path examples: 'user', 'organization/partups'
  path,
  options: {
    data: {
      file: File
    }
  }
) => {
  const file = options.data.file

  const {
    data: { url: presignedUrl },
  } = await preSign(path, { data: { type: file.type } })

  // dont use api since presignedUrl is for S3 directly
  await axios.put(presignedUrl, file, {
    headers: {
      'Content-Type': file.type,
    },
  })

  // is this really necessary?? why would preSign ever return an invalid url instead of throwing an error
  const parts = presignedUrl.split('?', 2)
  if (parts.length < 1) {
    throw new Error('some generic error from front-end about uploading to S3')
  }

  return { data: { url: parts[0] } }
}
