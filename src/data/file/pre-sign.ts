import api from '@/api'
import { makeFluentConfig } from '@/util/api'
import { RequestCancelable } from '@/types'

export default (
  // path examples: 'user', 'organization/partups'
  path: string,
  options: {
    data: {
      type: string
    }
  } & RequestCancelable
) => {
  const config = makeFluentConfig(options).toConfig()

  return api.post<{ url: string }>(`/${path}/image`, config)
}
