import currentSession from './current-session'
import get from 'lodash/get'

export default async (group) => {
  const session = await currentSession()

  if (session) {
    const userGroups = get(session, 'idToken.payload[cognito:groups]', false)
    if (userGroups) {
      return userGroups.includes(group)
    }
  }

  return false
}
