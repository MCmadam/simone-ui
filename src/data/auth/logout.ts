import Auth from '@aws-amplify/auth'

export default ({ global = false } = {}) => {
  return Auth.signOut({ global })
}
