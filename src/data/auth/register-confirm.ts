import Auth from '@aws-amplify/auth'
import { AUTH_ACTIONS } from '@/types'
import CognitoError from '@/util/error/cognito-error'
import presend from './presend'
import { nullable } from '@/util'

export default async (email, code) => {
  const username = nullable.toLower(email)

  try {
    await presend(username)
  } catch (error) {
    // console.error(error)
  }

  try {
    return await Auth.confirmSignUp(username, code)
  } catch (error) {
    throw new CognitoError(AUTH_ACTIONS.registerConfirm, error)
  }
}
