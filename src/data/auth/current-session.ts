import Auth from '@aws-amplify/auth'

export default () => Auth.currentSession()
