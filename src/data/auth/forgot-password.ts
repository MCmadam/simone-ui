import Auth from '@aws-amplify/auth'
import CognitoError from '@/util/error/cognito-error'
import { AUTH_ACTIONS } from '@/types'
import presend from './presend'
import { nullable } from '@/util'

export default async (email) => {
  const username = nullable.toLower(email)

  try {
    await presend(username)
  } catch (error) {
    // console.error('presend error', error)
  }

  try {
    return await Auth.forgotPassword(username)
  } catch (error) {
    throw new CognitoError(AUTH_ACTIONS.forgotPassword, error)
  }
}
