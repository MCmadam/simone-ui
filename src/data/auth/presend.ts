// Temporary workaround!
// let the back-end know from which branding domain the user is authenticating.
// this should be in place until the back-end handles the auth process entirely.
import api from '@/api'

export default (email) => {
  return api.post('/branding/email', {
    data: {
      email,
    },
  })
}
