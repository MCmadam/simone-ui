import Auth from '@aws-amplify/auth'
import { AUTH_ACTIONS } from '@/types'
import CognitoError from '@/util/error/cognito-error'
import presend from './presend'
import { nullable } from '@/util'

export default async (email, password, attrs = {}) => {
  const username = nullable.toLower(email)

  const attributes = attrs
  for (const key in attributes) {
    const a = attributes[key]

    if (typeof a === 'boolean') {
      attributes[key] = a.toString()
    }
  }

  try {
    await presend(username)
  } catch (error) {
    // console.error('presend error', error)
  }

  try {
    return await Auth.signUp({
      username,
      password,
      attributes,
    })
  } catch (error) {
    throw new CognitoError(AUTH_ACTIONS.register, error)
  }
}
