import api from '@/api'
import {
  RequestCancelable,
  Uuid,
  Message,
  MessageFindForPartupOptions,
} from '@/types'
import { makeFluentConfig } from '@/util/api'
import mapContextBackend from '@/lib/model/chat/map-context-backend'

export default (
  uuid: Uuid,
  options?: MessageFindForPartupOptions & RequestCancelable
) => {
  const config = makeFluentConfig(options)
    .transformResponse((messages) => messages.map(mapContextBackend))
    .toConfig()

  return api.get<Message[]>(`/messages/partup/${uuid}`, config)
}
