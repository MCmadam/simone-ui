import api from '@/api'
import {
  Uuid,
  MessageCreateForPartupOptions,
  RequestCancelable,
  Message,
} from '@/types'
import { makeFluentConfig } from '@/util/api'

export default (
  uuid: Uuid,
  options: MessageCreateForPartupOptions & RequestCancelable
) => {
  const config = makeFluentConfig(options).toConfig()

  return api.post<Message>(`/messages/partup/${uuid}`, config)
}
