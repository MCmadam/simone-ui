import api from '@/api'
jest.mock('@/api/api')

const mockAPI = api as jest.Mocked<typeof api>
const apiOptions = {
  endpoint: 'endpoint',
  region: 'region',
  service: 'service',
}
mockAPI.configure(apiOptions)

export default mockAPI
