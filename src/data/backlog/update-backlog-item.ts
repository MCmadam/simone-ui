import api from '@/api'
import { RequestCancelable, Identifiable } from '@/types'
import { makeFluentConfig, getIdentifier } from '@/util/api'

/* eslint-disable */

export default (
  backlogItem: Identifiable,
  options: {
    data: Partial<{
      name: string
      end_date: string | null
    }>
  } & RequestCancelable
) => {
  const id = getIdentifier(backlogItem)
  const config = makeFluentConfig(options).toConfig()

  return api.patch<{
    uuid: string
    name: string
    end_date: string | null
  }>(`/backlog/${id}`, config)
}
