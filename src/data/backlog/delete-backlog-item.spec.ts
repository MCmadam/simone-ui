/**
 * Delete Backlog Item Spec
 *
 * @group data
 * @group api/data/backlog
 *
 * This call is specifically for the Journey CreateWork
 */

import axios from 'axios'
import AxiosMockAdapter from 'axios-mock-adapter'
import { configureApi } from '@/util/test/index'
import deleteBacklogItem from './delete-backlog-item'

/* eslint-disable */

describe('when I want to delete a backlog item', () => {
  const axiosMock = new AxiosMockAdapter(axios)

  beforeAll(() => {
    configureApi()
  })

  beforeEach(() => {
    axiosMock.reset()
  })

  it('resolves', () => {
    expect.assertions(1)

    axiosMock.onDelete(/backlog/).reply(200, true)

    return expect(
      deleteBacklogItem({
        uuid: 'uuid',
      })
    ).resolves.toBe(true)
  })

  it('throws', () => {
    expect.assertions(1)

    axiosMock.onDelete(/backlog/).networkError()

    return expect(
      deleteBacklogItem({
        uuid: 'uuid',
      })
    ).rejects.toBeTruthy()
  })
})
