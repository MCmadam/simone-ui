/**
 * Update Backlog Item Spec
 *
 * @group data
 * @group api/data/backlog
 *
 * This call is specifically for the Journey CreateWork
 */

import axios from 'axios'
import AxiosMockAdapter from 'axios-mock-adapter'
import { configureApi } from '@/util/test/index'
import updateBacklogItem from './update-backlog-item'

/* eslint-disable */

describe('when I want to update a backlog item', () => {
  const axiosMock = new AxiosMockAdapter(axios)

  beforeAll(() => {
    configureApi()
  })

  beforeEach(() => {
    axiosMock.reset()
  })

  it('resolves', () => {
    expect.assertions(1)

    axiosMock.onPatch(/backlog/).reply(200, {
      data: {
        uuid: 'uuid',
        name: 'name',
        end_date: null,
      },
    })

    return expect(
      updateBacklogItem(
        {
          uuid: 'uuid',
        },
        {
          data: {
            name: 'name',
            end_date: null,
          },
        }
      )
    ).resolves.toMatchObject({
      data: {
        uuid: 'uuid',
        name: 'name',
        end_date: null,
      },
    })
  })

  it('throws', () => {
    expect.assertions(1)

    axiosMock.onPatch(/backlog/).networkError()

    return expect(
      updateBacklogItem(
        {
          uuid: 'uuid',
        },
        {
          data: {
            name: 'name',
            end_date: null,
          },
        }
      )
    ).rejects.toBeTruthy()
  })
})
