import api from '@/api'
import { BacklogItem, RequestCancelable } from '@/types'
import { makeFluentConfig } from '@/util/api'

export default (
  options: {
    data: Pick<BacklogItem, 'name' | 'end_date'>
  } & RequestCancelable
) => {
  const config = makeFluentConfig(options).toConfig()

  return api.post<BacklogItem>('/backlog', config)
}
