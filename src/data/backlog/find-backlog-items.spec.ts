/**
 * Find Backlog Items Spec
 *
 * @group data
 * @group api/data/backlog
 *
 * This call is specifically for the Journey CreateWork
 */

import axios from 'axios'
import AxiosMockAdapter from 'axios-mock-adapter'
import { configureApi } from '@/util/test/index'
import findBacklogItems from './find-backlog-items'

/* eslint-disable */

describe('when I want to find backlog items', () => {
  const axiosMock = new AxiosMockAdapter(axios)

  beforeAll(() => {
    configureApi()
  })

  beforeEach(() => {
    axiosMock.reset()
  })

  it('resolves', () => {
    expect.assertions(1)

    axiosMock.onGet(/backlog/).reply(200, {
      data: [
        {
          uuid: 'uuid',
          name: 'name',
          end_date: null,
        },
      ],
    })

    return expect(findBacklogItems()).resolves.toMatchObject({
      data: [
        {
          uuid: 'uuid',
          name: 'name',
          end_date: null,
        },
      ],
    })
  })

  it('throws', () => {
    expect.assertions(1)

    axiosMock.onGet(/backlog/).networkError()

    return expect(findBacklogItems()).rejects.toBeTruthy()
  })
})
