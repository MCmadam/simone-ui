import api from '@/api'
import { BacklogItem, RequestCancelable } from '@/types'
import { makeFluentConfig } from '@/util/api'

export default (options?: RequestCancelable) => {
  const config = makeFluentConfig(options).toConfig()

  return api.get<BacklogItem[]>('/backlog', config)
}
