import api from '@/api'
import { RequestCancelable, Identifiable } from '@/types'
import { makeFluentConfig, getIdentifier } from '@/util/api'

export default (backlogItem: Identifiable, options?: RequestCancelable) => {
  const id = getIdentifier(backlogItem)
  const config = makeFluentConfig(options).toConfig()

  return api.del<unknown>(`/backlog/${id}`, config)
}
