import currentSession from '@/data/auth/current-session'
import Queue from '@/util/queue'
import logger from '@/util/logger'
import Vue from 'vue'
import isObject from 'lodash/isObject'

/* eslint-disable require-jsdoc */
export default class PartupWebsocket {
  private _disposed = false
  // for now use Vue as emitter, replace with own emitter (teemitter) once published.
  private _emitter = new Vue()
  private _listeners = new Map<string, Map<number, Function>>()
  private _socket?: WebSocket
  private _queue = new Queue()
  private _url: string

  constructor(url = process.env.VUE_APP_WSS_ENDPOINT_URL) {
    this._url = url
  }

  private get isOpen() {
    return this._socket && this._socket.readyState === this._socket.OPEN
  }

  public subscribe(id: number, action: string, handler: Function) {
    if (this._disposed) {
      throw new Error(
        'PartupWebsocket, you are trying to subscribe to a disposed websocket'
      )
    }

    let listeners = this._listeners.get(action)

    if (!Array.isArray(listeners)) {
      listeners = new Map()
      this._listeners.set(action, listeners)
    }

    if (listeners.has(id)) {
      logger.debug(
        `PartupWebsocket, ${id} already has a listener for action ${action}. Both will be cleared when unsubscribe is called!`
      )
      return
    }

    listeners.set(id, handler)

    this.ensureEmitter()

    this._emitter.$on(action, handler)

    return () => this.unsubscribe(id, action)
  }

  public unsubscribe(id: number, action?: string) {
    this.ensureEmitter()

    if (action) {
      const listeners = this._listeners.get(action)

      if (!listeners.has(id)) {
        logger.debug(
          `tried to unsubscribe listener with id ${id} but couldn't find it.`
        )
      }

      const listener = listeners.get(id)
      this._emitter.$off(action, listener)
    } else {
      for (const [action, listeners] of this._listeners) {
        if (listeners.has(id)) {
          const listener = listeners.get(id)
          this._emitter.$off(action, listener)
          listeners.delete(id)
        }
      }
    }
  }

  public async write(data: Record<string, any>) {
    await this.ensureSocket()

    if (this.isOpen) {
      this._socket.send(JSON.stringify(data))
    } else {
      this._queue.enqueue(data)
    }
  }

  public async connect() {
    try {
      // throws when user does not have a session..
      await currentSession()
    } catch (error) {
      logger.error(error)
      logger.debug(
        'PartupWebsocket cannot connect, user does not have a session.'
      )
      return
    }

    try {
      const socket = new WebSocket(this._url)

      socket.addEventListener('open', this.onOpen.bind(this))
      socket.addEventListener('message', this.onMessage.bind(this))
      socket.addEventListener('close', this.onClose.bind(this))

      this._socket = socket
    } catch (error) {
      logger.debug('socket error')
      logger.error(error)
    }
  }

  public disconnect() {
    if (this._socket) {
      this._socket.close()
      this._socket.removeEventListener('open', this.onOpen)
      this._socket.removeEventListener('message', this.onMessage)
      this._socket.removeEventListener('close', this.onClose)
      this._socket = null
    }
  }

  public dispose() {
    if (this._disposed) {
      throw new Error('PartupWebsocket, this websocket is already disposed')
    }

    this.disconnect()

    for (const [action, listeners] of this._listeners) {
      for (const listener of listeners.values()) {
        this._emitter.$off(action, listener)
      }
      listeners.clear()
    }
    this._listeners.clear()

    this._listeners = undefined
    this._emitter = undefined

    this._disposed = true
  }

  private async onOpen() {
    await this.authorize()

    while (this._queue.count > 0) {
      const item = this._queue.dequeue()
      this.write(item)
    }
  }

  private onMessage(event) {
    this.ensureEmitter()
    let parsed: any

    try {
      parsed = JSON.parse(event.data)
    } catch (error) {
      logger.error('could not parsed socket event')
      return
    }

    if (Array.isArray(parsed)) {
      for (const message of parsed) {
        if (Array.isArray(message)) {
          const [action, data] = message

          this._emitter.$emit(action, data)
          logger.debug('multiplex array socket message: ', action, data)
          // if (data) {
          // } else {
          //   logger.debug('could not parse multiplex array socket message:', action, data)
          // }
        } else if (isObject(message)) {
          if ('action' in message && 'data' in message) {
            const objMessage = message as { action: string; data: object }
            this._emitter.$emit(objMessage.action, objMessage.data)
            logger.debug('multiplex object socket message: ', message)
          } else {
            logger.debug(
              'multiplex object socket message invalid format:',
              message
            )
          }
        } else {
          logger.debug('socket message invalid format:', message)
        }
      }
    } else if (isObject(parsed)) {
      if ('action' in parsed && 'data' in parsed) {
        const objMessage = parsed as { action: string; data: object }
        this._emitter.$emit(objMessage.action, objMessage.data)
        logger.debug('single object message', parsed)
      } else {
        logger.debug('single object message invalid format:', parsed)
      }
    } else {
      logger.debug('socket message invalid format:', parsed)
    }
  }

  private onClose() {
    this.reconnect()
  }

  private async authorize() {
    try {
      const session = await currentSession()
      const jwtToken = session.getIdToken().getJwtToken()

      await this.write({
        action: 'authorize',
        data: jwtToken,
      })
    } catch (error) {
      logger.debug('PartupWebsocket tried to authorize but failed', error)
    }
  }

  private reconnect() {
    this.disconnect()

    const min = 200
    const max = 700

    setTimeout(() => {
      logger.debug('Websocket reconnecting')
      this.connect()
    }, Math.floor(Math.random() * (max - min)) + min)
  }

  private async ensureSocket() {
    if (!this._socket) {
      await this.connect()
    }

    return !!this._socket
  }

  private ensureEmitter() {
    if (!this._emitter) {
      this._emitter = new Vue()
    }
  }
}
