# Icons

1. Iconset: [material](https://material.io/tools/icons)
1. theme: `filled`
1. type: `svg`
1. density: `24dp`
1. Include required icons manually

**Remove** the `height` and `width` property from the svg when including in project.

All icons in this dir are automatically included in an icon-sprite (webpack.common.js)

Reload the page to see a newly added icon appear

## Usage

You can use an icon by creating the `<pu-icon name="assetName" />` element
