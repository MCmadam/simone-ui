import Ability from './lib/authorization/ability/pkg/ability'
import Vue from 'vue'
import { Store } from 'vuex'

declare module 'vue/types/vue' {
  interface Vue {
    $ability: Ability
    $logger: any
  }
}

declare module 'vue/types/options' {
  interface ComponentOptions<V extends Vue> {
    ability?: Ability
    logger?: any
  }
}
declare module 'vuex' {
  interface Store<S> {
    hasModule: (path: string) => boolean
  }
}
