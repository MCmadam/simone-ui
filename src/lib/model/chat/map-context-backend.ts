import { BackendMessage, Message } from '@/types'
import fromBackendAttachment from '../attachment/map-context-backend'

const mapContextBackend = (backend: BackendMessage): Message => {
  /* eslint-disable camelcase */
  const {
    uuid,
    partup_uuid,
    posted_at,
    tagged_activities,
    tagged_attachments,
    text,
    user,
  } = backend
  /* eslint-enable camelcase */

  return {
    activities: tagged_activities,
    attachments: tagged_attachments.map(fromBackendAttachment),
    partupUuid: partup_uuid,
    postedAt: posted_at,
    postedBy: user,
    text,
    uuid,
  }
}

export default mapContextBackend
