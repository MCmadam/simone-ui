import { PARTUP_VISIBILITY } from '@/types'

export default (partup?: any, visibility?: PARTUP_VISIBILITY) => {
  if (!partup || !visibility) {
    return false
  }

  return partup.visibility === visibility
}
