export default (partup) => {
  if (!partup) {
    return null
  }

  return partup.slug
}
