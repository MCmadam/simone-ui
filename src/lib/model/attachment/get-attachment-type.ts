import { Attachment } from '@/types/model'

const getAttachmentType = (attachment: Attachment) => {
  return attachment.type
}

export default getAttachmentType
