import { Attachment } from '@/types'

const getAttachmentName = (attachment: Attachment) => {
  return attachment.name
}

export default getAttachmentName
