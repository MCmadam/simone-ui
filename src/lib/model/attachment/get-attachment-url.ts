import { Attachment } from '@/types'

const getAttachmentUrl = (attachment: Attachment) => {
  return attachment.url
}

export default getAttachmentUrl
