import { Attachment } from '@/types'

const isAttachmentPinned = (attachment: Attachment) => {
  return !!attachment.pinned
}

export default isAttachmentPinned
