import { Attachment } from '@/types/model'

const getAttachmentProvider = (attachment: Attachment) => {
  return attachment.provider
}

export default getAttachmentProvider
