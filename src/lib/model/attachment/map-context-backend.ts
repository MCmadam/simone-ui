import {
  BackendAttachment,
  Attachment,
  ATTACHMENT_PROVIDERS,
  ATTACHMENT_TYPES,
} from '@/types'

const tryMapProvider = (backend: string) => {
  const normalized = backend?.split('-').join('')
  const provider =
    ATTACHMENT_PROVIDERS[normalized] || ATTACHMENT_PROVIDERS.other
  return provider
}

const tryMapType = (backend: string) => {
  const type = ATTACHMENT_TYPES.file
  return type
}

const mapContextBackend = (
  backendAttachment: BackendAttachment
): Attachment => {
  /* eslint-disable camelcase */
  const {
    mime_type,
    name,
    pinned,
    provider: backendProvider,
    type: backendType,
    url,
    uuid,
  } = backendAttachment
  /* eslint-enable camelcase */

  const provider = tryMapProvider(backendProvider)
  const type = tryMapType(backendType)

  return {
    name,
    pinned,
    provider,
    type,
    url,
    uuid,
  }
}

export default mapContextBackend
