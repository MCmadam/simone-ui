/*
 is-email-invite
*/
import { SOLICIT_TYPES } from '@/types'
import isEmailInvite from './is-email-invite'

describe('solicit.is-email-invite', () => {
  let sut

  beforeEach(() => {
    sut = null
  })

  it('is true when solicit is invite and email key is present', () => {
    expect.assertions(1)

    sut = {
      type: SOLICIT_TYPES.invite,
      email: 'email',
    }

    expect(isEmailInvite(sut)).toBe(true)
  })

  it('is false when solicit is invite and email key is not present', () => {
    expect.assertions(1)

    sut = {
      type: SOLICIT_TYPES.invite,
    }

    expect(isEmailInvite(sut)).toBe(false)
  })

  it('is false when solicit is not invite and email key is present', () => {
    expect.assertions(1)

    sut = {
      type: SOLICIT_TYPES.request,
      email: 'email',
    }

    expect(isEmailInvite(sut)).toBe(false)
  })

  it('is false when solicit is not invite and email key is not present', () => {
    expect.assertions(1)

    sut = {
      type: SOLICIT_TYPES.request,
    }

    expect(isEmailInvite(sut)).toBe(false)
  })
})
