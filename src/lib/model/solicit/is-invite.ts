import { SOLICIT_TYPES } from '@/types'

export default (solicit: any) => {
  return solicit.type === SOLICIT_TYPES.invite
}
