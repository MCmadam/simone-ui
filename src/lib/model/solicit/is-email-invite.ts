import isInvite from './is-invite'

export default (solicit) => {
  return isInvite(solicit) && !!solicit.email
}
