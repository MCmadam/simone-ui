import { SOLICIT_ENTITIES, SOLICIT_TYPES } from '@/types'

export default (solicit) => {
  return (
    Object.values(SOLICIT_TYPES).includes(solicit.type) &&
    Object.values(SOLICIT_ENTITIES).includes(solicit.for)
  )
}
