/*
 * solicit is-for-entity unit tests
 *
 */

/* eslint-env jest */

import { isForActivity, isForPartup, isForOrganization } from './is-for-entity'
import { SOLICIT_ENTITIES } from '@/types'

describe('is-for-entity', () => {
  let sut

  beforeEach(() => {
    sut = null
  })

  describe('is-for-activity', () => {
    it('is true when activity is present without optional uuid for matching', () => {
      expect.assertions(1)

      sut = {
        for: SOLICIT_ENTITIES.activity,
        activity: {},
      }

      expect(isForActivity(sut)).toBe(true)
    })

    it('is true when activity matches given uuid', () => {
      expect.assertions(1)

      const uuid = '1'
      sut = {
        for: SOLICIT_ENTITIES.activity,
        activity: {
          uuid,
        },
      }

      expect(isForActivity(sut, uuid)).toBe(true)
    })

    it('is false when activity does not match given uuid', () => {
      expect.assertions(1)

      sut = {
        for: SOLICIT_ENTITIES.activity,
        activity: {
          uuid: '1',
        },
      }

      expect(isForActivity(sut, '2')).toBe(false)
    })
  })

  describe('is-for-partup', () => {
    it('is true when partup is present without optional uuid for matching', () => {
      expect.assertions(1)

      sut = {
        for: SOLICIT_ENTITIES.partup,
        partup: {},
      }

      expect(isForPartup(sut)).toBe(true)
    })

    it('is true when partup matches given uuid', () => {
      expect.assertions(1)

      const uuid = '1'
      sut = {
        for: SOLICIT_ENTITIES.partup,
        partup: {
          uuid,
        },
      }

      expect(isForPartup(sut, uuid)).toBe(true)
    })

    it('is false when partup does not matches given uuid', () => {
      expect.assertions(1)

      sut = {
        for: SOLICIT_ENTITIES.partup,
        partup: {
          uuid: '1',
        },
      }

      expect(isForPartup(sut, '2')).toBe(false)
    })
  })

  describe('is-for-organization', () => {
    it('is true when organization is present without optional uuid for matching', () => {
      expect.assertions(1)

      sut = {
        for: SOLICIT_ENTITIES.organization,
        organization: {},
      }

      expect(isForOrganization(sut)).toBe(true)
    })

    it('is true when organization matches given uuid', () => {
      expect.assertions(1)

      const uuid = '1'
      sut = {
        for: SOLICIT_ENTITIES.organization,
        organization: {
          uuid,
        },
      }

      expect(isForOrganization(sut, uuid)).toBe(true)
    })

    it('is false when organization does not match given uuid', () => {
      expect.assertions(1)

      sut = {
        for: SOLICIT_ENTITIES.organization,
        organization: {
          uuid: '1',
        },
      }

      expect(isForOrganization(sut, '2')).toBe(false)
    })
  })
})
