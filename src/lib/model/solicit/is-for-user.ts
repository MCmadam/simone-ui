export default (solicit, user) => {
  return !solicit.user || solicit.user.uuid === user.uuid
}
