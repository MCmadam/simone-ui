import { SOLICIT_ENTITIES } from '@/types'

export const isForActivity = (solicit, uuid?) => {
  return (
    solicit?.for === SOLICIT_ENTITIES.activity &&
    solicit?.activity &&
    !(uuid && solicit.activity.uuid !== uuid)
  )
}

export const isForPartup = (solicit, uuid?) => {
  return (
    solicit?.for === SOLICIT_ENTITIES.partup &&
    solicit?.partup &&
    !(uuid && solicit.partup.uuid !== uuid)
  )
}

export const isForOrganization = (solicit, uuid?) => {
  return (
    solicit?.for === SOLICIT_ENTITIES.organization &&
    solicit?.organization &&
    !(uuid && solicit.organization.uuid !== uuid)
  )
}

export default (solicit, entity, uuid?) => {
  switch (entity) {
    case SOLICIT_ENTITIES.activity:
      return isForActivity(solicit, uuid)
    case SOLICIT_ENTITIES.partup:
      return isForPartup(solicit, uuid)
    case SOLICIT_ENTITIES.organization:
      return isForOrganization(solicit, uuid)
    default:
      return false
  }
}
