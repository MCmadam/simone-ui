import isForUser from './is-for-user'

/*
 * solicit.is-for-user unit tests
 *
 */

/* eslint-env jest */
describe('solicit.is-for-user', () => {
  let sut
  let user

  beforeAll(() => {
    user = {
      uuid: '1',
    }
  })

  beforeEach(() => {
    sut = null
  })

  it('returns true if user not present on solicit', () => {
    expect.assertions(1)

    sut = {}

    expect(isForUser(sut, user)).toBe(true)
  })

  it('returns true if user uuid on solicit matches given user uuid', () => {
    expect.assertions(1)

    sut = {
      user: {
        uuid: '1',
      },
    }

    expect(isForUser(sut, user)).toBe(true)
  })

  it('returns false if user uuid on solicit does not match given user uuid', () => {
    expect.assertions(1)

    sut = {
      user: {
        uuid: '2',
      },
    }

    expect(isForUser(sut, user)).toBe(false)
  })
})
