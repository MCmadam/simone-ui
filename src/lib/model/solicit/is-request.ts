import { SOLICIT_TYPES } from '@/types'

export default (solicit) => {
  return solicit.type === SOLICIT_TYPES.request
}
