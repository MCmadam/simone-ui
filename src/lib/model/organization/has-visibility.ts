import { ORGANIZATION_VISIBILITY } from '@/types'

export default (organization?: any, visibility?: ORGANIZATION_VISIBILITY) => {
  if (!organization || !visibility) {
    return false
  }

  return organization.visibility === visibility
}
