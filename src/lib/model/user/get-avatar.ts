export default (user, size = 'small') => {
  if (!user || !user.images) {
    return null
  }

  return user.images[`avatar_${size}`]
}
