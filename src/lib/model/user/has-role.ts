export default (user?: any, role?: string) => {
  if (!user || !role) {
    return false
  }

  return user.role === role
}
