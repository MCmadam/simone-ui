export default (user) => {
  if (!user) {
    return null
  }

  return user.slug
}
