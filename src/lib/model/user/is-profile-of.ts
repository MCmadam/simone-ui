export default (profile?: any, currentUser?: any) => {
  if (!profile || !currentUser) {
    return false
  }

  return profile.uuid === currentUser.uuid
}
