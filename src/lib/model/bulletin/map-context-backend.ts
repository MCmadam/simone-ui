import { BackendBulletin, Bulletin } from '@/types'

export default (backendBulletin: BackendBulletin): Bulletin => {
  const { uuid, bulletin_type: bulletinType, params } = backendBulletin

  const firstUnderscoreIndex = bulletinType.indexOf('_')
  const scope = bulletinType.substr(0, firstUnderscoreIndex)
  const type = bulletinType
    .substr(firstUnderscoreIndex + 1, bulletinType.length - 1)
    .split('_')
    .join('-')

  return {
    uuid,
    scope,
    type,
    params,
  }
}
