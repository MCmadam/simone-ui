const getEndDate = (activity) => {
  if (!activity) {
    return null
  }

  return activity.end_date
}

export default getEndDate
