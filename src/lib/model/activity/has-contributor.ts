export default (activity, user) => {
  if (!activity || !user) {
    return false
  }

  return Boolean(activity.contributors.find(({ uuid }) => uuid === user.uuid))
}
