import { ACTIVITY_STATUS } from '@/types'
import { nullToNone } from '@/util/activity'

export default (activity, status: ACTIVITY_STATUS) => {
  return nullToNone(activity.status) === status
}
