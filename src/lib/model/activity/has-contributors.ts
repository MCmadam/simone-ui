export default (activity: any) => {
  if (!activity || !Array.isArray(activity.contributors)) {
    return false
  }

  return activity.contributors.length > 0
}
