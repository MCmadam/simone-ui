import { toBool } from '@/util/string'

const FEATURES = Object.freeze({
  domainBranding: toBool(process.env.VUE_APP_FEATURE_DOMAIN_BRANDING),
})

export const isFeatureEnabled = (feature) => !!FEATURES[feature]

export default FEATURES
