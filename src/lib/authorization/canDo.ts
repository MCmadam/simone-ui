import get from 'lodash/get'

export default (permission: string, entity: Record<string, any>) => {
  const permissions = get(entity, '_meta.permissions')

  if (permissions) {
    return permissions.includes(permission)
  }
  return false
}
