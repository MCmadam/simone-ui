import AbilityBuilder from './pkg/builder'

const { rules, can } = AbilityBuilder.extract()

export default rules
