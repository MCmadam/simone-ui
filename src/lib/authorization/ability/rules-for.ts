import AbilityBuilder from './pkg/builder'
import get from 'lodash/get'
import rootRules from './root-rules'
import Ability from './pkg/ability'

const findPermissions = (obj) => {
  return get(obj, '_meta.permissions')
}

Ability.addAlias('solicit_invite_activity_user', ['#solicit_invite_activity'])
Ability.addAlias('solicit_invite_activity_email', ['#solicit_invite_activity'])

Ability.addAlias('solicit_invite_organization_user', [
  '#solicit_invite_organization',
])
Ability.addAlias('solicit_invite_organization_email', [
  '#solicit_invite_organization',
])

Ability.addAlias('solicit_invite_partup_user', ['#solicit_invite_partup'])
Ability.addAlias('solicit_invite_partup_email', ['#solicit_invite_partup'])

Ability.addAlias('solicit_invite_activity_respond', [
  'solicit_invite_activity_accept',
  'solicit_invite_activity_reject',
])
Ability.addAlias('solicit_request_activity_respond', [
  'solicit_request_activity_accept',
  'solicit_request_activity_reject',
])

//#region partup
Ability.addAlias('solicit_invite_partup_respond', [
  'solicit_invite_partup_accept',
  'solicit_invite_partup_reject',
])
Ability.addAlias('solicit_request_partup_respond', [
  'solicit_request_partup_accept',
  'solicit_request_partup_reject',
])
//#endregion

//#region organization
Ability.addAlias('solicit_invite_organization_respond', [
  'solicit_invite_organization_accept',
  'solicit_invite_organization_reject',
])
Ability.addAlias('solicit_request_organization_respond', [
  'solicit_request_organization_accept',
  'solicit_request_organization_reject',
])
//#endregion

export default (data) => {
  const { rules, can } = AbilityBuilder.extract()
  const permissions = findPermissions(data)

  if (permissions) {
    for (const permission of permissions) {
      can(permission)
    }
  }

  const rulesFor = rootRules.concat(rules)
  return rulesFor
}
