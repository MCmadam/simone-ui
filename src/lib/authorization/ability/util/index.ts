import Vue from 'vue'

export const WATCHER_KEY = Symbol('ability.watcher')

export const abilityObserver = () =>
  Vue.observable({
    rules: [],
  })

export const watchAbility = (ability) => {
  const watcher = abilityObserver()

  ability.on('updated', ({ rules }) => {
    watcher.rules = rules
  })

  ability[WATCHER_KEY] = watcher

  return watcher
}
