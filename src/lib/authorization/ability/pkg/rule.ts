type RuleOptions = {
  actions: string[]
}

export default class Rule {
  public actions: string[]

  constructor(options: RuleOptions) {
    if (!options.actions) {
      throw new Error('Rule must have actions')
    }

    const actions = Array.isArray(options.actions)
      ? options.actions
      : [options.actions]

    if (actions.length < 1) {
      throw new Error('Rule cannot have empty actions')
    }

    this.actions = actions
  }

  public matches() {
    return true
  }
}
