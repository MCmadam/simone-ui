import { rawRule } from './types'

export default class AbilityBuilder {
  public rules: rawRule[]

  constructor() {
    this.rules = []
  }

  can(actions: string | string[]) {
    const rawRule: rawRule = {
      actions,
    }
    this.rules.push(rawRule)
  }

  static extract() {
    const builder = new this()

    return {
      can: builder.can.bind(builder),
      rules: builder.rules,
    }
  }
}
