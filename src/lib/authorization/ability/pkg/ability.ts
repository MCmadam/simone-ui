import Rule from './rule'
import { rawRule } from './types'
import flatten from 'lodash/flatten'

type indexedRules = {
  [action: string]: Rule
}

const ALIASES = {}

export default class Ability {
  private _listeners = {}
  private _rawRules: rawRule[]
  private _indexedRules: indexedRules
  private aliases

  constructor(rules: rawRule[] = []) {
    this.aliases = Object.assign({}, ALIASES)
    this.update(rules)
  }

  static addAlias(alias: string, actions: string[]) {
    let copy = actions
    if (ALIASES[alias]) {
      copy = actions.slice()
      copy = copy.concat(ALIASES[alias])
    }
    ALIASES[alias] = copy
    return this
  }

  public update(rules: rawRule[]) {
    if (!Array.isArray(rules)) {
      throw new Error('ability.update rules must be an array')
    }

    this._rawRules = rules
    this._indexedRules = this.buildIndex(rules)
    this.emit('updated', {
      rules,
      ability: this,
    })
  }

  public get rules() {
    return this._rawRules
  }

  private buildIndex(rules) {
    const indexed: indexedRules = {}

    for (const r of rules) {
      const rule = new Rule(r)
      const actions = this.expandActions(rule.actions)

      for (const action of actions) {
        indexed[action] = rule
      }
    }

    return indexed
  }

  public can(action) {
    const rule = this._indexedRules[action]
    return Boolean(rule && rule.matches())
  }

  public on(event, listener) {
    if (!this._listeners[event]) {
      this._listeners[event] = []
    }

    this._listeners[event].push(listener)

    return () => {
      const index = this._listeners[event].indexOf(listener)
      if (index > -1) {
        this._listeners[event].splice(index, 1)
      }
    }
  }

  private emit(event, payload) {
    const listeners = this._listeners[event]

    if (Array.isArray(listeners)) {
      for (const listener of listeners) {
        listener(payload)
      }
    }
  }

  private expandActions(actions: string[]) {
    for (const action of actions) {
      if (ALIASES[action]) {
        actions = actions.concat(ALIASES[action])
      }
    }

    return flatten(actions)
  }
}
