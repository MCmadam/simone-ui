import AbilityBuilder from './builder'

describe('ability builder', () => {
  let sut

  beforeEach(() => {
    sut = null
  })

  it('can build a rule via can method and adds it to the rules array', () => {
    expect.assertions(1)

    sut = new AbilityBuilder()
    sut.can('test')

    expect(sut.rules).toContainEqual({ actions: 'test' })
  })

  it('can build rules via the static extract method', () => {
    expect.assertions(1)

    const { rules, can } = AbilityBuilder.extract()

    can('test')

    expect(rules).toContainEqual({ actions: 'test' })
  })
})
