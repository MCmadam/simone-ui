import Ability from './ability'

describe('ability class', () => {
  let sut
  const rawRuleMock = {
    actions: 'test',
  }

  beforeEach(() => {
    sut = null
  })

  it('can be constructed with rawRules', () => {
    expect.assertions(1)

    sut = new Ability([rawRuleMock])
    expect(sut.rules[0]).toMatchObject(rawRuleMock)
  })

  it('can be updated with rawRules', () => {
    expect.assertions(1)

    sut = new Ability()
    sut.update([rawRuleMock])

    expect(sut.rules[0]).toMatchObject(rawRuleMock)
  })

  it('throws on update if rules is not an array', () => {
    expect.assertions(1)

    sut = new Ability()

    expect(() => {
      sut.update('a')
    }).toThrow()
  })

  it("emit's an updated event after update", () => {
    expect.assertions(1)
    const listenerMock = jest.fn()
    sut = new Ability()
    sut.on('updated', listenerMock)

    sut.update([rawRuleMock])
    expect(listenerMock).toBeCalledTimes(1)
  })

  it('can be unsubscribed to', () => {
    expect.assertions(1)

    const listenerMock = jest.fn()

    sut = new Ability()
    const unsubscribe = sut.on('updated', listenerMock)

    sut.update([rawRuleMock])

    unsubscribe()

    sut.update([])

    expect(listenerMock).toBeCalledTimes(1)
  })

  it("'can' returns true if action is indexed", () => {
    expect.assertions(2)

    sut = new Ability([rawRuleMock])

    expect(sut.can('test')).toBe(true)
    expect(sut.can('other')).toBe(false)
  })
})
