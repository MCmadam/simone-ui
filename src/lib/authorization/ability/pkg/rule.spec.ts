import Rule from './rule'

describe('ability rule', () => {
  let sut

  beforeEach(() => {
    sut = null
  })

  it('holds actions in an array', () => {
    expect.assertions(1)

    const actions = ['a']

    sut = new Rule({
      actions,
    })

    expect(sut.actions).toContain('a')
  })

  it('cannot have empty actions', () => {
    expect.assertions(1)

    expect(() => {
      const rule = new Rule({
        actions: [],
      })
    }).toThrow()
  })

  it('always matches', () => {
    expect.assertions(1)

    sut = new Rule({
      actions: ['a'],
    })

    expect(sut.matches()).toBe(true)
  })
})
