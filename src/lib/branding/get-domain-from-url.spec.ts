import getDomainFromUrl from './get-domain-from-url'

const setHost = (str) => {
  window.location.host = str
}

describe('get branding domain from url', () => {
  const { location } = window
  const defaultDomain = 'app'

  beforeAll(() => {
    delete window.location

    // @ts-ignore
    window.location = {
      host: '',
    }
  })

  it('returns default for localhost', () => {
    expect.assertions(1)

    setHost('localhost:9000')

    expect(getDomainFromUrl()).toBe(defaultDomain)
  })

  it('strips www', () => {
    expect.assertions(1)

    setHost('www.something.part-up.com')

    expect(getDomainFromUrl()).toBe('something')
  })

  it('defaults to app', () => {
    expect.assertions(1)

    setHost('part-up.com')

    expect(getDomainFromUrl()).toBe(defaultDomain)
  })
})
