import parseDomainFromLocation from '@/util/url/parse-domain-from-location'

export default (domain: string) => {
  const url = `https://${domain}.${parseDomainFromLocation()}`
  window.location.assign(url)
}
