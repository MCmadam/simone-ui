const defaultDomain = 'app'

export default () => {
  const parts = window.location.host.split('.')

  if (parts[0] === 'www') {
    parts.shift()
  }

  if (parts.length === 1 || parts[0] === 'part-up') {
    return defaultDomain
  } else {
    return parts[0]
  }
}
