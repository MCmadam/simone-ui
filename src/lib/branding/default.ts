import globals from '@/lib/globals'

export const defaultBrandingDomain = 'app'

export default Object.freeze({
  images: {
    background: globals.images.getPublicImageUrl('pu-home-cover.jpg'),
    logo: '',
  },
  payoff: 'Wij maken werk klein en talent groot',
  name: 'bij Part-up',
})
