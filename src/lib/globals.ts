export const getGtagId = () => {
  const tagId = process.env.VUE_APP_GTAG_ID

  if (tagId === '') {
    return null
  }

  return tagId
}

export default Object.freeze({
  BUILDHASH: process.env.VUE_APP_BUILDHASH,
  TERMS_OF_USE_URL: process.env.VUE_APP_TERMS_OF_USE_URL,
  PRIVACY_STATEMENT_URL: process.env.VUE_APP_PRIVACY_STATEMENT_URL,
  images: {
    getPublicImageUrl(image) {
      return `/${process.env.VUE_APP_BUILDHASH}/images/${image}`
    },
  },
  modals: {
    invite: 'invite',
    userProfile: 'user-profile',
    solicitResolve: 'solicit-resolve',
    partupAttachment: 'partup-attachment',
  },
})
