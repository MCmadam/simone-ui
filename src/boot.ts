import loadLocale from '@/util/lang/load-locale'
import logger from '@/util/logger'
import store from '@/store'
import router from '@/router'
import { sync } from 'vuex-router-sync'
import FEATURES from './lib/features'
import currentAuthenticatedUser from './data/auth/current-authenticated-user'

const initializeEnvironment = async () => {
  try {
    if (FEATURES.domainBranding) {
      await store.dispatch('getBranding')
    }

    await store.dispatch('getEnvironment')
    await store.dispatch('setDefaultOrganizationContext')
  } catch (error) {
    throw error
  }
}

export default async (callback) => {
  try {
    await Promise.all([
      loadLocale((store.state as any).preferences.locale),
      store.dispatch('auth/syncLogin'),
    ])

    try {
      if (await currentAuthenticatedUser()) {
        const user = store.getters['currentUser']

        if (user) {
          initializeEnvironment()
        } else {
          await initializeEnvironment()
        }
      }
    } catch (error) {
      if (FEATURES.domainBranding) {
        store.dispatch('getBranding')
      }
    } finally {
      if (!FEATURES.domainBranding) {
        store.dispatch('getBranding')
      }
    }

    sync(store, router)
  } catch (error) {
    logger.error('boot error!', error)
  } finally {
    callback()
  }
}
