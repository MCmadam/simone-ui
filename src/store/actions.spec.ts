import { getBranding, setDefaultOrganizationContext } from './actions'
import axios from 'axios'
import AxiosMockAdapter from 'axios-mock-adapter'
import defaultBranding, { defaultBrandingDomain } from '@/lib/branding/default'
import FEATURES from '@/lib/features'
import globals from '@/lib/globals'
import api from '@/api'
import switchBrandingLocation from '@/lib/branding/switch-branding-location'
import parseBrandingFromLocation from '@/util/url/parse-branding-from-location'

jest.dontMock('@/api')
jest.mock('@/util/url/parse-branding-from-location', () => jest.fn())
jest.mock('@/lib/branding/switch-branding-location', () => jest.fn())
jest.mock('@/lib/features', () => ({}))
jest.mock('@/lib/globals', () => ({
  images: {
    getPublicImageUrl: jest.fn(),
  },
}))

api.configure({
  endpoint: 'sample',
  region: 'sample',
  service: 'sample',
})

describe('getBranding', () => {
  const commit = jest.fn()
  const featuresMock = FEATURES as jest.Mocked<any>
  const globalsMock = globals as jest.Mocked<any>
  const axiosMock = new AxiosMockAdapter(axios)
  const parseBrandingMock = parseBrandingFromLocation as jest.Mocked<any>

  beforeEach(() => {
    commit.mockClear()
  })

  describe("feature 'domainBranding' enabled", () => {
    beforeAll(() => {
      featuresMock.domainBranding = true
    })

    describe('branding domain is default', () => {
      beforeAll(() => {
        parseBrandingMock.mockImplementation(() => 'app')
      })

      it('uses default branding and context', async (done) => {
        expect.assertions(2)

        await getBranding({ commit })

        expect(commit.mock.calls[0]).toMatchObject([
          'branding',
          defaultBranding,
        ])
        expect(commit.mock.calls[1]).toMatchObject([
          'brandingContext',
          defaultBrandingDomain,
        ])

        done()
      })
    })

    describe('branding domain is not default', () => {
      beforeAll(() => {
        parseBrandingMock.mockImplementation(() => 'branded')
      })

      it('will call the back-end', async (done) => {
        expect.assertions(2)

        axiosMock.onGet(/\/branding/).replyOnce(200, { data: 'test' })

        await getBranding({ commit })

        expect(commit.mock.calls[0]).toMatchObject(['branding', 'test'])
        expect(commit.mock.calls[1]).toMatchObject([
          'brandingContext',
          'branded',
        ])

        done()
      })

      it('will load defaults if back-end call does not respond with 200', async (done) => {
        expect.assertions(2)

        axiosMock.onGet(/\/branding/).networkErrorOnce()

        await getBranding({ commit })

        expect(commit.mock.calls[0]).toMatchObject([
          'branding',
          defaultBranding,
        ])
        expect(commit.mock.calls[1]).toMatchObject([
          'brandingContext',
          defaultBrandingDomain,
        ])

        done()
      })
    })
  })

  describe("feature 'domainBranding' disabled", () => {
    beforeAll(() => {
      featuresMock.domainBranding = false
    })

    it('uses slug for domain', async (done) => {
      expect.assertions(2)

      const slug = 'slug'

      axiosMock.onGet(/\/branding/).replyOnce(200, { data: slug })

      await getBranding({ commit }, slug)

      expect(commit.mock.calls[0]).toMatchObject(['branding', slug])
      expect(commit.mock.calls[1]).toMatchObject(['brandingContext', slug])

      done()
    })

    it('will load defaults if back-end call does not respond with 200', async (done) => {
      expect.assertions(2)

      const slug = 'slug'

      axiosMock.onGet(/\/branding/).networkErrorOnce()

      await getBranding({ commit }, slug)

      expect(commit.mock.calls[0]).toMatchObject(['branding', defaultBranding])
      expect(commit.mock.calls[1]).toMatchObject([
        'brandingContext',
        defaultBrandingDomain,
      ])

      done()
    })
  })
})

describe('setDefaultOrganizationContext', () => {
  const dispatch = jest.fn()
  const featuresMock = FEATURES as jest.Mocked<any>
  let storeContext: any

  beforeEach(() => {
    dispatch.mockReset()

    storeContext = {
      dispatch,
      state: {
        brandingContext: null,
      },
      rootGetters: {
        'user/userOrganizations': null,
      },
    }
  })

  describe("feature 'domainBranding' enabled", () => {
    beforeAll(() => {
      featuresMock.domainBranding = true
    })

    describe('default branding context', () => {
      beforeEach(() => {
        // @ts-ignore
        switchBrandingLocation.mockClear()

        storeContext.state.brandingContext = defaultBrandingDomain
      })

      it('will redirect to given organization', () => {
        expect.assertions(1)

        const organization = {
          slug: 'branded',
        }

        setDefaultOrganizationContext(storeContext, organization)

        expect(switchBrandingLocation).toHaveBeenCalledWith(
          expect.stringMatching('branded')
        )
      })

      it('will redirect if user is member of only one organization', () => {
        expect.assertions(1)

        const organization = {
          slug: 'branded',
        }

        storeContext.rootGetters['user/userOrganizations'] = [organization]

        setDefaultOrganizationContext(storeContext)

        expect(switchBrandingLocation).toHaveBeenCalledWith(organization.slug)
      })
    })

    describe('branded context', () => {
      beforeEach(() => {
        storeContext.state.brandingContext = 'branded'
      })

      it("set's the given organization as context if it matches the branded context", () => {
        expect.assertions(1)

        const organization = {
          slug: 'branded',
        }

        setDefaultOrganizationContext(storeContext, organization)

        expect(dispatch).toHaveBeenCalledWith(
          'setCurrentOrganization',
          organization
        )
      })

      it('matches an organization of the user and sets it if it matches', () => {
        expect.assertions(1)

        const organization = {
          slug: 'branded',
        }

        storeContext.rootGetters['user/userOrganizations'] = [
          organization,
          { slug: 'unbranded' },
          { slug: 'something-else' },
        ]

        setDefaultOrganizationContext(storeContext)

        expect(dispatch).toHaveBeenCalledWith(
          'setCurrentOrganization',
          organization
        )
      })
    })
  })

  describe("feature 'domainBranding' disabled", () => {
    beforeAll(() => {
      featuresMock.domainBranding = false
    })

    it("doesn't set an organization if already present", () => {
      expect.assertions(1)

      storeContext.rootGetters['currentOrganization'] = {}

      setDefaultOrganizationContext(storeContext)

      expect(dispatch).not.toHaveBeenCalled()
    })

    it('sets given organization if no organization context is present', () => {
      expect.assertions(1)

      storeContext.rootGetters['currentOrganization'] = null

      const organization = { slug: 'unbranded' }

      setDefaultOrganizationContext(storeContext, organization)

      expect(dispatch).toHaveBeenCalledWith(
        'setCurrentOrganization',
        organization
      )
    })

    it('uses the first organization from user if none given and none present', () => {
      expect.assertions(1)

      const organization = { slug: 'unbranded' }

      storeContext.rootGetters['currentOrganization'] = null
      storeContext.rootGetters['user/userOrganizations'] = [
        organization,
        {},
        {},
      ]

      setDefaultOrganizationContext(storeContext)

      expect(dispatch).toHaveBeenCalledWith(
        'setCurrentOrganization',
        organization
      )
    })
  })
})
