import toArray from '@/util/array/to-array'

export const partup = (state, partup) => {
  state.partup = partup
}

export const partners = (state, partners) => {
  state.partners = partners
}

export const solicit = (state, solicit) => {
  state.solicit = solicit
}

export const aside = (state, data) => {
  for (const [k, v] of Object.entries(data)) {
    state.aside[k] = v
  }
}
