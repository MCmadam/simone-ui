import get from 'lodash/get'

export const partup = (state) => {
  return state.partup
}

export const partupUuid = (state) => {
  return get(state.partup, 'uuid', null)
}

export const partners = (state) => {
  return state.partners || []
}

export const solicit = (state) => {
  return state.solicit
}

export const isAnyAsideModuleActive = (state) => {
  return !!state.aside.activeModule
}

export const isAsideModuleActive = (state) => {
  return (name) => {
    return name === state.aside.activeModule
  }
}
