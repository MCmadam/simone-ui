import Dispatcher from '@/api/dispatcher'
import create from '@/data/partup/create'
import partupDelete from '@/data/partup/delete'
import findOne from '@/data/partup/find-one'
import update from '@/data/partup/update'
import commitForAction from '@/util/store/commit-for-action'
import findForPartup from '@/data/user/find-for-partup'
import canDo from '@/lib/authorization/canDo'
import { convertPartupRequestOption } from '@/util'

// #region init and cleanup
export const initialize = async ({ commit, dispatch }, { slug, solicit }) => {
  commit('solicit', solicit)
  await dispatch('getPartup', { slug })
}

export const reset = async ({ commit, dispatch }) => {
  commit('partup', null)
  commit('partners', null)
  commit('solicit', null)
  commit('resetActions')
  dispatch('activities/clearActivities')
}
// #endregion

// #region aside
export const activateAsideModule = ({ commit, getters }, name) => {
  const isActive = getters.isAsideModuleActive(name)

  if (isActive) {
    return
  }

  commit('aside', {
    activeModule: name,
  })
}

export const deactivateAsideModule = ({ commit }) => {
  commit('aside', {
    activeModule: null,
  })
}
// #endregion

export const createPartup = async ({ commit, dispatch, getters }, fields) => {
  const action = 'create'

  if (getters.actionPending(action)) {
    return
  }

  const c = commitForAction(action, commit)

  c.pending()

  try {
    const { data: partup } = await create(fields, {
      data: fields,
    })

    c.success()
    commit('partup', partup)
    dispatch('user/getUserPartups', null, { root: true })
  } catch (error) {
    c.error(error)
  }
}

export const deletePartup = async ({ commit, getters }) => {
  const action = 'delete'

  if (getters.actionPending(action)) {
    return
  }

  const c = commitForAction(action, commit)

  c.pending()
  try {
    await partupDelete(getters.partup)
    c.success()
  } catch (error) {
    c.error(error)
  }
}

const getPartupDispatcher = new Dispatcher(findOne)

export const getPartup = async (
  { commit, dispatch, state },
  { slug, uuid }
) => {
  const action = 'get'
  const c = commitForAction(action, commit)

  c.pending()
  try {
    const solicit = state.solicit

    const { data: partup } = await getPartupDispatcher.cancel().exec(
      {
        slug,
        uuid,
      },
      {
        query: {
          solicit,
        },
      }
    )

    c.success()
    commit('partup', partup)

    if (canDo('partup_partner_list', partup)) {
      dispatch('getPartners')
    }
  } catch (error) {
    c.error(error)
  }
}

export const getPartners = async ({ commit, getters, state }) => {
  const action = 'findPartners'
  const c = commitForAction(action, commit)

  const partup = getters['partup']

  if (!partup) {
    return
  }

  c.pending()
  try {
    const solicit = state.solicit

    const { data: partners } = await findForPartup(partup, {
      query: {
        solicit,
      },
    })

    c.success()
    commit('partners', partners)
  } catch (error) {
    c.error(error)
  }
}

export const clearPartup = ({ commit }) => {
  commit('partup', null)
}

const updateFieldDispatcher = new Dispatcher(update)

export const updateField = async (
  { commit, getters },
  { field, value, done }
) => {
  const action = 'update'
  const c = commitForAction(action, commit)

  c.pending()

  try {
    const { data: partup } = await updateFieldDispatcher
      .cancel()
      .exec(getters.partup, {
        data: {
          [field]: value,
        },
      })

    c.success()
    commit('partup', partup)
  } catch (error) {
    c.error(error)
  } finally {
    done && done()
  }
}
