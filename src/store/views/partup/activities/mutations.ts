import Vue from 'vue'
import hasOwn from '@/util/object/has-own'
import toHashMap from '@/util/array/to-hash-map'

const partiallyUpdateActivity = (existing, partial) => {
  for (const key in partial) {
    if (hasOwn(partial, key)) {
      if (key === 'lane_order') {
        continue
      }

      if (existing[key] !== partial[key]) {
        Vue.set(existing, key, partial[key])
      }
    }
  }
}

export const activity = (state: any, activity: any) => {
  const activities = state.activities
  const existing = activities[activity.uuid]

  if (!existing) {
    Vue.set(activities, activity.uuid, activity)
  } else {
    partiallyUpdateActivity(existing, activity)
  }
}

export const activities = (state: any, activities: any[]) => {
  state.activities = toHashMap(activities, 'uuid')
}

export const selectActivity = (state: any, uuid: string | null) => {
  state.selectedUuid = uuid
}

export const removeActivity = (state: any, activity: any) => {
  const { uuid } = activity

  if (state.activities[uuid]) {
    Vue.delete(state.activities, uuid)
  }

  if (state.selectedUuid === uuid) {
    selectActivity(state, null)
  }
}

export const filtered = (state: any, filtered: boolean) => {
  state.isFiltered = filtered
}

export const lanes = (state, lanes) => {
  state.lanes = lanes
}

export const updateExisting = (state, activity) => {
  const existing = state.activities[activity.uuid]

  if (existing) {
    partiallyUpdateActivity(existing, activity)
  }
}

let updateTimeout
export const refreshBoardUpdateTimeout = (state) => {
  state.boardUpdateTimeout = true

  clearTimeout(updateTimeout)
  updateTimeout = setTimeout(() => {
    state.boardUpdateTimeout = false
  }, 6000)
}
