import commitForAction from '@/util/store/commit-for-action'
import contribute from '@/data/activity/contribute'
import contributeStop from '@/data/activity/contribute-stop'
import activityDelete from '@/data/activity/delete'
import findOne from '@/data/activity/find-one'
import findForPartup from '@/data/activity/find-for-partup'
import update from '@/data/activity/update'
import createForPartup from '@/data/activity/create-for-partup'
import reduce from 'lodash/reduce'
import sortBy from 'lodash/sortBy'
import each from 'lodash/each'
import { nullToNone, noneToNull } from '@/util/activity'
import { ensurePath } from '@/util'
import { ACTIVITY_STATUS } from '@/types'
import logger from '@/util/logger'
import Dispatcher from '@/api/dispatcher'

export const clearActivities = ({ commit }) => {
  commit('activities', [])
}

export const contributeActivity = async (
  { commit, getters, rootGetters },
  activity
) => {
  const action = 'contribute'
  const c = commitForAction(action, commit)

  if (getters.actionPending(action)) {
    return
  }

  c.pending()
  try {
    const { data } = await contribute(activity, {
      data: {
        userUuids: [rootGetters['currentUser'].uuid],
      },
    })

    c.success()
    commit('activity', data)
  } catch (error) {
    c.error(error)
  }
}

export const createActivity = async (
  { commit, getters, rootGetters },
  fields
) => {
  const action = 'create'
  const c = commitForAction(action, commit)

  const partup = rootGetters['views/partup/partup']

  if (!partup || getters.actionPending(action)) {
    return
  }

  c.pending()
  try {
    const { data } = await createForPartup(partup, {
      data: fields,
    })
    c.success()

    commit('activity', data)
  } catch (error) {
    c.error(error)
  }
}

export const deleteActivity = async ({ commit }, activity) => {
  const action = 'delete'
  const c = commitForAction(action, commit)

  c.pending()
  try {
    await activityDelete(activity)
    c.success()

    commit('removeActivity', activity)
  } catch (error) {
    c.error(error)
  }
}

export const getActivities = async (
  { commit, rootGetters, rootState },
  { query = null } = {}
) => {
  const action = 'find'
  const c = commitForAction(action, commit)

  c.pending()
  try {
    const solicit = rootState.views.partup.solicit

    const activitiesQuery: any = {
      solicit,
    }

    if (query) {
      Object.assign(activitiesQuery, query)

      if (activitiesQuery.status) {
        activitiesQuery.status = noneToNull(activitiesQuery.status)
      }
    }

    const { data } = await findForPartup(rootGetters['views/partup/partup'], {
      query: activitiesQuery,
    })
    c.success()

    commit('filtered', !!query)
    commit('activities', data)
  } catch (error) {
    c.error(error)
  }
}

export const getActivity = async ({ commit }, activity) => {
  const action = 'findOne'
  const c = commitForAction(action, commit)

  c.pending()
  try {
    const { data } = await findOne(activity)

    c.success()
    commit('activity', data)
  } catch (error) {
    c.error(error)
  }
}

export const selectActivity = async ({ commit }, activity) => {
  let uuid: string

  if (!activity) {
    commit('selectActivity', null)
    return
  } else if (typeof activity === 'string') {
    uuid = activity
  } else if ('uuid' in activity) {
    uuid = activity.uuid
  }

  if (uuid) {
    commit('selectActivity', uuid)
  }
}

export const stopContributeActivity = async (
  { dispatch, commit, getters },
  activity
) => {
  const action = 'stopContribute'
  const c = commitForAction(action, commit)

  if (getters.actionPending(action)) {
    return
  }

  c.pending()
  try {
    await contributeStop(activity)
    c.success()

    dispatch('getActivity', activity)
  } catch (error) {
    c.error(error)
  }
}

const updateActivityDispatcher = new Dispatcher(update)
export const updateActivity = async (
  { commit, state },
  { done, fields, uuid }
) => {
  const action = 'update'
  const c = commitForAction(action, commit)

  const activity = state.activities[uuid]

  if (!activity) {
    return
  }

  c.pending()
  commit('refreshBoardUpdateTimeout')

  if ('status' in fields) {
    fields.status = noneToNull(fields.status)
  }

  try {
    const { data } = await updateActivityDispatcher.cancel().exec(activity, {
      data: fields,
    })

    c.success()
    commit('activity', data)
  } catch (error) {
    c.error(error)
  } finally {
    done && done()
  }
}

export const initializeBoard = async ({ commit, dispatch, state }) => {
  await dispatch('getActivities')

  const activities = state.activities || []

  // 1. create empty lanes object
  const lanes = reduce(
    ACTIVITY_STATUS,
    (total, status, key) => {
      total[key] = []
      return total
    },
    {}
  )

  // 2. add activities to lanes
  each(activities, (activity) => {
    const status = nullToNone(activity.status)
    const lane = lanes[status]

    if (!lane) {
      logger.debug('no lane for status: ' + status.toString())
      return
    }

    lane.push(activity)
  })

  // 3. sort lanes
  each(lanes, (lane, status) => {
    lanes[status] = sortBy(lane, ['lane_order'])
  })

  commit('lanes', lanes)
}

export const onActivityChanged = ({ commit, rootGetters }, { activity }) => {
  const partup = rootGetters['views/partup/partup']

  if (!partup) {
    return
  }

  commit('updateExisting', activity)
}
