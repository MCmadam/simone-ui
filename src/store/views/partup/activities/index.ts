import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import state from './state'
import withActions from '@/util/store/with-actions'

export default withActions(
  [
    'contribute',
    'create',
    'delete',
    'find',
    'findOne',
    'stopContribute',
    'update',
  ],
  {
    namespaced: true,

    actions,
    getters,
    mutations,
    state,
  }
)
