export default {
  activities: {
    /*
      [uuid]: Activity,
    */
  },
  isFiltered: false,
  selectedUuid: null,
  /*
    lanes for board view
  */
  lanes: {},
  /*
    this flag is used to indicate the user updated an activity itself
    and is used for websocket events.
  */
  boardUpdateTimeout: false,
}
