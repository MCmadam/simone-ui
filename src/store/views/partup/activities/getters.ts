export const activities = (state) => {
  return Object.values(state.activities)
}

export const isFiltered = (state) => {
  return state.isFiltered
}

export const isSelected = (state) => {
  return (activity) => {
    if (!activity) {
      return false
    }

    return activity.uuid === state.selectedUuid
  }
}

export const selectedActivity = (state) => {
  return state.activities[state.selectedUuid]
}

export const selectedActivityUuid = (state) => {
  return state.selectedUuid
}

export const lanes = (state) => {
  return state.lanes
}

export const boardUpdateTimeout = (state) => {
  return state.boardUpdateTimeout
}
