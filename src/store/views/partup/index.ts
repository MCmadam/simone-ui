import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import state from './state'
import activities from './activities'
import withActions from '@/util/store/with-actions'

export default withActions(['create', 'delete', 'get', 'update'], {
  namespaced: true,

  actions,
  getters,
  modules: {
    activities,
  },
  mutations,
  state,
})
