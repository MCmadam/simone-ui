import findOne from '@/data/organization/find-one'
import findMembers from '@/data/user/find-for-organization'
import findPartups from '@/data/partup/find-for-organization'
import update from '@/data/organization/update'
import canDo from '@/lib/authorization/canDo'

export const initialize = async ({ dispatch, state }, { slug, solicit }) => {
  await dispatch('getOrganization', { slug, solicit })

  if (canDo('organization_member_list', state.organization)) {
    dispatch('getMembers')
  }

  if (canDo('organization_partup_list', state.organization)) {
    dispatch('getPartups')
  }
}

export const reset = async ({ commit }) => {
  commit('organization', null)
  commit('members', null)
  commit('partups', null)
}

export const getOrganization = async ({ commit }, { slug, solicit }) => {
  let query

  if (solicit) {
    query = {
      solicit,
    }
  }

  try {
    const { data: organization } = await findOne({ slug }, { query })
    commit('organization', organization)
  } catch (error) {
    // ...
  }
}

export const getMembers = async ({ commit, getters }) => {
  const organization = getters.organization

  if (!organization) {
    return
  }

  try {
    const { data: members } = await findMembers(organization)
    commit('members', members)
  } catch (error) {
    // ...
  }
}

export const getPartups = async ({ commit, getters }) => {
  const organization = getters.organization

  if (!organization) {
    return
  }

  try {
    const { data: partups } = await findPartups(organization)
    commit('partups', partups)
  } catch (error) {
    // ...
  }
}

export const updateField = async (
  { commit, getters },
  { field, value, done }
) => {
  const organization = getters.organization

  try {
    const { data } = await update(organization, {
      data: {
        [field]: value,
      },
    })

    commit('organization', data)
  } catch (error) {
    commit('organization', organization)
  } finally {
    done && done()
  }
}

export const solicitAccepted = async ({ dispatch }) => {}

export const solicitRequested = async ({ commit, getters }) => {}
