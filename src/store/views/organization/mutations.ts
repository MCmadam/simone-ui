export const organization = (state, organization) => {
  state.organization = organization
}

export const members = (state, members) => {
  state.members = members
}

export const partups = (state, partups) => {
  state.partups = partups
}
