import dashboard from './dashboard'
import organization from './organization'
import partup from './partup'
import search from './search'
import userProfile from './user-profile'

export default {
  namespaced: true,

  modules: {
    dashboard,
    organization,
    partup,
    search,
    userProfile,
  },
}
