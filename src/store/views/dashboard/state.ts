export default {
  activities: {
    data: [],
    loading: false,
    error: null,
    query: null,
  },
  partups: {
    data: [],
    collapsedCount: 4,
    isCollapsed: true,
    loading: false,
    error: null,
  },
}
