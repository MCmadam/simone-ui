import activityFindForSelf from '@/data/activity/find-for-self'
import activityUpdate from '@/data/activity/update'
import partupFindForSelf from '@/data/partup/find-for-self'
import commitWithKey from '@/util/store/commit-with-key'
import { noneToNull } from '@/util/activity'

export const getActivities = async ({ commit }, { query }: any = {}) => {
  const c = commitWithKey('activities', commit)

  try {
    c('loading', true)

    const { data: activities } = await activityFindForSelf({
      query,
    })

    commit('activitiesQuery', query || null)
    c('setData', activities)
  } catch (error) {
    c('error', error)
  } finally {
    c('loading', false)
  }
}

export const getPartups = async ({ commit }) => {
  const c = commitWithKey('partups', commit)

  try {
    c('loading', true)

    const { data: partups } = await partupFindForSelf()

    c('setData', partups)
  } catch (error) {
    c('error', error)
  } finally {
    c('loading', false)
  }
}

export const updateActivityStatus = async ({ commit }, activity) => {
  const status = noneToNull(activity.status)

  try {
    const { data } = await activityUpdate(activity, {
      data: {
        status,
      },
    })

    commit('updateActivity', data)
  } catch (error) {
    // console.error(error)
  }
}
