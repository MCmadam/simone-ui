import keyBy from 'lodash/keyBy'
import Vue from 'vue'

export const solicits = (state, solicits) => {
  state.solicits = keyBy(solicits, 'uuid')
}

export const removeSolicit = (state, solicit) => {
  if (state.solicits[solicit.uuid]) {
    Vue.delete(state.solicits, solicit.uuid)
  }
}
