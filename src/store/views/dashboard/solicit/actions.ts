import solicitFindPending from '@/data/solicit/find-pending'

export const getPendingSolicits = async ({ commit }) => {
  try {
    const { data: solicits } = await solicitFindPending({
      entity: 'user',
    })

    commit('solicits', solicits)
  } catch (error) {
    // console.error(error)
  }
}

export const removeSolicit = ({ commit }, solicit) => {
  commit('removeSolicit', solicit)
}
