import isInvite from '@/lib/model/solicit/is-invite'
import isForEntity from '@/lib/model/solicit/is-for-entity'
import { SOLICIT_ENTITIES } from '@/types'
import filterAnd from '@/util/array/filter-and'
import filter from 'lodash/filter'

export const solicits = (state) => {
  return Object.values(state.solicits)
}

export const organizationInvites = (state, getters) => {
  return filter(
    getters.solicits,
    filterAnd(isInvite, (s) => isForEntity(s, SOLICIT_ENTITIES.organization))
  )
}

export const otherInvites = (state, getters) => {
  return filter(
    getters.solicits,
    filterAnd(isInvite, (s) => !isForEntity(s, SOLICIT_ENTITIES.organization))
  )
}
