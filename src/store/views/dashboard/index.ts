import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import state from './state'

// import solicit from './solicit'

export default {
  namespaced: true,

  actions,
  getters,
  // modules: {
  //   solicit,
  // },
  mutations,
  state,
}
