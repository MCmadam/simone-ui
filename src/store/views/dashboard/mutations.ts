import filter from 'lodash/filter'
import findIndex from 'lodash/findIndex'
import Vue from 'vue'

// #region activities
export const activitiesQuery = (state, value) => {
  state.activities.query = value
}
// #endregion

// #region partups
export const isPartupsCollapsed = (state, { value }) => {
  state.partups.isCollapsed = value
}
// #endregion

export const updateActivity = (state, activity) => {
  const activities = state.activities.data
  const activityIndex = findIndex(
    activities,
    (x: any) => x.uuid === activity.uuid
  )

  if (activityIndex > -1) {
    Vue.set(activities, activityIndex, activity)
  }
}

// #region generic
export const error = (state, { key, value }) => {
  state[key].error = value
}

export const loading = (state, { key, value }) => {
  state[key].loading = value
}

export const setData = (state, { key, value }) => {
  state[key].data = value
}
// #endregion
