import take from 'lodash/take'

// #region activities
export const activities = (state) => {
  return state.activities.data
}

export const activitiesQuery = (state) => {
  return state.activities.query
}

export const isDoneActivitiesVisible = (state) => {
  return state.activities.isDoneVisible
}

export const isActivitiesFiltered = (state) => {
  return !!state.activities.query
}

export const userIsContributingToActivities = (state, getters) => {
  return getters.isActivitiesFiltered || getters.activities.length > 0
}
// #endregion

// #region partups
export const partups = (state) => {
  return state.partups.data
}

export const collapsedPartupsCount = (state) => {
  return state.partups.collapsedCount
}

export const totalPartupsCount = (state) => {
  return state.partups.data.length
}

export const filteredPartups = (state, getters) => {
  return getters.isPartupsCollapsed
    ? take(getters.partups, getters.collapsedPartupsCount)
    : getters.partups
}

export const isPartupsCollapsed = (state) => {
  return state.partups.isCollapsed
}

export const userIsPartnerInPartups = (state, getters) => {
  return getters.partups.length > 0
}
// #endregion

// #region generic
export const error = (state) => {
  return (key) => {
    return state[key].error
  }
}

export const isLoading = (state) => {
  return (key) => {
    return state[key].loading
  }
}
// #endregion
