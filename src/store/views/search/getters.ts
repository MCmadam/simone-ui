import get from 'lodash/get'

export const totalCount = (state) => {
  return get(state.data, 'totals.total', 0)
}

export const activityCount = (state) => {
  return get(state.data, 'totals.activities', 0)
}

export const partupCount = (state) => {
  return get(state.data, 'totals.partups', 0)
}

export const userCount = (state) => {
  return get(state.data, 'totals.users', 0)
}

export const activities = (state) => {
  return get(state.data, 'activities', [])
}

export const partups = (state) => {
  return get(state.data, 'partups', [])
}

export const users = (state) => {
  return get(state.data, 'users', [])
}
