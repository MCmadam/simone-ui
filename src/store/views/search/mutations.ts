export const data = (state, data) => {
  state.data = data
}

export const query = (state, query) => {
  state.query = query
}
