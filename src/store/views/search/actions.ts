import commitForAction from '@/util/store/commit-for-action'
import find from '@/data/search/find'
import Dispatcher from '@/api/dispatcher'

const findDispatcher = new Dispatcher(find)

export const executeSearch = async ({ commit }, { query }) => {
  const c = commitForAction('search', commit)

  c.pending()

  try {
    commit('query', query)

    const { data } = await findDispatcher.cancel().exec({
      query: {
        q: query,
      },
    })

    commit('data', data)
    c.success()
  } catch (error) {
    c.error(error)
  }
}
