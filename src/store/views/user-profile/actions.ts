import commitForAction from '@/util/store/commit-for-action'
import Dispatcher from '@/api/dispatcher'
import findOne from '@/data/user/find-one'
import findPartups from '@/data/partup/find-for-user'
import updateSelf from '@/data/user/update-self'
import canDo from '@/lib/authorization/canDo'
import logger from '@/util/logger'

const getUserDispatcher = new Dispatcher(findOne)

export const initialize = async ({ dispatch }, { slug, uuid, solicit }) => {
  await dispatch('getProfile', { slug, uuid, solicit })
  dispatch('getPartups')
}

export const reset = ({ commit }) => {
  commit('user', null)
  commit('partups', null)
}

export const getProfile = async ({ commit }, { slug, uuid, solicit }) => {
  const action = 'getProfile'
  const c = commitForAction(action, commit)

  const options = Object.create(null)

  if (solicit) {
    options.query = {
      solicit,
    }
  }

  c.pending()
  try {
    const { data: user } = await getUserDispatcher.cancel().exec(
      {
        slug,
        uuid,
      },
      options
    )

    commit('user', user)
    c.success()
  } catch (error) {
    c.error(error)
  }
}

export const getPartups = async ({ commit, getters }) => {
  const user = getters.user

  if (!user) {
    return
  }
  if (!canDo('user_partup_list', user)) {
    logger.debug(
      'view user profile getPartups called without proper permissions'
    )
    return
  }

  const c = commitForAction('getPartups', commit)

  c.pending()
  try {
    const { data: partups } = await findPartups(user)

    commit('partups', partups)
    c.success()
  } catch (error) {
    c.error(error)
  }
}

const userUpdateSelfDispatcher = new Dispatcher(updateSelf)

export const updateField = async (
  { commit, dispatch },
  { field, value, done }
) => {
  const c = commitForAction('updateField', commit)

  c.pending()

  try {
    const { data: user } = await userUpdateSelfDispatcher.cancel().exec({
      data: {
        [field]: value,
      },
    })

    c.success()

    commit('user', user)
    dispatch('user/updateUserLocal', user, { root: true })

    if (field === 'image_url') {
      dispatch('getPartups')
    }
  } catch (error) {
    c.error(error)
  } finally {
    done && done()
  }
}
