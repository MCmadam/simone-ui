import withActions from '@/util/store/with-actions'

import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import state from './state'

export default withActions(['getProfile', 'getPartups'], {
  namespaced: true,

  actions,
  getters,
  mutations,
  state,
})
