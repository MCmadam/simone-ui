import get from 'lodash/get'

export const user = (state) => {
  return state.user
}

export const partups = (state) => {
  return get(state, 'partups', [])
}
