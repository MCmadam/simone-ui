/*
 * Initial state
 **/

export default {
  /**
   * App branding
   * @see model src/lib/branding.ts
   */
  branding: null,
  brandingContext: null,

  currentOrganizationUuid: null,

  /**
   * Layout used by the current route
   */
  layout: null,

  searchToolbarActive: false,
}
