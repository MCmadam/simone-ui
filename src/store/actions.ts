import branding from '@/data/organization/branding'
import environment from '@/data/user/environment'
import defaultBranding, { defaultBrandingDomain } from '@/lib/branding/default'
import { Findable } from '@/types'
import FEATURES from '@/lib/features'
import parseBrandingFromLocation from '@/util/url/parse-branding-from-location'
import switchBrandingLocation from '@/lib/branding/switch-branding-location'

/*
 * Global actions
 **/
export const getEnvironment = async (context) => {
  try {
    const { data } = await environment()

    const user = Object.assign({}, data.user, {
      organizations: data.organizations || [],
    })

    if (
      context.getters.currentOrganization === null &&
      data.organizations?.length === 1
    ) {
      context.dispatch('setDefaultOrganizationContext', data.organizations[0])
    }

    context.dispatch('bulletin/receiveBackendBulletins', data.bulletins)
    context.dispatch('user/setUserLocal', user, { root: true })
    context.dispatch('user/updateUserStateLocal', data.state, { root: true })
    context.dispatch('solicit/getUserSolicits')
  } catch (error) {
    // ...
  }
}

export const getBranding = async ({ commit }, slug?) => {
  let domain

  if (FEATURES.domainBranding) {
    domain = parseBrandingFromLocation()

    if (domain === defaultBrandingDomain) {
      commit('branding', defaultBranding)
      commit('brandingContext', defaultBrandingDomain)
      return
    }
  } else {
    domain = slug
  }

  if (domain) {
    try {
      const { data } = await branding(domain)
      commit('branding', data)
      commit('brandingContext', domain)
    } catch (error) {
      commit('branding', defaultBranding)
      commit('brandingContext', defaultBrandingDomain)
    }
  } else {
    commit('branding', defaultBranding)
    commit('brandingContext', defaultBrandingDomain)
  }
}

export const setLayout = ({ commit }, layout) => {
  commit('layout', layout)
}

export const setCurrentOrganization = ({ commit }, organization: Findable) => {
  const uuid = organization && 'uuid' in organization ? organization.uuid : null
  commit('currentOrganizationUuid', uuid)
}

export const setDefaultOrganizationContext = (
  { dispatch, state, rootGetters },
  organization?
) => {
  const organizations = rootGetters['user/userOrganizations']

  if (FEATURES.domainBranding) {
    const context = state.brandingContext

    if (context === defaultBrandingDomain) {
      if (organization) {
        switchBrandingLocation(organization.slug)
      } else if (
        organizations.length === 1 &&
        organizations[0].slug !== 'part-up'
      ) {
        switchBrandingLocation(organizations[0].slug)
      }
    } else {
      if (organization && context === organization.slug) {
        dispatch('setCurrentOrganization', organization)
      } else {
        for (const organization of organizations) {
          if (context === organization.slug) {
            dispatch('setCurrentOrganization', organization)
            break
          }
        }
      }
    }
  } else {
    const currentOrganization = rootGetters['currentOrganization']

    if (!currentOrganization) {
      dispatch('setCurrentOrganization', organization || organizations[0])
    }
  }
}

export const setSearchToolbarActive = ({ commit }, active) => {
  commit('searchToolbarActive', active)
}
