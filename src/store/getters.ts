import { getField as getFieldGetter } from 'vuex-map-fields'
import { AppBranding } from '@/types'

export const getField = getFieldGetter

/*
 * global getters
 **/

export const branding = (state): AppBranding => {
  return state.branding
}

export const brandingContext = (state) => {
  return state.brandingContext
}

export const layout = (state, getters, rootState) => {
  return rootState.layout.current
}

export const loggedIn = (state, getters, rootState) => {
  return rootState.auth.loggedIn
}

export const currentUser = (state, getters, rootState) => {
  return rootState.user.user
}

export const currentOrganization = (state, getters, rootState, rootGetters) => {
  const userOrganizations = rootGetters['user/userOrganizations']
  const organizationUuid = state.currentOrganizationUuid

  return userOrganizations.find(({ uuid }) => uuid === organizationUuid) || null
}
