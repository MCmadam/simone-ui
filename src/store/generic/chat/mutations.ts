import Vue from 'vue'
import { Mutation } from 'vuex'
import { StoreModuleChatState } from './types'
import { Message, Uuid } from '@/types'
import { getChat, createChat } from './util'

type ChatMutation = Mutation<StoreModuleChatState>

export const initializeChat: ChatMutation = (state, uuid: Uuid) => {
  Vue.set(state.partups, uuid, createChat())
}

export const activateChat: ChatMutation = (state, uuid: Uuid) => {
  for (const key in state.partups) {
    const chat = getChat(state, uuid)
    chat.active = key === uuid
  }
}

export const deactivateChat: ChatMutation = (state, uuid: Uuid) => {
  const chat = getChat(state, uuid)
  chat.active = false
}

export const incrementUnread: ChatMutation = (state, uuid: Uuid) => {
  const chat = getChat(state, uuid)
  chat.unreadCount = chat.unreadCount + 1
}

export const resetUnread: ChatMutation = (state, uuid: Uuid) => {
  const chat = getChat(state, uuid)
  chat.unreadCount = 0
}

export const message: ChatMutation = (state, message: Message) => {
  const chat = getChat(state, message.partupUuid)
  chat.messages.set(message)
}

export const messages: ChatMutation = (
  state,
  payload: { partupUuid: string; messages: Message[] }
) => {
  const chat = getChat(state, payload.partupUuid)
  chat.messages.clear()
  chat.messages.set(...payload.messages)
}

export const clear: ChatMutation = (state, partupId) => {
  const chat = getChat(state, partupId)
  if (chat) {
    chat.messages.clear()
  }
}
