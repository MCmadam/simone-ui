import { Message, Uuid } from '@/types'
import { Action } from 'vuex'
import { StoreModuleChatState } from './types'
import { getChat, messagesPerPartup } from './util'

type ChatAction = Action<StoreModuleChatState, unknown>

export const activateChat: ChatAction = ({ commit, state }, uuid: Uuid) => {
  const chat = getChat(state, uuid)

  if (!chat) {
    commit('initializeChat', uuid)
  }

  commit('activateChat', uuid)
  commit('resetUnread', uuid)
}

export const deactivateChat: ChatAction = ({ commit }, uuid: Uuid) => {
  commit('deactivateChat', uuid)
}

export const set: ChatAction = (
  { commit, state },
  payload: Message | Message[]
) => {
  for (const [partupUuid, messages] of Object.entries(
    messagesPerPartup(payload)
  )) {
    const chat = getChat(state, partupUuid)

    if (!chat) {
      commit('initializeChat', partupUuid)
    }

    commit('messages', {
      partupUuid,
      messages,
    })
  }
}

export const receive: ChatAction = (
  { commit, getters, state },
  message: Message
) => {
  let chat = getChat(state, message.partupUuid)

  if (!chat) {
    commit('initializeChat', message.partupUuid)
    chat = getChat(state, message.partupUuid)
  }

  commit('message', message)

  if (!getters.isActive(message.partupUuid)) {
    commit('incrementUnread', message.partupUuid)
  }
}

export const clear: ChatAction = ({ commit }, partupId) => {
  commit('clear', partupId)
}
