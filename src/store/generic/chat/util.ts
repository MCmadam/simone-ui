import Vue from 'vue'
import { StoreModuleChatState } from './types'
import ObjectCache from '@/util/store/cache/object-cache'
import { Message } from '@/types'
import toArray from '@/util/array/to-array'

export const createChat = () => Vue.observable({
  active: false,
  messages: new ObjectCache<Message>(),
  unreadCount: 0,
})

export const getChat = (state: StoreModuleChatState, uuid: string) => state.partups[uuid]

export const messagesPerPartup = (messages: Message | Message[]) => {
  return toArray(messages).reduce((total, message) => {
    if (!total[message.partupUuid]) {
      total[message.partupUuid] = []
    }
    total[message.partupUuid].push(message)
    return total
  }, {})
}
