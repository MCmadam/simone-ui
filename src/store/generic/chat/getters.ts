import { Getter } from 'vuex'
import { Uuid } from '@/types'
import { StoreModuleChatState } from './types'

export const isActive: Getter<StoreModuleChatState, unknown> = (state) => {
  return (uuid: Uuid) => {
    return !!state.partups[uuid]?.active
  }
}

export const messagesForPartup: Getter<StoreModuleChatState, unknown> = (
  state
) => {
  return (uuid: Uuid) => {
    const chat = state.partups[uuid]

    if (chat) {
      return chat.messages.value.sort(
        (a, b) =>
          new Date(a.postedAt).valueOf() - new Date(b.postedAt).valueOf()
      )
    }

    return []
  }
}

export const unreadCount: Getter<StoreModuleChatState, unknown> = (state) => {
  let count = 0

  for (const chat of Object.values(state.partups)) {
    count += chat.unreadCount
  }

  return count
}

export const unreadCountForPartup: Getter<StoreModuleChatState, unknown> = (
  state
) => {
  return (uuid: Uuid) => {
    const chat = state.partups[uuid]

    if (chat) {
      return chat.unreadCount
    }

    return 0
  }
}
