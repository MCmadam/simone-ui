import ObjectCache from '@/util/store/cache/object-cache'
import { Message } from '@/types'

export type StoreModuleChatState = {
  partups: {
    [key: string]: {
      active: boolean,
      messages: ObjectCache<Message>
      unreadCount: number,
    }
  }
}
