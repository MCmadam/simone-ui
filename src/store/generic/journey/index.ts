import findAwesomeWork from './find-awesome-work'

export default {
  modules: {
    findAwesomeWork,
  },
}
