import {
  CAROUSEL_ITEM_TYPES,
  CarouselCard,
  CarouselItem,
  CarouselSummary,
} from '@/types'

export const addBookmark = (state, data: CarouselCard) => {
  state.bookmarks.push(data)
}

export const clearBookmarks = (state) => {
  state.bookmarks = []
}

export const setItems = (state, items: CarouselItem[]) => {
  state.items = items || []
}

export const clearSeenCountWalks = (state) => {
  state.seenCountWalk = 0
}

export const incrementSeenCount = (state) => {
  state.seenCountWalk += 1
  state.seenCountTotal += 1
}

export const incrementWalk = (state) => {
  state.walks += 1
}

export const next = (state) => {
  // Remove the first item
  state.items.shift()

  // check if the walk has reached one of it's limits
  // and add a summary as next item
  if (
    // has max bookmarks
    state.bookmarks.length >= state.maxBookmarks ||
    // has max seenCount for walk
    state.seenCountWalk >= state.maxSeenCountWalk ||
    // has reached the end
    state.items.length < 1
  ) {
    const summary: CarouselSummary = {
      type: CAROUSEL_ITEM_TYPES.summary,
      data: state.bookmarks,
    }

    state.items.unshift(summary)
  } else {
    incrementSeenCount(state)
  }
}

export const partupRequest = (state, partup) => {
  state.partupRequests.push(partup.uuid)
}

export const reset = (state) => {
  state.bookmarks = []
  state.items = []
  state.raw = []
  state.seenCountWalk = 0
  state.seenCountTotal = 0
  state.walks = 0
  state.partupRequests = []
}
