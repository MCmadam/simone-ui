import { CarouselItem, CarouselCard, MatchingActivityResponse } from '@/types'

/**
 * Carousel state.
 */
export default {
  maxBookmarks: 4,
  maxSeenCountWalk: 15,
  maxWalks: 2,
  seenCountWalk: 0,
  seenCountTotal: 0,
  walks: 0,
  bookmarks: [] as CarouselCard[],
  items: [] as CarouselItem[],
  raw: [] as MatchingActivityResponse[],
}
