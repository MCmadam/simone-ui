export const bookmarks = (state) => {
  return state.bookmarks
}

export const items = (state) => {
  return state.items
}

export const hasPendingRequestForPartup = (state) => {
  return (partup) => {
    return state.partupRequests.includes(partup.uuid)
  }
}

/*
  Conditions that allow the user to start another walk.
*/
export const mayWalkAgain = (state) => {
  return (
    state.walks < state.maxWalks &&
    // items contain the summary which counts for 1
    state.items.length > 1
  )
}
