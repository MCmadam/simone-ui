import Dispatcher from '@/api/dispatcher'
import bookmark from '@/data/activity/bookmark'
import contribute from '@/data/activity/contribute'
import findMatchingActivities from '@/data/matching/find-matching-activities'
import accept from '@/data/solicit/accept'
import request from '@/data/solicit/request'
import commitForAction from '@/util/store/commit-for-action'
import {
  SOLICIT_ENTITIES,
  SOLICIT_TYPES,
  CarouselCard,
  CAROUSEL_ITEM_TYPES,
} from '@/types'

export const initialize = async ({ commit, dispatch }) => {
  await dispatch('reset')
  await dispatch('getRecommendations')
  dispatch('startNextWalk', false)
}

export const reset = ({ commit }) => {
  commit('reset')
}

const findMatchingActivitiesDispatcher = new Dispatcher(findMatchingActivities)
export const getRecommendations = async ({ commit }) => {
  const action = 'findMatchingActivities'
  const c = commitForAction(action, commit)

  c.pending()
  try {
    const { data } = await findMatchingActivitiesDispatcher.cancel().exec()

    const items = data.map(
      (rawItem): CarouselCard => {
        const { affordances, reasons, activity, partup, solicit } = rawItem

        const item: CarouselCard = {
          type: CAROUSEL_ITEM_TYPES.card,
          affordances,
          reasons,
          data: {
            activity,
            partup,
            solicit,
          },
        }

        return item
      }
    )

    commit('setItems', items)
    c.success()
  } catch (error) {
    c.error(error)
  }
}

export const skip = ({ commit }) => {
  commit('next')
}

export const startNextWalk = ({ commit }, next = true) => {
  commit('clearBookmarks')
  commit('clearSeenCountWalks')
  commit('incrementWalk')
  if (next) {
    commit('next')
  }
}

export const activityBookmark = async ({ commit }, item: CarouselCard) => {
  try {
    const activity = item.data.activity
    bookmark(activity)
    commit('addBookmark', item)
    commit('next')
  } catch (error) {
    throw error
  }
}

export const activityContribute = async (
  { rootGetters },
  item: CarouselCard
) => {
  try {
    const activity = item.data.activity
    const currentUser = rootGetters['currentUser']
    await contribute(activity, {
      data: {
        userUuids: [currentUser.uuid],
      },
    })
  } catch (error) {
    throw error
  }
}

export const solicitAccept = async (context, { item, next = false }) => {
  try {
    const solicit = item.data.solicit

    await accept({
      for: SOLICIT_ENTITIES.activity,
      type: SOLICIT_TYPES.invite,
      uuid: solicit.uuid,
    })

    if (next) {
      context.commit('next')
    }
  } catch (error) {
    throw error
  }
}

export const solicitRequestPartup = async (context, item: CarouselCard) => {
  try {
    const partup = item.data.partup

    await request({
      entity: SOLICIT_ENTITIES.partup,
      entity_uuid: partup.uuid,
    })

    context.commit('partupRequest', partup)
  } catch (error) {
    throw error
  }
}
