import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import state from './state'
import withActions from '@/util/store/with-actions'

export default withActions(['findMatchingActivities'], {
  namespaced: true,

  actions,
  getters,
  mutations,
  state,
})
