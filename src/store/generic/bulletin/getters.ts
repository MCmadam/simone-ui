import { Getter } from 'vuex'
import { BULLETIN_SCOPES } from '@/types/model'
import { BulletinState } from './types'

export const forScopeAndType: Getter<BulletinState, unknown> = (
  state,
  getters,
  rootState,
  rootGetters
) => {
  return (scope: BULLETIN_SCOPES, type: string) => {
    for (const [_, bulletin] of Object.entries(state.bulletins.value)) {
      if (bulletin.scope === scope && bulletin.type === type) {
        return bulletin
      }
    }

    return null
  }
}
