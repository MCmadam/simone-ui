import { Mutation } from 'vuex'
import { BulletinState } from './types'
import { Bulletin } from '@/types'

export const bulletins: Mutation<BulletinState> = (
  state,
  bulletins: Bulletin[]
) => {
  for (const bulletin of bulletins) {
    state.bulletins.set(bulletin)
  }
}

export const removeBulletin: Mutation<BulletinState> = (
  state,
  bulletin: Bulletin
) => {
  state.bulletins.delete(bulletin)
}
