import { Bulletin } from '@/types/model'
import { BulletinState } from './types'
import ObjectCache from '@/util/store/cache/object-cache'

export default (): BulletinState => ({
  bulletins: new ObjectCache<Bulletin>(),
})
