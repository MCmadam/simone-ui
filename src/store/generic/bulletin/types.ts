import ObjectCache from '@/util/store/cache/object-cache'
import { Bulletin } from '@/types/model'

export type BulletinState = {
  bulletins: ObjectCache<Bulletin>
}
