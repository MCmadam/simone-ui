import { Action } from 'vuex'
import { BulletinState } from './types'
import { BackendBulletin, Bulletin } from '@/types'
import mapContextBackend from '@/lib/model/bulletin/map-context-backend'
import toArray from '@/util/array/to-array'
import resolveBulletin from '@/data/bulletin/resolve'

export const resolve: Action<BulletinState, unknown> = async (
  { commit },
  bulletin: Bulletin
) => {
  try {
    await resolveBulletin(bulletin.uuid)
    commit('removeBulletin', bulletin)
  } catch (error) {
    throw error
  }
}

export const receiveBackendBulletins: Action<BulletinState, unknown> = (
  { commit },
  backendBulletins: BackendBulletin | BackendBulletin[]
) => {
  const bulletins = toArray(backendBulletins).map(mapContextBackend)
  commit('bulletins', bulletins)
}
