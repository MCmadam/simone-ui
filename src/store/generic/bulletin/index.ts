import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import state from './state'

const namespaced = true

export default {
  namespaced,

  actions,
  getters,
  mutations,
  state,
}
