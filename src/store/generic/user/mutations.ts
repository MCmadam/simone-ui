import Vue from 'vue'
import hasOwn from '@/util/object/has-own'

export const user = (state, user) => {
  state.user = user
}

export const updateUser = (state, user) => {
  if (!user) {
    return
  }

  const updated = state.user || {}

  for (const key in user) {
    if (hasOwn(user, key)) {
      updated[key] = user[key]
    }
  }

  state.user = updated
}

export const userState = (state, userState) => {
  for (const key in userState) {
    if (hasOwn(userState, key)) {
      Vue.set(state, key, userState[key])
    }
  }
}

export const deleteRequested = (state) => {
  state.deleteRequested = true
}

export const partups = (state, partups) => {
  state.partups = partups
}
