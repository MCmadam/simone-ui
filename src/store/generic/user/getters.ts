import get from 'lodash/get'

export const deleteRequested = (state) => {
  return state.deleteRequested
}

export const hasProfile = (state) => {
  if (!state.user) {
    return null
  }

  return Boolean(state.user.name)
}

export const userState = (state) => {
  return (key) => {
    return state[key] || {}
  }
}

export const matchingState = (state) => {
  const matching = state.smatching || {}
  return (key) => {
    return matching[key] || {}
  }
}

export const isLoading = (state, getters) => {
  return getters.actionPending('find')
}

export const userOrganizations = (state) => {
  const user = state.user

  return get(user, 'organizations', [])
}

export const userPartups = (state) => {
  return state.partups
}
