import ObjectCache from '@/util/store/cache/object-cache'

const state = () => ({
  solicits: new ObjectCache(),
})

export default state
