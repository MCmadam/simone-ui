import toArray from '@/util/array/to-array'

export const addSolicit = (state, solicit) => {
  const cache = state.solicits

  cache.set(solicit)
}

export const setSolicits = (state, solicits) => {
  const cache = state.solicits

  cache.clear()

  const arr = toArray(solicits)
  if (arr.length > 0) {
    cache.set(...arr)
  }
}

export const removeSolicit = (state, solicit) => {
  const cache = state.solicits

  cache.delete(solicit)
}
