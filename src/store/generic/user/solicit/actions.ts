import findPending from '@/data/solicit/find-pending'
import Dispatcher from '@/api/dispatcher'
import logger from '@/util/logger'
import isForUser from '@/lib/model/solicit/is-for-user'

const findPendingDispatcher = new Dispatcher(findPending)

export const getPendingSolicits = async ({ commit }) => {
  try {
    const { data } = await findPendingDispatcher.cancel().exec({
      entity: 'user',
    })

    commit('setSolicits', data)
  } catch (error) {
    logger.error(error)
  }
}

export const receivePendingSolicit = ({ commit, rootGetters }, solicit) => {
  const user = rootGetters['currentUser']

  if (isForUser(solicit, user)) {
    commit('addSolicit', solicit)
  }
}

export const removePendingSolicit = ({ commit }, solicit) => {
  commit('removeSolicit', solicit)
}
