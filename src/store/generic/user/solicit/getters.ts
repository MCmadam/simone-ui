import isForEntity from '@/lib/model/solicit/is-for-entity'
import isInvite from '@/lib/model/solicit/is-invite'
import isRequest from '@/lib/model/solicit/is-request'
import { SOLICIT_ENTITIES } from '@/types'
import filterAnd from '@/util/array/filter-and'

export const myInviteFor = (state) => {
  return (entity: SOLICIT_ENTITIES, uuid: string) => {
    const cache = state.solicits

    return cache.value.find(
      filterAnd(isInvite, (s) => isForEntity(s, entity, uuid))
    )
  }
}

export const myInvites = (state) => {
  return state.solicits.value.filter(isInvite)
}

export const myRequests = (state) => {
  return state.solicits.value.filter(isRequest)
}

export const myRequestFor = (state) => {
  return (entity: SOLICIT_ENTITIES, uuid: string) => {
    const cache = state.solicits

    return cache.value.find(
      filterAnd(isRequest, (s) => isForEntity(s, entity, uuid))
    )
  }
}
