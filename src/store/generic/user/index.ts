import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import state from './state'
import withActions from '@/util/store/with-actions'

// import solicit from './solicit'

export default withActions(['find', 'findPartups', 'requestDelete'], {
  namespaced: true,

  actions,
  getters,
  mutations,
  state,

  // modules: {
  //   solicit,
  // },
})
