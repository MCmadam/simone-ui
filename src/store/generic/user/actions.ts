import commitForAction from '@/util/store/commit-for-action'
import deleteSelf from '@/data/user/delete-self'
import findSelf from '@/data/user/find-self'
import Dispatcher from '@/api/dispatcher'
import logger from '@/util/logger'
import findForSelf from '@/data/partup/find-for-self'

export const requestDelete = async ({ commit, getters, state }) => {
  const action = 'requestDelete'
  const c = commitForAction(action, commit)

  if (!state.user || getters.actionPending(action, commit)) {
    return
  }

  c.pending()
  try {
    await deleteSelf()

    c.success()
    commit('deleteRequested')
  } catch (error) {
    c.error(error)
  }
}

const findSelfDispatcher = new Dispatcher(findSelf)

export const getUser = async ({ commit, state, rootGetters }) => {
  const action = 'find'
  const c = commitForAction(action, commit)

  if (!rootGetters['loggedIn']) {
    return
  }

  c.pending()
  try {
    const { data: user } = await findSelfDispatcher.cancel().exec()

    c.success()
    commit('user', user)
  } catch (error) {
    c.error(error)
  }
}

export const getUserPartups = async ({ commit, state }) => {
  const action = 'findPartups'
  const c = commitForAction(action, commit)

  if (!state.user) {
    logger.debug('action getUserPartups no user.')
    return
  }

  c.pending()
  try {
    const { data: partups } = await findForSelf()

    c.success()
    commit('partups', partups)
  } catch (error) {
    logger.debug('getUserPartups error', error)
    c.error(error)
  }
}

export const clearUserPartups = ({ commit }) => {
  commit('partups', null)
}

export const updateUserStateLocal = ({ commit }, userState) => {
  commit('userState', userState)
}

export const setUserLocal = ({ commit }, user) => {
  commit('user', user)
}

export const updateUserLocal = ({ commit }, user) => {
  commit('updateUser', user)
}
