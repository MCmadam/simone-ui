import auth from './auth'
import bulletin from './bulletin'
import chat from './chat'
import journey from './journey'
import layout from './layout'
import preferences from './preferences'
import solicit from './solicit'
import user from './user'

export default {
  auth,
  bulletin,
  chat,
  journey,
  layout,
  preferences,
  solicit,
  user,
}
