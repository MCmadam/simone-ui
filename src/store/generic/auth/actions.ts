import authLogout from '@/data/auth/logout'
import currentAuthenticatedUser from '@/data/auth/current-authenticated-user'

import { AUTH_ACTIONS, AuthFormFields } from '@/types'
import CognitoError from '@/util/error/cognito-error'
import Cookies from 'js-cookie'
import socket from '@/wss'
import router from '@/router'
import { Action } from 'vuex'
import { AuthState, FlowDef, beforeEnterHook, beforeLeaveHook } from './types'
import get from 'lodash/get'

const forgotPassword: FlowDef = {
  from: {
    [AUTH_ACTIONS.login]: {
      beforeEnter: (context) => {
        context.commit('error', null)
      },
    },
  },
}

const forgotPasswordSubmit: FlowDef = {
  beforeEnter: (context, formData) => {
    if (formData) {
      if (formData.verificationCode) {
        context.commit('formData', {
          verificationCode: formData.verificationCode,
        })
      }
    }

    context.dispatch('getEmailCookie')
  },
}

const login: FlowDef = {
  to: {
    [AUTH_ACTIONS.forgotPassword]: {
      beforeLeave: (context) => {
        context.dispatch('setEmailCookie', context.state.email)
      },
    },
  },
  from: {
    [AUTH_ACTIONS.register]: {
      beforeEnter: (context) => {
        context.commit('error', null)
      },
    },
    [AUTH_ACTIONS.forgotPassword]: {
      beforeEnter: (context) => {
        context.commit('error', null)
      },
    },
    [AUTH_ACTIONS.forgotPasswordSubmit]: {
      beforeEnter: (context) => {
        context.commit('error', null)
      },
    },
  },
}

const register: FlowDef = {
  from: {
    [AUTH_ACTIONS.login]: {
      beforeEnter: (context) => {
        context.commit('error', null)
      },
    },
  },
}

const registerConfirm: FlowDef = {
  beforeEnter: (context, formData) => {
    if (formData) {
      const data: any = {}

      if (formData.cognitoUserId) {
        data.cognitoUserId = formData.cognitoUserId
      }
      if (formData.verificationCode) {
        data.verificationCode = formData.verificationCode
      }

      context.commit('formData', data)
    }

    context.dispatch('getEmailCookie')
  },
  beforeLeave: (context) => {
    context.commit('formData', {
      cognitoUserId: null,
      verificationCode: null,
    })
  },
}

const registerResend: FlowDef = {
  beforeEnter: (context) => {
    context.dispatch('getEmailCookie')
  },
  beforeLeave: (context) => {
    context.dispatch('clearEmailCookie')
  },
}

const flows = {
  forgotPassword,
  forgotPasswordSubmit,
  login,
  register,
  registerConfirm,
  registerResend,
}

export const resolve: Action<AuthState, unknown> = async (
  context,
  {
    to,
    formData,
  }: {
    to: AUTH_ACTIONS
    from?: AUTH_ACTIONS
    formData?: Partial<AuthFormFields>
  }
) => {
  const from = context.state.flow

  try {
    if (from) {
      const fromFlow = flows[from]

      if (fromFlow) {
        const genericBeforeLeaveHook: beforeLeaveHook = get(
          fromFlow,
          'beforeLeave'
        )
        if (genericBeforeLeaveHook) {
          await genericBeforeLeaveHook(context)
        }

        const specificBeforeLeaveHook: beforeLeaveHook = get(
          fromFlow,
          `to.${to}.beforeLeave`
        )
        if (specificBeforeLeaveHook) {
          await specificBeforeLeaveHook(context)
        }
      }
    }
  } catch (error) {
    // console.error('resolve flow from hooks:', error)
  }

  try {
    const genericBeforeEnterHook: beforeEnterHook = get(
      flows[to],
      'beforeEnter'
    )
    if (genericBeforeEnterHook) {
      await genericBeforeEnterHook(context, formData)
    }

    const specificBeforeEnterHook: beforeEnterHook = get(
      flows[to],
      `from.${from}.beforeEnter`
    )
    if (specificBeforeEnterHook) {
      await specificBeforeEnterHook(context, formData)
    }
  } catch (error) {
    // console.error('resolve flow to hooks:', error)
  }

  context.commit('flow', to)
}

export const error = (context, error: CognitoError) => {
  context.commit('error', error)
}

export const logout = async ({ commit }, { global = false } = {}) => {
  try {
    await authLogout({
      global,
    })

    commit('loggedIn', false)
    socket.disconnect()
    router.go(0)
  } catch (error) {
    // console.error(error)
  }
}

export const getEmailCookie = ({ commit }) => {
  const email = Cookies.get('temp-email')
  if (email) {
    commit('email', email)
  }
}

export const clearEmailCookie = () => {
  Cookies.remove('temp-email')
}

export const setEmailCookie = (context, email) => {
  if (typeof email === 'string') {
    Cookies.set('temp-email', email, {
      expires: 1 / 24,
    })
  }
}

export const syncLogin = async ({ commit, state }) => {
  try {
    if (await currentAuthenticatedUser()) {
      if (!state.loggedIn) {
        commit('loggedIn', true)
      }
    } else if (state.loggedIn) {
      commit('loggedIn', false)
      socket.disconnect()
    }
  } catch (error) {
    commit('loggedIn', false)
    socket.disconnect()
  }
}
