import { AUTH_ACTIONS, AuthFormFields } from '@/types'
import CognitoError from '@/util/error/cognito-error'

export type AuthState = {
  flow: AUTH_ACTIONS
  error: CognitoError
  loggedIn: boolean
} & AuthFormFields

export type beforeEnterHook = (
  context,
  formData?: Partial<AuthFormFields>
) => void
export type beforeLeaveHook = (context) => void
// export type successHook = (context) => void
// export type errorHook = (context, error: CognitoError) => void

export type FlowHooks = {
  beforeEnter?: beforeEnterHook
  beforeLeave?: beforeLeaveHook
}

export type FlowDef = {
  to?: {
    [key: string]: FlowHooks
  }
  from?: {
    [key: string]: FlowHooks
  }
} & FlowHooks
