import * as actions from './actions'
import * as mutations from './mutations'
import state from './state'

import { Module } from 'vuex'
import { AuthState } from './types'

const namespaced = true

const auth: Module<AuthState, unknown> = {
  namespaced,

  actions,
  mutations,
  state,
}

export default auth
