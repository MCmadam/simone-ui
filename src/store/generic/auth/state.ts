import { AuthState } from './types'

export default (): AuthState => ({
  cognitoUserId: null,
  flow: null,
  email: null,
  error: null,
  verificationCode: null,
  loggedIn: null,
})
