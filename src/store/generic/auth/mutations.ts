import { Mutation } from 'vuex'
import { AuthState } from './types'
import { AUTH_ACTIONS, AuthFormFields } from '@/types'
import CognitoError from '@/util/error/cognito-error'

export const formData: Mutation<AuthState> = <T>(
  state,
  payload: AuthFormFields
) => {
  for (const [k, v] of Object.entries(payload)) {
    state[k] = v
  }
}

export const flow: Mutation<AuthState> = (state, flow: AUTH_ACTIONS) => {
  state.flow = flow
}

export const error: Mutation<AuthState> = (state, error: CognitoError) => {
  state.error = error
}

// export const reset = (state) => {
//   resetFields(state)
// }

export const loggedIn = (state, loggedIn) => {
  state.loggedIn = loggedIn
}

// export const resetFields = (state) => {
//   email(state, null)
//   cognitoUserId(state, null)
//   verificationCode(state, null)
// }
