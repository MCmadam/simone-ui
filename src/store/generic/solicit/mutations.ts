import Vue from 'vue'
import { Mutation } from 'vuex'
import toArray from '@/util/array/to-array'
import ObjectCache from '@/util/store/cache/object-cache'
import { StoreModuleSolicitState } from './types'
import { getUserCache, getPartupCache } from './util'

type SolicitMutation = Mutation<StoreModuleSolicitState>

export const setUserSolicits: SolicitMutation = (state, solicits) => {
  const cache = getUserCache(state)

  cache.clear()
  cache.set(...toArray(solicits))
}

export const setPartupSolicits: SolicitMutation = (state, { id, solicits }) => {
  const cache = getPartupCache(state, id)

  cache.clear()
  cache.set(...toArray(solicits))
}

export const addUserSolicit: SolicitMutation = (state, solicit) => {
  const cache = getUserCache(state)

  cache.set(solicit)
}

export const addPartupSolicit: SolicitMutation = (state, { id, solicit }) => {
  const cache = getPartupCache(state, id)

  cache.set(solicit)
}

export const removeUserSolicit: SolicitMutation = (state, solicit) => {
  const cache = getUserCache(state)

  cache.delete(solicit)
}

// export const removePartupSolicit: SolicitMutation = (state, solicit) => {}

export const initializePartupCache: SolicitMutation = (state, partupId) => {
  const cache = new ObjectCache()
  Vue.set(state.partups, partupId, cache)
}
