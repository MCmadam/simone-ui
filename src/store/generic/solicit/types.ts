import ObjectCache from '@/util/store/cache/object-cache'
import { Identifiable } from '@/types'

export type StoreModuleSolicitState = {
  user: ObjectCache<Identifiable>
  partups: {
    [key: string]: ObjectCache<Identifiable>
  }
}
