import { Getter } from 'vuex'
import { StoreModuleSolicitState } from './types'
import isInvite from '@/lib/model/solicit/is-invite'
import isRequest from '@/lib/model/solicit/is-request'
import filterAnd from '@/util/array/filter-and'
import isForEntity from '@/lib/model/solicit/is-for-entity'
import { SOLICIT_ENTITIES } from '@/types'

type SolicitGetter = Getter<StoreModuleSolicitState, unknown>

//#region invites
export const invitesForActivity: SolicitGetter = (state) => {
  return (partupId: string, activityId: string) => {
    const partups = state.partups
    const cache = partups[partupId]

    if (cache) {
      return cache.value.filter(
        filterAnd(isInvite, (s) =>
          isForEntity(s, SOLICIT_ENTITIES.activity, activityId)
        )
      )
    }

    return []
  }
}

export const invitesForPartup: SolicitGetter = (state) => {
  return (partupId: string) => {
    const partups = state.partups
    const cache = partups[partupId]

    if (cache) {
      return cache.value.filter(isInvite)
    }

    return []
  }
}

export const myInvites: SolicitGetter = (state) => {
  return state.user.value.filter(isInvite)
}

export const myInviteForActivity: SolicitGetter = (state) => {
  return (activityId: string) => {
    return state.user.value.find(
      filterAnd(isInvite, (s) =>
        isForEntity(s, SOLICIT_ENTITIES.activity, activityId)
      )
    )
  }
}

export const myInviteForPartup: SolicitGetter = (state) => {
  return (partupId: string) => {
    return state.user.value.find(
      filterAnd(isInvite, (s) =>
        isForEntity(s, SOLICIT_ENTITIES.partup, partupId)
      )
    )
  }
}
//#endregion

//#region request
export const requestsForActivity: SolicitGetter = (state) => {
  return (partupId: string, activityId: string) => {
    const partups = state.partups
    const cache = partups[partupId]

    if (cache) {
      return cache.value.filter(
        filterAnd(isRequest, (s) =>
          isForEntity(s, SOLICIT_ENTITIES.activity, activityId)
        )
      )
    }

    return []
  }
}

export const requestsForPartup: SolicitGetter = (state) => {
  return (partupId: string) => {
    const partups = state.partups
    const cache = partups[partupId]

    if (cache) {
      return cache.value.filter(isRequest)
    }

    return []
  }
}

export const myRequests: SolicitGetter = (state) => {
  return state.user.value.filter(isRequest)
}

export const myRequestForActivity: SolicitGetter = (state) => {
  return (activityId: string) => {
    return state.user.value.find(
      filterAnd(isRequest, (s) =>
        isForEntity(s, SOLICIT_ENTITIES.activity, activityId)
      )
    )
  }
}

export const myRequestForPartup: SolicitGetter = (state) => {
  return (partupId: string) => {
    return state.user.value.find(
      filterAnd(isRequest, (s) => {
        isForEntity(s, SOLICIT_ENTITIES.partup, partupId)
      })
    )
  }
}
//#endregion
