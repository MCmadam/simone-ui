import { StoreModuleSolicitState } from './types'

export const getUserCache = (state: StoreModuleSolicitState) => state.user

export const getPartupCache = (state: StoreModuleSolicitState, partupId) =>
  state.partups[partupId]
