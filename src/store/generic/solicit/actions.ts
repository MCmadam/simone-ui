import { Action } from 'vuex'
import Dispatcher from '@/api/dispatcher'
import findPending from '@/data/solicit/find-pending'
import logger from '@/util/logger'
import { SOLICIT_ENTITIES } from '@/types'
import { StoreModuleSolicitState } from './types'
import isForUser from '@/lib/model/solicit/is-for-user'
import { isForPartup, isForActivity } from '@/lib/model/solicit/is-for-entity'
import { getPartupCache } from './util'

type SolicitAction = Action<StoreModuleSolicitState, unknown>

const userSolicitDispatcher = new Dispatcher(findPending)
export const getUserSolicits: SolicitAction = async ({ commit }) => {
  try {
    const { data } = await userSolicitDispatcher.cancel().exec({
      entity: 'user',
    })

    commit('setUserSolicits', data)
  } catch (error) {
    logger.error(error)
  }
}

const partupSolicitDispatcher = new Dispatcher(findPending)
export const getSolicitsForPartup: SolicitAction = async (
  { commit, state },
  partupId: string
) => {
  try {
    const { data } = await partupSolicitDispatcher.cancel().exec({
      entity: SOLICIT_ENTITIES.partup,
      // eslint-disable-next-line
      entity_uuid: partupId,
    })

    const cache = getPartupCache(state, partupId)

    if (!cache) {
      commit('initializePartupCache', partupId)
    }

    commit('setPartupSolicits', {
      id: partupId,
      solicits: data,
    })
  } catch (error) {
    logger.error(error)
  }
}

export const receiveSolicit: SolicitAction = (
  { commit, rootGetters, state },
  solicit
) => {
  const currentUser = rootGetters['currentUser']

  if (isForUser(solicit, currentUser)) {
    commit('addUserSolicit', solicit)
  } else if (isForPartup(solicit) || isForActivity(solicit)) {
    const partupId = solicit.partup.uuid
    const cache = getPartupCache(state, partupId)

    if (!cache) {
      commit('initializePartupCache', partupId)
    }

    commit('addPartupSolicit', { id: partupId, solicit })
  }
}

export const removeSolicit: SolicitAction = (
  { commit, rootGetters },
  solicit
) => {
  const currentUser = rootGetters['currentUser']

  if (isForUser(solicit, currentUser)) {
    commit('removeUserSolicit', solicit)
  } else if (isForPartup(solicit) || isForActivity(solicit)) {
    commit('removePartupSolicit', solicit)
  }
}
