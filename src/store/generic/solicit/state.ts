import Vue from 'vue'
import ObjectCache from '@/util/store/cache/object-cache'
import { StoreModuleSolicitState } from './types'

const state = (): StoreModuleSolicitState => ({
  user: new ObjectCache(),
  partups: Vue.observable({}),
})

export default state
