export const isSectionExpanded = (state) => {
  return (section) => {
    const layoutSection = state.layouts[state.current][section]
    return layoutSection && layoutSection.expanded
  }
}
