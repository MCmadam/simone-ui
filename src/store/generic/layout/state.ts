export default {
  current: null,

  layouts: {
    default: {
      aside: {
        expanded: null,
      },
      header: {
        expanded: null,
      },
      userNavigation: {
        expanded: null,
      },
      userNavigationMobile: {
        expanded: null,
      },
    },
  },

  views: {
    /*
      [view]: {
        defaults: {

        },
      }
    */
  },
}
