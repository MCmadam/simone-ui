export const configure = ({ commit }, { layout, config }) => {
  commit('config', {
    layout,
    config,
  })
}

export const setDefaults = ({ commit }, { view, config }) => {
  commit('defaults', {
    view,
    config,
  })
}

export const loadPreferences = (
  { commit, state, rootState },
  { view, uuid }
) => {
  const layout = state.current

  const staticPrefs = rootState.preferences.layout.layouts[layout]
  const viewPrefs = rootState.preferences.layout.views[view]

  const config = {
    ...staticPrefs,
  }

  if (viewPrefs) {
    if (uuid) {
      Object.assign(config, viewPrefs[uuid])
    } else {
      Object.assign(config, viewPrefs)
    }
  } else {
    const defaultPrefs = state.views[view].defaults
    Object.assign(config, defaultPrefs)
  }

  commit('config', {
    layout,
    config,
  })
}

export const setSectionExpanded = (
  { commit, dispatch, state },
  { expanded, section, view, uuid, preference = true }
) => {
  const layout = state.current

  commit('sectionExpanded', {
    expanded,
    layout,
    section,
  })

  if (preference) {
    dispatch(
      'preferences/layout/setSectionExpanded',
      {
        expanded,
        layout,
        section,
        view,
        uuid,
      },
      { root: true }
    )
  }
}

export const setLayout = ({ commit }, layout) => {
  commit('layout', layout)
}
