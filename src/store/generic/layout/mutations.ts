import { ensurePath } from '@/util'

export const config = (state, { layout, config }) => {
  const found = ensurePath(state, `layouts.${layout}`)

  for (const key in config) {
    if (!found[key]) {
      found[key] = {}
    }
    for (const x in config[key]) {
      found[key][x] = config[key][x]
    }
  }
}

export const defaults = (state, { view, config }) => {
  const found = ensurePath(state, `views.${view}`)

  if (found) {
    found.defaults = config
  } else {
    // @TODO: add debug.
  }
}

export const layout = (state, layout) => {
  state.current = layout
}

export const sectionExpanded = (state, { expanded, layout, section }) => {
  const found = ensurePath(state, `layouts.${layout}.${section}`)
  found.expanded = expanded
}
