import loadLocale from '@/util/lang/load-locale'

export const setLocale = async ({ commit }, locale: string) => {
  commit('locale', await loadLocale(locale))
}
