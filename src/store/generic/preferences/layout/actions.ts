export const setSectionExpanded = (
  { commit },
  { expanded, layout, section, view, uuid }
) => {
  commit('sectionExpanded', {
    expanded,
    layout,
    section,
    view,
    uuid,
  })
}
