import { ensurePath } from '@/util'

const isStaticSection = (state, layout, section) => {
  const staticConfig = state.layouts[layout]
  return staticConfig && staticConfig[section]
}

export const sectionExpanded = (
  state,
  { expanded, layout, section, view, uuid }
) => {
  if (isStaticSection(state, layout, section)) {
    state.layouts[layout][section].expanded = expanded
  } else if (view) {
    const found = ensurePath(
      state,
      `views.${view}${uuid ? `.${uuid}` : ''}.${section}`
    )
    found.expanded = expanded
  }
}
