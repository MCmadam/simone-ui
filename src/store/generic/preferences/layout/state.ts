export default {
  layouts: {
    default: {
      userNavigation: {
        expanded: null,
      },
    },
  },
  views: {
    /*
      [view]: {
        [uuid]?: {
          some views are by uuid, others don't use uuids as extra pref.

          aside|header|...: {
            expanded: boolean,
          }
        }
      }
    */
  },
}
