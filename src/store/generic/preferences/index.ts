import * as actions from './actions'
// import * as getters from './getters'
import layout from './layout'
import * as mutations from './mutations'
import state from './state'

export default {
  namespaced: true,

  actions,
  // getters,
  modules: {
    layout,
  },
  mutations,
  state,
}
