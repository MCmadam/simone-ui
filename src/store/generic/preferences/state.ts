import defaultLocale from '@/lib/lang/default-locale'

export default {
  locale: defaultLocale,
}
