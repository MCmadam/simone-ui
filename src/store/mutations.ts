import { AppBranding } from '@/types'

/*
 * global mutations
 **/

import { updateField as updateFieldMutation } from 'vuex-map-fields'
export const updateField = updateFieldMutation

export const branding = (state, branding: AppBranding) => {
  state.branding = branding
}

export const brandingContext = (state, context) => {
  state.brandingContext = context
}

export const layout = (state, layout) => {
  state.layout = layout
}

export const currentOrganizationUuid = (state, uuid) => {
  state.currentOrganizationUuid = uuid
}

export const searchToolbarActive = (state, active) => {
  state.searchToolbarActive = active
}
