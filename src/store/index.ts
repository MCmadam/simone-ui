import createPersistedState from 'vuex-persistedstate'

import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import state from './state'

import generic from './generic'
import views from './views'

import { Store } from 'vuex'

export default new Store({
  actions,
  getters,
  modules: {
    ...generic,
    views,
  },
  mutations,
  plugins: [
    createPersistedState({
      key: 'partup-state',
      paths: [
        'auth.loggedIn', // @TODO: loggedIn will no longer be stored.
        'user.user', // Only store essential user data!
        'preferences',
      ],
    }),
  ],
  state,
})
