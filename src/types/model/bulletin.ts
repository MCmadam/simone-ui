import { Identifiable } from '@/types/model'

/* eslint-disable camelcase */
export type BackendBulletin = {
  uuid: string
  bulletin_type: string
  params: object | null
  shown_at: string
}
/* eslint-enable camelcase */

export type Bulletin = {
  scope: string
  type: string
  params: object
} & Identifiable

export enum BULLETIN_SCOPES {
  organization = 'organization',
  user = 'user',
}

export enum BULLETIN_TYPES_ORGANIZATION {
  invitation = 'invitation',
  welcome = 'welcome',
  involvement = 'involvement',
}

export enum BULLETIN_TYPES_USER {
  welcome = 'welcome',
}
