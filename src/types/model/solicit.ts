export enum SOLICIT_ACTIONS {
  accept = 'accept',
  reject = 'reject',
}

export enum SOLICIT_ENTITIES {
  activity = 'activity',
  organization = 'organization',
  partup = 'partup',
}

export enum SOLICIT_TYPES {
  invite = 'invite',
  request = 'request',
}
