import { Uuid, UTCString } from '@/types/common'
import { Attachment, BackendAttachment } from './attachment'

/* eslint-disable camelcase */
export type BackendMessage = {
  changed_at: string,
  partup_uuid: string,
  posted_at: string,
  tagged_activities: unknown[],
  tagged_attachments: BackendAttachment[],
  tagged_users: unknown[],
  text: string,
  uuid: string,
  user: unknown,
}
/* eslint-enable camelcase */

export type Message = {
  activities?: unknown[],
  attachments?: Attachment[],
  partupUuid: Uuid,
  postedAt: UTCString,
  postedBy: unknown,
  text: string,
  uuid: Uuid,
}
