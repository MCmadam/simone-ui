export interface Identifiable {
  uuid: string
}

export interface Sluggable {
  slug: string
}

export type Findable = Identifiable | Sluggable
