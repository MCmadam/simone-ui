export enum PARTUP_VISIBILITY {
  public = 'public',
  open = 'open',
  organization = 'organization',
  closed = 'closed',
}

export enum PARTUP_ALLOW_SOLICIT_REQUEST {
  public = 'public',
  open = 'open',
  organization = 'organization',
  closed = 'closed',
}

export enum PARTUP_USER_ROLES {
  partner = 'partner',
}
