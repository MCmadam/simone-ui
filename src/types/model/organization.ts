export enum ORGANIZATION_VISIBILITY {
  public = 'public',
  open = 'open',
  closed = 'closed',
}

export enum ORGANIZATION_USER_ROLES {
  admin = 'admin',
  member = 'member',
}
