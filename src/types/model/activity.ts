export enum ACTIVITY_STATUS {
  none = 'none',
  todo = 'todo',
  doing = 'doing',
  done = 'done',
}
