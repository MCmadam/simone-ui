export enum USER_VISIBILITY {
  public = 'public',
  open = 'open',
  closed = 'closed',
}
