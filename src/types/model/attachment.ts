import { Uuid } from '@/types/common'

export enum ATTACHMENT_PROVIDERS {
  googledocs = 'google-docs',
  googledrive = 'google-drive',
  dropbox = 'dropbox',
  slack = 'slack',
  onedrive = 'onedrive',
  other = 'other',
}

export enum ATTACHMENT_TYPES {
  file = 'file',
  document = 'document',
  sheet = 'sheet',
  presentation = 'presentation',
}

/* eslint-disable camelcase */
export type BackendAttachment = {
  mime_type: string
  type: string
  pinned: boolean
  provider: string
  name: string
  url: string
  uuid: string
}
/* eslint-enable camelcase */

export type Attachment = {
  type: ATTACHMENT_TYPES
  name: string
  pinned: boolean
  provider: ATTACHMENT_PROVIDERS
  url: string
  uuid: Uuid
}
