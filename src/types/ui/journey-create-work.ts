export type BacklogItem = {
  uuid: string
  name: string
  // eslint-disable-next-line
  end_date: string | null,
}
