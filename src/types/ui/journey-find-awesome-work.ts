import { MatchingActivity } from '@/types/data'

export enum CAROUSEL_ITEM_TYPES {
  card = 'card',
  summary = 'summary',
}

export type CarouselCard = {
  type: CAROUSEL_ITEM_TYPES.card
  affordances: string[]
  reasons: string[]
  data: MatchingActivity
}

export type CarouselSummary = {
  type: CAROUSEL_ITEM_TYPES.summary
  data: MatchingActivity[]
}

export type CarouselItem = CarouselCard | CarouselSummary
