export type Dictionary<T> = {
  [key: string]: T
}

export type ValueOf<T> = T[keyof T]
