import { BackendMessage } from '@/types/model'

export type NewMessageEvent = {
  messages: BackendMessage[],
}
