import { AxiosTransformer, CancelToken, CancelTokenSource } from 'axios'
import { Dictionary } from './generic'
import ApiError from '@/util/error/api-error'
import CognitoError from '@/util/error/cognito-error'

// #region API

export interface ApiClient {
  del: <T>(path: string, config: RequestConfig) => Promise<ApiResponse<T>>
  head: <T>(path: string, config: RequestConfig) => Promise<ApiResponse<T>>
  get: <T>(path: string, config: RequestConfig) => Promise<ApiResponse<T>>
  patch: <T>(path: string, config: RequestConfig) => Promise<ApiResponse<T>>
  post: <T>(path: string, config: RequestConfig) => Promise<ApiResponse<T>>
  put: <T>(path: string, config: RequestConfig) => Promise<ApiResponse<T>>
}

export interface ApiOptions extends RequestServiceInfo {
  customHeader?: Function
  endpoint: string
}

export type ApiResponse<T> = {
  data: T
}

export enum API_RESPONSE_STATES {
  error = 'error',
  pending = 'pending',
  success = 'success',
}

export type ApiResponseState = {
  [API_RESPONSE_STATES.error]?: ApiError | CognitoError
  [API_RESPONSE_STATES.pending]: boolean
  [API_RESPONSE_STATES.success]?: boolean
}

// #endregion

// #region Request

export interface RequestCancelable {
  cancelToken?: CancelToken
}

export interface RequestConfig<Data = any, Query = any, Headers = any>
  extends RequestCancelable {
  headers?: Dictionary<Headers>
  data?: Dictionary<Data>
  query?: Dictionary<Query>
  transformResponse?: AxiosTransformer | AxiosTransformer[]
  transformRequest?: AxiosTransformer | AxiosTransformer[]
}

export type RequestMethod =
  | 'get'
  | 'GET'
  | 'delete'
  | 'DELETE'
  | 'head'
  | 'HEAD'
  | 'options'
  | 'OPTIONS'
  | 'post'
  | 'POST'
  | 'put'
  | 'PUT'
  | 'patch'
  | 'PATCH'

export interface RequestServiceInfo {
  region: string
  service: string
}

export interface RequestSigner {
  sign(
    requestConfig: any,
    accessInfo: any,
    serviceInfo: RequestServiceInfo
  ): any
  signUrl(
    url: string,
    accessInfo: any,
    serviceInfo?: RequestServiceInfo,
    expiration?: number
  ): any
}

// #endregion

// #region dispatcher

export type DispatcherRequest = {
  id: number
  cancelSource: CancelTokenSource
  status: REQUEST_STATUS
  params?: any
  init?: any
}

export enum REQUEST_STATUS {
  canceled = 'canceled',
  initialized = 'initialized',
  sent = 'sent',
  received = 'received',
}

// #endregion
