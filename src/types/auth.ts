export interface Credentials {
  accessKeyId: string
  authenticated: boolean
  identityId: string
  secretAccessKey: string
  sessionToken: string
}

export interface CredentialManager {
  get(): Promise<Credentials>
}

export enum AUTH_ACTIONS {
  forgotPassword = 'forgotPassword',
  forgotPasswordSubmit = 'forgotPasswordSubmit',
  login = 'login',
  register = 'register',
  registerConfirm = 'registerConfirm',
  registerResend = 'registerResend',
}

export type AuthFormFields = {
  cognitoUserId: string
  email: string
  verificationCode: number
}
