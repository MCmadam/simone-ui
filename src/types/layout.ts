export enum LAYOUT_DEFAULT_SECTIONS {
  aside = 'aside',
  header = 'header',
  content = 'content',
  userNavigation = 'userNavigation',
  userNavigationMobile = 'userNavigationMobile',
}
