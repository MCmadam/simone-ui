export interface AppBranding {
  images: {
    background: string
    logo: string
  }
  payoff: string
  name: string
}
