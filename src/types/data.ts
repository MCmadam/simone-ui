import { ORGANIZATION_VISIBILITY } from './model'

/* eslint-disable camelcase */
import { Attachment, Message } from './model'
import { Uuid } from '@/types'

export type MatchingActivity = {
  activity: any
  partup?: any
  solicit?: any
}

export type MatchingActivityResponse = {
  affordances: []
  reasons: []
} & MatchingActivity

export type OrganizationData = {
  auto_add_user: boolean
  background_url: string
  domains: string
  introduction: string
  logo_url: string
  name: string
  payoff: string
  slug: string
  solicited_user_invitation: string
  user_welcome: string
  visibility: ORGANIZATION_VISIBILITY
}

export type MessageCreateForPartupData = {
  activities?: Uuid[]
  attachments?: Attachment[]
  text: string
}

export type MessageCreateForPartupOptions = {
  data: MessageCreateForPartupData
}

export type MessageFindForPartupQuery = {
  activity?: Uuid
  solicit?: Uuid
}

export type MessageFindForPartupOptions = {
  query?: MessageFindForPartupQuery
}
