export const clickedInElement = (el, x, y) => {
  const { top, right, bottom, left } = el.getBoundingClientRect()

  return y >= top && y <= bottom && x >= left && x <= right
}

export const clickedInElements = (elements, x, y) => {
  let el
  for (el of elements) {
    if (clickedInElement(el, x, y)) {
      return true
    }
  }
  return false
}
