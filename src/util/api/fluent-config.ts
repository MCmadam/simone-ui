import { RequestConfig } from '@/types'
import snakeCase from 'lodash/snakeCase'
import reduce from 'lodash/reduce'

export default class FluentConfig<
  T extends RequestConfig,
  C extends RequestConfig
> {
  private config: RequestConfig = {}
  private options?: T

  constructor(options?: T, custom?: C) {
    this.options = Object.assign({}, options, custom)
    this.defaults(options)
  }

  public data<K extends keyof T>(key: K, transform?: (arg: T[K]) => any) {
    if (this.options) {
      const data: any = this.options[key]

      if (data) {
        let transformed = reduce(
          data,
          (total, val, key) => {
            total[snakeCase(key)] = val
            return total
          },
          {}
        )

        if (transform) {
          transformed = transform(Object.assign({}, data))
        }

        this.config.data = transformed
      }
    }

    return this
  }

  public query<K extends keyof T>(key: K, transform?: (arg: T[K]) => any) {
    if (this.options) {
      const query: any = this.options[key]

      if (query) {
        let transformed = reduce(
          query,
          (total, val, key) => {
            total[snakeCase(key)] = val
            return total
          },
          {}
        )

        if (transform) {
          transformed = transform(Object.assign({}, query))
        }

        this.config.query = transformed
      }
    }

    return this
  }

  public transformRequest(fn) {
    this.config.transformRequest = fn
    return this
  }

  public transformResponse(fn) {
    this.config.transformResponse = (data) => {
      if (data && data.data) {
        data.data = fn(data.data)
      }
      return data
    }

    return this
  }

  public toConfig() {
    return this.config
  }

  private defaults(options?: T): void {
    if (!options) {
      return
    }

    if (options.cancelToken) {
      this.config.cancelToken = options.cancelToken
    }

    if (options.data) {
      this.data('data')
    }

    if (options.query) {
      this.query('query')
    }
  }
}
