import FluentConfig from './fluent-config'
import { Findable, Identifiable, Sluggable } from '@/types'

export const makeFluentConfig = <T, C>(options?: T, custom?: C) => {
  return new FluentConfig(options, custom)
}

export const getIdentifier = <T extends Findable>(params: T) => {
  return (params as Identifiable).uuid || (params as Sluggable).slug
}
