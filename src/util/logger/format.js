/* eslint-disable require-jsdoc */

const isValid = (format) => {
  return (
    format && 'transform' in format && typeof format.transform === 'function'
  )
}

class LogFormat {
  constructor(formatter, options = {}) {
    this.options = options
    this.formatter = formatter
  }

  transform(info, options) {
    return this.formatter(info, options || this.options)
  }
}

export { isValid }
export default (formatter) => {
  return (options) => {
    return new LogFormat(formatter, options)
  }
}
