import { consoleLevels } from './log-levels'
import { isValid } from './format'

/**
 * Logger
 * @class
 */
class Logger {
  /**
   * constructor
   * @param {LoggerOptions} options logger options
   */
  constructor(options = {}) {
    this.transports = []
    this.format = null
    this.configure(options)
  }

  /**
   * configure
   * @public
   * @param {LoggerOptions} options logger options
   * @return {Logger} this
   */
  configure({ defaultMeta, format, level, levels, transports }) {
    if (defaultMeta && defaultMeta.args) {
      // console.warn('logger.configure defaultMeta has args which will be overriden by applyMeta, choose another key instead')
    }

    if (format !== undefined) {
      if (isValid(format)) {
        this.format = format
      } else {
        this.format = null
        // console.warn('logger.configure format is not valid')
      }
    }

    this.defaultMeta = defaultMeta || null
    this.level = level

    this.setLevels(levels || consoleLevels)

    if (transports) {
      if (this.transports.length) {
        this.clear()
      }

      for (const transport of Array.isArray(transports)
        ? transports
        : [transports]) {
        this.add(transport)
      }
    }

    return this
  }

  /**
   * _log
   * @private
   * @param  {...any} args [level, message, ...rest]
   * @return {Logger} this
   */
  _log(...args) {
    const [level, message, ...rest] = args

    let log = {}

    if (args.length === 1) {
      if (typeof level === 'object') {
        log = level
      } else {
        log.message = level
      }
    } else {
      if (typeof message === 'object') {
        log = message
      } else {
        log.message = message
      }
      log.level = level
    }

    this.applyMeta(log, ...rest)

    if (this.format) {
      log = this.format.transform(log, this.format.options)
    }
    for (const transport of this.transports) {
      transport.write(log)
    }

    return this
  }

  /**
   * this is needed for // console.log immitation
   * @param  {...any} args
   * @return {Logger} this
   */
  log(...args) {
    this._log(null, ...args)
    return this
  }

  /**
   * add a transport to this logger
   * @param {Transport} transport
   * @return {Logger} this
   */
  add(transport) {
    this.transports.push(transport)
    return this
  }

  /**
   * remove a transport from this logger
   * @param {Transport} transport
   * @return {Logger} this
   */
  remove(transport) {
    this.transports.splice(this.transports.indexOf(transport), 1)
    return this
  }

  /**
   * remove all transports from this logger
   * @return {Logger} this
   */
  clear() {
    while (this.transports.length > 0) {
      this.transports.shift()
    }
    return this
  }

  /**
   * an object of all levels for this logger
   * @param {object} levels, key: level, value: severity
   */
  setLevels(levels = {}) {
    for (const key in this.levels) {
      if (key === 'log') {
        continue
      }
      if (key in this) {
        delete this[key]
      }
    }

    for (const level in levels) {
      if (level === 'log') {
        continue
      }

      this[level] = function(...args) {
        this._log(level, ...args)
      }
    }

    this.levels = levels || null
  }

  /* eslint-disable require-jsdoc */
  applyMeta(log, ...args) {
    if (!log.meta) {
      Object.assign(log, {
        meta: {},
      })
    }

    let parsed = {}

    if (args.length === 1) {
      const arg = args[0]
      parsed.args = Array.isArray(arg) ? arg : [arg]
    } else if (args.length > 1) {
      parsed = {
        args,
      }
    }

    Object.assign(log.meta, this.defaultMeta || {}, parsed)
    return this
  }
  /* eslint-enable require-jsdoc */
}

export default (options) => new Logger(options)
