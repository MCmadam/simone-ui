/* eslint-disable require-jsdoc */
import get from 'lodash/get'
import each from 'lodash/each'
import createLogger from './logger'

/**
 * LoggerContainer
 * @class
 */
export default class LoggerContainer {
  /**
   * constructor
   * @param {LoggerOptions} options
   */
  constructor(options = {}) {
    this.loggers = new Map()
    this.options = options
  }

  /**
   * add a logger
   * @param {string} id logger id
   * @param {LoggerOptions} options for logger instance
   * @return {Logger} logger instance
   */
  add(id, options) {
    if (!this.loggers.has(id)) {
      const loggerOptions = {}

      Object.assign(loggerOptions, options || this.options, {
        transports: get(
          options,
          'transports',
          get(this.options, 'transports', [])
        ).slice(),
      })

      const logger = createLogger(loggerOptions)
      this.loggers.set(id, logger)
    }

    return this.loggers.get(id)
  }

  get(id, options) {
    return this.add(id, options)
  }

  has(id) {
    return this.loggers.has(id)
  }

  close(id = null) {
    if (id) {
      return this._removeLogger(id)
    }

    each(this.loggers, (_, key) => this._removeLogger(key))
  }

  _removeLogger(id) {
    if (!this.loggers.has(id)) {
      return
    }

    this.loggers.get(id).close()
    this.loggers.delete(id)
  }
}
