import clone from 'lodash/clone'
import { isValid } from './format'

/**
 * Transport
 * @class
 */
export default class Transport {
  /**
   * constructor
   * @param {TransportOptions} options
   */
  constructor(options = {}) {
    const { level, format, ...rest } = options

    if (format && !isValid(format)) {
      throw new Error(
        'Transport has received an invalid format, a format must have a transform function'
      )
    }

    this.level = level
    this.format = format

    this.options = {
      level,
      ...rest,
    }
  }

  /**
   * transform
   * @param {log} log
   * @return {log} transformed log
   */
  transform(log = {}) {
    return this.format ? this.format.transform(clone(log), this.options) : log
  }

  /**
   * write
   * @param {log} log
   */
  write(log = {}) {
    this.log(this.transform(log))
  }

  /**
   * no-op log
   * @abstract
   */
  log() {
    void 0
  }
}
