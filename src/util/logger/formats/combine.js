import format, { isValid } from '@/util/logger/format.js'

export default (...formats) => {
  if (!formats.every(isValid)) {
    return
  }

  const combine = format((info) => {
    let data = info

    for (const f of formats) {
      data = f.transform(data, f.options)

      if (!data) {
        return false
      }
    }

    return data
  })

  return combine()
}
