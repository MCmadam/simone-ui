/*
 * Logger unit tests
 */

/* eslint-disable */
/* eslint-env jest */

import createLogger from './logger'
import ConsoleTransport from './transports/console'

describe('Logger', () => {
  jest.spyOn(console, 'warn').mockImplementation()

  const makeTestTransport = () => {
    const transform = (v) => v
    const log = (v) => (expectLog = v)
    const write = (v) => log(v)

    return {
      transform,
      log,
      write,
    }
  }

  let expectLog
  let testTransport
  let logger

  beforeEach(() => {
    expectLog = null
    testTransport = makeTestTransport()

    logger = createLogger({
      transports: [testTransport],
    })
  })

  it('can be used with all given levels', () => {
    expect.assertions(3)

    logger = createLogger({
      levels: {
        warn: 1,
        debug: 0,
        error: 2,
      },
      transports: [testTransport],
    })

    logger.warn('warn')
    expect(expectLog).toHaveProperty('message', 'warn')
    logger.debug('debug')
    expect(expectLog).toHaveProperty('message', 'debug')
    logger.error('error')
    expect(expectLog).toHaveProperty('message', 'error')
  })

  it('can log an instance of Error', () => {
    expect.assertions(1)

    const message = 'test'
    const err = new Error(message)
    logger.log(err)

    expect(expectLog).toMatchObject({
      message,
    })
  })

  describe('constructor', () => {
    it('can be constructed without parameters', () => {
      expect.assertions(1)

      expect(() => {
        createLogger()
      }).not.toThrow()
    })

    it('has an empty transports array after construction', () => {
      expect.assertions(2)

      const sut = createLogger()

      expect(sut.transports).toBeInstanceOf(Array)
      expect(sut.transports).toHaveLength(0)
    })

    // it(`calls configure with options`, () => {
    //   // expect.assertions(1)
    // })
  })

  describe('configure', () => {
    it("set's defaultMeta", () => {
      expect.assertions(1)

      const defaultMeta = {
        a: 'b',
        b: 'c',
      }
      logger = createLogger({
        defaultMeta,
      })

      expect(logger.defaultMeta).toBe(defaultMeta)
    })

    it("set's format", () => {
      expect.assertions(1)

      const format = {
        transform: jest.fn(),
      }
      const sut = createLogger({
        format,
      })

      expect(sut.format).toBe(format)
    })

    it("set's level", () => {
      expect.assertions(1)

      const level = 'debug'
      logger.configure({
        level,
      })

      expect(logger.level).toBe(level)
    })

    it('creates a function per level', () => {
      expect.assertions(2)

      logger = createLogger({
        levels: {
          debug: 1,
        },
      })

      expect(logger).toHaveProperty('debug')
      expect(typeof logger.debug).toBe('function')
    })

    it('removes old levels when new given', () => {
      expect.assertions(4)

      logger = createLogger({
        levels: {
          debug: 1,
        },
      })

      expect(typeof logger.debug).toBe('function')

      logger.configure({
        levels: {
          info: 1,
          error: 2,
        },
      })

      expect(typeof logger.error).toBe('function')
      expect(typeof logger.info).toBe('function')
      expect(logger).not.toHaveProperty('debug')
    })

    it('replaces existing transports with given ones', () => {
      expect.assertions(1)

      const sut = createLogger({
        transports: [
          new ConsoleTransport(),
          new ConsoleTransport(),
          new ConsoleTransport(),
        ],
      })

      sut.configure({
        transports: [new ConsoleTransport()],
      })

      expect(sut.transports).toHaveLength(1)
    })

    it('checks for validity when configuring the logger with a format', () => {
      expect.assertions(1)

      logger.configure({
        format: {},
      })

      expect(logger).toHaveProperty('format', null)
    })

    it("overrides an existing format when 'null' is given to configure", () => {
      expect.assertions(2)

      const format = {
        transform: () => {},
      }
      logger.configure({
        format,
      })
      expect(logger).toHaveProperty('format', format)

      logger.configure({
        format: null,
      })
      expect(logger).toHaveProperty('format', null)
    })
  })

  describe('levels', () => {
    it("skips setting 'log' as level", () => {
      expect.assertions(1)

      const levels = {
        log: 0,
        info: 1,
      }

      const logFn = logger.log
      logger.setLevels(levels)
      expect(logger.log).toBe(logFn)
    })
  })

  describe('meta', () => {
    let defaultMeta

    beforeEach(() => {
      defaultMeta = {
        a: 'b',
        b: 'c',
      }
    })

    it('adds a meta key to every log', () => {
      expect.assertions(1)
      const log = {}
      logger.applyMeta(log)
      expect(log).toHaveProperty('meta', {})
    })

    it('adds defaultMeta to a log', () => {
      expect.assertions(1)

      logger.configure({
        defaultMeta,
      })

      const log = {}
      logger.applyMeta(log)
      expect(log).toMatchObject({
        meta: defaultMeta,
      })
    })

    it('adds rest arguments to meta.args', () => {
      expect.assertions(1)

      const log = {}
      const arg = { name: 'test' }
      logger.applyMeta(log, arg)

      expect(log).toMatchObject({
        meta: {
          args: [arg],
        },
      })
    })

    it('adds both defaultMeta and args to a log', () => {
      expect.assertions(1)

      logger.configure({
        defaultMeta,
      })

      const log = {}
      const arg = { name: 'test' }

      logger.applyMeta(log, arg)

      expect(log).toMatchObject({
        meta: {
          ...defaultMeta,
          args: [arg],
        },
      })
    })

    it('overrides defaultMeta.args if an argument is present', () => {
      expect.assertions(1)

      logger.configure({
        defaultMeta: {
          args: ['test'],
        },
      })

      const log = {}
      const arg = { name: 'test' }

      logger.applyMeta(log, arg)

      expect(log).toMatchObject({
        meta: {
          args: [arg],
        },
      })
    })

    it('adds multiple args to the args key', () => {
      expect.assertions(1)

      const log = {}
      const args = [1, 2]
      logger.applyMeta(log, ...args)

      expect(log).toMatchObject({
        meta: {
          args,
        },
      })
    })
  })

  describe('transport', () => {
    it('can be added using add', () => {
      expect.assertions(2)

      // const transport = new ConsoleTransport()

      expect(logger.transports).toHaveLength(1)
      logger.add(testTransport)
      expect(logger.transports).toHaveLength(2)
    })

    it('can be removed using remove', () => {
      expect.assertions(3)

      expect(logger.transports).toHaveLength(1)
      logger.add(testTransport)
      expect(logger.transports).toHaveLength(2)
      logger.remove(testTransport)
      expect(logger.transports).toHaveLength(1)
    })

    it('can be cleared using clear', () => {
      expect.assertions(1)

      logger.clear()
      expect(logger.transports).toHaveLength(0)
    })
  })

  describe('_log', () => {
    it('when only 1 argument is object, it will be log', () => {
      expect.assertions(1)

      const log = {
        name: 'hello',
      }

      logger._log(log)
      expect(expectLog).toMatchObject({
        name: 'hello',
      })
    })

    it('when only 1 argument is string, it will be message', () => {
      expect.assertions(1)

      const message = 'hello'

      logger._log(message)
      expect(expectLog).toMatchObject({
        message,
      })
    })

    it('uses message as log when 2 arguments and message is object', () => {
      expect.assertions(1)

      const match = {
        level: 'level',
        message: 'message',
      }

      logger._log('level', {
        message: 'message',
      })
      expect(expectLog).toMatchObject(match)
    })

    // it(`always set's the given level`, () => {

    // })

    // it(`uses message as log when it's an object`, () => {

    // })

    // it(`only transforms when a valid transform is present`, () => {

    // })

    // it(`writes to all transports`, () => {

    // })
  })
})
