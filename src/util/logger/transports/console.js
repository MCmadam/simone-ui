import Transport from '../transport'

/**
 * Console transport
 */
class ConsoleTransport extends Transport {
  /**
   * log
   * @param {log} log
   */
  log(log = {}) {
    const {
      level,
      message,
      meta: { args = [] },
    } = log

    // if (process.env.NODE_ENV === 'production') {
    //   return
    // }

    /* eslint-disable no-console */
    if (level && console[level]) {
      console[level](message, ...args)
    } else {
      console.log(message, ...args)
    }
    /* eslint-enable no-console */
  }
}

export default ConsoleTransport
