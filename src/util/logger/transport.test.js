import Transport from './transport'
jest.dontMock('./transport')

/*
 * Transport unit tests
 */

/* eslint-env jest */

const makeSubject = (options) => {
  return new Transport(options)
}

describe('Transport', () => {
  describe('constructor', () => {
    it('can be constructed using the new keyword', () => {
      expect.assertions(1)

      const sut = makeSubject()
      expect(sut).toBeInstanceOf(Transport)
    })

    it('sets level given in options', () => {
      expect.assertions(1)

      const level = 'debug'
      const sut = makeSubject({
        level,
      })

      expect(sut.level).toBe(level)
    })

    it('sets format given in options', () => {
      expect.assertions(1)

      function testFormat() {
        return {
          transform: (v) => v,
        }
      }

      const TF = testFormat()
      const sut = makeSubject({
        format: TF,
      })
      expect(sut.format).toBe(TF)
    })

    it('throws when an invalid format is given', () => {
      expect.assertions(2)

      expect(() => {
        makeSubject({
          format: {},
        })
      }).toThrow()
      expect(() => {
        makeSubject({
          format: 'transform',
        })
      }).toThrow()
    })

    it('sets options given', () => {
      expect.assertions(1)

      const options = {
        a: 'b',
        b: 'c',
      }
      const sut = makeSubject(options)

      expect(sut.options).toMatchObject(options)
    })
  })

  describe('transform', () => {
    let log

    beforeEach(() => {
      log = {
        message: 'test',
      }
    })

    it('does not throw without a format', () => {
      expect.assertions(1)

      const sut = makeSubject()
      expect(() => {
        sut.transform(log)
      }).not.toThrow()
    })

    it('gives a copy of log with a format', () => {
      expect.assertions(2)

      const sut = makeSubject({
        format: {
          transform: (x) => x,
        },
      })

      const transformed = sut.transform(log)
      expect(transformed).not.toBe(log)
      expect(transformed).toMatchObject(log)
    })

    it('does not modify the log without a format', () => {
      expect.assertions(1)

      const sut = makeSubject()
      const transformed = sut.transform(log)
      expect(transformed).toBe(log)
    })
  })

  describe('write', () => {
    it('can write without a format', () => {
      expect.assertions(1)

      const log = {
        message: 'test',
      }
      const sut = makeSubject()
      expect(() => {
        sut.write(log)
      }).not.toThrow()
    })
  })
})
