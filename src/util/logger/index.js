import LoggerContainer from './container'
import * as levels from './log-levels'
import createLogger from './logger'
import ConsoleTransport from './transports/console'

/**
 * @typedef {Object} LogFormat
 * @property {Function} transform
 */

/**
 * @typedef {Object} LoggerOptions
 * @property {string[]} levels
 * @property {LogFormat} format
 * @property {Transport[]} transports
 */

const logger = {}
const defaultLogger = createLogger({
  transports: [new ConsoleTransport()],
})

logger.loggers = new LoggerContainer()

for (const method of [
  'log',
  'configure',
  ...Object.keys(levels.consoleLevels),
]) {
  logger[method] = (...args) => defaultLogger[method](...args)
}

for (const method of ['add', 'close', 'get']) {
  logger[method] = (...args) => logger.loggers[method](...args)
}

Object.defineProperty(logger, 'level', {
  get() {
    return defaultLogger.level
  },
  set(val) {
    defaultLogger.level = val
  },
})

export default logger
