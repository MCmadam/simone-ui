import DateTime from 'luxon/src/datetime'

export default (date) => {
  if (DateTime.isDateTime(date)) {
    return date
  } else if (date instanceof Date) {
    return DateTime.fromJSDate(date)
  } else if (typeof date === 'string') {
    // eslint-disable-next-line eqeqeq
    if (date == parseInt(date, 10).toString()) {
      return undefined
    }

    const parsed = DateTime.fromJSDate(new Date(date))

    if (parsed.isValid) {
      return parsed
    }
  }

  return undefined
}
