export const LITERALS = {
  default: '\u200A/\u200A',
}

/**
 * These formats are language independant
 * @see docs ~/docs/internationalization.md
 */
export default Object.freeze({
  default: `dd${LITERALS.default}LL${LITERALS.default}yyyy`,
  message: `dd${LITERALS.default}LL HH:mm`,
})
