export const composeRouteRedirect = (name, to) => ({
  name,
  query: {
    redirect: to.fullPath,
  },
})

export const redirect = (name, back = false) => {
  return (to, from, next) => {
    if (from.name === name) {
      next(false)
    } else {
      next(back ? composeRouteRedirect(name, to) : { name })
    }
  }
}
