export const toBool = (str) => {
  return str.toLowerCase() === 'true'
}
