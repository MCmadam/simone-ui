import replace from './replace'

describe('when I want to replace values in an array', () => {
  it('mutates the given source', () => {
    expect.assertions(1)

    const source = [1, 2]
    const currentValue = 1
    const newValue = 3

    replace(source, currentValue, newValue)

    expect(source).toMatchObject([3, 2])
  })

  it('returns the given source', () => {
    expect.assertions(1)

    const source = [1, 2]
    const currentValue = 1
    const newValue = 3

    const source2 = replace(source, currentValue, newValue)

    expect(source).toBe(source2)
  })

  it('will perform a deep comparison by default', () => {
    expect.assertions(1)

    const source = [
      {
        name: 'item1',
        meta: {
          something: 'useful',
        },
      },
      {
        name: 'item2',
        meta: {
          something: 'useless',
        },
      },
    ]

    const currentValue = {
      name: 'item1',
      meta: {
        something: 'useful',
      },
    }

    const newValue = {
      name: 'item3',
      meta: {
        something: 'different',
      },
    }

    replace(source, currentValue, newValue)

    expect(source).toMatchObject([
      {
        name: 'item3',
        meta: {
          something: 'different',
        },
      },
      {
        name: 'item2',
        meta: {
          something: 'useless',
        },
      },
    ])
  })

  it('can use a custom value comparator', () => {
    expect.assertions(1)

    const source = [
      {
        uuid: '1',
      },
      {
        uuid: '2',
      },
    ]
    const currentValue = {
      uuid: '1',
    }
    const newValue = {
      uuid: '3',
    }
    const comparator = (a, b) => a.uuid === b.uuid

    replace(source, currentValue, newValue, comparator)

    expect(source).toMatchObject([
      {
        uuid: '3',
      },
      {
        uuid: '2',
      },
    ])
  })

  it('replaces all occurances', () => {
    expect.assertions(1)

    const source = [1, 2, 1]
    const currentValue = 1
    const newValue = 3

    replace(source, currentValue, newValue)

    expect(source).toMatchObject([3, 2, 3])
  })
})
