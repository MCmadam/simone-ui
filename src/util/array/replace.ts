import isEqual from 'lodash/isEqual'

/**
 * Replace all occurances of in an array by using `valueComparator` for equality comparisons.
 *
 * **note**: this method mutates the array.
 *
 * @param {T[]} array the array to replace values in.
 * @param {T} target items will be compared against this target.
 * @param {T} replacer new items in source after replace.
 * @param {(item: T, target: T) => boolean} valueComparator comparator used for equality comparisons. Defaults to lodash/isEqual
 * @example
 *
 * replace([1, 2], 1, 3, (item, target) => item === target)
 * => results in [3, 2]
 */
export default <T>(
  array: T[],
  target: T,
  replacer: T,
  valueComparator: (item: T, target: T) => boolean = isEqual
) => {
  let i = 0
  for (; i < array.length; i += 1) {
    if (valueComparator(array[i], target)) {
      array[i] = replacer
    }
  }

  return array
}
