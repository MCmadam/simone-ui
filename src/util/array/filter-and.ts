/**
 * applies all predicates on each element
 * @returns true if all predicates return a truthy value, false otherwise
 */
export default <T>(...predicates: ((...args: T[]) => any)[]) => (
  ...args: T[]
) => {
  let isTrue = false

  for (const fn of predicates) {
    isTrue = Boolean(fn(...args))

    if (!isTrue) {
      return isTrue
    }
  }

  return isTrue
}
