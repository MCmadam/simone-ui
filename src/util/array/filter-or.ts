export default (...fns) => (...args) => {
  let matches = false

  for (const fn of fns) {
    matches = Boolean(fn(...args))

    if (matches) {
      return matches
    }
  }

  return matches
}
