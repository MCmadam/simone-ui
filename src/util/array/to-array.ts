const toArray = <T>(value: T | T[]) => {
  if (Array.isArray(value)) {
    return value
  } else if (value !== undefined && value !== null) {
    return [value]
  }

  return []
}

export default toArray
