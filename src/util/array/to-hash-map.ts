import reduce from 'lodash/reduce'

/**
 * Reduce an array of objects with a unique identifier to a hashmap.
 *
 * @template T an object with a unique identifier
 * @param {T} array array to reduce
 * @param {keyof T} key unique key of each item
 *
 * @example
 *
 * toHashMap([{ id: 1 }, { id: 2 }], 'id')
 * => Creates an object { '1': { id: 1 }, '2': { id: 2 } }
 */
export default <T extends object>(array: T[], key: keyof T) => {
  return reduce(
    array,
    (total, message) => {
      total[message[key.toString()]] = message
      return total
    },
    {}
  )
}
