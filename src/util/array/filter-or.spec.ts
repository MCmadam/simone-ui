import filterOr from './filter-or'

describe('filterOr', () => {
  it('includes all items that match one of the predecates', () => {
    expect.assertions(1)

    const result = [1, 2, 3].filter(
      filterOr(
        (x) => x === 1,
        (x) => x === 3
      )
    )

    expect(result).toMatchObject([1, 3])
  })

  it('includes no item if none of the predicate matches', () => {
    expect.assertions(1)

    const result = [1, 2, 3].filter(
      filterOr(
        (x) => x === 4,
        (x) => x === 5
      )
    )

    expect(result).toMatchObject([])
  })

  it('returns an empty array if no predicates given', () => {
    expect.assertions(1)

    const result = [1, 2, 3].filter(filterOr())

    expect(result).toMatchObject([])
  })
})
