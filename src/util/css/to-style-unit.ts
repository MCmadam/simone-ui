const validCssUnits = ['%', 'em', 'rem', 'px']

/**
 * Add a unit to a value to use it as CSS style on elements
 * @param {Number|String} value that requires a CSS unit
 * @param {String} unit that is a valid CSS unit, default is `px`
 * @return {String} value+unit
 */
export default (value, unit = 'px') => {
  let stripped = value
  if (typeof value === 'string') {
    stripped = value.replace(/%|(r)em|px/, '')
  }

  const parsed = parseFloat(stripped)
  if (!Number.isNaN(parsed)) {
    if (typeof unit !== 'string') {
      throw new Error('toStyleUnit: unit must be of type string')
    }
    const validUnit = validCssUnits.includes(unit) ? unit : 'px'
    return `${parsed}${validUnit}`
  }
}
