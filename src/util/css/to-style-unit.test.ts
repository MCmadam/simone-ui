/* eslint-env jest */
import toStyleUnit from './to-style-unit'

describe('toStyleUnit css util', () => {
  const defaultUnit = 'px'

  it("returns input with default unit 'px' if input is valid and unitless", () => {
    expect(toStyleUnit(1)).toBe(`1${defaultUnit}`)
  })

  it('accepts numbers as input', () => {
    expect.assertions(2)

    expect(toStyleUnit(1)).toBe(`1${defaultUnit}`)
    expect(toStyleUnit(2.3)).toBe(`2.3${defaultUnit}`)
  })

  it('accepts strings that can be converted to numbers as input', () => {
    expect(toStyleUnit('1')).toBe(`1${defaultUnit}`)
  })

  it('strips any unit when parsing and replaces it with unit param', () => {
    expect.assertions(2)

    expect(toStyleUnit('1em')).toBe(`1${defaultUnit}`)
    expect(toStyleUnit('3rem')).toBe(`3${defaultUnit}`)
  })

  it('returns undefined when input is not valid', () => {
    expect.assertions(2)

    expect(toStyleUnit('word!')).toBeUndefined()
    expect(toStyleUnit(true)).toBeUndefined()
  })

  it('accepts any valid CSS unit as unit parameter', () => {
    expect.assertions(4)
    const validCSSUnits = ['%', 'em', 'rem', '%']

    let unit
    for (unit of validCSSUnits) {
      expect(toStyleUnit(1, unit)).toBe(`1${unit}`)
    }
  })
})
