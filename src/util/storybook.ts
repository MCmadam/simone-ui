export const addKnobs = (knobs) => (props = {}) => {
  for (const key in knobs) {
    if (Object.prototype.hasOwnProperty.call(knobs, key) && !props[key]) {
      props[key] = {
        default: knobs[key],
      }
    }
  }

  return props
}

export const withNone = (obj) => {
  return {
    none: null,
    ...obj,
  }
}
