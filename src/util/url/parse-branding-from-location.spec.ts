import parseBrandingFromLocation from './parse-branding-from-location'

const setHost = (str) => {
  window.location.host = str
}

describe('parse branding', () => {
  const { location } = window

  beforeAll(() => {
    delete window.location

    // @ts-ignore
    window.location = {
      host: '',
    }
  })

  it('takes the first part', () => {
    expect.assertions(1)

    setHost('something.part-up.com')

    expect(parseBrandingFromLocation()).toBe('something')
  })

  it('defaults to app if first part is part-up', () => {
    expect.assertions(1)

    setHost('part-up.com')

    expect(parseBrandingFromLocation()).toBe('app')
  })

  it('defaults to app if first part is localhost', () => {
    expect.assertions(1)

    setHost('part-up.com')

    expect(parseBrandingFromLocation()).toBe('app')
  })
})
