import stripWWW from './strip-www'

const defaultBranding = 'app'

export default () => {
  const parts = window.location.host.split('.')

  stripWWW(parts)

  // 1. exclude localhost or
  // 2. part-up is the first item
  if (parts.length === 1 || parts[0] === 'part-up') {
    return defaultBranding
  } else {
    return parts[0]
  }
}
