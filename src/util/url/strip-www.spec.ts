import stripWWW from './strip-www'

describe('strip www', () => {
  it("takes parts and removes www if it's the first", () => {
    expect.assertions(1)

    const parts = ['www', 'aaa', 'www']

    expect(stripWWW(parts)).toMatchObject(
      expect.arrayContaining(['aaa', 'www'])
    )
  })
})
