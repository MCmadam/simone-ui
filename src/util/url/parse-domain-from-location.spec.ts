import parseDomainFromLocation from './parse-domain-from-location'

const setHost = (str) => {
  window.location.host = str
}

describe('parse domain from location', () => {
  beforeAll(() => {
    delete window.location

    // @ts-ignore
    window.location = {
      host: '',
    }
  })

  it('strips the first part', () => {
    expect.assertions(1)

    setHost('app.part-up.com')

    expect(parseDomainFromLocation()).toMatch('part-up.com')
  })

  it('includes any sub domain after the first', () => {
    expect.assertions(1)

    setHost('app.develop.test.part-up.com')

    expect(parseDomainFromLocation()).toMatch('develop.test.part-up.com')
  })

  it("doesn't strip part-up.com if first", () => {
    expect.assertions(1)

    setHost('part-up.com')

    expect(parseDomainFromLocation()).toMatch('part-up.com')
  })

  it("doesn't strip localhost", () => {
    expect.assertions(1)

    setHost('localhost:9000')

    expect(parseDomainFromLocation()).toMatch('localhost')
  })
})
