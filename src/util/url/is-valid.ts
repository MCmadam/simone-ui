
const pattern = /^(https?|ftp):\/\/[^\s$.?#].[^\s]*$/i

const isValid = (url: string) => {
  if (!url) {
    return false
  }
  return pattern.test(url.trim())
}

export default isValid
