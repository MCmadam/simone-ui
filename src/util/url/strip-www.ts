export default (parts: string[]) => {
  if (parts[0] === 'www') {
    parts.shift()
  }

  return parts
}
