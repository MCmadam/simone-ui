import stripWWW from './strip-www'

export default () => {
  const parts = window.location.host.split('.')

  stripWWW(parts)

  const last = parts[parts.length - 1]
  const portIndex = last.indexOf(':')
  if (portIndex > -1) {
    parts[parts.length - 1] = last.substr(0, portIndex)
  }

  if (parts[0] === 'localhost' || parts.length === 2) {
    return parts.join('.')
  }

  // the first part is always the branding
  parts.shift()

  return parts.join('.')
}
