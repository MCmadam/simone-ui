import vm from './vm'
import { baseStubs } from './stubs'
import createLocalVue from './create-local-vue'
import mount from './mount'
import shallowMount from './shallow-mount'

export { baseStubs, createLocalVue, mount, shallowMount, vm }

export default {
  baseStubs,
  createLocalVue,
  mount,
  shallowMount,
  vm,
}
