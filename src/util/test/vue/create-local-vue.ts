import { createLocalVue } from '@vue/test-utils'

export default (...plugins) => {
  const localVue = createLocalVue()

  for (const plugin of plugins) {
    if (Array.isArray(plugin)) {
      const [plug, options] = plugin
      localVue.use(plug, options)
    } else {
      localVue.use(plugin)
    }
  }

  return localVue
}
