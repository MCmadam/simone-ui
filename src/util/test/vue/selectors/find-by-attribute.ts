import Vue, { ComponentOptions, FunctionalComponentOptions } from 'vue'
import { Wrapper, VueClass, NameSelector, RefSelector } from '@vue/test-utils'

// @ts-ignore
function findByAttribute<T extends Vue, R extends Vue>(
  wrapper: Wrapper<T>,
  selector: VueClass<R>,
  attribute: string,
  value: string | RegExp
): Wrapper<R>
function findByAttribute<T extends Vue, R extends Vue>(
  wrapper: Wrapper<T>,
  selector: ComponentOptions<R>,
  attribute: string,
  value: string | RegExp
): Wrapper<R>
function findByAttribute<T extends Vue>(
  wrapper: Wrapper<T>,
  selector: FunctionalComponentOptions,
  attribute: string,
  value: string | RegExp
): Wrapper<Vue>
function findByAttribute<T extends Vue>(
  wrapper: Wrapper<T>,
  selector: RefSelector,
  attribute: string,
  value: string | RegExp
): Wrapper<Vue>
function findByAttribute<T extends Vue>(
  wrapper: Wrapper<T>,
  selector: NameSelector,
  attribute: string,
  value: string | RegExp
): Wrapper<Vue>
function findByAttribute<T extends Vue>(
  wrapper: Wrapper<T>,
  selector: string,
  attribute: string,
  value: string | RegExp
): Wrapper<Vue> {
  const result = wrapper.findAll(selector).filter((w) => {
    const attr = w.attributes(attribute)
    return attr && !!attr.match(value)
  })
  return result.wrappers[0]
}

export default findByAttribute
