import Vue, { ComponentOptions, FunctionalComponentOptions } from 'vue'
import { Wrapper, VueClass, NameSelector, RefSelector } from '@vue/test-utils'

// @ts-ignore
function findByContent<T extends Vue, R extends Vue>(
  wrapper: Wrapper<T>,
  selector: VueClass<R>,
  content: string | RegExp
): Wrapper<R>
function findByContent<T extends Vue, R extends Vue>(
  wrapper: Wrapper<T>,
  selector: ComponentOptions<R>,
  content: string | RegExp
): Wrapper<R>
function findByContent<T extends Vue>(
  wrapper: Wrapper<T>,
  selector: FunctionalComponentOptions,
  content: string | RegExp
): Wrapper<Vue>
function findByContent<T extends Vue>(
  wrapper: Wrapper<T>,
  selector: RefSelector,
  content: string | RegExp
): Wrapper<Vue>
function findByContent<T extends Vue>(
  wrapper: Wrapper<T>,
  selector: NameSelector,
  content: string | RegExp
): Wrapper<Vue>
function findByContent<T extends Vue>(
  wrapper: Wrapper<T>,
  selector: string,
  content: string | RegExp
): Wrapper<Vue> {
  const array = wrapper
    .findAll(selector)
    .filter((w) => w.html().match(content).length > 0)
  return array.wrappers[0]
}

export default findByContent
