import findByAttribute from './find-by-attribute'
import findByContent from './find-by-content'

export { findByAttribute, findByContent }

export default {
  findByAttribute,
  findByContent,
}
