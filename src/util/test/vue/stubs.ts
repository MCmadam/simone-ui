import { RouterLinkStub } from '@vue/test-utils'

import action from '@/components/util/action/action'
import avatar from '@/components/base/avatar/avatar.vue'
import button from '@/components/base/button/button.vue'
import date from '@/components/base/date/date.vue'
import icon from '@/components/base/icon/icon.vue'
import link from '@/components/base/link/link.vue'
import list from '@/components/base/list/list.vue'
import toolbar from '@/components/base/toolbar/toolbar.vue'

import form from '@/components/base/form/form.vue'
import textarea from '@/components/base/input/textarea/textarea.vue'
import textfield from '@/components/base/input/textfield/textfield.vue'
import contenteditable from '@/components/base/input/content-editable.vue'
import datepicker from '@/components/base/picker/date/date-picker.vue'

export const baseStubs = {
  'pu-action': action,
  'pu-avatar': avatar,
  'pu-button': button,
  'pu-date': date,
  'pu-icon': icon,
  'pu-link': link,
  'pu-list': list,
  'pu-toolbar': toolbar,
  'pu-form': form,
  'pu-textarea': textarea,
  'pu-textfield': textfield,
  'pu-content-editable': contenteditable,
  'pu-datepicker': datepicker,
  'router-link': RouterLinkStub,
}

export default {
  baseStubs,
}
