import Vue, { VueConstructor } from 'vue'
import { mount, MountOptions, Wrapper } from '@vue/test-utils'
import merge from 'lodash/merge'

import MockAbility from '@mocks/plugins/ability'
import MockI18n from '@mocks/plugins/i18n'
import { baseStubs } from './stubs'

export default <T extends Vue>(
  component: VueConstructor<T, Record<never, any>>,
  options: MountOptions<T> = {}
): Wrapper<T> => {
  const abilityMock = new MockAbility()
  const i18nMock = new MockI18n()

  return mount(
    component,
    merge(
      {
        stubs: baseStubs,
        mocks: {
          ...abilityMock.methods,
          ...i18nMock.methods,
        },
      },
      options
    )
  )
}
