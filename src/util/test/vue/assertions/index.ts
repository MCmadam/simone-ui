import Vue from 'vue'
import { Wrapper } from '@vue/test-utils'
import lazyModel from './lazy-model'

function assertions<T extends Vue>(wrapper: Wrapper<T>) {
  return {
    lazyModel: (value?: any, prop?: string) => lazyModel(wrapper, value, prop),
  }
}

export default assertions
