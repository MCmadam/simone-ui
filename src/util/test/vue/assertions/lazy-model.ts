import Vue from 'vue'
import { Wrapper } from '@vue/test-utils'
import { vm } from '@/util/test/vue'

export default <T extends Vue, V>(
  wrapper: Wrapper<T>,
  value?: V,
  prop = 'value'
) => {
  it('implements lazy model mixin', () => {
    expect.assertions(1)

    let expected = value

    if (value) {
      wrapper.setProps({
        [prop]: value,
      })
    } else {
      expected = wrapper.props(prop)
    }

    expect(vm(wrapper).internalValue).toBe(expected)
  })
}
