import Vue from 'vue'
import { Wrapper } from '@vue/test-utils'

export default <T extends Vue>(wrapper: Wrapper<T>): any => {
  return wrapper.vm as any
}
