import api from '@/api'

export const configureApi = () => {
  api.configure({
    endpoint: 'mocked',
    region: 'mocked',
    service: 'mocked',
  })
}
