const _dirRatio = 0.5
const _minOffset = 16

export const movedHorizontally = (
  { offsetX, offsetY },
  dirRatio = _dirRatio
) => {
  return Math.abs(offsetY) < dirRatio * Math.abs(offsetX)
}

export const movedVertically = ({ offsetX, offsetY }, dirRatio = _dirRatio) => {
  return Math.abs(offsetX) < dirRatio * Math.abs(offsetY)
}

export const swipeLeft = (
  { touchstartX, touchendX, offsetX, offsetY },
  minOffset = _minOffset,
  dirRatio = _dirRatio
) => {
  return (
    movedHorizontally({ offsetX, offsetY }, dirRatio) &&
    touchendX < touchstartX - minOffset
  )
}

export const swipeRight = (
  { touchstartX, touchendX, offsetX, offsetY },
  minOffset = _minOffset,
  dirRatio = _dirRatio
) => {
  return (
    movedHorizontally({ offsetX, offsetY }, dirRatio) &&
    touchendX > touchstartX + minOffset
  )
}

export const swipeUp = (
  { touchstartY, touchendY, offsetX, offsetY },
  minOffset = _minOffset,
  dirRatio = _dirRatio
) => {
  return (
    movedVertically({ offsetX, offsetY }, dirRatio) &&
    touchendY < touchstartY - minOffset
  )
}

export const swipeDown = (
  { touchstartY, touchendY, offsetX, offsetY },
  minOffset = _minOffset,
  dirRatio = _dirRatio
) => {
  return (
    movedVertically({ offsetX, offsetY }, dirRatio) &&
    touchendY > touchstartY + minOffset
  )
}
