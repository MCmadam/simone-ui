import i18n from '@/util/lang/i18n'
import reduce from 'lodash/reduce'
import { ACTIVITY_STATUS, PARTUP_ALLOW_SOLICIT_REQUEST } from '@/types'

export const ensurePath = (source, path, val = {}) => {
  const parts = path.split('.')

  return parts.reduce((total, part, index) => {
    if (!total[part]) {
      if (parts.length - 1 === index) {
        total[part] = val
      }
      total[part] = {}
    }

    return total[part]
  }, source)
}

export const nullable = {
  toLower: (str: string) =>
    typeof str === 'string' ? str.toLowerCase() : null,
}

export const equalRules = (a, b) => {
  if (a === b) {
    return true
  }
  if (a !== Object(a) || b !== Object(b)) {
    return false
  }

  const props = Object.keys(a)

  if (props.length !== Object.keys(b).length) {
    return false
  }

  return props.every((p) => {
    return equalRules(a[p].toString(), b[p].toString())
  })
}

export const visibilityOptions = (entity: string, levels: any) => {
  return reduce(
    levels,
    (total, value, key) => {
      total.push({
        key,
        value,
        label: i18n.t(`picker-visibility-${entity}-label-${key}`),
        description: i18n.t(`picker-visibility-${entity}-description-${key}`),
        icon: 'person',
      })

      return total
    },
    []
  )
}

const makeActivityStatusItems = () => {
  const items = []

  for (const key in ACTIVITY_STATUS) {
    const value = ACTIVITY_STATUS[key]
    const item: any = {
      key,
      value,
    }

    let icon

    switch (value) {
      case ACTIVITY_STATUS.none:
      case ACTIVITY_STATUS.todo:
      case ACTIVITY_STATUS.done:
        icon = 'status_circle_full'
        break
      case ACTIVITY_STATUS.doing:
        icon = 'status_circle_half'
        break
    }

    item.icon = icon

    items.push(item)
  }

  return items
}

export const activityPickerStatusItems = makeActivityStatusItems()

export const applyMixins = (derivedCtor: any, baseCtors: any[]) => {
  for (const baseCtor of baseCtors) {
    for (const name of Object.getOwnPropertyNames(baseCtor.prototype)) {
      Object.defineProperty(
        derivedCtor.prototype,
        name,
        Object.getOwnPropertyDescriptor(baseCtor.prototype, name)
      )
    }
  }
}

export const convertPartupRequestOption = (requestOptionOrBool) => {
  switch (requestOptionOrBool) {
    case true:
      return PARTUP_ALLOW_SOLICIT_REQUEST.open
    case false:
      return PARTUP_ALLOW_SOLICIT_REQUEST.organization
    case PARTUP_ALLOW_SOLICIT_REQUEST.public:
    case PARTUP_ALLOW_SOLICIT_REQUEST.open:
      return true
    case PARTUP_ALLOW_SOLICIT_REQUEST.organization:
    case PARTUP_ALLOW_SOLICIT_REQUEST.closed:
      return false
  }
}
