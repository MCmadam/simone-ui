import axios from 'axios'
import i18n from './i18n'
import Settings from 'luxon/src/settings'

export default (locale) => {
  i18n.locale = locale
  Settings.defaultLocale = locale
  axios.defaults.headers['Accept-Language'] = locale
  if (document) {
    document.querySelector('html').setAttribute('lang', locale)
  }
  return locale
}
