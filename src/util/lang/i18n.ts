import VueI18n from 'vue-i18n'
import defaultLocale from '@/lib/lang/default-locale'

/*
  locale and messages are being loaded in `boot.ts`
*/
export default new VueI18n({
  fallbackLocale: defaultLocale,
  locale: defaultLocale,
  messages: {},
})
