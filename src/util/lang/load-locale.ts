import i18n from './i18n'
import setLocale from './set-locale'
import supportedLocales from '@/lib/lang/supported-locales'

const loadedLocales = []

export default async (locale) => {
  if (supportedLocales.includes(locale)) {
    if (!loadedLocales.includes(locale)) {
      const messages = await import(
        /* webpackChunkName: "locale-[request]" */ `@/../locales/${locale}`
      )
      i18n.setLocaleMessage(locale, messages)
      loadedLocales.push(locale)
      return setLocale(locale)
    }

    return setLocale(locale)
  }

  return locale
}
