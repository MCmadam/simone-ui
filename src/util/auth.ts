import { AUTH_ACTIONS } from '@/types'

export const actionToStep = (action) => {
  switch (action) {
    case AUTH_ACTIONS.forgotPassword:
      return 5
    case AUTH_ACTIONS.forgotPasswordSubmit:
      return 6
    case AUTH_ACTIONS.register:
      return 2
    case AUTH_ACTIONS.registerConfirm:
      return 3
    case AUTH_ACTIONS.registerResend:
      return 4
    case AUTH_ACTIONS.login:
    default:
      return 1
  }
}

export const stepToAction = (step) => {
  switch (step) {
    case 5:
      return AUTH_ACTIONS.forgotPassword
    case 6:
      return AUTH_ACTIONS.forgotPasswordSubmit
    case 2:
      return AUTH_ACTIONS.register
    case 3:
      return AUTH_ACTIONS.registerConfirm
    case 4:
      return AUTH_ACTIONS.registerResend
    case 1:
    default:
      return AUTH_ACTIONS.login
  }
}
