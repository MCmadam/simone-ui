import { ValueOf } from '@/types'
import { ComposeQueryOption, ComposeQueryOptions } from './types'

/**
 * Compose an [`axios`]() query out of an object by removing all keys that match the options.
 */
const compose = <Input extends object>(
  input: Input,
  options: ComposeQueryOptions<Input>
): Partial<Input> => {
  let result = Object.create(null)

  if (input === undefined || input === null) {
    if (options.nullable) {
      return null
    } else {
      return result
    }
  }

  // 1. loop over input to validate properties
  for (const prop of Object.getOwnPropertyNames(input)) {
    // 2. skip functions
    if (typeof prop === 'function') {
      continue
    }

    const value: ValueOf<Input> = input[prop]

    if (value === undefined) {
      continue
    }

    let opts: ComposeQueryOption<Input>

    if (options.fields) {
      opts = options.fields[prop]
    }

    // 3. check for undefined and existance of field options
    if (!opts) {
      result[prop] = value
      continue
    }

    // // 4. transform the value
    // let transformed: TransformerReturnType<Input>
    if (opts.transform) {
      // console.warn('query compose transform not implemented')
      // value = opts.transform(value)
    }

    // 5. match against exclude rules
    const exclude = opts.exclude.some((fn) => fn(value))
    if (exclude) {
      continue
    }

    result[prop] = value
  }

  // 6. if transform replace result with transformed
  if (options.transform) {
    result = options.transform(Object.assign({}, result)) || {}
  }

  // 6. if no props are valid and result may be nullable return null
  if (options.nullable && Object.keys(result).length < 1) {
    return null
  }

  return result
}

/**
 * compose a query with options partially applied.
 * @template T query template
 * @param {ComposeQueryOptions} options pre-applied options
 * @returns {(query: T) => void} function that invokes compose with given query
 */
const composeWithOptions = <T extends object>(
  options: ComposeQueryOptions<T>
) => (query: T) => compose(query, options)

export { composeWithOptions }

export default compose
