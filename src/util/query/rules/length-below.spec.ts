/*
 * Rules/lengthBelow unit tests
 *
 */

/* eslint-env jest */
import lengthBelow from './length-below'

describe('Rules/lengthBelow', () => {
  it('returns a guard function', () => {
    expect.assertions(1)

    const executor = lengthBelow(5)

    expect(typeof executor).toBe(typeof Function)
  })

  it('can be used with strings', () => {
    expect.assertions(1)

    const executor = lengthBelow(5)

    expect(executor('1')).toBe(true)
  })
})
