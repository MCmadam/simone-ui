type LengthBelowTemplate = string | { length: number }

/**
 * Higher order guard that checks if length is below given value.
 * @param {number} length assert length is below this number.
 * @returns {(source) => boolean} guard executor.
 * @example
 *
 * const guard = lengthBelow(5)
 * guard([1, 2, 3])
 * => Returns true
 */
export default (length: number) => <T extends LengthBelowTemplate>(
  source: T
): boolean => {
  if (source === undefined || source === null) {
    return false
  }

  return source.length < length
}
