import { Dictionary, ValueOf } from '@/types'

type Excluder<T> = (value: T) => boolean
type Transformer<T> = (value: ValueOf<T>) => any
export type TransformerReturnType<F> = F extends (v: any) => infer U ? U : any

export interface ComposeQueryOption<Query, T = Transformer<Query>> {
  exclude: Excluder<TransformerReturnType<T>>[]
  transform?: T
  nullable?: boolean
}

export interface ComposeQueryOptions<Query extends Record<string, any>> {
  nullable?: boolean
  fields?: {
    [key: string]: ComposeQueryOption<Query>
  }
  transform?: (query: any) => object | null
}
