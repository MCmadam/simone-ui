export default <T>(fn: (...args: any[]) => T, expected: T) => {
  return (): boolean => {
    return fn() === expected
  }
}
