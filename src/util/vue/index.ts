import isUndefined from 'lodash/isUndefined'
import remove from 'lodash/remove'

/* eslint-disable */
// see: https://github.com/vuejs/vue/blob/59d4351ad8fc042bc263a16ed45a56e9ff5b013e/src/core/vdom/helpers/update-listeners.js
function createFunctionInvoker(...fns) {
  function invoker(...args) {
    const fns = invoker.fns

    const cloned = fns.slice()
    for (const fn of cloned) {
      fn.apply(null, args)
    }
  }

  invoker.fns = fns
  return invoker
}
/* eslint-enable */

/* eslint-disable */
// see: https://github.com/vuejs/vue/blob/59d4351ad8fc042bc263a16ed45a56e9ff5b013e/src/core/vdom/helpers/merge-hook.js
export function mergeVNodeHook(vnode, key, listener) {
  const originalHook = vnode[key]
  
  let invoker

  function wrapped(this: any, ...args) {
    listener.apply(this, args)
    remove(invoker.fns, wrapped)
  }

  if (isUndefined(originalHook)) {
    invoker = createFunctionInvoker(wrapped)
  } else {
    if (originalHook.fns != null && originalHook.merged === true) {
      invoker = originalHook
      invoker.fns.push(wrapped)
    } else {
      invoker = createFunctionInvoker(originalHook, wrapped)
    }
  }

  invoker.wrapped = true
  vnode[key] = invoker
}
/* eslint-enable */
