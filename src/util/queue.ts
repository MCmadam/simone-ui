export default class Queue<T = any> {
  private _queue: T[] = []

  public enqueue(item: T) {
    this._queue.push(item)
  }

  public dequeue(): T {
    return this._queue.shift()
  }

  public get count() {
    return this._queue.length
  }

  public peek() {
    return this._queue[0]
  }

  *[Symbol.iterator]() {
    for (const item of this._queue) {
      yield item
    }
  }
}
