import hasOwn from './has-own'

const replaceKeys = <
  S extends object,
  K extends Partial<{ [U in keyof S]: string }> & Partial<S>,
  R = Partial<S> & { [U in K[keyof K]]: S[keyof S] }
>(
  source: S,
  keys: K
): R => {
  for (const key in keys) {
    if (hasOwn(source, key)) {
      const sourceKey = key.toString()
      const replaceKey = keys[sourceKey].toString()

      const val = source[sourceKey]
      source[replaceKey] = val
      delete source[sourceKey]
    }
  }

  return (source as unknown) as R
}

export default replaceKeys
