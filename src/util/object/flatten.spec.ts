import flatten from './flatten'

describe('flatten', () => {
  it('flattens a nested object', () => {
    expect.assertions(1)

    const source = {
      name: 'name',
      images: {
        logo: 'logo',
        background: 'background',
      },
    }

    const result = flatten(source)

    expect(result).toMatchObject({
      name: 'name',
      logo: 'logo',
      background: 'background',
    })
  })

  it('flattens one level deep', () => {
    expect.assertions(1)

    const source = {
      name: 'name',
      images: {
        profile: {
          background: 'background',
          logo: 'logo',
        },
      },
    }

    const result = flatten(source)

    expect(result).toMatchObject({
      name: 'name',
      profile: {
        background: 'background',
        logo: 'logo',
      },
    })
  })

  it('throws when source and nested keys match', () => {
    expect.assertions(1)

    const source = {
      profile: 'profile',
      images: {
        profile: {},
      },
    }

    expect(() => {
      flatten(source)
    }).toThrow()
  })
})
