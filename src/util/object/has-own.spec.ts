import hasOwn from './has-own'

describe('hasOwn', () => {
  it('calls hasOwnProperty from Object.prototype', () => {
    expect.assertions(2)

    const prototype = {
      say: 'hello',
    }
    const source: any = Object.create(prototype)
    source.greet = 'hello'

    expect(hasOwn(source, 'say')).toBe(false)
    expect(hasOwn(source, 'greet')).toBe(true)
  })
})
