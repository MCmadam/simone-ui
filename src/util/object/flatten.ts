import isObject from 'lodash/isObject'
import hasOwn from './has-own'

/**
 * flatten all values that are objects
 * @param source source object
 */
const flatten = <S extends object, R extends object>(source: S): R => {
  if (!source) {
    return null
  }

  const flattened: R = Object.create(null)

  for (const [sourceKey, sourceVal] of Object.entries(source)) {
    if (hasOwn(source, sourceKey)) {
      if (isObject(sourceVal)) {
        for (const [nestedKey, nestedVal] of Object.entries(sourceVal)) {
          if (nestedKey in source) {
            throw new Error(
              `cannot flatten object when nested keys already exist on root, ${sourceKey}`
            )
          }
          flattened[nestedKey] = nestedVal
        }
      } else {
        flattened[sourceKey] = sourceVal
      }
    }
  }

  return flattened
}

export default flatten
