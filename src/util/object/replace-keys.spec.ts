import replaceKeys from './replace-keys'

describe('replaceKeys', () => {
  it('replaces keys by mutating source', () => {
    expect.assertions(1)

    const source = {
      background: 'background',
      logo: 'logo',
    }

    const result = replaceKeys(source, {
      background: 'background_url',
    })

    expect(result).toMatchObject({
      // eslint-disable-next-line
      background_url: 'background',
      logo: 'logo',
    })
  })
})
