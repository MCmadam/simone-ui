import isEqual from 'lodash/isEqual'
import isObject from 'lodash/isObject'
import hasOwn from './has-own'

/**
 * Create a new object with all enumerable and non-Symbol
 * own properties from source that differ from target
 * @param source include all properties from source that doesn't match target
 * @param target match keys against
 * @param comparator {optional} custom value comparator, defaults to lodash/isEqual
 */
const difference = <S extends object, T extends object>(
  source: S,
  target: T,
  comparator: (
    sourceValue: S[keyof S],
    targetValue: T[keyof T]
  ) => boolean = isEqual
) => {
  const diff: Partial<S> = {}

  for (const key in source) {
    if (hasOwn(source, key)) {
      const sourceKey = key.toString()
      const sourceValue = source[sourceKey]
      const targetValue = target[sourceKey]

      if (hasOwn(target, key)) {
        if (isObject(sourceValue) && isObject(targetValue)) {
          diff[sourceKey] = difference(sourceValue, targetValue)
        } else if (!comparator(sourceValue, targetValue)) {
          diff[sourceKey] = sourceValue
        }
      } else if (!comparator(sourceValue, targetValue)) {
        diff[sourceKey] = sourceValue
      }
    }
  }

  return diff
}

export default difference
