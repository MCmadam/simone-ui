import difference from './difference'

describe('difference', () => {
  it("doesn't mutate source or target", () => {
    expect.assertions(2)

    const source = { say: 'hello' }
    const target = { say: 'bye' }

    difference(source, target)

    expect(source).toBe(source)
    expect(target).toBe(target)
  })

  it('only uses own properties from source and target', () => {
    expect.assertions(1)

    const source = Object.create({
      protoText: 'sourceProto',
    })

    const target = Object.create({
      protoText: 'targetProto',
    })

    const result = difference(source, target)

    expect(result).toMatchObject({})
  })

  it('only uses enumerable non-Symbol own properties from source and target', () => {
    expect.assertions(1)

    const source = {
      text: 'source',
      fn: () => void 0,
      sym: Symbol('source'),
    }

    const target = {
      text: 'target',
      fn: () => void 0,
      sym: Symbol('target'),
    }

    const result = difference(source, target)

    expect(result).toMatchObject({
      text: 'source',
    })
  })

  it('recursevly checks nested objects', () => {
    expect.assertions(1)

    const source = {
      text: 'source',
      profile: {
        name: 'source',
      },
    }

    const target = {
      text: 'target',
      profile: {
        name: 'target',
      },
    }

    const result = difference(source, target)

    expect(result).toMatchObject({
      text: 'source',
      profile: {
        name: 'source',
      },
    })
  })

  it('copies all properties from source not in target', () => {
    expect.assertions(1)

    const source = {
      a: 'a',
      b: 'b',
    }

    const target = {
      a: 'a',
      c: 'c',
    }

    const result = difference(source, target)

    expect(result).toMatchObject({
      b: 'b',
    })
  })
})
