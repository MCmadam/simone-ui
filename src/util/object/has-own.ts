const hasOwn = (source, key) => {
  return Object.prototype.hasOwnProperty.call(source, key)
}

export default hasOwn
