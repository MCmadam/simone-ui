import Queue from './queue'

describe('queue', () => {
  let queue: Queue

  beforeEach(() => {
    queue = new Queue()
  })

  it('can add a single item with enqueue', () => {
    const item = 1

    queue.enqueue(item)

    expect(queue.count).toBe(1)
  })

  it('returns the first item with dequeue', () => {
    const item1 = 1

    queue.enqueue(item1)

    expect(queue.dequeue()).toBe(1)
  })

  it('is an iterator', () => {
    const items = [1, 2, 3]

    for (const item of items) {
      queue.enqueue(item)
    }

    const result = []
    for (const q of queue) {
      result.push(q)
    }

    expect(result).toMatchObject(items)
  })
})
