export default <T extends { affordances: string[] }>(
  item: T,
  affordance: string
) => {
  return item.affordances.includes(affordance)
}
