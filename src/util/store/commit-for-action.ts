import { API_RESPONSE_STATES } from '@/types'
import ApiError from '@/util/error/api-error'
import CognitoError from '@/util/error/cognito-error'

const mutation = 'actionState'

export default (action, commit) => {
  function committer({ key, value }) {
    commit(mutation, {
      action,
      key,
      value,
    })
  }

  committer.error = (value: ApiError | CognitoError) => {
    commit(mutation, {
      action,
      key: API_RESPONSE_STATES.error,
      value,
    })
  }

  committer.pending = (value = true) => {
    commit(mutation, {
      action,
      key: API_RESPONSE_STATES.pending,
      value,
    })
  }

  committer.success = (value = true) => {
    commit(mutation, {
      action,
      key: API_RESPONSE_STATES.success,
      value,
    })
  }

  committer.resolve = (key?: API_RESPONSE_STATES) => {
    commit('resolveAction', {
      action,
      key,
    })
  }

  return committer
}
