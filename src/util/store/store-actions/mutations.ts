import Vue from 'vue'
import { ApiResponseState, API_RESPONSE_STATES } from '@/types'
import hasOwn from '@/util/object/has-own'

export const actionState = (state, { action, key, value }) => {
  if (!action) {
    return
  }

  const found: ApiResponseState = state.actions[action]
  if (!found) {
    return
  }

  Vue.set(found, key, value)

  switch (key) {
    case API_RESPONSE_STATES.error:
      Vue.set(found, API_RESPONSE_STATES.pending, false)
      Vue.set(found, API_RESPONSE_STATES.success, false)
      break
    case API_RESPONSE_STATES.pending:
      Vue.set(found, API_RESPONSE_STATES.error, null)
      Vue.set(found, API_RESPONSE_STATES.success, null)
      break
    case API_RESPONSE_STATES.success:
      Vue.set(found, API_RESPONSE_STATES.pending, false)
      Vue.set(found, API_RESPONSE_STATES.error, null)
      break
  }
}

export const resolveAction = (state, { action, key }) => {
  if (!action) {
    return
  }

  const found: ApiResponseState = state.actions[action]
  if (!found) {
    return
  }

  if (key) {
    Vue.set(found, key, null)
  }

  Vue.set(state.actions, action, {})
}

export const resetActions = (state) => {
  for (const key in state.actions) {
    if (hasOwn(state.actions, key)) {
      Vue.set(state.actions, key, {})
    }
  }
}
