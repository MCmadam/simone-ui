import { API_RESPONSE_STATES } from '@/types'

export const resolveAction = ({ commit }, action) => {
  commit('resolveAction', {
    action,
  })
}

export const resolveError = ({ commit }, action) => {
  commit('resolveAction', {
    action,
    key: API_RESPONSE_STATES.error,
  })
}

export const resolveSuccess = ({ commit }, action) => {
  commit('resolveAction', {
    action,
    key: API_RESPONSE_STATES.success,
  })
}
