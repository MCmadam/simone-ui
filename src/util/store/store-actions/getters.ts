import ApiError from '@/util/error/api-error'
import CognitoError from '@/util/error/cognito-error'
import { ApiResponseState, API_RESPONSE_STATES } from '@/types'

export const actionError = (state) => {
  return (action: API_RESPONSE_STATES): ApiError | CognitoError | null => {
    const found: ApiResponseState = state.actions[action]

    if (found) {
      return found.error
    }

    return null
  }
}

export const actionPending = (state) => {
  return (action: API_RESPONSE_STATES): boolean => {
    const found: ApiResponseState = state.actions[action]

    if (found) {
      return !!found.pending
    }

    return false
  }
}

export const actionSuccess = (state) => {
  return (action: API_RESPONSE_STATES): boolean => {
    const found: ApiResponseState = state.actions[action]

    if (found) {
      return found.success
    }

    return false
  }
}
