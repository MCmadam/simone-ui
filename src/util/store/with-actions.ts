import * as actions from './store-actions/actions'
import * as getters from './store-actions/getters'
import * as mutations from './store-actions/mutations'
import merge from 'lodash/merge'

export default <T>(moduleActions: string[], storeModule: T) => {
  const actionModule = {
    actions,
    getters,
    mutations,
    state: {
      actions: {},
    },
  }

  for (const action of moduleActions) {
    actionModule.state.actions[action] = {}
  }

  for (const key in actionModule) {
    if (!storeModule[key]) {
      storeModule[key] = {}
    }

    if (key === 'state' && typeof storeModule[key] === 'function') {
      const stateFn = storeModule[key]

      storeModule[key] = () => ({
        ...actionModule[key],
        ...stateFn(),
      })
    } else {
      merge(storeModule[key], actionModule[key])
    }
  }

  return storeModule
}
