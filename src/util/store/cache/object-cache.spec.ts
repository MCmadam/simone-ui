/**
 * Specs Object Cache
 * @group cache/object
 * @group store/cache
 */
import ObjectCache from './object-cache'

describe('Object Cache', () => {
  it('can set an item', () => {
    expect.assertions(1)

    const cache = new ObjectCache()

    const item = {
      uuid: '1',
    }

    cache.set(item)

    expect(cache.has(item)).toBe(true)
  })

  it('can update an item via set', () => {
    expect.assertions(1)

    const cache = new ObjectCache<{ uuid: string; name: string }>()

    const item1 = {
      uuid: 'uuid',
      name: 'item1',
    }

    const item2 = {
      uuid: 'uuid',
      name: 'item2',
    }

    cache.set(item1)
    cache.set(item2)

    expect(cache.has(item1)).toBe(true)
  })

  it('can remove an item', () => {
    expect.assertions(2)

    const cache = new ObjectCache<{ uuid: string; name: string }>()

    const item = {
      uuid: 'uuid',
      name: 'item1',
    }

    cache.set(item)

    expect(cache.has(item)).toBe(true)

    cache.delete(item)

    expect(cache.has(item)).toBe(false)
  })

  it('updates value after changes', () => {
    expect.assertions(1)

    const cache = new ObjectCache<{ uuid: string; name: string }>()

    const item = {
      uuid: 'uuid',
      name: 'name',
    }

    cache.set(item)

    expect(cache.value).toMatchObject([item])
  })
})
