import Vue from 'vue'
import { Identifiable } from '@/types'

interface ArrayCacheOptions {
  prepend?: boolean
}

const defaultOptions: ArrayCacheOptions = {
  prepend: false,
}

/**
 * A reactive array cache that works with vuex
 * this chache only works with Identifiable objects and
 * only checks by uuid instead of using equality operators.
 */
export default class ArrayCache<T extends Identifiable> {
  private cache: T[]
  private options: ArrayCacheOptions

  constructor(options?: ArrayCacheOptions) {
    this.cache = []
    this.configure(options || {})
  }

  public get value(): T[] {
    return this.cache
  }

  public configure(options: ArrayCacheOptions) {
    const defaults = {
      ...defaultOptions,
    }
    this.options = Object.assign(defaults, options)
  }

  /**
   * check if an identifier is already present in the cache,
   * to check if that item is expected use 'is'
   * @param uuid item identifier
   */
  public has(uuid: T['uuid']): boolean {
    return this.indexOf(uuid) > -1
  }

  /**
   * check if the item with identifier is equal to target
   * @param uuid item identifier
   * @param target equality target
   */
  public is(uuid: T['uuid'], target: T): boolean {
    const index = this.indexOf(uuid)

    if (index < 0) {
      return false
    }

    return Object.is(this.cache[index], target)
  }

  /**
   * find item index by identifier,
   * this does not mean it is the same item, use 'is' to check for equality
   * @param uuid item identifier
   */
  public indexOf(uuid: T['uuid']): number {
    return this.cache.findIndex((item) => item.uuid === uuid)
  }

  /**
   * add or update an item in the cache
   * @param item T
   */
  public set(item: T): T {
    const index = this.indexOf(item.uuid)

    if (index === -1) {
      if (this.options.prepend) {
        this.cache.unshift(item)
      } else {
        this.cache.push(item)
      }

      return item
    } else {
      return Vue.set(this.cache, index, item)
    }
  }

  /**
   * add an item to a specific index
   * @param index cache index
   * @param item T
   * @throws Error if item is already present
   */
  public setAt(index: number, item: T) {
    if (index >= this.cache.length) {
      this.cache.push(item)
    } else {
      const hasIndex = this.cache.indexOf(item)

      if (hasIndex === -1) {
        this.cache.splice(index, 0, item)
      } else {
        throw new Error(`item already present at index: ${hasIndex}`)
      }
    }

    return item
  }

  /**
   * delete index of item identifier
   * @param item T
   */
  public delete(item: T): void {
    const index = this.indexOf(item.uuid)

    if (index > -1) {
      Vue.delete(this.cache, index)
    }
  }
}
