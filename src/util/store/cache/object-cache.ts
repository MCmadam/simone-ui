import Vue from 'vue'
import { Identifiable } from '@/types'

/**
 * A reactive object cache that works with vuex
 * @summary This wrapper is meant to be replaced by the native reactive Map in Vue 3.0
 * @see https://medium.com/js-dojo/vue-next-and-why-maps-solve-a-big-problem-4bd1b8b3cbc6
 */
export default class ObjectCache<T extends Identifiable> {
  private cache: { [key: string]: T }

  constructor() {
    this.cache = Vue.observable({})
  }

  /**
   * reactive getter to retrieve the values
   * @summary this does not work like Map and returns object values as an array.
   * @example vuex getter
   *
   * const myGetter = (state) => {
   * return state.myCache.value
   * }
   */
  public get value(): T[] {
    return Object.values(this.cache)
  }

  /**
   * check if the cache has item, works like Map.has using the SameValueZero equality
   * @param item T
   * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Equality_comparisons_and_sameness#Same-value-zero_equality
   */
  public has(item: T): boolean {
    const found = this.cache[item.uuid]
    return !!found //&& Object.is(found, item)
  }

  /**
   * set an item in the cache, works like Map.set
   * @param item T
   */
  public set(...items: T[]): T | T[] {
    for (const item of items) {
      Vue.set(this.cache, item.uuid, item)
    }

    if (items.length === 1) {
      return items[0]
    }

    return items
  }

  /**
   * remove an item from the cache, works like Map.delete
   * @param item T
   */
  public delete(item: T): void {
    if (this.has(item)) {
      Vue.delete(this.cache, item.uuid)
    }
  }

  /**
   * clear all items from cache
   */
  public clear() {
    this.cache = Vue.observable({})
  }
}
