/**
 * Array Cache specs
 * @group cache/array
 * @group store/cache
 */

import ArrayCache from './array-cache'

describe('Array Cache', () => {
  it('will append a new item by default', () => {
    expect.assertions(1)

    const cache = new ArrayCache()

    const item1 = {
      uuid: '1',
    }
    const item2 = {
      uuid: '2',
    }

    cache.set(item1)
    cache.set(item2)

    expect(cache.indexOf(item2.uuid)).toBe(1)
  })

  it('can prepend via options', () => {
    expect.assertions(1)

    const cache = new ArrayCache({
      prepend: true,
    })

    const item1 = {
      uuid: '1',
    }
    const item2 = {
      uuid: '2',
    }

    cache.set(item1)
    cache.set(item2)

    expect(cache.indexOf(item2.uuid)).toBe(0)
  })

  it('can insert an item at a specified index', () => {
    expect.assertions(1)

    const cache = new ArrayCache()

    Array.from(Array(10)).forEach((_, i) => {
      cache.set({
        uuid: `uuid-${i}`,
      })
    })

    cache.setAt(5, {
      uuid: 'uuid-11',
    })

    const index = cache.indexOf('uuid-11')

    expect(index).toBe(5)
  })

  it('will update an entry if already present', () => {
    expect.assertions(2)

    const cache = new ArrayCache<{ uuid: string; name: string }>()

    const item1 = {
      uuid: 'uuid',
      name: 'item1',
    }
    const item2 = {
      uuid: 'uuid',
      name: 'item2',
    }

    cache.set(item1)
    cache.set(item2)

    expect(cache.is(item1.uuid, item1)).toBe(false)
    expect(cache.is(item1.uuid, item2)).toBe(true)
  })

  it('can remove an item', () => {
    expect.assertions(1)

    const cache = new ArrayCache()

    const item = {
      uuid: 'uuid',
    }

    cache.set(item)
    cache.delete(item)

    expect(cache.has(item.uuid)).toBe(false)
  })
})
