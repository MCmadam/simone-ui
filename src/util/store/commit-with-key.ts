export default (key, commit) => {
  return (mutation, value) => {
    commit(mutation, {
      key,
      value,
    })
  }
}
