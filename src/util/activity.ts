import { ACTIVITY_STATUS } from '@/types'

export const nullToNone = (status) => (status === null ? 'none' : status)
export const noneToNull = (status) => (status === 'none' ? null : status)

export const getIconProps = (status: ACTIVITY_STATUS) => {
  const attrs: any = {
    round: true,
    size: 24,
  }

  switch (nullToNone(status)) {
    case ACTIVITY_STATUS.none:
      attrs.name = 'status_circle_full'
      attrs.color = 'white'
      attrs.class = 'pu-icon-border-light'
      break
    case ACTIVITY_STATUS.todo:
      attrs.name = 'status_circle_full'
      attrs.color = 'white'
      attrs.class = 'pu-icon-border'
      break
    case ACTIVITY_STATUS.doing:
      attrs.name = 'status_circle_half'
      attrs.color = 'blue'
      attrs.class = 'pu-icon-border'
      break
    case ACTIVITY_STATUS.done:
      attrs.name = 'status_circle_full'
      attrs.color = 'blue'
      attrs.class = 'pu-icon-border'
      break
    default:
      break
  }

  return attrs
}
