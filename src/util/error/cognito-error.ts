import cognitoErrorMap from './cognito-error-map'
import logger from '@/util/logger'
import get from 'lodash/get'
import isString from 'lodash/isString'
import isObject from 'lodash/isObject'

export default class CognitoError {
  private readonly _definition: any

  constructor(action: string, message: string | Error) {
    const msg = this.getMessage(message)
    this._definition = this.findDefinition(action, msg)

    if (process.env.NODE_ENV === 'development') {
      // console.error('cognito error', action, message)
    }
  }

  public get key() {
    return get(this._definition, 'key', '')
  }

  public get fields() {
    return get(this._definition, 'fields', {})
  }

  private getMessage(msg) {
    if (isString(msg)) {
      return msg.toLowerCase()
    } else if (isObject(msg) || msg instanceof Error) {
      return (msg as any).message.toLowerCase()
    }
  }

  private findDefinition(action: string, message: string) {
    let def

    const actionDef = cognitoErrorMap[action]
    if (actionDef) {
      def = actionDef.find((x) => x.matchers.some((y) => y.test(message)))
    }
    if (!def) {
      def = cognitoErrorMap.generic.find((x) =>
        x.matchers.some((y) => y.test(message))
      )
      logger.debug(
        'CognitoError, no definition for action with message.',
        action,
        message
      )
    }
    if (!def) {
      def = cognitoErrorMap.defaults[action]
    }
    if (!def) {
      def = cognitoErrorMap.defaults.generic
    }

    return def
  }
}
