import { AUTH_ACTIONS } from '@/types'

export default {
  defaults: {
    generic: {
      key: 'error.cognito.generic',
    },
    [AUTH_ACTIONS.forgotPassword]: {
      key: `error.cognito.${AUTH_ACTIONS.forgotPassword}.default`,
    },
    [AUTH_ACTIONS.forgotPasswordSubmit]: {
      key: `error.cognito.${AUTH_ACTIONS.forgotPasswordSubmit}.default`,
    },
    [AUTH_ACTIONS.login]: {
      key: `error.cognito.${AUTH_ACTIONS.login}.default`,
    },
    [AUTH_ACTIONS.register]: {
      key: `error.cognito.${AUTH_ACTIONS.register}.default`,
    },
    [AUTH_ACTIONS.registerConfirm]: {
      key: `error.cognito.${AUTH_ACTIONS.registerConfirm}.default`,
    },
    [AUTH_ACTIONS.registerResend]: {
      key: `error.cognito.${AUTH_ACTIONS.registerResend}.default`,
    },
  },
  generic: [
    {
      matchers: [/username cannot be empty/],
      key: 'error.cognito.generic.email-empty',
      fields: {
        email: true,
      },
    },
    {
      matchers: [/username should be an email/],
      key: 'error.cognito.generic.email-invalid',
      fields: {
        email: true,
      },
    },
    {
      matchers: [/null failed with error/, /password cannot be empty/],
      key: 'error.cognito.generic.password-empty',
      fields: {
        password: true,
      },
    },
    {
      matchers: [
        /password not long enough/,
        /length greater than or equal to 6/,
      ],
      key: 'error.cognito.generic.password-length',
      fields: {
        password: true,
      },
    },
    {
      matchers: [/password must have numeric/],
      key: 'error.cognito.generic.password-numeric',
      fields: {
        password: true,
      },
    },
    {
      matchers: [/password must have uppercase/],
      key: 'error.cognito.generic.password-uppercase',
      fields: {
        password: true,
      },
    },
    {
      matchers: [/user does not exist/, /incorrect username or password/],
      key: 'error.cognito.generic.email-or-password-invalid',
      fields: {
        email: true,
        password: true,
      },
    },
    {
      matchers: [
        /user is not confirmed/,
        /no registered\/verified email or phone_number/,
      ],
      key: 'error.cognito.generic.user-not-confirmed',
    },
  ],
  [AUTH_ACTIONS.forgotPassword]: [
    {
      matchers: [/username cannot be empty/],
      key: `error.cognito.${AUTH_ACTIONS.forgotPassword}.email-empty`,
      fields: {
        email: true,
      },
    },
    {
      matchers: [
        /username\/client id combination not found/,
        /username should be an email/,
      ],
      key: `error.cognito.${AUTH_ACTIONS.forgotPassword}.email-invalid`,
      fields: {
        email: true,
      },
    },
  ],
  [AUTH_ACTIONS.forgotPasswordSubmit]: [
    {
      matchers: [/code cannot be empty/],
      key: `error.cognito.${AUTH_ACTIONS.forgotPasswordSubmit}.code-empty`,
      fields: {
        email: false,
        password: false,
        code: true,
      },
    },
    {
      matchers: [/please request a code again/],
      key: `error.cognito.${AUTH_ACTIONS.forgotPasswordSubmit}.code-expired`,
      fields: {
        code: true,
      },
    },
    {
      matchers: [/username cannot be empty/],
      key: `error.cognito.${AUTH_ACTIONS.forgotPasswordSubmit}.email-empty`,
      fields: {
        email: true,
        password: false,
        code: false,
      },
    },
    {
      matchers: [/user does not exist/],
      key: `error.cognito.${AUTH_ACTIONS.forgotPasswordSubmit}.email-invalid`,
      fields: {
        email: true,
        password: false,
        code: false,
      },
    },
    {
      matchers: [/null failed with error/, /password cannot be empty/],
      key: `error.cognito.${AUTH_ACTIONS.forgotPasswordSubmit}.password-empty`,
      fields: {
        email: false,
        password: true,
        code: false,
      },
    },
  ],
  [AUTH_ACTIONS.login]: [
    {
      matchers: [/username cannot be empty/],
      key: `error.cognito.${AUTH_ACTIONS.login}.email-empty`,
      fields: {
        email: true,
        password: false,
      },
    },
    {
      matchers: [/null failed with error/],
      key: `error.cognito.${AUTH_ACTIONS.login}.password-empty`,
      fields: {
        email: false,
        password: true,
      },
    },
    {
      matchers: [/should either be a string/],
      key: `error.cognito.${AUTH_ACTIONS.login}.email-or-password-empty`,
      fields: {
        email: true,
        password: true,
      },
    },
    {
      matchers: [/user does not exist/, /incorrect username or password/],
      key: `error.cognito.${AUTH_ACTIONS.login}.email-or-password-invalid`,
      fields: {
        email: true,
        password: true,
      },
    },
  ],
  [AUTH_ACTIONS.register]: [
    {
      matchers: [/username cannot be empty/],
      key: `error.cognito.${AUTH_ACTIONS.register}.email-empty`,
      fields: {
        email: true,
        password: false,
        tos: false,
      },
    },
    {
      matchers: [/username should be an email/],
      key: `error.cognito.${AUTH_ACTIONS.register}.email-invalid`,
      fields: {
        email: true,
        password: false,
        tos: false,
      },
    },
    {
      matchers: [/account with the given email already exists/],
      key: `error.cognito.${AUTH_ACTIONS.register}.email-already-exists`,
      fields: {
        email: true,
        password: false,
        tos: false,
      },
    },
    {
      matchers: [/null failed with error/, /password cannot be empty/],
      key: `error.cognito.${AUTH_ACTIONS.register}.password-empty`,
      fields: {
        email: false,
        password: true,
        tos: false,
      },
    },
    {
      matchers: [
        /password not long enough/,
        /length greater than or equal to 6/,
      ],
      key: `error.cognito.${AUTH_ACTIONS.register}.password-length`,
      fields: {
        email: false,
        password: true,
        tos: false,
      },
    },
    {
      matchers: [/password must have numeric/],
      key: `error.cognito.${AUTH_ACTIONS.register}.password-numeric`,
      fields: {
        email: false,
        password: true,
        tos: false,
      },
    },
    {
      matchers: [/password must have uppercase/],
      key: `error.cognito.${AUTH_ACTIONS.register}.password-uppercase`,
      fields: {
        email: false,
        password: true,
        tos: false,
      },
    },
  ],
  [AUTH_ACTIONS.registerConfirm]: [
    {
      matchers: [/username\/client id combination not found/],
      key: `error.cognito.${AUTH_ACTIONS.registerConfirm}.username-or-code-not-found`,
    },
    {
      matchers: [/user is already confirmed/, /current status is confirmed/],
      key: `error.cognito.${AUTH_ACTIONS.registerConfirm}.already-confirmed`,
    },
    {
      matchers: [/attempt limit exceeded/],
      key: `error.cognito.${AUTH_ACTIONS.registerConfirm}.attempt-limit-exceeded`,
    },
    {
      matchers: [/username cannot be empty/],
      key: `error.cognito.${AUTH_ACTIONS.registerConfirm}.email-empty`,
      fields: {
        email: true,
      },
    },
    {
      matchers: [/please request a code again/],
      key: `error.cognito.${AUTH_ACTIONS.registerConfirm}.code-expired`,
      fields: {
        code: true,
      },
    },
    {
      matchers: [/invalid verification code provided/],
      key: `error.cognito.${AUTH_ACTIONS.registerConfirm}.code-invalid`,
      fields: {
        code: true,
      },
    },
    {
      matchers: [/username should be an email/],
      key: `error.cognito.${AUTH_ACTIONS.registerConfirm}.email-invalid`,
      fields: {
        email: true,
      },
    },
  ],
  [AUTH_ACTIONS.registerResend]: [
    {
      matchers: [/username\/client id combination not found/],
      key: `error.cognito.${AUTH_ACTIONS.registerResend}.username-not-found`,
      fields: {
        email: true,
      },
    },
  ],
}
