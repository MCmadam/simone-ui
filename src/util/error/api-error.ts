// import isCancel from 'axios/lib/cancel/isCancel'
import axios, { AxiosError } from 'axios'

export default class ApiError {
  private _error: AxiosError
  private _key: string
  private _fields?: object
  private _message: string
  private _isCancel: boolean

  constructor(error: AxiosError) {
    this._error = error

    if ('response' in error && error.response) {
      const { fields, key, message } = error.response.data

      this._fields = fields
      this._key = key
      this._message = message
      this._isCancel = axios.isCancel(error)
    } else {
      // // console.error(error)
    }
  }

  public get fields() {
    return this._fields || {}
  }

  public get key() {
    return this._key && `error.${this._key}`
  }

  public field(field) {
    const found = this.fields[field]
    return found && `error.field.${found}`
  }

  public get summary() {
    return this._message
  }

  public get isCancel() {
    return this._isCancel
  }

  toString() {
    return this._error.toString()
  }
}
