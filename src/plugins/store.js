import Vue from 'vue'
import Vuex from 'vuex'

Vuex.Store.prototype.hasModule = function(path) {
  if (typeof path === 'string') {
    // eslint-disable-next-line
    path = [path]
  }

  let m = this._modules.root

  return path.every((p) => {
    m = m._children[p]
    return m
  })
}

Vue.use(Vuex)
