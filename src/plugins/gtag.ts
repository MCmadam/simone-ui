import Vue from 'vue'
import VueGtag from 'vue-gtag'
import router from '@/router'
import { getGtagId } from '@/lib/globals'

const id = getGtagId()

if (id) {
  Vue.use(
    VueGtag,
    {
      /* eslint-disable camelcase */
      config: {
        id,
        anonymize_ip: true,
      },
      /* eslint-enable camelcase */
    },
    router
  )
}
