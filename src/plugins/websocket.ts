import Vue from 'vue'
import partupWebsocket from '@/wss'

Vue.use({
  install(Vue) {
    Object.defineProperty(Vue.prototype, '$socket', {
      writable: false,
      value: partupWebsocket,
    })
  },
})
