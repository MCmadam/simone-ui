import Vue from 'vue'
import plugin from './plugin'
import logger from '@/util/logger'

Vue.use(plugin, logger)
