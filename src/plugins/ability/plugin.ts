import Ability from '@/lib/authorization/ability/pkg/ability'
import { WATCHER_KEY, watchAbility } from '@/lib/authorization/ability/util'

export default (Vue, ability) => {
  Object.defineProperty(Vue.prototype, '$ability', {
    writable: true,
    value: ability || new Ability(),
  })

  Vue.mixin({
    beforeCreate() {
      const { ability, parent } = this.$options

      if (ability === true) {
        this.$ability = new Ability()
      } else if (ability instanceof Ability) {
        this.$ability = ability
      } else if (parent && parent.$ability) {
        this.$ability = parent.$ability
      }
    },
    methods: {
      $can(...args) {
        const ability = this.$ability
        if (!ability) {
          return false
        }

        const watcher = ability[WATCHER_KEY]
          ? ability[WATCHER_KEY]
          : watchAbility(ability)

        // force re-render
        // eslint-disable-next-line
        watcher.rules = watcher.rules

        return ability.can(...args)
      },
    },
  })
}
