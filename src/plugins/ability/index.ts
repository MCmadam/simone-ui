import Vue from 'vue'
import plugin from './plugin'
import Ability from '@/lib/authorization/ability/pkg/ability'

Vue.use(plugin, new Ability())
