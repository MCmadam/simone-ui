import globals from '@/lib/globals'
import Vue from 'vue'

Vue.use({
  install() {
    Vue.prototype.$globals = globals
  },
})
