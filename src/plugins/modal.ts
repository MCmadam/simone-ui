import Vue from 'vue'

export const modalManager = {
  _emitter: new Vue(),

  open(name, options) {
    this._emitter.$emit('open', name, options)
  },

  on(event, callback) {
    this._emitter.$on(event, callback)
  },

  off(event, callback) {
    this._emitter.$off(event, callback)
  },
}

Vue.use({
  install(Vue) {
    Object.assign(Vue.prototype, { $modal: modalManager })
  },
})
