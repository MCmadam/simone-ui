import Vue from 'vue'

export const dialog = {
  _emitter: new Vue(),

  open(options) {
    this._emitter.$emit('open', options)
  },

  on(event, cb) {
    this._emitter.$on(event, cb)
  },
  off(event, cb) {
    this._emitter.$off(event, cb)
  },
}

Vue.use({
  install(Vue) {
    Object.assign(Vue.prototype, {
      $dialog: dialog,
    })
  },
})
