import { ComponentOptions, VueConstructor } from 'vue'
import { CombinedVueInstance, Vue } from 'vue/types/vue'
import {
  ThisTypedComponentOptionsWithArrayProps,
  ThisTypedComponentOptionsWithRecordProps,
  FunctionalComponentOptions,
  RecordPropsDefinition,
} from 'vue/types/options'

declare module 'vue/types/vue' {
  export type OptionsVue<
    Instance extends Vue,
    Data,
    Methods,
    Computed,
    Props,
    Options = {}
  > = VueConstructor<
    CombinedVueInstance<Instance, Data, Methods, Computed, Props> & Vue,
    Options
  >

  interface VueConstructor<V extends Vue = Vue, Options = Record<string, any>> {
    options: Options

    extend<Data, Methods, Computed, Options, PropNames extends string = never>(
      options?: ThisTypedComponentOptionsWithArrayProps<
        V,
        Data,
        Methods,
        Computed,
        PropNames
      > &
        Options
    ): OptionsVue<V, Data, Methods, Computed, Record<PropNames, any>, Options>
    extend<Data, Methods, Computed, Props, Options>(
      options?: ThisTypedComponentOptionsWithRecordProps<
        V,
        Data,
        Methods,
        Computed,
        Props
      > &
        Options
    ): OptionsVue<V, Data, Methods, Computed, Props, Options>
    extend<Options, PropNames extends string = never>(
      definition: FunctionalComponentOptions<
        Record<PropNames, any>,
        PropNames[]
      > &
        Options
    ): OptionsVue<V, {}, {}, {}, Record<PropNames, any>, Options>
    extend<Props, Options>(
      definition: FunctionalComponentOptions<
        Props,
        RecordPropsDefinition<Props>
      > &
        Options
    ): OptionsVue<V, {}, {}, {}, Props, Options>
    extend<V extends Vue = Vue>(
      options?: ComponentOptions<V> & Options
    ): OptionsVue<V, {}, {}, {}, {}, Options>
  }
}
