import { storiesOf } from '@storybook/vue'
import ColorPalette from '@/components/util/storybook/color-palette.vue'

const colors = {
  grayscale: [
    'gray-0-0',
    'gray-0',
    'gray-0-5',
    'gray-1',
    'gray-2',
    'gray-3',
    'gray-4',
    'gray-5',
    'gray-6',
    'gray-7',
    'gray-7-0',
    'gray-8',
    'gray-9',
  ],
  blue: [
    'blue',
    'blue-highlight',
    'blue-superlight',
    'blue-lesssuperlight',
    'blue-lesssuperlightslightlydarker',
    'blue-dark',
    'blue-info',
    'blue-verydark',
    'blue-veryverydark',
  ],
  orange: ['orange', 'orange-light', 'orange-gray'],
  green: [
    'green',
    'green-darker',
    'green-lighter',
    'green-messages',
    'green-messages-dark',
    'green-gray',
  ],
  red: ['red', 'red-light', 'red-200', 'red-dark'],
}

const stories = storiesOf('Styleguide/Colors', module)

stories.add('All', () => ({
  components: {
    ColorPalette,
  },
  data() {
    return {
      colors,
    }
  },
  computed: {
    rootStyles() {
      return {
        columns: '200px 3',
      }
    },
  },
  template: `
  <div :style="rootStyles">
    <color-palette
      v-for="(color, name) in colors"
      :key="name"
      :colors="color"
      :scheme="name"
    />
  </div>
  `,
}))
