# Layouts

- All views will be rendered inside a layout which defines the basic structure of the page.
- All layouts are registered as global component to ensure it can be rendered before the view.
- A layout lives outside of the main `<router-view>` which prevents a reload between pages sharing the same layout.

## Define the layout for a view

All views **must** specify which layout is required before rending the view, this is done inside the `beforeEnter` hook in the router.
The layout name get's stored in the store's `rootState` and reacted upon by the `App component`.
