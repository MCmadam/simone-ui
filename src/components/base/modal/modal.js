import './modal.scss'

export default {
  name: 'pu-modal',

  methods: {
    success(payload) {
      this.$emit('success', payload)
    },
    cancel(payload) {
      this.$emit('cancel', payload)
    },
  },
}
