import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, boolean, select, text } from '@storybook/addon-knobs'
import { addKnobs, withNone } from '@/util/storybook'
import PuButton from '@/components/base/button/button.vue'

const props = {
  closeOnContentClick: boolean('close on content click', false, 'PROPS'),
}

const events = {}

const stories = storiesOf('Components/menu', module)
stories.addDecorator(withKnobs)

// stories.add('playground', () => ({
//   props: addKnobs(props)(),
// }))

stories.add('more options menu', () => ({
  components: {
    PuButton,
  },
  template: `
    <div>
      <pu-menu>
        <template v-slot:activator="{ on }">
          <pu-button
            flat
            icon
            v-on="on"
          >
            <pu-icon name="more_options" :size="24" />
          </pu-button>
        </template>

        <pu-list>
          <li><pu-button flat>list item 1</pu-button></li>
          <li><pu-button flat>list item 2</pu-button></li>
          <li><pu-button flat>list item 3</pu-button></li>
          <li><pu-button flat>list item 4</pu-button></li>
        </pu-list>
      </pu-menu>
    </div>
  `,
}))
