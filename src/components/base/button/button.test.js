/* eslint-env jest */
import { mount, shallowMount } from '@vue/test-utils'
import PuButton from './button.vue'

const makeSubject = (options) => shallowMount(PuButton, options)

describe('Component tests', () => {
  describe('button tests', () => {
    it("get's the right properties assigned", () => {
      expect.assertions(1)

      const sut = makeSubject()
      sut.setProps({
        color: 'primary',
      })

      expect(sut.props('color')).toBe('primary')
    })

    it('renders a button as base element', () => {
      const sut = makeSubject()
      const expected = /^<button.*/

      expect(sut.html()).toMatch(expected)
    })

    it('renders with the right classes', () => {
      expect.assertions(1)

      const sut = makeSubject()

      expect(sut.classes()).toContain('pu-button')
    })

    it('has a default slot', () => {
      const sut = makeSubject({
        slots: {
          default: 'testbutton',
        },
      })

      expect(sut.text()).toBe('testbutton')
    })

    it('can be disabled', async () => {
      const sut = makeSubject()
      sut.setProps({
        disabled: true,
      })
      await sut.vm.$nextTick()
      expect(sut.classes('is-disabled')).toBe(true)
    })
  })
})
