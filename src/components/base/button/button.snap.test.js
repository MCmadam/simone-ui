/* eslint-env jest */

import { mount } from '@vue/test-utils'
import PuButton from './button.vue'

const wrapper = mount(PuButton)

describe('Snapshot tests', () => {
  describe('button shapshots', () => {
    it('matches snapshot', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})
