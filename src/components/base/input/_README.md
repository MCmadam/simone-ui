# Input base components

All inputs should work with a v-model on it's own,
however they should always be inside either a form-control or content-editable component which handles the value for them

## content editable

@jeroen
When a content editable is in viewer state and get's selected via a tab (tabindex), which key should the user then press to set the component to edit state?