import { lazyModelFactory } from '@/mixins/lazy-model'
import { registerableInject } from '@/mixins/registerable'
import Vue from 'vue'

import './input-base.scss'

/**
 * base for all input components
 * @mixin
 */
export default Vue.extend({
  name: 'pu-input',

  /**
   * This component does apply any props not specified as prop directly to it's input
   * @see See computed property inputAttributes
   */
  inheritAttrs: false,

  mixins: [lazyModelFactory('change'), registerableInject('formControl')],

  props: {
    /**
     * @prop disabled
     * @type {Boolean}
     */
    disabled: {
      type: Boolean,
      default: false,
    },
    /**
     * @prop id
     * @type {String}
     * @description the input id
     */
    id: {
      type: String,
    },
    /**
     * @prop readonly
     * @type {Boolean}
     * @default false
     * @description set the input's readonly state
     */
    readonly: {
      type: Boolean,
      default: false,
    },
  },

  data() {
    return {
      dirty: false,
    }
  },

  computed: {
    isChild() {
      return Boolean(this.formControl)
    },
    isDirty() {
      return this.dirty
    },
    hasError() {
      return this.isChild && this.formControl.hasError
    },
    internalValue: {
      get() {
        return this.lazyValue
      },
      set(val) {
        this.lazyValue = val

        if (!this.dirty) {
          this.dirty = true
        }

        this.$emit('change', val)
      },
    },
  },

  mounted() {
    if (this.isChild) {
      this.formControl.register(this)
    }
  },

  beforeDestroy() {
    if (this.isChild) {
      this.formControl.unregister(this)
    }
  },

  methods: {
    /**
     * @method getValue
     * @public
     * @description programatically get the value
     * @return {any}
     */
    getValue() {
      return this.internalValue
    },
    /**
     * @method setValue
     * @public
     * @description programatically set the value
     * @param {any} val new value
     */
    setValue(val) {
      this.lazyValue = val
    },
    /**
     * @method blur
     * @public
     * @description programatically blur the input node,
     * this can be used when this component is referenced by`ref="x"`
     */
    blur() {
      this.$refs.input.blur()
    },
    /**
     * @method focus
     * @public
     * @description programatically focus the input node,
     * this can be used when this component is referenced by `ref="x"`
     */
    focus() {
      this.$refs.input.focus()
    },
    /**
     * @method onBlur
     * @protected
     * @virtual
     * @param {BlurEvent} e blur event
     */
    onBlur(e) {
      /**
       * Blur event
       *
       * @event blur
       * @type {BlurEvent}
       */
      this.$emit('blur', e)
    },
    /**
     * @method onFocus
     * @protected
     * @virtual
     * @param {FocusEvent} e focus event
     */
    onFocus(e) {
      /**
       * Focus event
       *
       * @event focus
       * @type {FocusEvent}
       */
      this.$emit('focus', e)
    },
    /**
     * @method onInput
     * @protected
     * @virtual
     * @param {Event} e input event
     * @description this handler is the unified handler for all event types,
     * this handler is to be used for all input events e.g. 'change' and 'input'
     */
    onInput(e) {
      /**
       * Change event
       *
       * @event change
       * @type {any} the actual used value, e.g. e.target.value
       */
      this.internalValue = e.target.value
    },
    /**
     * onEnter
     *
     * @method onEnter
     * @protected
     * @virtual
     * @param {Event} e enter event
     */
    onEnter(e) {
      /**
       * Enter event
       *
       * @event enter
       * @type {Event} e enter event
       */
      this.$emit('enter', e)
    },
  },
})
