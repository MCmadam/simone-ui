import focusable from '@/mixins/focusable'
import validatable from '@/mixins/validatable'
import { registerableInject, registerableProvide } from '@/mixins/registerable'
import { lazyModelFactory } from '@/mixins/lazy-model'
import Vue from 'vue'

/**
 * This is the base for any direct child of a form
 * it provides functionality for both content-editable and form-control
 * any functionality added must be used by all deriving components
 * @mixin
 */
export default Vue.extend({
  name: 'pu-form-control',

  mixins: [
    focusable,
    lazyModelFactory('change'),
    registerableProvide('formControl'),
    registerableInject('form'),
    validatable,
  ],

  provide() {
    const formControl = {}
    Object.defineProperty(formControl, 'hasError', {
      get: () => this.hasError,
    })
    return {
      formControl,
    }
  },

  props: {
    /**
     * Placeholder for when there's no content
     * @prop placeholder
     * @type {String}
     */
    placeholder: {
      type: String,
    },
  },

  data() {
    return {
      /*
       * A reference to the child input component's VNode.
       **/
      input: null,
    }
  },

  watch: {
    value: {
      handler(newVal) {
        this.$nextTick(() => {
          if (this.input) {
            this.input.setValue(newVal)
          }
        })
      },
      immediate: true,
    },
  },

  mounted() {
    this.form.register(this)
  },

  beforeDestroy() {
    this.form.unregister(this)
  },

  methods: {
    /**
     * Programatically blur the input component
     * @method blur
     * @public
     * @description this can be used when this component is referenced by`ref="x"`
     */
    blur() {
      if (this.input) {
        this.input.blur()
      }
    },
    /**
     * Programatically focus the input component
     * @method focus
     * @public
     * @description this can be used when this component is referenced by `ref="x"`
     */
    focus() {
      if (this.input) {
        this.input.focus()
      }
    },
    /**
     * Programatically get the value of this input control
     * @method getValue
     * @public
     * @description this can be used when this component is referenced by `ref="x"`
     * or with the register/provide mechanic.
     */
    getValue() {
      return this.internalValue
    },
    /**
     * Programatically set the value of this input control
     * @method setValue
     * @public
     * @description this can be used when this component is referenced by `ref="x"`
     * or with the register/provide mechanic.
     */
    setValue(val) {
      this.lazyValue = val

      if (this.input) {
        this.input.setValue(val)
      }

      this.resetValidation()
    },
    /**
     * Allows a child input to register itself, this establishes communication
     * @method register
     * @private
     * @param {VNode} input registering to this control
     * @see See [input-base](./input-base.js)
     */
    register(input) {
      // @TODO: check if a wrap to check if the input exists is necessary and fix problem there
      this.input = input
      // Events are piped as if this is the actual input component we care about
      input.$on('blur', this.onBlur)
      input.$on('focus', this.onFocus)
      input.$on('change', this.onInput)
      input.$on('enter', this.onEnter)
    },
    /**
     * Allows an input component to unregister itself
     * @method unregister
     * @private
     * @param {VNode} input unregistering to this control
     * @see See [input-base](./input-base.js)
     */
    unregister(input) {
      // @TODO: check if a wrap to check if the input exists is necessary and fix problem there
      input.$off('blur', this.onBlur)
      input.$off('focus', this.onFocus)
      input.$off('change', this.onInput)
      input.$off('enter', this.onEnter)
      this.input = undefined
    },
    /**
     * Default behavior for responding to changes from an input component
     * @protected
     * @virtual override this when different behavior is required
     * @param {any} val value emitted by the input
     */
    onInput(val) {
      this.internalValue = val
      this.resetValidation()
    },

    // @TODO: DOCS
    onEnter(e) {
      // ... noop
    },
  },
})
