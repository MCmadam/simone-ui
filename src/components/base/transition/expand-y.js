/* eslint-disable no-param-reassign */
const resetStyles = (el) => {
  el.style.removeProperty('overflow')
  el.style.removeProperty('height')
}

const getSize = (el) => `${el.offsetHeight}px`

export default {
  name: 'pu-transition-expand-y',

  functional: true,

  render(h, context) {
    const data = {
      props: {
        ...context.props,
      },
      on: {
        enter(el) {
          const size = getSize(el)
          el.style.overflow = 'hidden'
          el.style.height = 0
          void el.offsetHeight

          requestAnimationFrame(() => {
            el.style.height = size
          })
        },
        leave(el) {
          const size = getSize(el)
          el.style.overflow = 'hidden'
          el.style.height = size
          void el.offsetHeight

          requestAnimationFrame(() => {
            el.style.height = 0
          })
        },

        afterEnter: resetStyles,
        afterLeave: resetStyles,
        enterCanceled: resetStyles,
        leaveCanceled: resetStyles,
      },
    }

    return h('transition', data, context.children)
  },
}
/* eslint-enable no-param-reassign */
