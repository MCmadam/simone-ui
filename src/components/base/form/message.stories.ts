import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'
import { addKnobs } from '@/util/storybook'
import ApiError from '@/util/error/api-error'

import i18n from '@/util/lang/i18n'

const props = {}

const events = {
  dismiss: action('dismiss'),
}

const stories = storiesOf('Components/Form', module)
stories.addDecorator(withKnobs)

const makeApiError = () =>
  new ApiError({
    message: 'sample error message',
    code: '401',
    config: null,
    name: null,
    response: {
      data: {
        message: 'sample error message',
        key: 'sample-error-key',
      } as any,
    } as any,
  })

stories.add('Form Message', () => ({
  i18n,
  props: addKnobs(props)(),
  methods: {
    makeApiError,
    ...events,
  },
  data() {
    return {
      error: makeApiError(),
    }
  },
  template: `
    <pu-form-message
      v-model="error"
      dismissable
      @dismiss="dismiss"
    />
  `,
}))
