import base from './base'
import Vue from 'vue'

export default Vue.extend({
  name: 'solicit-content',

  extends: base,

  props: {
    link: {
      type: Boolean,
      default: false,
    },
  },
})
