import solicits from '@mocks/model/solicit'
import solicitBaseComponent from './base'
import solicitComponent from './solicit'
import solicitContentComponent from './solicit-content'
import solicitActionable from './mixins/actionable'
import solicitTypeaware from './mixins/typeaware'

import abilityPlugin from '@/plugins/ability/plugin'
import camelCase from 'lodash/camelCase'

import { createLocalVue, shallowMount } from '@/util/test/vue'
import mixins from '@/util/vue/mixins'

// import {
//   MountOptions, Wrapper
// } from '@vue/test-utils'
import { SOLICIT_ACTIONS, SOLICIT_TYPES, SOLICIT_ENTITIES } from '@/types'

const abilityMock: any = {}
abilityMock.update = jest.fn()
abilityMock.can = jest.fn()

describe('solicit', () => {
  it('IS EMPTY', () => void 0)

  // describe('actionable mixin', () => {
  //   const localVue = createLocalVue([abilityPlugin, abilityMock])
  //   const mock = mixins(solicitActionable).extend(solicitComponent).extend({
  //     render: (h) => h('div'),
  //   })

  //   it(`exposes solicit actions`, () => {
  //     expect.assertions(1)

  //     const wrapper = shallowMount(mock, {
  //       localVue,
  //       propsData: {
  //         value: solicits.activity.invite,
  //       },
  //     })

  //     expect(wrapper.vm.SOLICIT_ACTIONS).toMatchObject(SOLICIT_ACTIONS)
  //   })

  //   it(`uses permissions to devide which actions are allowed`, () => {
  //     expect.assertions(2)
  //     const wrapper = shallowMount(mock, {
  //       localVue,
  //       ability: abilityMock,
  //       propsData: {
  //         value: solicits.activity.invite,
  //       },
  //     })

  //     abilityMock.can.mockImplementation((action) => {
  //       return action.includes(SOLICIT_ACTIONS.accept)
  //     })

  //     expect(wrapper.vm.allowedActions).toHaveLength(1)
  //     expect(wrapper.vm.allowedActions).toContain(SOLICIT_ACTIONS.accept)
  //   })
  // })

  // describe('typeaware mixin', () => {
  //   const mock = solicitTypeaware.extend({
  //     props: {
  //       value: {
  //         type: Object,
  //       },
  //     },
  //     render: (h) => h('div')
  //   })

  //   it(`exposes solicit types`, () => {
  //     expect.assertions(1)
  //     const wrapper = shallowMount(mock)
  //     expect(wrapper.vm.SOLICIT_TYPES).toMatchObject(SOLICIT_TYPES)
  //   })

  //   for (const key in SOLICIT_TYPES) {
  //     const type = SOLICIT_TYPES[key]
  //     it(`provides a check to see if the solicit is of type ${type}`, () => {
  //       expect.assertions(1)
  //       const wrapper = shallowMount(mock)
  //       wrapper.setProps({
  //         value: solicits.activity[type]
  //       })
  //       expect(wrapper.vm[camelCase(`is-${type}`)]).toBe(true)
  //     })
  //   }

  //   it(`can check the entity for each solicit`, () => {
  //     expect.assertions(3)
  //     const wrapper = shallowMount(mock)
  //     for (const key in SOLICIT_ENTITIES) {
  //       const entity = SOLICIT_ENTITIES[key]

  //       wrapper.setProps({
  //         value: solicits[entity].invite
  //       })
  //       expect(wrapper.vm.isForEntity(entity)).toBe(true)
  //     }
  //   })
  // })

  // describe('base', () => {
  //   it('should provide value prop', () => {
  //     expect.assertions(1)
  //     expect(solicitBaseComponent.options.props).toHaveProperty('value')
  //   })

  //   it('should be typeaware', () => {
  //     expect.assertions(1)
  //     expect(solicitBaseComponent.options.mixins).toContain(solicitTypeaware)
  //   })
  // })

  // describe('component', () => {
  //   const localVue = createLocalVue()
  //   localVue.use(abilityPlugin)

  //   const mock = solicitComponent.extend({
  //     render: (h) => h('div'),
  //   })

  //   // type Instance = InstanceType<typeof mock>
  //   // const mountShallowFn = (options = {} as MountOptions<Instance>) => {
  //   //   return shallowMount(mock, {
  //   //     localVue,
  //   //     ...options,
  //   //   })
  //   // }

  //   it('should extend base', () => {
  //     expect.assertions(1)
  //     expect(solicitComponent.options.extends).toBe(solicitBaseComponent)
  //   })

  //   it('should provide an ability that checks value for permissions', () => {
  //     expect.assertions(1)

  //     const wrapper = shallowMount(mock, {
  //       propsData: {
  //         value: {
  //           ...solicits.activity.invite,
  //           _meta: {
  //             permissions: [
  //               '1',
  //               '2',
  //               '3',
  //               '4',
  //             ]
  //           }
  //         }
  //       }
  //     })

  //     expect(wrapper.vm.$ability.rules).toHaveLength(4)
  //   })
  // })
})
