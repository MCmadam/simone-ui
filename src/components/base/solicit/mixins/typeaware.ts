import isForEntity from '@/lib/model/solicit/is-for-entity'
import isInvite from '@/lib/model/solicit/is-invite'
import isRequest from '@/lib/model/solicit/is-request'
import { SOLICIT_TYPES, SOLICIT_ENTITIES } from '@/types'
import Vue from 'vue'

export default Vue.extend({
  name: 'solicit-typeaware',

  data() {
    return {
      SOLICIT_TYPES,
    }
  },

  computed: {
    isInvite() {
      return isInvite(this.value)
    },
    isRequest() {
      return isRequest(this.value)
    },
  },

  methods: {
    isForEntity(entity: SOLICIT_ENTITIES, uuid?: string) {
      return isForEntity(this.value, entity, uuid)
    },
  },
})
