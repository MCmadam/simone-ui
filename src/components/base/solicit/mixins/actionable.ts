import Vue from 'vue'
import { SOLICIT_ACTIONS } from '@/types'
import solicitResolve from '@/data/solicit/resolve'
import some from 'lodash/some'
import reduce from 'lodash/reduce'

export default Vue.extend({
  name: 'solicit-actionable',

  data() {
    return {
      pending: {},
      SOLICIT_ACTIONS,
    }
  },

  computed: {
    allowedActions() {
      return reduce(
        this.SOLICIT_ACTIONS,
        (allowed, action) => {
          if (this.canDo(action)) {
            allowed.push(action)
          }
          return allowed
        },
        []
      )
    },
    actionHandlers() {
      return reduce(
        this.allowedActions,
        (handlers, action) => {
          handlers[action] = (e) => {
            if (e && 'stopPropagation' in e) {
              e.stopPropagation()
            }

            this.resolve({
              action,
              value: this.value,
            })
          }

          return handlers
        },
        {}
      )
    },
    actionPending() {
      return (action) => {
        return this.pending[action]
      }
    },
    actionDisabled() {
      // if any other action is pending this one is disabled.
      return (action) => {
        return some(this.pending, (val, key) => {
          if (val && key !== action) {
            return true
          }
        })
      }
    },
    canDo() {
      return (action) => {
        return this.$can(
          `solicit_${this.value.type}_${this.value.for}_${action}`
        )
      }
    },
  },

  methods: {
    async resolve({ action, message, value }) {
      this.setPending(action, true)

      try {
        await solicitResolve(
          {
            action,
            solicit: value,
          },
          {
            body: {
              message,
            },
          }
        )
        this.$emit(action, value)
      } catch (error) {
        this.$logger.error(error.message)
      } finally {
        this.setPending(action, false)
      }
    },
    setPending(action, value) {
      this.$set(this.pending, action, value)
    },
  },
})
