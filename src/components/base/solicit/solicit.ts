import base from './base'
import ability from '@/mixins/ability'
import mixins from '@/util/vue/mixins'

export default mixins(
  ability({
    attribute: 'value',
  })
).extend({
  name: 'pu-solicit',

  extends: base,
})
