import typeaware from './mixins/typeaware'
import mixins from '@/util/vue/mixins'

export default mixins(typeaware).extend({
  name: 'solicit-base',

  props: {
    value: {
      type: Object,
      required: true,
    },
    // temp to disable activity name in activity detail view
    displayContext: {
      type: Boolean,
      default: true,
    },
  },
})
