
# Storage picker

1. get presigned url with permission to upload to the bucket
1. upload the file to the bucket
1. signal the back-end that the file is uploaded by posting without data to trigger the resizing etc.
