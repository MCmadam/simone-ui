/* eslint-env node */

import { storiesOf } from '@storybook/vue'
import viewer from '@/components/util/story/viewer'

import PuPickerVisibility from './picker-visibility'
import PuRadio from '@/components/base/input/radio/radio'

storiesOf('components/Visibility picker', module)
  .addDecorator(viewer)
  .add('test', () => {
    return {
      components: {
        PuPickerVisibility,
        PuRadio,
      },
      data() {
        return {
          radio: {
            one: 'value 1',
            two: 'value 2',
            three: 'value 3',
          },
        }
      },
      template: `
        <flextory>
          <pu-picker-visibility
          >
            <pu-radio :value="radio.one" icon="home" indeterminate>
              <h4>testing</h4>
              <span slot="description">DESCRIPTIONES</span>
            </pu-radio>
            <pu-radio :value="radio.two" icon="close" indeterminate>
              <template v-slot:default><h4>radio 2</h4></template>
              <template v-slot:description><span>SOME DESCRIPTION</span></template>
            </pu-radio>
          </pu-picker-visibility>
        </flextory>
      `,
    }
  })
