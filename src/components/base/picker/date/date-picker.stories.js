/* eslint-env node */

import { storiesOf } from '@storybook/vue'
import viewer from '@/components/util/story/viewer'

storiesOf('components/picker/date', module)
  .addDecorator(viewer)
  .add('default', () => {
    return {
      template: `
        <flextory>
          <pu-datepicker
          
          />
        </flextory>
      `,
    }
  })
