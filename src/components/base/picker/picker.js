import PuInputBase from '@/components/base/input/_base/input-base'

export default {
  name: 'pu-picker',

  extends: PuInputBase,
}
