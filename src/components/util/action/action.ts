import Vue from 'vue'

export default Vue.extend({
  name: 'pu-action',

  inheritAttrs: false,

  props: {
    action: {
      type: Function,
      required: true,
    },
    disabled: {
      type: Boolean,
      default: false,
    },
  },

  data() {
    return {
      error: null,
      pending: false,
      success: null,
    }
  },

  methods: {
    async trigger(...args) {
      if (this.pending || this.disabled) {
        return
      }

      this.pending = true

      try {
        const result = await this.action(...args)
        this.success = true
        this.error = null
        this.$emit('success', result)
      } catch (error) {
        this.error = error
        this.success = false
        this.$emit('error', error)

        if (process.env.NODE_ENV === 'development') {
          this.$logger.error(error)
        }
      } finally {
        this.pending = false
      }
    },
  },

  render() {
    return this.$scopedSlots.default({
      trigger: this.trigger,
      pending: this.pending,
      error: this.error,
      success: this.success,
    })
  },
})
