import Vue from 'vue'

export default Vue.extend({
  name: 'pu-scope',

  functional: true,
  inheritAttrs: false,

  render: (_, context) => {
    return context.scopedSlots.default(context.props)
  },
})
