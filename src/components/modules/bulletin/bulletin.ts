import Vue, { PropType } from 'vue'
import { mapGetters } from 'vuex'
import {
  BULLETIN_SCOPES,
  BULLETIN_TYPES_ORGANIZATION,
  BULLETIN_TYPES_USER,
} from '@/types/model'
import { PropValidator } from 'vue/types/options'
import BulletinTypes from './bulletin-types-per-scope'

export default Vue.extend({
  name: 'pu-bulletin',

  props: {
    scope: {
      type: String as PropType<BULLETIN_SCOPES>,
      required: true,
    } as PropValidator<BULLETIN_SCOPES>,
    type: {
      type: String as PropType<
        BULLETIN_TYPES_ORGANIZATION | BULLETIN_TYPES_USER
      >,
      required: true,
    },
  },

  created() {
    this.validateProps()
  },

  computed: {
    ...mapGetters('bulletin', ['forScopeAndType']),
    component() {
      return () => import(`./templates/${this.scope}/${this.type}`)
    },
  },

  methods: {
    validateProps() {
      if (!(this.scope in BULLETIN_SCOPES)) {
        throw new Error(`bulletin domain unknown: ${this.scope}`)
      }
      if (!(this.type in BulletinTypes[this.scope])) {
        throw new Error(`bulletin type unknown for domain: ${this.scope}`)
      }
    },
  },

  render(h) {
    const { scope, type } = this
    const bulletin = this.forScopeAndType(scope, type)

    if (bulletin) {
      return h(this.component, {
        props: {
          bulletin,
          resolve: (bulletin) =>
            this.$store.dispatch('bulletin/resolve', bulletin),
        },
      })
    }

    return null
  },
})
