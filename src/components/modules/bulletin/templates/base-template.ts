import Vue, { PropType } from 'vue'
import { Bulletin } from '@/types/model'

export default Vue.extend({
  inheritAttrs: false,

  props: {
    bulletin: {
      type: Object as PropType<Bulletin>,
      required: true,
    },
    resolve: {
      type: Function,
      default: () => void 0,
    },
  },
})
