import { mergeVNodeHook } from '@/util/vue'

export default {
  name: 'pu-dialog-activator',

  abstract: true,

  data() {
    return {
      activator: null,
      assigned: false,
    }
  },

  props: {
    title: {
      type: String,
      required: true,
    },
    text: {
      type: String,
      required: true,
    },
    labelSuccess: {
      type: String,
    },
    labelCancel: {
      type: String,
    },
    success: {
      type: Object,
    },
    cancel: {
      type: Object,
    },
  },

  mounted() {
    this.addListener()
  },

  beforeDestroy() {
    this.removeListener()
  },

  methods: {
    openDialog() {
      this.$dialog.open({
        title: this.title,
        text: this.text,
        labelSuccess: this.labelSuccess,
        labelCancel: this.labelCancel,
        props: {
          success: this.success,
          cancel: this.cancel,
        },
        onSuccess: () => {
          this.$emit('success')
        },
        onCancel: () => {
          this.$emit('cancel')
        },
      })
    },
    onActivatorClick(e) {
      e.preventDefault()
      e.stopPropagation()
      this.openDialog()
    },
    addListener() {
      const vnode = this.$slots.default[0]
      this.activator = vnode.elm

      if (this.activator) {
        mergeVNodeHook(vnode, 'beforeDestroy', this.removeListener)
        this.activator.addEventListener('click', this.onActivatorClick)
        this.assigned = true
      }
    },
    removeListener() {
      if (this.assigned && this.activator) {
        this.activator.removeEventListener('click', this.onActivatorClick)
        this.assigned = false
      } else if (this.assigned) {
        throw new Error('dialog-activator, it seems theres a memory leak')
      }
    },
  },

  render() {
    return this.$slots.default[0]
  },
}
