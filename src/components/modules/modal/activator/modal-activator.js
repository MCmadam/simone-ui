import { mergeVNodeHook } from '@/util/vue'

export default {
  name: 'pu-modal-activator',

  abstract: true,

  inheritAttrs: false,

  data() {
    return {
      activator: null,
      assigned: false,
    }
  },

  props: {
    for: {
      type: String,
      required: true,
    },
    modalProps: {
      type: Object,
    },
  },

  mounted() {
    try {
      this.addListener()
    } catch (error) {
      this.$logger.error(error)
    }
  },

  beforeDestroy() {
    try {
      this.removeListener()
    } catch (error) {
      this.$logger.error(error)
    }
  },

  methods: {
    /** @public */
    openModal() {
      this.$modal.open(this.for, {
        onSuccess: this.$listeners.success,
        onCancel: this.$listeners.cancel,
        props: this.modalProps,
      })
    },
    onActivatorClick(e) {
      e.preventDefault()
      this.openModal()
    },
    addListener() {
      const vnode = this.$slots.default[0]
      this.activator = vnode.elm

      if (this.activator) {
        mergeVNodeHook(vnode, 'beforeDestroy', this.removeListener)
        this.activator.addEventListener('click', this.onActivatorClick)
        this.assigned = true
      }
    },
    removeListener() {
      if (this.assigned && this.activator) {
        this.activator.removeEventListener('click', this.onActivatorClick)
        this.assigned = false
      } else if (this.assigned) {
        throw new Error("modal-activator, it seems there's a memory leak")
      }
    },
  },

  render() {
    return this.$slots.default[0]
  },
}
