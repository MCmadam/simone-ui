import Vue from 'vue'

// #util
import action from '@/components/util/action/action'
import scope from '@/components/util/scope'
//

// #region small components
import alert from '@/components/base/alert/alert.vue'
import avatar from '@/components/base/avatar/avatar.vue'
import badge from '@/components/base/badge/badge.vue'
import button from '@/components/base/button/button.vue'
import date from '@/components/base/date/date.vue'
import icon from '@/components/base/icon/icon.vue'
import link from '@/components/base/link/link.vue'
import list from '@/components/base/list/list.vue'
import navigationdrawer from '@/components/base/navigation-drawer/navigation-drawer.vue'
import menu from '@/components/base/menu/menu.vue'
import progresscircular from '@/components/base/progress/circular/circular.vue'
import svg from '@/components/base/image/svg/svg.vue'
import tip from '@/components/base/tip/tip.vue'
import toolbar from '@/components/base/toolbar/toolbar.vue'
// #endregion

// #region form
import form from '@/components/base/form/form.vue'
import formMessage from '@/components/base/form/message.vue'
import label from '@/components/base/label/label.vue'
import contentEditable from '@/components/base/input/content-editable.vue'
import formControl from '@/components/base/input/form-control.vue'
import checkbox from '@/components/base/input/checkbox/checkbox.vue'
import radio from '@/components/base/input/radio/radio.vue'
import radioGroup from '@/components/base/input/radio/radio-group.vue'
import select from '@/components/base/input/select/select.vue'
import switchComponent from '@/components/base/input/switch/switch.vue'
import textarea from '@/components/base/input/textarea/textarea.vue'
import textfield from '@/components/base/input/textfield/textfield.vue'
// #endregion

// #region modules
import dialogActivator from '@/components/modules/dialog/activator/dialog-activator.js'
import modalActivator from '@/components/modules/modal/activator/modal-activator.js'
// #endregion

const dynamic: any[] = []
;[
  require.context('./layouts', true, /\.vue$/),
  require.context('./base/transition', true, /\.(js|ts|vue)$/),
].forEach((context) => {
  context.keys().forEach((fileName) => {
    const resolved = context(fileName)
    const component = resolved.default || resolved
    dynamic.push(component)
  })
})
;[
  scope,
  action,

  ...dynamic,
  alert,
  avatar,
  badge,
  button,
  date,
  icon,
  link,
  list,
  navigationdrawer,
  menu,
  progresscircular,
  svg,
  tip,
  toolbar,

  form,
  formMessage,
  formControl,
  contentEditable,
  checkbox,
  radio,
  radioGroup,
  select,
  textarea,
  textfield,
  switchComponent,
  label,

  dialogActivator,
  modalActivator,
].forEach((component) => {
  const componentName = component.options?.name || component.name
  Vue.component(componentName, component)
})
