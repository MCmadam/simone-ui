import solicitContent from '@/components/base/solicit/solicit-content'
import isForUser from '@/lib/model/solicit/is-for-user'
import Vue from 'vue'
import { mapGetters } from 'vuex'

export default Vue.extend({
  name: 'solicit-content-base',

  extends: solicitContent,

  computed: {
    ...mapGetters(['currentUser']),
    isMine() {
      return isForUser(this.value, this.currentUser)
    },
    headerMessage() {
      const dynamicPart = this.isMine ? '-self' : ''
      return this.$t(
        `list-item-solicit-${this.value.type}-content-${this.value.for}-header${dynamicPart}`
      )
    },
  },
})
