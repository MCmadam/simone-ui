import solicit from '@/components/base/solicit/solicit'
import Vue from 'vue'

export default Vue.extend({
  name: 'solicit-base',

  extends: solicit,

  props: {
    link: {
      type: Boolean,
      default: false,
    },
  },
})
