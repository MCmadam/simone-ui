// import {
//   RouterLinkStub, MountOptions,
// } from '@vue/test-utils'
// import {
//   createLocalVue,
//   mount,
// } from '@/util/test/vue'

// import SOLICIT_ACTIONS from '@/lib/model/solicit/solicit-actions'
// import SOLICIT_TYPES from '@/lib/model/solicit/solicit-types'
// import SOLICIT_ENTITIES from '@/lib/model/solicit/solicit-entities'
// import PuListItemSolicit from './list-item-solicit.vue'
// import PuSolicitContentActivity from './content/content-activity.vue'
// import PuSolicitContentOrganization from './content/content-organization.vue'
// import PuSolicitContentPartup from './content/content-partup.vue'
// import solicits from '@mocks/model/solicit'

// import merge from 'lodash/merge'
// import Vue from 'vue'
// import Vuex from 'vuex'
// import { ExtractVue } from '@/util/vue/mixins'

// jest.mock('@/api/api')

// const contentStubs = {
//   'pu-solicit-content-activity': PuSolicitContentActivity,
//   'pu-solicit-content-organization': PuSolicitContentOrganization,
//   'pu-solicit-content-partup': PuSolicitContentPartup,
// }

describe('list item solicit', () => {
  it('IS EMPTY', () => void 0)
})

// describe('list-item-solicit component', () => {
//   const localVue = createLocalVue(Vuex)
//   // const mock = Vue.extend(PuListItemSolicit)

//   // const createWrapper = (options: MountOptions<PuListItemSolicit>) => {
//   //   return mount(PuListItemSolicit, merge({
//   //     localVue,
//   //     store: new Vuex.Store({
//   //       modules: {
//   //         user: {
//   //           namespaced: true,

//   //           state: {
//   //             currentUser: {
//   //               uuid: 'user-uuid',
//   //             },
//   //           },
//   //         },
//   //       },
//   //     }),
//   //     stubs: {
//   //       ...contentStubs,
//   //     },
//   //   }))
//   // }

//   let solicit

//   describe('When the solicit is for you', () => {
//     beforeEach(() => {
//       solicit = {
//         ...solicits.organization.invite,
//         user: null,
//       }
//     })

//     it(`does not display user information`, () => {
//       expect.assertions(2)
//       const wrapper = mount(PuListItemSolicit, {
//         propsData: {
//           value: solicit,
//         },
//       })

//       expect(wrapper.find('pu-avatar').exists()).toBe(false)
//       expect(wrapper.find(RouterLinkStub).exists()).toBe(false)
//     })

//     it(`adds -self to the key used for header message`, () => {
//       expect.assertions(1)

//       const wrapper = createWrapper({
//         propsData: {
//           value: solicit,
//         }
//       })

//       const contentInstance = wrapper.find(contentStubs['pu-solicit-content-organization'])
//       expect(contentInstance.html()).toMatch(/-self<\/h3>/)
//     })
//   })

//   describe('When the solicit is for someone else', () => {
//     beforeEach(() => {
//       solicit = solicits.organization.invite
//     })

//     it(`displays user information as link to user profile`, () => {
//       expect.assertions(1)

//       // const wrapper = createWrapper({
//       //   propsData: {
//       //     value: solicit,
//       //   },
//       // })

//       const wrapper = mount(PuListItemSolicit)

//       wrapper.vm.contentComponent

//       expect(wrapper.find(RouterLinkStub).props().to).toMatchObject({
//         name: 'user-profile',
//         params: {
//           user_slug: solicit.user.slug,
//         },
//       })
//     })

//     it(`displays the user avatar as part of the link`, () => {
//       expect.assertions(2)

//       const wrapper = createWrapper({
//         propsData: {
//           value: solicit,
//         },
//       })

//       expect(wrapper.find('pu-avatar').props().src).toBe(solicit.user.images.avatar_small)
//       expect(wrapper.find('pu-avatar').props().text).toBe(solicit.user.name)
//     })

//     it(`uses user name as text in link`, () => {
//       expect.assertions(1)

//       const wrapper = createWrapper({
//         propsData: {
//           value: solicit,
//         },
//       })

//       expect(wrapper.find(RouterLinkStub).html()).toContain(`<span>${solicit.user.name}</span>`)
//     })
//   })

//   describe(`when I want to resolve the solicit`, () => {
//     it(`uses permissions to decide which actions are allowed`, () => {
//       expect.assertions(2)

//       solicit = solicits.organization.invite
//       const wrapper = mount(PuListItemSolicit)

//       expect(wrapper.vm.allowedActions).toHaveLength(1)
//       expect(wrapper.vm.allowedActions).toContain(SOLICIT_ACTIONS.accept)
//     })

//     it(`displays a button for all actions that are allowed`, () => {
//       expect.assertions(2)

//       solicit = solicits.organization.invite

//       const wrapper = createWrapper({
//         propsData: {
//           value: solicit,
//         }
//       })

//       expect(wrapper
//         .find('div.actions')
//         .findAll('pu-button')
//         .filter((wrapper) => {
//           return wrapper.html().includes(`list-item-solicit-modal-action-accept`)
//         })).toHaveLength(1)

//       abilityFnMock.mockImplementation(() => false)

//       expect(wrapper
//         .find('div.actions')
//         .findAll('pu-button')).toHaveLength(0)
//     })

//     it(`requests to open the modal for specified action`, () => {
//       expect.assertions(1)

//       const openModalMock = jest.fn()

//       solicit = solicits.organization.invite

//       abilityFnMock.mockImplementation((action) => {
//         return action.includes(SOLICIT_ACTIONS.accept)
//       })

//       sut = makeSut({
//         value: solicit,
//       })

//       wrapper.setMethods({
//         openModal: openModalMock,
//       })

//       const acceptButtonWrapper = wrapper
//         .find('div.actions')
//         .findAll(button)
//         .filter((x) => x.html().includes('list-item-solicit-modal-action-accept'))
//         .wrappers[0]

//       acceptButtonWrapper.trigger('click')

//       expect(openModalMock).toHaveBeenCalledWith({ action: SOLICIT_ACTIONS.accept, value: solicit })
//     })

//     it(`emit's {action} with {data} when resolve is sucessful`, async (done) => {
//       expect.assertions(1)

//       solicit = solicits.organization.invite

//       sut = makeSut({
//         value: solicit,
//       })

//       await sut.vm.resolve({
//         action: SOLICIT_ACTIONS.accept,
//         value: solicit,
//       })

//       expect(sut.emitted(SOLICIT_ACTIONS.accept)[0]).toMatchObject([{
//         action: SOLICIT_ACTIONS.accept,
//         value: solicit,
//       }])

//       done()
//     })
//   })

//   describe(`When I see information about the solicit`, () => {
//     Object.values(SOLICIT_ENTITIES).forEach((entity) => {
//       it(`${entity} get's displayed via dynamic component`, () => {
//         expect.assertions(1)

//         solicit = solicits[entity].invite

//         sut = makeSut({
//           value: solicit,
//         })

//         expect(sut.find(contentStubMap[entity]).exists()).toBe(true)
//       })

//       it(`${entity} provides a header slot`, () => {
//         expect.assertions(2)

//         solicit = solicits[entity].invite

//         sut = makeSut({
//           value: solicit,
//         }, {
//           header: {
//             template: `<div class="headerSlot">headerSlotContent</div>`,
//           },
//         })

//         const contentInstance = sut.find(contentStubMap[entity])

//         expect(contentInstance.find('h3').exists()).toBe(false)
//         expect(contentInstance.find('div.headerSlot').html()).toMatch('headerSlotContent')
//       })

//       it(`${entity} provides a default slot for extra information`, () => {
//         expect.assertions(2)

//         solicit = solicits[entity].invite

//         sut = makeSut({
//           value: solicit,
//         }, {
//           default: {
//             template: `<div class="defaultSlot">defaultSlotContent</div>`,
//           },
//         })

//         const contentInstance = sut.find(contentStubMap[entity])

//         expect(contentInstance.find('h3').exists()).toBe(true)
//         expect(contentInstance.find('div.defaultSlot').html()).toMatch('defaultSlotContent')
//       })
//     })

//     describe(`and it is a solicit for an activity`, () => {
//       it(`uses a translation key to display a header`, () => {
//         expect.assertions(1)

//         solicit = solicits.activity.invite

//         sut = makeSut({
//           value: solicit,
//         })

//         const contentInstance = sut.find(contentStubMap.activity)

//         expect(contentInstance.find('h3').html()).toContain(`${solicit.type}-content-${solicit.for}-header`)
//       })

//       it(`displays a link to the activity detail with activity name as text`, () => {
//         expect.assertions(2)

//         solicit = solicits.activity.invite

//         sut = makeSut({
//           value: solicit,
//           link: true,
//         })

//         const contentInstance = sut.find(contentStubMap.activity)
//         const linkInstance = contentInstance.find(RouterLinkStub)

//         expect(linkInstance.props().to).toMatchObject({
//           name: 'partup-activity',
//           params: {
//             activity_uuid: solicit.activity.uuid,
//             partupSlug: solicit.partup.slug,
//           },
//         })

//         expect(linkInstance.html()).toContain(solicit.activity.name)
//       })
//     })

//     describe('and it is a solicit for an organization', () => {
//       it(`uses a translation key to display a header`, () => {
//         expect.assertions(1)

//         solicit = solicits.organization.invite

//         sut = makeSut({
//           value: solicit,
//         })

//         const contentInstance = sut.find(contentStubMap.organization)

//         expect(contentInstance.find('h3').html()).toContain(`${solicit.type}-content-${solicit.for}-header`)
//       })

//       it(`displays a link to the organization with organization name as text`, () => {
//         expect.assertions(2)

//         solicit = solicits.organization.invite

//         sut = makeSut({
//           value: solicit,
//           link: true,
//         })

//         const contentInstance = sut.find(contentStubMap.organization)
//         const linkInstance = contentInstance.find(RouterLinkStub)

//         expect(linkInstance.props().to).toMatchObject({
//           name: 'organization',
//           params: {
//             organization_slug: solicit.organization.slug,
//           },
//         })

//         expect(linkInstance.html()).toContain(solicit.organization.name)
//       })
//     })

//     describe('and it is a solicit for a partup', () => {
//       it(`uses a translation key to display a header`, () => {
//         expect.assertions(1)

//         solicit = solicits.partup.invite

//         sut = makeSut({
//           value: solicit,
//         })

//         const contentInstance = sut.find(contentStubMap.partup)

//         expect(contentInstance.find('h3').html()).toContain(`${solicit.type}-content-${solicit.for}-header`)
//       })

//       it(`displays a link to the partup with partup name as text`, () => {
//         expect.assertions(2)

//         solicit = solicits.partup.invite

//         sut = makeSut({
//           value: solicit,
//           link: true,
//         })

//         const contentInstance = sut.find(contentStubMap.partup)
//         const linkInstance = contentInstance.find(RouterLinkStub)

//         expect(linkInstance.props().to).toMatchObject({
//           name: 'partup',
//           params: {
//             partupSlug: solicit.partup.slug,
//           },
//         })

//         expect(linkInstance.html()).toContain(solicit.partup.name)
//       })
//     })
//   })
// })
