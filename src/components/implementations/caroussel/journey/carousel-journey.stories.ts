// import { storiesOf } from '@storybook/vue'
// import { action } from '@storybook/addon-actions'
// import { withKnobs } from '@storybook/addon-knobs'
// import { addKnobs } from '@/util/storybook'

// import PuCarouselJourney from './carousel.vue'
// import PuTileActivity from '@/components/modules/journey/carousel/tile-activity.vue'
// import PuTileIntermission from '@/components/modules/journey/carousel/tile-intermission.vue'

// import range from 'lodash/range'

// const props = {

// }

// const events = {

// }

// const stories = storiesOf('Journey/Vind leuk werk', module)
// stories.addDecorator(withKnobs)

// stories.add('Carousel', () => ({
//   components: {
//     PuCarouselJourney,
//     PuTileActivity,
//     PuTileIntermission,
//   },
//   props: addKnobs(props)(),
//   methods: events,
//   data() {
//     return {
//       items: range(0, 10),
//     }
//   },
//   template: `
//     <pu-carousel-journey :items="items" :intermission="2">
//       <template v-slot:default="{ item, next }">
//         <pu-tile-activity :value="item" @forget="next" @bookmark="next" />
//       </template>

//       <template v-slot:intermission="{ index, next }">
//         <pu-tile-intermission @click="next">
//           {{ index }}
//         </pu-tile-intermission>
//       </template>
//     </pu-carousel-journey>
// `,
// }))
