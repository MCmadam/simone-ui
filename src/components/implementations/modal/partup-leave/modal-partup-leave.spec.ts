import Vue from 'vue'

import PuModal from '@/components/base/modal/modal'
import PuModalPartupLeave from './modal-partup-leave.vue'

import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import VueI18n from 'vue-i18n'
import Vuex, { Store } from 'vuex'

import { configureApi } from '@/util/test'
import { createLocalVue, mount } from '@/util/test/vue'

import icon from '@/components/base/icon/icon.vue'
import button from '@/components/base/button/button.vue'
import progresscircular from '@/components/base/progress/circular/circular.vue'
import action from '@/components/util/action/action'

configureApi()

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueI18n)

const mockPartup = {
  uuid: 'partup-uuid',
  name: 'partup-name',
}

const mockUser = {
  uuid: 'user-uuid',
}

const store = new Store({
  getters: {
    currentUser: () => mockUser,
  },
})

const i18n = new VueI18n()
const mockAdapter = new MockAdapter(axios, { delayResponse: 500 })

const findByHtml = (wrapper, selector, match) => {
  const wrappers = wrapper.findAll(selector).filter((vnodeWrapper) => {
    return vnodeWrapper.html().includes(match)
  })

  if (wrappers.length > 0) {
    return wrappers.at(0)
  }
}

describe('Modal Partup Leave', () => {
  const lastPartnerHeading = 'heading-info-last-partner'
  const lastPartnerDescription = 'description-last-partner'

  const contributorDescription = 'description-is-contributor'

  const defaultHeading = 'heading-info'
  const defaultDescription = 'description'

  const component = mount(PuModalPartupLeave, {
    localVue,
    store,
    i18n,
    stubs: {
      [button.options.name]: button,
      [icon.name]: icon,
      [progresscircular.name]: progresscircular,
      [action.name]: action,
    },
    propsData: {
      partup: mockPartup,
    },
  })

  const clearEmittedEvents = (component, event) => {
    component._emitted[event] = []
  }

  const canBeCanceled = () => {
    it('can be canceled by clicking the button stay', () => {
      expect.assertions(2)

      clearEmittedEvents(component, 'cancel')

      expect(component.html()).toContain('button-stay')

      const cancelButton = findByHtml(component, 'button', 'button-stay')

      cancelButton.trigger('click')

      expect(component.emitted('cancel').length).toBe(1)
    })
  }

  const isLastPartner = (bool, text?: string) => {
    it(text || `isLastPartner is ${bool}`, () => {
      expect.assertions(1)

      const vm = component.vm as any

      expect(vm.isLastPartner).toBe(bool)
    })
  }

  const isContributor = (bool, text?: string) => {
    it(text || `isContributor is ${bool}`, () => {
      expect.assertions(1)

      const vm = component.vm as any

      expect(vm.isContributor).toBe(bool)
    })
  }

  it('extends the base modal', () => {
    expect.assertions(1)

    expect(PuModalPartupLeave.options.extends).toBe(PuModal)
  })

  it('is persistent', () => {
    expect.assertions(1)

    expect(component.emitted('config')[0]).toMatchObject([{ persistent: true }])
  })

  it('displays a loading indicator when booting', () => {
    expect.assertions(1)

    component.setData({
      isBooted: false,
    })

    const indicatorWrapper = component.find({ name: 'pu-progress-circular' })

    expect(indicatorWrapper.isVisible()).toBe(true)
  })

  it('last partner takes precedence above contributor', async () => {
    expect.assertions(3)

    component.setData({
      partners: [store.getters['currentUser']],
      activities: [1, 2, 3, 4],
      isBooted: true,
    })

    await component.vm.$nextTick()

    const componentHtml = component.html()
    const componentVm = component.vm as any

    expect(componentVm.isLastPartner).toBe(true)
    expect(componentVm.isContributor).toBe(true)
    expect(componentHtml).toContain(lastPartnerHeading)
  })

  describe('When the user is the last partner', () => {
    beforeAll(() => {
      component.setData({
        partners: [store.getters['currentUser']],
        isBooted: true,
      })
    })

    isLastPartner(true)
    isContributor(true, 'isContributor may be true')
    canBeCanceled()

    it('renders last partner i18n keys', () => {
      expect.assertions(3)

      expect(component.html()).toContain(lastPartnerHeading)
      expect(component.html()).toContain(lastPartnerDescription)
      expect(component.element).toMatchSnapshot()
    })

    it('does not render the contributor content', () => {
      expect.assertions(2)

      const iconWrapper = component.find({ name: 'pu-icon' })

      expect(iconWrapper.exists()).toBe(false)
      expect(findByHtml(component, 'p', 'contributor')).toBeUndefined()
    })

    it('uses last partner stay button text', () => {
      expect.assertions(1)

      const stayButtonWrapper = findByHtml(
        component,
        'button',
        'button-stay-last-partner'
      )

      expect(stayButtonWrapper.exists()).toBe(true)
    })

    it('does not render the leave button', () => {
      expect.assertions(1)

      expect(component.html()).not.toContain('button-leave')
    })
  })

  describe('When the user is a contributor', () => {
    beforeAll(() => {
      component.setData({
        partners: [{}, {}, {}],
        activities: [1, 2, 3, 4],
        isBooted: true,
      })
    })

    isLastPartner(false)
    isContributor(true)
    canBeCanceled()

    it('renders the contributor i18n keys', () => {
      expect.assertions(3)

      expect(component.html()).toContain(defaultHeading)
      expect(component.html()).toContain(contributorDescription)
      expect(component.element).toMatchSnapshot()
    })

    it('renders the contributor content', () => {
      expect.assertions(2)

      const iconWrapper = component.find({ name: 'pu-icon' })

      expect(iconWrapper.exists()).toBe(true)
      expect(component.html()).toContain('<p>contributor</p>')
    })

    it('does render the leave button', () => {
      expect.assertions(1)

      const leaveButtonWrapper = findByHtml(component, 'button', 'button-leave')

      expect(leaveButtonWrapper.exists()).toBe(true)
    })
  })

  describe('When the user is not a partner and not a contributor', () => {
    beforeAll(() => {
      component.setData({
        isBooted: true,
        activities: [],
        partners: [],
      })
    })

    isLastPartner(false)
    isContributor(false)

    it('renders the default i18n keys', () => {
      expect.assertions(3)

      expect(component.html()).toContain(defaultHeading)
      expect(component.html()).toContain(defaultDescription)
      expect(component.element).toMatchSnapshot()
    })
  })

  describe('When the leave button is clicked', () => {
    beforeAll(() => {
      component.setData({
        activities: [],
        partners: [],
        isBooted: true,
      })
    })

    it('calls leavePartner with partup', (done) => {
      expect.assertions(1)

      const leavePartupHandler = jest.fn(() => [200])
      mockAdapter.onDelete(/partners/).reply(leavePartupHandler)

      const leaveButton = findByHtml(component, 'button', 'button-leave')

      leaveButton.trigger('click')

      setImmediate(() => {
        expect(leavePartupHandler).toHaveBeenCalledWith(
          expect.objectContaining({
            url: `mocked/partup/${mockPartup.uuid}/partners`,
          })
        )
        done()
      })
    })

    // this test depends on leavePartner to be successful
    it("EMPTY: emit's success when successful", () => {
      // expect.assertions(1)
      // setImmediate(() => {
      //   expect(component.emitted('success')).toHaveLength(1)
      //   done()
      // })
    })

    it("EMPTY: set's the error when an error is thrown", () => {
      // expect.assertions(1)
      // const vm = component.vm as any
      // mockAdapter.onDelete(/partners/).reply(501)
      // const leaveButton = findByHtml(component, 'button', 'button-leave')
      // leaveButton.trigger('click')
      // expect(vm.error).not.toBeNull()
    })
  })
})
