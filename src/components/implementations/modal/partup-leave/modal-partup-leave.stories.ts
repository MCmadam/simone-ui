import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'

import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { Store } from 'vuex'

import api from '@/api'
import i18n from '@/util/lang/i18n'

import PuModalActivator from '@/components/modules/modal/activator/modal-activator'
import PuModalShell from '@/components/modules/modal/shell/modal-shell.vue'
import PuModalPortal from '@/components/modules/modal/portal/modal-portal.vue'
import PuModalPartupLeave from './modal-partup-leave.vue'

import PuFlex from '@/components/util/storybook/flex.vue'

const mockUser = {
  uuid: 'user-uuid',
}

const mockPartup = {
  uuid: 'partup-uuid',
  name: 'Test partup naam',
}

const store = new Store({
  getters: {
    currentUser: () => mockUser,
  },
})

const stories = storiesOf('Modals/Leave Partup', module)
stories.addDecorator(withKnobs)

stories.add(
  'Leave without consequences',
  () => {
    const mockAdapter = new MockAdapter(axios)

    mockAdapter.onGet(/activities/).reply(200, { data: [] })
    mockAdapter.onGet(/partners/).reply(200, { data: [{}, {}, {}] })
    mockAdapter.onDelete(/partners/).reply(200)

    return {
      i18n,
      store,
      components: {
        PuFlex,
        PuModalShell,
        PuModalPartupLeave,
      },
      data() {
        return {
          partup: mockPartup,
        }
      },
      template: `
      <pu-flex align="center" justify="center">
        <pu-modal-shell>
          <pu-modal-partup-leave
            :partup="partup"
          />
        </pu-modal-shell>
      </pu-flex>
    `,
    }
  },
  {
    notes: 'The user is not the last partner and is not a contributor',
  }
)

stories.add(
  'User is last partner',
  () => {
    const mockAdapter = new MockAdapter(axios)

    mockAdapter
      .onGet(/partners/)
      .reply(200, { data: [store.getters['currentUser']] })
    mockAdapter.onGet(/activities/).reply(200, { data: [1, 2, 3, 4] })
    mockAdapter.onDelete(/partners/).reply(200, { data: {} })

    return {
      i18n,
      store,
      components: {
        PuModalShell,
        PuModalPartupLeave,
      },
      data() {
        return {
          partup: mockPartup,
        }
      },
      template: `
      <pu-modal-shell>
        <pu-modal-partup-leave :partup="partup" />
      </pu-modal-shell>
    `,
    }
  },
  {
    notes: 'poop!',
  }
)

stories.add('User is a contributor', () => {
  const mockAdapter = new MockAdapter(axios)

  mockAdapter
    .onGet(/partners/)
    .reply(200, { data: [store.getters['currentUser'], {}] })
  mockAdapter.onGet(/activities/).reply(200, { data: [1, 2, 3, 4] })
  mockAdapter.onDelete(/partners/).reply(200, { data: {} })

  return {
    i18n,
    store,
    components: {
      PuModalShell,
      PuModalPartupLeave,
    },
    data() {
      return {
        partup: mockPartup,
      }
    },
    methods: {
      onSuccess: action('success'),
    },
    template: `
      <pu-modal-shell>
        <pu-modal-partup-leave :partup="partup" @success="onSuccess" />
      </pu-modal-shell>
    `,
  }
})

stories.add('Error on load', () => {
  const mockAdapter = new MockAdapter(axios, { delayResponse: 2000 })

  mockAdapter.onGet(/activities/).reply(200, { data: [] })
  mockAdapter.onGet(/partners/).reply(500)

  return {
    i18n,
    store,
    components: {
      PuModalActivator,
      PuModalPortal,
      PuModalPartupLeave,
    },
    data() {
      return {
        partup: mockPartup,
      }
    },
    methods: {
      onCancel: action('cancel'),
    },
    template: `
      <div>
        <pu-modal-activator
          for="partup-leave"
          :modal-props="{
            partup,
          }"
          @success="onSuccess"
          @cancel="onCancel"
        >
          <pu-button>
            Open de modal
          </pu-button>
        </pu-modal-activator>

        <pu-modal-portal />
      </div>
    `,
  }
})

stories.add('Error flow when leaving', () => {
  const mockAdapter = new MockAdapter(axios, { delayResponse: 2000 })

  mockAdapter
    .onGet(/partners/)
    .reply(200, { data: [store.getters['currentUser'], {}, {}, {}] })
  mockAdapter.onGet(/activities/).reply(200, { data: [] })
  mockAdapter.onDelete(/partners/).networkError()

  return {
    i18n,
    store,
    components: {
      PuModalShell,
      PuModalPartupLeave,
    },
    data() {
      return {
        partup: mockPartup,
      }
    },
    methods: {
      onSuccess: action('success'),
      onCancel: action('cancel'),
    },
    template: `
      <pu-modal-shell>
        <pu-modal-partup-leave
          :partup="partup"
          @success="onSuccess"
          @cancel="onCancel"
        />
      </pu-modal-shell>
    `,
  }
})
