# Auth flows

## Steps to cover all cases

All steps are take with account A unless specified as account B.

1. register account B (and wait 24hrs so the code can expire);
1. register account A;
1. register account A;
    - error 'already in use';
1. login;
    - error 'user not confirmed' -> 'registerResend'
1. follow resend procedure;
1. goto flow forgotPassword;
    - error 'user not confirmed' -> 'registerResend'
1. goto mail and open first verification mail
    - copy all text after `/` and paste it at the url
    - example: test.alpha.part-up.com (copy next -->) /registreren/.....
    - paste url in browser and change code={code} to code=1234
    - end result should be something like this: test.alpha.part-up.com/registreren/bevestigen/{cognito_id}?code=1234
    - error 'code invalid' -> 'registerResend'
1. goto mail and follow original verification link;
    - error 'code expired' -> 'registerResend'
1. goto mail and follow newer verification link;
1. login;
1. logout;
1. goto flow forgotPassword and start password reset;
1. goto mail and follow reset password procedure;
1. goto flow forgotPassword and start password reset;
1. login;
1. wait 1hr for account A's password reset code to expire;
1. goto mail and follow reset password procedure;
    - error: 'code expired' -> error message -> 'forgotPassword'
1. wait 24hrs for account B's verification code to expire;
1. goto mail for account B and follow verification procedure;
    - error: 'code-expired' -> 'registerResend'
