import Vue from 'vue'
import { mapGetters, mapState } from 'vuex'
import PuAlertForm from '@/components/implementations/alert/form/alert-form.vue'
import { AUTH_ACTIONS } from '@/types'
import get from 'lodash/get'

export default Vue.extend({
  name: 'auth-flow-base',

  components: {
    PuAlertForm,
  },

  data() {
    return {
      AUTH_ACTIONS,
    }
  },

  computed: {
    ...mapState('auth', ['flow', 'error']),
    errorField() {
      return (field) => {
        return get(this.error, `fields.${field}`, false)
      }
    },
    errorSummary() {
      const key = get(this.error, 'key', null)
      if (key) {
        return this.$t(key)
      }
      return null
    },
  },

  methods: {
    requestFlow(flow: AUTH_ACTIONS) {
      this.$nextTick(() => {
        this.resetLocalState()
      })
      this.$emit('request', flow)
    },
    resetLocalState() {
      /* virtual */
    },
  },
})
