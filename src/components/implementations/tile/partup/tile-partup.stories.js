// /* eslint-env node */

// import { storiesOf } from '@storybook/vue'
// import viewer from '@/components/util/story/viewer'

// import PuTilePartup from './tile-partup.vue'
// import '@/lib/lang/config'

// import { makeStore } from '@/store'

// const store = makeStore()

// storiesOf('implementations/tiles', module)
//   .addDecorator(viewer)
//   .add('partup', () => {
//     return {
//       store,
//       components: {
//         PuTilePartup,
//       },
//       data() {
//         return {
//           partup: {
//             uuid: '123-456-789',
//             name: 'Organisatie uitvoeren 2018',
//             slug: 'part-up-organisatie-uitvoeren-2018',
//             result: 'dit is een verwacht resultaat van deze part-up',
//             images: {
//               // image_url: 'https://loremflickr.com/cache/resized/4858_32345344368_f178f4d5bf_400_160_g.jpg',
//             },
//             end_date: new Date(),
//             activities_summary: {
//               total: 10,
//               status: 'done',
//             },
//             partners_summary: {
//               total: 76,
//               picks: [
//                 {
//                   uuid: '1',
//                   name: 'Partner 1',
//                   images: {
//                     avatar_small: 'https://randomuser.me/api/portraits/med/men/65.jpg',
//                     avatar_small: 'https://randomuser.me/api/portraits/med/men/65.jpg',
//                   },
//                 },
//                 {
//                   uuid: '2',
//                   name: 'Partner 2',
//                   images: {
//                     avatar_small: 'https://randomuser.me/api/portraits/med/men/65.jpg',
//                     avatar_small: 'https://randomuser.me/api/portraits/med/men/65.jpg',
//                   },
//                 },
//                 {
//                   uuid: '3',
//                   name: 'Partner 3',
//                   images: {
//                     avatar_small: 'https://randomuser.me/api/portraits/med/men/65.jpg',
//                     avatar_small: 'https://randomuser.me/api/portraits/med/men/65.jpg',
//                   },
//                 },
//               ],
//             },
//             keywords: [
//               {
//                 uuid: '1',
//                 name: 'aaaaaaaaaaaaa',
//               },
//               {
//                 uuid: '2',
//                 name: 'bbbb',
//               },
//             ],
//           },
//         }
//       },
//       template: `
//         <flextory>
//           <pu-tile-partup
//             :partup="partup"
//           />
//         </flextory>
//       `,
//     }
//   })
