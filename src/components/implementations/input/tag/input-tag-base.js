import PuControlBase from '@/components/base/input/_base/control-base'
import PuTag from '@/components/base/tag/tag.vue'
import { comparableFactory } from '@/mixins/comparable'

export default {
  extends: PuControlBase,

  mixins: [
    comparableFactory((x, y) => {
      return x && y && (x.name && x.name.trim()) === (y.name && y.name.trim())
    }),
  ],

  components: {
    PuTag,
  },

  props: {
    id: {
      type: String,
    },
  },

  methods: {
    /**
     * Add a tag to the model
     * @method
     * @public
     * @param {Tag} tag
     * @return {Boolean} true if success, false otherwise
     */
    add(tag) {
      // @TODO: replace this with validation rules
      if (!tag) {
        this.$logger.debug("can't add tag cause it's undefined..")
        return false
      }
      if (!tag.name || tag.name.trim().length < 1) {
        this.$logger.debug("can't add tag with less than 1 character")
        return false
      }
      if (this.has(tag)) {
        this.$logger.debug("can't add tag cause already have it")
        this.$refs.input.value = null
        return false
      }

      let current = this.internalValue || []
      if (!Array.isArray(current)) {
        current = [current]
      }

      current.push(tag)
      this.internalValue = current
      return true
    },
    /**
     * used internally to prevent key event being passed as tag
     * @method
     * @private
     */
    addInternal(e) {
      const value = this.$refs.input.value

      if (value) {
        // only handle key events or icon clicks or no event data at all.
        if (
          !e ||
          (e && e.key && (e.key === ',' || e.key === 'Enter')) ||
          !('key' in e)
        ) {
          const name = value.replace(',', '').trim()
          const keyword = {
            name,
          }

          if (this.add(keyword)) {
            this.$refs.input.value = null
          }
        }
      }
    },
    /**
     * check if tag already present in model
     * @param {Tag} tag
     * @return {Boolean} true if tag in model, false otherwise
     */
    has(tag) {
      const current = this.internalValue

      return Array.isArray(current)
        ? current.findIndex((x) => this.valueComparator(x, tag)) > -1
        : this.valueComparator(current, tag)
    },
    /**
     * remove tag from model
     * @param {Tag} tag
     */
    remove(tag) {
      let current = this.internalValue

      if (!Array.isArray(current)) {
        if (!current === tag) {
          return
        }
        current = null
      } else {
        const tagIndex = current.findIndex((x) => this.valueComparator(x, tag))
        if (tagIndex > -1) {
          current.splice(tagIndex, 1)
        }
      }

      this.internalValue = current
    },
  },
}
