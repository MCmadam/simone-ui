// @TODO: Refactor item in sheet;
export default (step: string | string[] | number | undefined, def = 1) => {
  let s = step

  if (Array.isArray(s)) {
    s = step[0]
  }

  if (s === undefined) {
    return def
  } else if (typeof s === 'number') {
    return s
  } else if (typeof s === 'string') {
    return parseInt(s, 10)
  }

  return def
}
