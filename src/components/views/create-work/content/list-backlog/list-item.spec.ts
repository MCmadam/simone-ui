/**
 * Test Create Work List Item Activity
 * @group Journey/CreateWork
 */

import PuListItemBacklog from './list-item.vue'
import { createLocalVue, mount, vm } from '@/util/test/vue'
import assertions from '@/util/test/vue/assertions'
import Vuex, { Store } from 'vuex'

import { findByAttribute } from '@/util/test/vue/selectors'

describe('List Item', () => {
  const localVue = createLocalVue(Vuex)
  const wrapper = mount(PuListItemBacklog, {
    propsData: {
      value: {
        name: 'name',
        end_date: 'end_date',
      },
    },
    localVue,
  })

  assertions(wrapper).lazyModel()

  it('uses given value for form fields', () => {
    expect.assertions(3)

    const value = {
      name: 'name',
      end_date: 'end_date',
    }

    const wrapper = mount(PuListItemBacklog, {
      propsData: {
        value,
      },
      localVue,
    })

    for (const [k, v] of Object.entries(value)) {
      const w = findByAttribute(
        wrapper,
        { name: 'pu-content-editable' },
        'id',
        new RegExp(`field-${k}`)
      )
      expect(w && vm(w).internalValue).toBe(v)
    }

    expect(wrapper.element).toMatchSnapshot()
  })

  it('ensures valid model if not given via value prop', () => {
    expect.assertions(1)

    const wrapper = mount(PuListItemBacklog, {
      localVue,
    })

    expect(vm(wrapper).internalValue).toMatchObject({
      name: null,
      end_date: null,
    })
  })

  it('starts the form in edit mode if create is true', () => {
    expect.assertions(2)

    const wrapper = mount(PuListItemBacklog, {
      propsData: {
        create: true,
      },
    })

    const wrapperArray = wrapper.findAll({ name: 'pu-content-editable' })

    wrapperArray.wrappers.forEach((w) => {
      expect(vm(w).isActive).toBe(true)
    })
  })

  it('dispatches updates when not in create mode', async () => {
    expect.assertions(1)

    const updateItem = jest.fn()

    const wrapper = mount(PuListItemBacklog, {
      localVue,
      propsData: {
        create: false,
        value: {
          uuid: 'uuid',
          name: 'name',
          end_date: 'end_date',
        },
      },
      store: new Store({
        state: {},
        modules: {
          createWork: {
            namespaced: true,
            actions: {
              updateItem,
            },
          },
        },
      }),
    })

    const contentEditable = findByAttribute(
      wrapper,
      { name: 'pu-content-editable' },
      'id',
      new RegExp('field-name')
    )

    vm(contentEditable).internalValue = 'newVal'
    vm(contentEditable).$emit('save', {
      value: 'newVal',
    })

    expect(updateItem).toHaveBeenCalledWith(
      expect.anything(),
      expect.objectContaining({
        uuid: 'uuid',
        key: 'name',
        value: 'newVal',
      })
    )
  })

  it('does not dispatch updates when in create mode', () => {
    expect.assertions(1)

    const updateItem = jest.fn()

    const wrapper = mount(PuListItemBacklog, {
      localVue,
      propsData: {
        create: true,
      },
      store: new Store({
        state: {},
        modules: {
          createWork: {
            namespaced: true,
            actions: {
              updateItem,
            },
          },
        },
      }),
    })

    const contentEditable = findByAttribute(
      wrapper,
      { name: 'pu-content-editable' },
      'id',
      new RegExp('field-name')
    )

    vm(contentEditable).internalValue = 'newVal'
    vm(contentEditable).$emit('save', {
      value: 'newVal',
    })

    expect(updateItem).not.toHaveBeenCalled()
  })

  it('dispatches create when submitting in create mode', async () => {
    expect.assertions(1)

    const createItem = jest.fn()

    const wrapper = mount(PuListItemBacklog, {
      localVue,
      propsData: {
        create: true,
      },
      store: new Store({
        state: {},
        modules: {
          createWork: {
            namespaced: true,
            actions: {
              createItem,
            },
          },
        },
      }),
    })

    wrapper.find({ name: 'pu-form' }).trigger('submit')
    await wrapper.vm.$nextTick()

    expect(createItem).toHaveBeenCalled()
  })

  it('does not dispatch submit when not in create mode', async () => {
    expect.assertions(1)

    const createItem = jest.fn()

    const wrapper = mount(PuListItemBacklog, {
      localVue,
      propsData: {
        create: false,
      },
      store: new Store({
        state: {},
        modules: {
          createWork: {
            namespaced: true,
            actions: {
              createItem,
            },
          },
        },
      }),
    })

    wrapper.find({ name: 'pu-form' }).trigger('submit')
    await wrapper.vm.$nextTick()

    expect(createItem).not.toHaveBeenCalled()
  })
})
