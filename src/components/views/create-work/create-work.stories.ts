import { storiesOf } from '@storybook/vue'
import { withKnobs } from '@storybook/addon-knobs'
import { Store } from 'vuex'

import i18n from '@/util/lang/i18n'

import PuListBacklog from './content/list-backlog/list.vue'
import createWorkStoreModule from './store'

const stories = storiesOf('Journey/Create Work', module)
stories.addDecorator(withKnobs)

stories.add('Backlog List', () => ({
  i18n,
  store: new Store({
    modules: {
      createWork: createWorkStoreModule,
    },
  }),
  components: {
    PuListBacklog,
  },
  computed: {
    backlog() {
      return this.$store.getters['createWork/backlog']
    },
  },
  mounted() {
    ;[
      { uuid: 'uuid1', name: 'item 1', end_date: new Date().toUTCString() },
      { uuid: 'uuid2', name: 'item 2', end_date: new Date().toUTCString() },
      { uuid: 'uuid3', name: 'item 3', end_date: new Date().toUTCString() },
      { uuid: 'uuid4', name: 'item 4', end_date: new Date().toUTCString() },
      { uuid: 'uuid5', name: 'item 5', end_date: new Date().toUTCString() },
      { uuid: 'uuid6', name: 'item 6', end_date: new Date().toUTCString() },
    ].forEach((x) => this.$store.commit('createWork/setItem', x))
  },
  template: `
    <pu-list-backlog :items="backlog" :form="true" />
  `,
}))
