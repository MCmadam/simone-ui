import { ValueOf } from '@/types'

/* eslint-disable camelcase */
export type BacklogFormData = {
  name: string
  end_date: string
}

export type PartupFormData = {
  name: string
}

export type SaveEvent = {
  uuid: string
  key: keyof FormData
  value: ValueOf<FormData>
  done: () => void
}
