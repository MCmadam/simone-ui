import { Action } from 'vuex'
import { CreateWorkState, CreateWorkJourneySteps } from './types'
import { BacklogFormData, SaveEvent, PartupFormData } from '../types'

import findBacklogItems from '@/data/backlog/find-backlog-items'
import createBacklogItem from '@/data/backlog/create-backlog-item'
import updateBacklogItem from '@/data/backlog/update-backlog-item'
import deleteBacklogItem from '@/data/backlog/delete-backlog-item'
import partupCreate from '@/data/partup/create'
import { PARTUP_VISIBILITY_DEFAULT } from '@/lib/model/partup'
import commitForAction from '@/util/store/commit-for-action'
import { Identifiable, PARTUP_ALLOW_SOLICIT_REQUEST } from '@/types'

export const getItems: Action<CreateWorkState, unknown> = async (context) => {
  try {
    const { data } = await findBacklogItems()
    context.commit('setItems', data)
  } catch (error) {
    throw error
  }
}

/**
 * Create a backlog item
 * @param context Store Context
 */
export const createItem: Action<CreateWorkState, unknown> = async (
  context,
  data: BacklogFormData
) => {
  try {
    const { data: item } = await createBacklogItem({
      data,
    })

    context.commit('setItem', item)
  } catch (error) {
    throw error
  }
}

// this action state is required by multiple components on the same level so it requires store state management
export const createPartup: Action<CreateWorkState, unknown> = async (
  context,
  data: PartupFormData
) => {
  const action = 'createPartup'
  const c = commitForAction(action, context.commit)
  const organization = context.rootGetters['currentOrganization']

  if (!organization) {
    throw new Error('cannot create a partup without an organization context')
  }

  c.pending()
  try {
    const { data: partup } = await partupCreate(
      {
        organization_uuid: organization.uuid,
      },
      {
        query: {
          import_backlog: true,
        },
        data: {
          ...data,
          visibility: PARTUP_VISIBILITY_DEFAULT,
          request_option: PARTUP_ALLOW_SOLICIT_REQUEST.open,
        },
      }
    )

    context.commit('setPartup', partup)
    c.success()
  } catch (error) {
    c.error(error)
  }
}

/**
 * Delete a backlog item
 * @param context Store Context
 */
export const deleteItem: Action<CreateWorkState, unknown> = async (
  context,
  item: Identifiable
) => {
  try {
    await deleteBacklogItem(item)
    context.commit('deleteItem', item)
  } catch (error) {
    throw error
  }
}

/**
 * Update a backlog item
 * @param context Store Context
 */
export const updateItem: Action<CreateWorkState, unknown> = async (
  context,
  event: SaveEvent
) => {
  if (!event.uuid) {
    return
  }

  try {
    const { data: item } = await updateBacklogItem(
      {
        uuid: event.uuid,
      },
      {
        data: {
          [event.key]: event.value,
        },
      }
    )

    context.commit('setItem', item)
    event.done()
  } catch (error) {
    throw error
  }
}

export const step: Action<CreateWorkState, unknown> = async (
  context,
  step: CreateWorkJourneySteps
) => {
  context.commit('step', step)
}

/**
 * Reset the state for this module
 * @param context Store context
 * @description set's the state to a shallow clone of the initial state
 */
export const reset: Action<CreateWorkState, unknown> = (context) => {
  context.commit('reset')
}
