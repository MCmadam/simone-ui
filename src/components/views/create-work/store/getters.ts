import { Getter } from 'vuex'
import { CreateWorkState } from './types'

export const backlog: Getter<CreateWorkState, unknown> = (state) => {
  return state.backlog.value
}
