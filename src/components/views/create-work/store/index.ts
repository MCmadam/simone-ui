import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import state from './state'
import { CreateWorkState } from './types'
import { Module } from 'vuex'
import withActions from '@/util/store/with-actions'

const namespaced = true

const createWork: Module<CreateWorkState, unknown> = withActions(
  ['createPartup'],
  {
    namespaced,

    actions,
    getters,
    mutations,
    state,
  }
)

export default createWork
