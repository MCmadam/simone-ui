import { Mutation } from 'vuex'
import { CreateWorkState, CreateWorkJourneySteps, BacklogItem } from './types'
import defaultState from './state'

export const setItem: Mutation<CreateWorkState> = (
  state,
  item: BacklogItem
) => {
  state.backlog.set(item)
}

export const setItems: Mutation<CreateWorkState> = (
  state,
  items: BacklogItem[]
) => {
  if (Array.isArray(items)) {
    for (const item of items) {
      setItem(state, item)
    }
  } else if (typeof items === 'object' && 'uuid' in items) {
    setItem(state, items)
  }
}

export const deleteItem: Mutation<CreateWorkState> = (
  state,
  item: BacklogItem
) => {
  state.backlog.delete(item)
}

export const setPartup: Mutation<CreateWorkState> = (state, partup) => {
  state.partup = partup
}

export const step: Mutation<CreateWorkState> = (
  state,
  step: CreateWorkJourneySteps
) => {
  state.step = step
}

export const reset: Mutation<CreateWorkState> = (state) => {
  state = defaultState()
}
