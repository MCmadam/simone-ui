/**
 * CreateWork Store Module Mutations Specs
 *
 * @group store
 * @group store/createWork
 */

import { setItem, setItems, deleteItem, step, reset } from './mutations'
import { CreateWorkState } from './types'

describe('CreateWork/Store/Mutations', () => {
  const set = jest.fn()
  const del = jest.fn()
  const state = ({
    backlog: {
      set,
      delete: del,
    },
  } as any) as CreateWorkState

  beforeEach(() => {
    set.mockReset()
    del.mockReset()
  })

  describe('setItem', () => {
    it('adds item to cache', () => {
      expect.assertions(1)

      const item = {
        uuid: 'uuid',
        name: 'name',
        end_date: 'end_date',
      }

      setItem(state, item)

      expect(set).toHaveBeenCalledWith(item)
    })
  })

  describe('setItems', () => {
    it('adds items to cache', () => {
      expect.assertions(1)

      const items = Array.from(Array(3)).map((_, i) => ({
        uuid: `uuid-${i}`,
        name: `name-${i}`,
        end_date: `end_date-${i}`,
      }))

      setItems(state, items)

      expect(set).toHaveBeenCalledTimes(3)
    })
  })

  describe('deleteItem', () => {
    it('removes item from cache', () => {
      expect.assertions(1)

      const item = {
        uuid: 'uuid',
      }

      deleteItem(state, item)

      expect(del).toHaveBeenCalledWith(item)
    })
  })
})
