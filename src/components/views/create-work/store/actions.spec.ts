/**
 * Create Work Store Module Actions Specs
 * @group CreateWork/store
 * @group store
 */

import axios from 'axios'
import AxiosMockAdapter from 'axios-mock-adapter'
import { configureApi } from '@/util/test'
import {
  createItem,
  updateItem,
  reset,
  getItems,
  deleteItem,
  createPartup,
} from './actions'

configureApi()

describe('CreateWork/Store/Actions', () => {
  const axiosMock = new AxiosMockAdapter(axios)
  const commit = jest.fn()
  const context: any = { commit }

  beforeEach(() => {
    axiosMock.reset()
    commit.mockReset()
  })

  describe('createItem', () => {
    it('commits', async () => {
      expect.assertions(1)

      const response = {
        data: {
          uuid: 'uuid',
          name: 'name',
          end_date: 'end_date',
        },
      }

      axiosMock.onPost(/backlog/).reply(200, response)

      // @ts-ignore
      await createItem(context, {
        name: 'name',
        end_date: 'end_date',
      })

      expect(commit).toHaveBeenCalledWith('setItem', response.data)
    })

    it('throws', () => {
      expect.assertions(1)

      axiosMock.onPost(/backlog/).networkError()

      return expect(
        // @ts-ignore
        createItem(context, {
          name: 'name',
          end_date: 'end_date',
        })
      ).rejects.toBeTruthy()
    })
  })

  describe('deleteItem', () => {
    it('commits', async () => {
      expect.assertions(1)

      axiosMock.onDelete(/backlog/).reply(200)

      const item = {
        uuid: 'uuid',
        name: 'name',
        end_date: 'end_date',
      }

      // @ts-ignore
      await deleteItem(context, item)

      expect(commit).toHaveBeenCalledWith('deleteItem', item)
    })

    it('throws', () => {
      expect.assertions(1)

      axiosMock.onDelete(/backlog/).networkError()

      return expect(
        // @ts-ignore
        deleteItem(context, {
          uuid: 'uuid',
        })
      ).rejects.toBeTruthy()
    })
  })

  describe('getItems', () => {
    it('commits', async () => {
      expect.assertions(1)

      const response = {
        data: [
          {
            uuid: 'uuid',
            name: 'name',
            end_date: 'end_date',
          },
        ],
      }

      axiosMock.onGet(/backlog/).reply(200, response)

      // @ts-ignore
      await getItems(context)

      expect(commit).toHaveBeenCalledWith('setItems', response.data)
    })

    it('throws', () => {
      expect.assertions(1)

      axiosMock.onDelete(/backlog/).networkError()

      return expect(
        // @ts-ignore
        getItems()
      ).rejects.toBeTruthy()
    })
  })

  describe('updateItem', () => {
    it('commits', async () => {
      expect.assertions(2)

      const done = jest.fn()
      const response = {
        data: {
          uuid: 'uuid',
          name: 'name',
          end_date: 'end_date',
        },
      }

      axiosMock.onPatch(/backlog/).reply(200, response)

      // @ts-ignore
      await updateItem(context, {
        uuid: 'uuid',
        key: 'name',
        value: 'name',
        done,
      })

      expect(commit).toHaveBeenCalledWith('setItem', response.data)
      expect(done).toHaveBeenCalled()
    })

    it('throws', () => {
      expect.assertions(1)

      axiosMock.onPatch(/backlog/).networkError()

      return expect(
        // @ts-ignore
        updateItem(context, {
          uuid: 'uuid',
          key: 'name',
          value: 'name',
        })
      ).rejects.toBeTruthy()
    })
  })

  describe('createPartup', () => {
    it('resolves created partup', async () => {
      expect.assertions(1)

      axiosMock.onPost(/partups\/organization/).reply(200, {
        data: {
          uuid: 'partup-uuid',
          name: 'partup-name',
        },
      })

      const context = {
        commit,
        rootGetters: {
          currentOrganization: {
            uuid: 'uuid',
          },
        },
      }

      // @ts-ignore
      await createPartup(context, {
        name: 'name',
      })

      expect(commit).toHaveBeenCalledWith(
        'setPartup',
        expect.objectContaining({
          uuid: 'partup-uuid',
          name: 'partup-name',
        })
      )
    })

    it('uses flag import_backlog', async () => {
      expect.assertions(1)

      axiosMock.onPost(/partups\/organization/).reply(200, {
        data: {
          uuid: 'partup-uuid',
          name: 'partup-name',
        },
      })

      const context = {
        commit,
        rootGetters: {
          currentOrganization: {
            uuid: 'organization-uuid',
          },
        },
      }

      // @ts-ignore
      await createPartup(context, {
        name: 'partup-name',
      })

      const query = axiosMock.history.post[0].params

      return expect(query).toMatchObject({
        import_backlog: true,
      })
    })

    it('throws when organization context is not present', () => {
      expect.assertions(1)

      const context = {
        commit,
        rootGetters: {},
      }

      return expect(
        // @ts-ignore
        createPartup(context, {
          name: 'name',
        })
      ).rejects.toThrow(
        'cannot create a partup without an organization context'
      )
    })

    it('commits data error', async () => {
      expect.assertions(1)

      axiosMock.onPost(/partups\/organization/).networkError()

      const context = {
        commit,
        rootGetters: {
          currentOrganization: {
            uuid: 'uuid',
          },
        },
      }

      // @ts-ignore
      await createPartup(context, {
        name: 'name',
      })

      expect(commit).toHaveBeenCalledWith(
        'actionState',
        expect.objectContaining({
          key: 'error',
        })
      )
    })
  })
})
