import ArrayCache from '@/util/store/cache/array-cache'

export type BacklogItem = {
  uuid: string
  name: string
  // eslint-disable-next-line
  end_date?: string,
}

export type CreateWorkJourneySteps = 1 | 2 | 3

export interface CreateWorkState {
  backlog: ArrayCache<BacklogItem>
  step: CreateWorkJourneySteps
  partup: {
    name: string
  } | null
}
