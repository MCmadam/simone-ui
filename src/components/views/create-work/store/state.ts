import { CreateWorkState, BacklogItem } from './types'
import ArrayCache from '@/util/store/cache/array-cache'

export default (): CreateWorkState => ({
  backlog: new ArrayCache<BacklogItem>({
    prepend: true,
  }),
  partup: null,
  step: 1,
})
