import { Attachment } from '@/types'

export type ChatMessageFormData = {
  text: string,
  attachments: Attachment[]
}
