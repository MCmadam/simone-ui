/* eslint-env node */

import { storiesOf } from '@storybook/vue'
import { storyViewer } from '@/components/util/story/viewer'
import { map, range } from 'lodash'

import PuLane from './lane'
import PuCardActivity from './card'

const makeActivities = () =>
  map(range(20), (i) => ({
    uuid: `${i}${i}${i}`,
    name: `Activity ${i}`,
    description: 'Description',
  }))

storiesOf('modules', module)
  .addDecorator(
    storyViewer({
      align: 'start',
    })
  )
  // .add('board', () => {
  //   return {
  //     template: `
  //       <flextory>

  //       </flextory>
  //     `,
  //   }
  // })
  .add('lane', () => {
    return {
      components: {
        PuLane,
        PuCardActivity,
      },
      data() {
        const activities = makeActivities()
        const lastIndex = activities.length - 1
        return {
          lane1: activities.slice(0, lastIndex / 2),
          lane2: activities.slice(lastIndex / 2, lastIndex),
        }
      },
      template: `
        <flextory align="start" style="padding: 10px 30px;">
          <pu-lane>
            <div slot="header" class="header">
              <span>todo</span>
            </div>
            <pu-card-activity
              v-for="activity in lane1"
              :key="activity.uuid"
              :value="activity"
            />
          </pu-lane>
          <pu-lane>
            <div slot="header" class="header">
              <span>doing</span>
            </div>
            <pu-card-activity
              v-for="activity in lane2"
              :key="activity.uuid"
              :value="activity"
            />
          </pu-lane>
        </flextory>
      `,
    }
  })
