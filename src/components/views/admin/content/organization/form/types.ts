import { ORGANIZATION_VISIBILITY } from '@/types'

export type AdminOrganizationFormData = {
  backgroundUrl: string
  domains: string
  introduction: string
  name: string
  logoUrl: string
  payoff: string
  slug: string
  visibility: ORGANIZATION_VISIBILITY
  userInvitationMessage: string
  userWelcomeMessage: string
  autoAddNewUserWithMatchingEmail: boolean
}

/* eslint-disable camelcase */
export type AdminOrganization = {
  domains: string
  introduction: string
  images: {
    background: string
    logo: string
  }
  name: string
  payoff: string
  slug: string
  visibility: ORGANIZATION_VISIBILITY
  solicited_user_invitation: string
  user_welcome: string
  auto_add_user: boolean
}
/* eslint-enable camelcase */

export type ComponentData = {
  organization: AdminOrganization
}

// // this is how I think the organization model should be.

// export type OrganizationProfile = {
//   introduction: string,
// }

// export enum ORGANIZATION_FEATURES {
//   branding = 'branding',
// }

// export type OrganizationFeatures = {
//   [K in ORGANIZATION_FEATURES]: boolean
// }

// export type Organization = {
//   name: string,
//   slug: string,
//   visibility: ORGANIZATION_VISIBILITY,
//   domains: string[],

//   images: {
//     backgroundUrl: string,
//     logoUrl: string,
//   },

//   branding: {
//     name: string,
//     payoff: string,
//     images: {
//       backgroundUrl: string,
//       logoUrl: string,
//     }
//   },

//   features: {
//     branding: boolean,
//   },

//   profile: {
//     name: string,
//     introduction: string,
//   },

//   onboarding: {
//     autoAddNewUserWithMatchingEmail: boolean,
//     messages: {
//       userWelcome: string,
//       userInvitation: string,
//     },
//   },
// }
