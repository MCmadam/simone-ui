import { AdminOrganizationFormData, AdminOrganization } from './types'

export default (formData: AdminOrganizationFormData): AdminOrganization => {
  if (!formData) {
    throw new Error(
      'cannot convert admin organization form data to organization when formdata is null'
    )
  }

  const organization: AdminOrganization = {
    auto_add_user: formData.autoAddNewUserWithMatchingEmail,
    domains: formData.domains,
    images: {
      background: formData.backgroundUrl,
      logo: formData.logoUrl,
    },
    introduction: formData.introduction,
    name: formData.name,
    payoff: formData.payoff,
    slug: formData.slug,
    solicited_user_invitation: formData.userInvitationMessage,
    user_welcome: formData.userWelcomeMessage,
    visibility: formData.visibility,
  }

  return organization
}
