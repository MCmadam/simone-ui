import defaultOrganizationVisibility from '@/lib/model/organization/default-visibility'
import { AdminOrganization, AdminOrganizationFormData } from './types'

export default (organization: AdminOrganization): AdminOrganizationFormData => {
  const fields: AdminOrganizationFormData = {
    autoAddNewUserWithMatchingEmail: false,
    backgroundUrl: null,
    domains: null,
    introduction: null,
    name: null,
    logoUrl: null,
    payoff: null,
    slug: null,
    userInvitationMessage: null,
    userWelcomeMessage: null,
    visibility: defaultOrganizationVisibility,
  }

  if (organization) {
    fields.autoAddNewUserWithMatchingEmail = organization.auto_add_user
    fields.domains = organization.domains
    fields.introduction = organization.introduction
    fields.name = organization.name
    fields.payoff = organization.payoff
    fields.slug = organization.slug
    fields.userInvitationMessage = organization.solicited_user_invitation
    fields.userWelcomeMessage = organization.user_welcome
    fields.visibility = organization.visibility

    if ('images' in organization) {
      fields.backgroundUrl = organization.images.background
      fields.logoUrl = organization.images.logo
    }
  }

  return fields
}
