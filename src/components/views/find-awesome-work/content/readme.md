
# Carousel specs

1. You may walk the carousel `twice`;
1. You may bookmark `4` activities per walk;
1. You may see `15` activities in total shared between walks;

## Activity card

1. All actions mark the card as seen and exlude the card for a second walk;
1. When you skip an activity no action is sent to the back-end;
    - this means it will return in future walks when refreshing the page.
1. When you bookmark an activity it will send an API request;
1. When you contribute a card directly it will continue the carousel when the action is done;
    - this card is not bookmarked and will not show in the summary;
1. When you are invited a button is shown so that you can directly accept the invite.

## Summary card

1. A summary card can hold up to 4 bookmarks;
1. A summary may be empty, in that case it shows a special empty state;
    - the empty state will show an action that sends the user to their profile.
1. When the limit (conditions above) are met a final summary is shown without the option to see more.

### Summary card bookmarks

1. A button is shown that can resolve different actions, these actions are (in the correct order):
    - when you are invited: `accept`;
    - when you are a partner: `contribute`;
    - none of the above: `request to join partup`.
1. when an action is successful a green `success icon` is shown next to the activity name;
1. when an action is unsuccessful a red `error icon` is shown next to the activity name;
1. when you don't have te permission to do the activity directly the item will expand to provide more information (for request).
    - the other items will disappear;
    - a back button is shown;
