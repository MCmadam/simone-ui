
# Polyfills

Some polyfills are required depending on the browser, to prevent unnecessary HTTP requests we ship the polyfills ourselves and only include them via feature detection.

All the polywills we ship can be found in the [polyfills](../src/polyfills) folder. The polywills will be included via the [boot sequence](../src/boot.ts)

Since most polyfills only affect IE which we don't oficially support impact will be minimal.

## Transpiling

Vue helps us create a legacy and modern version of the app. The browser can then decide which version it will use. This also prevents unnessesary transpilation for all browsers fully supporting modules. To learn more check [Vue modern mode](https://cli.vuejs.org/guide/browser-compatibility.html#modern-mode)
