# URL plan

We have a plan for the layout of the URLs.

/                                           homepage
/dashboard                                  personal dashboard OR within an organization
/partups                                    list of all public and/or open partups OR within an
/mensen                                     list of all people public and/or open OR within an organization

/inloggen                                   login page
/registreren                                register page
/registreren/bevestigen/{cognito_user_id}   confirm registration
/wachtwoord-vergeten                        forgot password
/wachtwoord-vergeten/bevestigen             enter new password

/mensen/{slug}                              view a single user

/partups/aanmaken                           create a new partup
/partups/{slug}                             view a single partup
/partups/{slug}/activiteiten                view a single partup
/partups/{slug}/activiteiten/{uuid}         view a single partup
/partups/{slug}/berichten                   view a single partup
/partups/{slug}/

/organisaties/{slug}                        view a single organization

/instellingen
