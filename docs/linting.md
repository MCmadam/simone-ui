# Linting

In order to keep the code readable and maintainable a strict set of rules are applied to formatting code. These rules are not set in stone and will change once the project takes shape so feel free to make a PR with new rules.

## Editor extensions

Your editor needs a linter installed in order to lint while typing, this is highly recommended as your commit's could get rejected otherwise.

## Configuration

We have chosen to use the ESLint + Prettier setup with Typescript support for this project. Because we use Vue they are wrapped and provided by Vue to support .vue extensions as well. The implementation and configuration are not as straight forward because the linter has to lint both .js and .vue files and will be explained here.

## Code styles

We use a code style that aims to minimize commit changes, like the dangling comma.

- [plugin:vue/strongly-recommended](https://github.com/vuejs/eslint-plugin-vue#priority-b-strongly-recommended-improving-readability)
- [@vue/typescript](https://github.com/vuejs/eslint-config-typescript)
- [@vue/prettier](https://github.com/vuejs/eslint-config-prettier)

### ESLint environments

Telling eslint we live in a node environment is OK because all code will before compiling. Adding environments will tell eslint certain globals are available. But not all code lives in the same environments, like the test files. In this case you can add comments to set environments for specific files by adding a line `/* eslint-env jest */` for example.

## Lint staged files

```JSON
"gitHooks": {
  "pre-commit": "lint-staged"
}
```

## Conventional commits

Commitlint is currently disabled and can be re-enabled by adding the following in package.json:

```JSON
"gitHooks": {
  "pre-commit": "lint-staged",
  "commit-msg": "commitlint -e $HUSKY_GIT_PARAMS"
}
```

and re-enabling the job in gitlab-ci.

