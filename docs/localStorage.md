
# localStorage

We use localStorage to help speed up the boot process. Some data from the [store](../src/store/index.ts) is synced using the `vuex-persistedstate` library. If state is to be stored in localStorage it should also be present in the store so there's no need to use the localStorage directly.

## What do we store

1. the current user profile
1. the user's preferences, note that they are not keyed;
1. the current organization context the user is working in.

