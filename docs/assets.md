# Assets

...

## Icons

We use [material icons]() as our baseline icon-set, when material doesn't provide the required icon we use []() or create one ourselves.
Webpack is configured to pickup all icons in `src/assets/icons` and compiles it to a [svg-sprite]() which is included in index.html

All icons must have the following specs:

- filename: [name]-[variation]-[mod]
- SVG extension;
- viewBox of `0 0 48 48`;
- no unnessecary properties;
  - like....
