# Environment

There are two ways in which environment variables can be set when deploying.
Variables starting with `VUE_APP_` will automatically be included in the build ([Vue docs](https://cli.vuejs.org/guide/mode-and-env.html#environment-variables)) and therefore must start with the prefix.

    note that we only use local and not the full vue-cli approach to environment variables! See the section about deploying to different stages

## Extending the environment

Some environment variables are dynamically set in the vue & webpack configurations. This should be prevented whenever possible and may never be used for static variables.

## Local development

For local deployments with the dev server you can manually set the environment variables through `/config/env/local`.

## Deploying to AWS

Since Vue imports all environment variables beginning with VUE_APP_ we can just export them in scripts, like [environment.sh](/scripts/environment.sh) which is used for all builds & deployments.

## Using environment variables

Environment variables can be accessed by using `process.env.VUE_APP_XXX` and will be set when building the app. Some are exposed via the `$globals` plugin found in [/src/plugins/globals.ts](/src/plugins/globals.ts) so that they can be used easily within vue components.
