
# Components

Components are organized in many folders to ensure scalability as the app grows. The folders are organized in such a way it's easy to navigate to using your editor's search.

e.g. in vscode press CMD+P and type `views/partup/activities`

Some generic rules apply to all components:

1. every component name starts with prefix `pu-`

## Structure

### base

these building blocks are *small and **reusable*** across the whole app and may never access the store directly. When new functionality is required think very hard if it belongs in the base component or an implementation is a better fit.

**filename** `base/{component}/{component}.vue`

### implementations

are customized base components which may or may not use the base component as root node,

`e.g. the password input is an impementation of but does not use textfield`

these **must** be made when *props || functionality* is required in a base component that you will not reuse

**filename** `implementations/{component}/{component}-something.vue`

Special implementations:

- **modals**: a modal implementation must match existing folder structure or they cannot be loaded dynamically, check modal-portal component for path if unsure;

### modules

are mostly used in one place, but not tied to one view and thus do not fit into the categories above or below.

### util

mostly storybook / documenting components

### views

**filename** `views/{component}/view-{component}.vue`

### views/_modules

**filename** `_modules/{component}/{parent}-{component}.vue`

### views/{subview}

**filename** `{component}/view-{parent}-{component}.vue`

When using nested routes it get's a dedicated folder in it's root abiding the same rules as above.

## Where to place a component

Besides implementations, which directly correlate to base components, when creating a component and you don't see it fit into base right away you can use the _modules sub-folder, you can always move it to modules or implementations later when there's need to use it in other places.

It's not uncommon to have the following folder structure for a view

```TXT
/viewA
  /_modules
    /x
    /y
  /subViewA
    /_modules
      /z
    subViewA.vue
  /subViewB
    subViewB.vue
  viewA.vue
```
