# Storybook

We are using storybook to develop components in isolation and test how components are functioning together.
Storybook is configured in a way it picks up on any `.stories.js` file inside `src/components`

Storybook is configured so it will automatically take all files that end with `.stories.js` inside `src/components`.
It's important you register a component to `src/components/index.js` so it can be registered to storybook.

run `yarn storybook` and go to `localhost:9001`, happy developing!

## Create a story

[Vue storybook documentation](https://storybook.js.org/basics/guide-vue/)

Your component folder could look like this

```JS
- componentName
  - componentName.vue
  - componentName.stories.js
```

Make sure to always use the following return in the add function

```JS
return {
  // optional components key
  components: {
    //list of required components not exposed via index.js
  },
  // required template key
  template: // must be string literal or an actual component
}
```

Full example

```JS
import { storiesOf } from '@storybook/vue'
import centered from '@storybook/addon-centered'
import { my addon or decorator } from '@storybook/myaddon' // check addons below

const componentStories = storiesOf('pu-component', module)

componentStories
  .addDecorator(centered)

componentStories
  .add('without props', () => {
    return {
      template: `
        <pu-component />
      `
    }
  })

componentStories
  .add('with props', () => {
    return {
      template: `
        <pu-component
          prop1=hello
          prop2=world
        />
      `
    }
  })
```

## Addons

### Actions

Display data received by event handlers
[reference](https://github.com/storybooks/storybook/tree/master/addons/actions)

### Centered

Center stories, used locally!
[reference](https://github.com/storybooks/storybook/tree/master/addons/centered)

### Events

Use pre-defined events and emit them to components (requires an emiter)
[reference](https://github.com/storybooks/storybook/tree/master/addons/events)

### Knobs

Add knobs so that props can be adjusted without creating multiple stories
[reference](https://github.com/storybooks/storybook/tree/master/addons/knobs)

### Links

Create links between stories
[reference](https://github.com/storybooks/storybook/tree/master/addons/links)

### Notes

Add notes for stories
[reference](https://github.com/storybooks/storybook/tree/master/addons/notes)
