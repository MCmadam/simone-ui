
# Boot sequence

The boot sequence runs before instantiating and mounting the application. Therefore it's very important it doesn't do more than absolutely required.
The following steps are an absolute necessity before booting the app:

1. loading [polyfills](./polyfills.md);
2. loading [locale] (./internationalization.md);
3. synchronizing the currentUser from localStorage.

`While the app can't work without the steps above it must *never throw an error*.`
**Feature request**: There is another mechanism that signals the user the app won't work.

## Synchronizing the currentUser

The router depends on the correct state of the currentUser being present whenever cognito credentials are present. If for some reason cognito credentials are present in localStorage without the currentUser being present the `getUser` call will be blocking!
