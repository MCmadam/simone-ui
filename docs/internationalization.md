
# Internationalization

We keep track of the current locale in the store module [preferences](../src/store/generic/preferences). Syncing the last known locale to [localStorage](./localStorage.md) means we can set `lang=;` on the browser before the app is initialized.

Business rules regarding locales can be found [here](../src/lib/lang)

## Working with dates

We use [Luxon](https://github.com/moment/luxon) to work and format the values based on locales. Luxon also makes it easier to perform calculations on dates.

The back-end will always send a date in UTC String format.

In order to ensure dates will be rendered properly the [pu-date](../src/components/base/date/date.vue) component **must be used** when displaying dates on the screen.

When a computation is required never mutate the original string date value, you may only use [helpers](../src/util/date) when perfoming computations.

### Formats

We have a number of different [date formats](../src/util/date/formats.ts) we use. These formats are language independant for now. The `pu-date` component will always call a format and falls back to the default if none specified.

## Translation (i18n)

For translation we use a ***component based approach*** with [vue-i18n](https://kazupon.github.io/vue-i18n/). Global locales are also included when loading a locale, when keys are used in implementations that are used all over the app it's not ideal to bake in all posible locales inside the component. You can find the global locales in `/locales`

### Key formatting & semantics

We have no standardized way to name locale keys yet, however it's best practice to name the keys after the action they are representing in the UI.

Error messages are following certain standards. An error key always starts with `error.` Some generic messages are defined globally.

**examples:** 'header', 'header-sub', 'button.submit', 'button.invite'

### Component based locales

In order for component based locales to work a [webpack loader](../config/webpack/extend-webpack.js) is used.

#### Adding component based locales

The reason we include locales the way below is so that when a component defines it's own locales we can easily identify it.

```Vue
<script>
import locales from './locales'

export default {
  i18n: {
    messages: locales,
  },
}
</script>
```

## Managing Locales

### Changing the locale

The only way the locale may be changed is by calling the store action `setLocale`. This ensures all libraries are updated as well.

Within a component

```JS
this.$store.dispatch('preferences/setLocale', 'nl')
```

In Javascript

```JS
import store from '@/store'

store.dispatch('preferences/setLocale', 'nl')
```

### Add support for a new locale

Try to use simplified locale codes, e.g. `en instead of en-GB`

1. ensure there is a global locale file in `/locales` with the name of the locale code *(it may by an empty json object)*.
1. add the locale code to [supportedLocales](../src/lib/lang/supportedLocales.ts)
1. check all components that have locale declarations.
1. add Intl formats, see [Working with dates](#workingwithdates) above.
