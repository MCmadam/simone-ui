# Modals

A modal is wrapped with a `.pu-modal-shell` component, this wrapper will set limits but does not control the height. Each modal should define it's own height and width so the scroll containers will work properly.
