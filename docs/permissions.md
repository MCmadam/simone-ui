# Permissions

Our permission system is heavily inspired by [@casl](https://github.com/stalniy/casl).

- The ability code can be found in [@/lib/authorization/ability/pkg](../src/lib/authorization/ability/pkg/ability.ts).
- The tests can be found alongside the code *AND* in [global test folder](../tests/unit/ability.spec.js)

## Defining permissions

Permissions will be defined in the back-end and only read / mapped on the front-end. If a permission you require does not exist please create an issue. Often permissions can only be set when required data has been loaded, therefore they will always be set from within a component.

A mapping for permissions from the back to front-end can be found in [rules-for](../src/lib/authorization/ability/rules-for.ts) Some root rules are in place that should always be present and are also loaded in the rules for function.

To set permissions for a component (and all it's children that inherit) you must use a helper to update the ability with new rules:

**WARNING**: an ability does not reset when swithing routes, if the same component get's reused you must reset the ability yourself by updating with an empty array.

```JS
import rulesFor from '@/lib/authorization/ability/rules-for'

// pseudo
VueComponent {
  init() {
    const { data } = await loadData()
    this.$ability.update(rulesFor(data))
  },
  routeUpdate() {
    this.$ability.update([])
  }
}
```

## Checking for permissions

Every component can call `$can()` with the required action to check if the user currently holds that permission.

```VUE
<template>
  <div
    v-if="$can('someAction')
  >Content only rendered if user can do someAction</div>
</template>
```

## Permission hierarchy

There is always an empty root ability provided so any component can use `$can()` without throwing errors. Unless the root ability is overridden on a component itself which implemented the permission it will always return **false**

To define an ability on a component you must specify it as a component option or use the [ability mixin](../src/mixins/ability.ts) (**recommended**).

```VUE
export default {
  name: 'my-component',
  ability: new Ability(),
}

or

export default {
  name: 'my-component'
  mixins: [
    ability,
  ],
}
```
