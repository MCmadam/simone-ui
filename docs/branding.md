
# Branding

At Part-up we provide a unique branding experience when enabled for the customer (organization). At this moment all organizations have custom branding.
For more information about branding please check the [documentation repo]() *(in progress)*.

## Model

All the branding info is found on the model of an organization, you could say the branding is a `Partial<Organization>`.

```TS
// @/types.ts

type AppBranding = {
  images: {
    background: string,
    logo: string,
  },
  payoff: string,
  name: string,
}
```

## Store

The active branding is stored in the root level of the store and can be set by calling the `getBranding` action.

```MD
To prevent loading branding twice it's `null` when booting
```

## Branding context

There are two ways the app decides which should be the current branding, the type used is set via an environment variable called [VUE_APP_FEATURE_DOMAIN_BRANDING](../scripts/environment.sh). This flag must have either "true" or "false" and will be evaluated as a boolean value.

The app is always branded and defaults if the current organization context does not provide one. The default branding can be found at `@/lib/branding/default`.

The branding will be loaded when booting ASAP, check the [boot sequence](../src/boot.ts) for more info

1. window.location.host (feature enabled);
1. current organization context (feature disabled).
