# Testing

We test both vue components and JS functionality extensivly. Tests will be run before each commit to see if the change is acceptable. The reason we are so strict is that it's easier to prevent bad code into production than to fix it.

We use [jest](https://jestjs.io/) for the following tests

- [Unit tests](#unit-tests);
- [Integration tests](#integration-tests);
- [Component tests](#component-tests);
- [Snapshot tests](#snapshop-tests);

## Writing tests

### Unit|Component|Snapshot-tests

A unit/component test file should always be placed alongside the file that requires testing and end with `.test.js`
A snapshot testing file should also be placed in the same folder as the component that requires snapshotting, in addition the file should end in `.test.snap.js`

All tests require proper encapsulation. Snippets have been made for `vscode`, feel free to contribute and make snippers for your editor of choice.

Snippets (vscode):

- unittest;
- comptest;
- snaptest;
- inttest;

snippet output:

```JS
describe('[file]', () => {

  it(`global behavior test`, () => {})

  describe('method', () => {
    it(`method implementation test`, () => {})
  })
})
```

## Test frameworks

### Jest

Jest is a testing framework that provides unique advantages versus other testing frameworks. While being good at running pure JS unit tests it can also handle vue components very well which allows for component and snapshot testing without the need for another framework.

#### Jest-Setup

You can find the jest configuration files in `<root>/config/jest`

### Unit tests

Unit tests will test business logic that's not related to UI and therefore never .vue files

### Snapshot tests

Snapshot's provide the first tell something has changed in the UI and will therefore be run before component tests.

### Component tests

Component tests will test individual and sets of components together.
