# Releasing

## Credentials

You need AWS credentials in order to build & deploy. You can ask a team member for an account. We have 2 seperate credentials for `dev` and `prod` environments. For convenience we store these under `partup-test` and `partup-prd`. Make sure you export one of these before continuing the process.

```SH
export AWS_PROFILE=partup-test
or
export AWS_PROFILE=partup-prd
```

## Deployment scripts

The deployment scripts are dependant on 3 arguments:

- `STACK_ENV`: AWS Stack, **alpha** or **prod**
- `TARGET`: deployment target within a stack, e.g. **develop**, **test**, **acc**, etc..
- `BUILD_MODE`: either **development** or **production**, this defaults to production.

### Environment .sh

We use environment variables to prevent code changes as much as possible. Not all variables are static, some must be changed based on the deploy environment.
We aim to add these as CLI params when we have more time.

You can find the variables [here](../scripts/environment.sh)

### Build .sh

This script is responsible for building the app.

### Deploy .sh

This script will run the deployment process from start to finish and uses the other scripts to do so. When deploying to a STACK_ENV other than prod you can use ```yarn deploy {TARGET}``` which already predefines the alpha stack_env.

## Release to production

Although we don't use SemVer yet and haven't automated much in the CI we do use [GitFlow](https://datasift.github.io/gitflow/IntroducingGitFlow.html).

### Prepare a release branch and deploy to acceptance environment

1. ```git checkout develop```
1. ```git pull```
1. check the latest tag by using ```git tag -l```
1. determine the next release version
1. ```git checkout -b release/#.#.#``` where # represents the next release version
1. ```git push -u origin release/#.#.#```
1. Prepare [environment variables](../scripts/environment.sh), e.g. domain branding
1. ```export AWS_PROFILE=partup-test```
1. ```scripts/deploy.sh alpha acc```

### After final approval merge bugfixes from release branch back into develop

1. ```git fetch origin```
1. ```git checkout release/#.#.#```
1. ```git pull```
1. ```git checkout develop```
1. ```git merge release/#.#.#```
1. ```git push```

### Merge release branch to master and create tag

1. go to gitlab
1. create a merge request **from *release/#.#.#* to *master***
1. make sure to delete source branch (release branch) upon merge
1. accept merge request on gitlab
1. create a tag **from master**

### Deploy to production

1. ```git checkout master```
1. ```git pull```
1. optional: ```git checkout #.#.#``` where #.#.# is the newly created tag
1. prepare [environment variables](../scripts/environment.sh), e.g. domain branding
1. ```export AWS_PROFILE=partup-prd```
1. ```scripts/deploy.sh prod prod```
