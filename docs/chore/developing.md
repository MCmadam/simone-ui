# Developing

## Installation

### Linux

1. ensure [python3.4+](https://linuxconfig.org/install-python-2-on-ubuntu-18-04-bionic-beaver-linux) is installed;
1. ensure python3-pip is installed;
    - `sudo apt-get install python3-pip`;
1. ensure build-essentials are installed;
    - `sudo apt-get install build-essential`;

### General

1. ensure [Node 10+](https://nodejs.org/en/download/current/) is installed;
1. ensure [awscli-v2](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) is installed and [configured](#configure-aws);
1. ensure [jq](https://stedolan.github.io/jq/) is installed;
1. ensure [yarn](https://yarnpkg.com/en/docs/install) is installed (I don't want to see package-lock.json);
1. run `yarn` to install packages;
1. run `export AWS_PROFILE=partup-test`
1. run `yarn serve` to boot up the devServer;
1. open `localhost:9000` in your browser.

### Configure AWS

We use 2 aws profiles, one for testing `partup-test` and one for production `partup-prd`. You can read more about configuring AWS [here](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html). You can obtain access keys by getting in touch with one of the developers already working on this project.

## Serve locally

By default the serve script invoked by `yarn serve` will use `develop` as environment, this requires that your AWS_PROFILE is set. You can point to a local .env file by specifying an argument to `yarn serve`, e.g. `local`. When pointing to a local .env file ensure that it is present by adding it to the `/config/env` directory. A local .env file will always take precedence above the environment set by the environment.sh script in the case both are present.

## Tooling

We make use of many tools to make the developing life easier and standardized so we can all make sense of what's going on. We use seperate configuration files instead of package.json and they should be .js whenever possible so that we can add comments.

### ESLint

You should use an editor that supports `ESLint` and `Prettier` or risk merge requests being declined. We use the most strict ruleset possible, it's just better in the long run.

Vue uses wrappers for eslint to be able to lint vue files, for more info check [their docs](https://eslint.vuejs.org/).

    We recommend using format on save for prettier
