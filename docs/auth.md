
# Authentication

## Cognito

We use cognito for authentication. Cognito stores the user's credentials in `localStorage` which we use to sync the store's state. You can call `currentAuthenticatedUser` to see if the cognito credentials are still present in localStorage and thus the user is loggedIn.

All the authentication actions are encapsulated within a store module named `auth`.
The router guard `auto-logout` is responsible for updating the store's state to match the state of cognito.

For example, when the store action `logout` is called, a router.go(0) is called so that the guard can sync the state. This prevents the UI updating before leaving the route which would cause a corrupt state in the view.
