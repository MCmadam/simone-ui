/**
 * i18n mock for all vue injected methods
 * @see https://kazupon.github.io/vue-i18n/api/#vue-injected-methods
 * @description
 * for now this is very basic just returning the given key,
 * when the need arises these implementations can be improved.
 * needs to be invoked to prevent sharing the same mock function
 * @example must be spread / merged
 *
 * const i18nMock = new MockI18n()
 *
 * stubs: {
 * ...i18nMock.methods,
 * }
 *
 * beforeEach(() => { i18nMock.mockClear() })
 *
 * it(() => { i18nMock.$t.mockImplementation(() => 'foo') })
 */
export default class MockI18n {
  private _$t = jest.fn((key) => key)
  private _$tc = jest.fn((key) => key)
  private _$te = jest.fn((key) => key)
  private _$d = jest.fn((key) => key)
  private _$n = jest.fn((key) => key)

  /**
   * expose all injectable methods for stubbing,
   * this prevents unwanted methods also being stubbed.
   */
  public get methods() {
    return {
      $t: this.$t,
      $tc: this.$tc,
      $te: this.$te,
      $d: this.$d,
      $n: this.$n,
    }
  }

  public get $t() {
    return this._$t
  }

  public get $tc() {
    return this._$tc
  }

  public get $te() {
    return this._$te
  }

  public get $d() {
    return this._$d
  }

  public get $n() {
    return this._$n
  }

  public mockClear() {
    Object.values(this.methods).forEach((m) => { m.mockClear() })
  }

  public mockReset() {
    Object.values(this.methods).forEach((m) => { m.mockReset() })
  }
}
