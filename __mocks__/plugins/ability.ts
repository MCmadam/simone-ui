/**
 * ability mock for all vue injected methods
 * @see plugins/ability
 * @example must be spread / merged
 *
 * const abm = new AbilityMock()
 *
 * stubs: {
 * ...abm.methods
 * }
 *
 * beforeEach(() => { abm.mockReset() })
 *
 * it(() => { abm.can.mockImplementation(() => true) })
 */
export default class MockAbility {
  private _can = jest.fn()
  private _update = jest.fn()

  /**
   * expose all injectable methods for stubbing,
   * this prevents unwanted methods also being stubbed.
   */
  public get methods() {
    return {
      can: this.can,
      update: this.update,
    }
  }

  public get can() {
    return this._can
  }

  public get update() {
    return this._update
  }

  public mockClear() {
    Object.values(this.methods).forEach((m) => { m.mockClear() })
  }

  public mockReset() {
    Object.values(this.methods).forEach((m) => { m.mockReset() })
  }
}
