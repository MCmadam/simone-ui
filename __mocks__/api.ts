
const api: any = jest.genMockFromModule('../src/api/api')

api.get = jest.fn()
api.del = jest.fn()
api.head = jest.fn()
api.post = jest.fn()
api.patch = jest.fn()
api.put = jest.fn()

export default api
