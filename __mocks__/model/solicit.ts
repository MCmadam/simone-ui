import { SOLICIT_ENTITIES, SOLICIT_TYPES } from '@/types'

export default {
  [SOLICIT_ENTITIES.activity]: {
    get invite() {
      return Object.assign({}, {
        type: SOLICIT_TYPES.invite,
        for: SOLICIT_ENTITIES.activity,
        activity: {
          uuid: 'activity-uuid',
          name: 'activity-name',
        },
        partup: {
          uuid: 'partup-uuid',
          name: 'partup-name',
          slug: 'partup-slug',
        },
        user: {
          uuid: 'user-uuid',
          name: 'user-name',
          slug: 'user-slug',
          images: {
            avatar_small: 'avatar-small.jpg',
            avatar_medium: 'avatar-medium.jpg',
          },
        },
      })
    },
    get request() {
      return Object.assign({}, {
        type: SOLICIT_TYPES.request,
        for: SOLICIT_ENTITIES.activity,
        activity: {
          uuid: 'activity-uuid',
          name: 'activity-name',
        },
        partup: {
          uuid: 'partup-uuid',
          name: 'partup-name',
          slug: 'partup-slug',
        },
        user: {
          uuid: 'user-uuid',
          name: 'user-name',
          slug: 'user-slug',
          images: {
            avatar_small: 'avatar-small.jpg',
            avatar_medium: 'avatar-medium.jpg',
          },
        },
      })
    },
  },
  [SOLICIT_ENTITIES.organization]: {
    get invite() {
      return Object.assign({}, {
        type: SOLICIT_TYPES.invite,
        for: SOLICIT_ENTITIES.organization,
        organization: {
          uuid: 'organization-uuid',
          name: 'organization-name',
        },
        user: {
          uuid: 'user-uuid',
          name: 'user-name',
          slug: 'user-slug',
          images: {
            avatar_small: 'avatar-small.jpg',
            avatar_medium: 'avatar-medium.jpg',
          },
        },
      })
    },
    get request() {
      return Object.assign({}, {
        type: SOLICIT_TYPES.request,
        for: SOLICIT_ENTITIES.organization,
        organization: {
          uuid: 'organization-uuid',
          name: 'organization-name',
        },
        user: {
          uuid: 'user-uuid',
          name: 'user-name',
          slug: 'user-slug',
          images: {
            avatar_small: 'avatar-small.jpg',
            avatar_medium: 'avatar-medium.jpg',
          },
        },
      })
    },
  },
  [SOLICIT_ENTITIES.partup]: {
    get invite() {
      return Object.assign({}, {
        type: SOLICIT_TYPES.invite,
        for: SOLICIT_ENTITIES.partup,
        partup: {
          uuid: 'partup-uuid',
          name: 'partup-name',
          slug: 'partup-slug',
        },
        user: {
          uuid: 'user-uuid',
          name: 'user-name',
          slug: 'user-slug',
          images: {
            avatar_small: 'avatar-small.jpg',
            avatar_medium: 'avatar-medium.jpg',
          },
        },
      })
    },
    get request() {
      return Object.assign({}, {
        type: SOLICIT_TYPES.request,
        for: SOLICIT_ENTITIES.partup,
        partup: {
          uuid: 'partup-uuid',
          name: 'partup-name',
          slug: 'partup-slug',
        },
        user: {
          uuid: 'user-uuid',
          name: 'user-name',
          slug: 'user-slug',
          images: {
            avatar_small: 'avatar-small.jpg',
            avatar_medium: 'avatar-medium.jpg',
          },
        },
      })
    },
  },
}
