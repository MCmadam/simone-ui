# Part-up - Simone UI

Please check our other documentation if you want to work on this project.

- [developing](./docs/chore/developing.md);
- [releasing](./docs/chore/releasing.md);

## Testing

We have seperate testing suites, which can be run using yarn: `yarn test:(unit|component|snapshot)`.
All of the above will run when committing.

For more information about writing tests check our [testing guidelines](./docs/testing.md)

## About stages / environments

AWS resources needed by the front-end are managed through gitlab.com/part-up/partops and cloudformation. In case a new
environment should be created, a corresponding stack created before is a prerequisite to performing builds and
deployements in this repository.

Once a stack has been created, in this repo a `config/env/TARGET` file can be created with all values needed by this
front-end. Then can be commenced with building and deploying.

There is WIP on the configuration values

- API_ENDPOINT_URL: will become a nice DNS that support branch convention
- AWS_S3_BUCKET: will become parameterized to a bucket per branchx

## License

Copyright (C) 2019 Part-up

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. You can find it at /part-up/LICENSE and the supplement at /part-up/License supplement
