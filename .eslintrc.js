module.exports = {
  root: true,
  env: {
    node: true,
  },
  parserOptions: {
    ecmaVersion: 2020,
  },
  extends: [
    'plugin:vue/recommended',
    'eslint:recommended',
    '@vue/typescript/recommended',
    '@vue/prettier',
    '@vue/prettier/@typescript-eslint'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'comma-dangle': ['warn', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      imports: 'always-multiline',
      exports: 'always-multiline',
      functions: 'never',
    }],
    'quotes': ['warn', 'single', {
      avoidEscape: true,
    }],
    'semi': ['warn', 'never'],
    'vue/name-property-casing': ['warn', 'kebab-case'],
    '@typescript-eslint/ban-ts-ignore': 'warn',

    // The following overrides are temporary disables so that we can release.
    'no-useless-catch': 'warn',
    'camelcase': 'warn',
    '@typescript-eslint/camelcase': 'off',
    '@typescript-eslint/no-empty-function': 'warn',
    '@typescript-eslint/no-unused-vars': 'warn',
    'vue/no-unused-vars': 'warn',
  },
  overrides: [
    /*
      override test files.
    */
    {
      files: [
        '**/__mocks__/**/*.ts?',
        '**/*.{spec|test}.{j|t}s?'
      ],
      env: {
        jest: true,
      }
    },
    /*
      override configuration files
    */
    {
      files: [
        '**/*.config.js?',
        '*.config.js',
        'config/**/*.js?',
      ],
      rules: {
        '@typescript-eslint/no-var-requires': 'off',
      }
    }
  ]
}
