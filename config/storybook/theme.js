import { create } from '@storybook/theming'

export default create({
  base: 'light',

  colorPrimary: '#55bec8',
  colorSecondary: '#fda724',

  // UI
  appBg: 'white',
  appContentBg: '#f9f9f9',
  appBorderColor: '#b2b2b2',
  appBorderRadius: 8,

  // Typography
  fontBase: 'proxima-nova, sans-serif',
  fontCode: 'monospace',

  // Text colors
  textColor: '#191919',
  textInverseColor: 'white',

  // Toolbar default and active colors
  barTextColor: 'white',
  barSelectedColor: 'white',
  barBg: '#4aa6ae',

  // Form colors
  inputBg: '#e5e5e5',
  inputBorder: 'green',
  inputTextColor: '#191919',
  inputBorderRadius: 8,

  brandTitle: 'Part-up',
  brandUrl: 'https://prod.part-up.com',
})
