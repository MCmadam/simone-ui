/*
 * Import all plugins used for vue here so components can be rendered
 */
import '@/plugins/i18n'
import '@/plugins/store'
import '@/plugins/logger'
import '@/plugins/modal'
import '@/plugins/dialog'
