// Order of import is the order in which the addons appear

/* eslint-disable import/no-extraneous-dependencies */
import '@storybook/addon-knobs/register'
import '@storybook/addon-actions/register'
import '@storybook/addon-a11y/register'
import '@storybook/addon-links/register'
import '@storybook/addon-notes/register'
import '@storybook/addon-options/register'
