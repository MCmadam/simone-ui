import { get } from 'lodash'
import Vue from 'vue'

const loadContext = (Vue, context) => {
  context.keys().forEach((fileName) => {
    const resolved = context(fileName)
    const component = get(resolved, 'default', resolved)
    const componentName = get(component, 'options.name', component.name)

    if (componentName) {
      Vue.component(componentName, component)
    }
  })
}

;[
  require.context('../../src/components/base', true, /\.vue$/),
  require.context('../../src/components/base/transition', true, /\.js$/),
  require.context('../../src/components/util', true, /\.(vue|ts)$/),
].forEach((context) => {
  loadContext(Vue, context)
})
