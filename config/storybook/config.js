/* eslint-disable import/no-extraneous-dependencies */
import { addParameters, configure, addDecorator } from '@storybook/vue'
import { withA11y } from '@storybook/addon-a11y'
import api from '@/api'

// eslint-disable-next-line
import '@/assets/scss/main.scss'
import './components'
import './plugins'
import theme from './theme'

api.configure({
  endpoint: 'storybook',
  region: 'storybook',
  service: 'storybook',
})

addDecorator(withA11y)
addParameters({
  options: {
    // addonPanelInRight: true,
    sortStoriesByKind: true,
    theme,
  },
})

const loadStories = () => {
  ;[
    require.context('../../src/components', true, /\.stories.ts$/),
    require.context('../../src/stories', true, /\.stories.ts$/),
  ].forEach((context) => {
    context.keys().forEach(context)
  })
}

configure(loadStories, module)
