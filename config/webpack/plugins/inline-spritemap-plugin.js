/* eslint-env node */
/* eslint-disable require-jsdoc */

const chalk = require('chalk')

module.exports = class InlineSpriteMapPlugin {
  constructor (options = {}) {
    this.options = options
  }

  apply (compiler) {
    const ID = 'inline-spritemap-plugin'
    compiler.hooks.compilation.tap(ID, (compilation) => {
      compilation.hooks.htmlWebpackPluginAlterAssetTags.tapAsync(ID, async (data, cb) => {
        let assets = this.options.assets
        assets = Array.isArray(assets)
          ? assets
          : assets
            ? [assets]
            : []

        for (const asset of assets) {
          const rawAsset = compilation.assets[asset]

          if (!rawAsset) {
            console.debug(chalk.yellow(`cannot find asset ${asset}`))
            continue
          }

          data.body.unshift({
            tagName: 'div',
            attributes: {
              id: 'pu-icons'
            },
            innerHTML: rawAsset._value
          })
        }

        cb()
      })
    })
  }
}
