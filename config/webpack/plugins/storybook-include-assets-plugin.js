/* eslint-disable */
const fs = require('fs-extra')
const chalk = require('chalk')

module.exports = class StorybookIncludeAssetsPlugin {
  constructor(options = {}) {
    this.options = options
  }

  apply(compiler) {
    const source = this.options.source
    let out = this.options.out
    let assets = this.options.assets
    let files = this.options.files

    let sourceHTML

    if (source) {
      if (fs.existsSync(source)) {
        sourceHTML = fs.readFileSync(source)
      } else {
        console.warn(chalk.yellow(`source file does not exist. path: ${source}`))
      }
    }

    assets = assets
      ? Array.isArray(assets)
        ? assets
        : [assets]
      : []

    files = files
      ? Array.isArray(files)
        ? files
        : [files]
      : []

    const ID = 'StorybookIncludeAssetsPlugin'
    compiler.hooks.afterEmit.tap(ID, (compilation) => {
      let previewHTML = sourceHTML || ''

      previewHTML += `<style>body > svg:first-child { display: none; }</style>`

      for (const asset of assets) {
        const raw = compilation.assets[asset]

        if (!raw) {
          console.warn(chalk.yellow(`couldn't find asset in compilation assets: ${asset}`))
          return
        }

        previewHTML += raw._value
      }

      for (const file of files) {
        if (!fs.existsSync(file)) {
          console.warn(chalk.yellow(`file does not exist: ${file}`))
        }

        const text = fs.readFileSync(file)

        if (!text) {
          console.warn(chalk.yellow(`file has no text..`))
        }

        previewHtml += text
      }

      if (!out) {
        out = `${compiler.context}/.storybook/preview-head.html`
      }

      console.log('out?', out)
      console.log('html: ', previewHTML)
      if (!fs.existsSync(out)) {
        fs.createFileSync(out)
      }

      fs.outputFileSync(out, previewHTML)
    })
  }
}
