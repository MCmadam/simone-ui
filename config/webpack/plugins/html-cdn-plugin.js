/* eslint-env node */
/* eslint-disable require-jsdoc */
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = class HTMLCDNPlugin {
  constructor (options) {
    this.options = options
  }

  apply (compiler) {
    const ID = 'html-cdn-plugin'

    compiler.hooks.compilation.tap(ID, (compilation) => {
      compilation.hooks.htmlWebpackPluginAlterAssetTags.tapAsync(ID, async (data, cb) => {
        if (Array.isArray(this.options.urls)) {
          for (const url of this.options.urls) {
            data.head.push(this.createTag(url))
          }
        }
        if (Array.isArray(this.options.polyfills)) {
          const url = `https://cdn.polyfill.io/v2/polyfill.js?features=default,${this.options.polyfills.join(',')}`
          data.head.push(this.createTag(url))
        }

        cb(null, data)
      })
    })
  }

  createTag (url) {
    return {
      tagName: 'script',
      closeTag: true,
      attributes: {
        type: 'text/javascript',
        src: url
      }
    }
  }
}
