
const path = require('path')

module.exports = (options) => (chainableConfig) => {
  const isLegacyBundle = process.env.VUE_CLI_MODERN_MODE && !process.env.VUE_CLI_MODERN_BUILD

  const {
    env,
    buildHash,
  } = options

  chainableConfig
    .output
    .filename(`${buildHash}/js/${isLegacyBundle ? '[name]-legacy.js' : '[name].js'}`)

  // This is a temporary fix to prevent the service-worker.js to be included.
  // tested on win10 + chrome where it malfunctiones.
  chainableConfig
    .plugins
    .delete('pwa')

  chainableConfig
    .module
    .rule('i18n')
    .resourceQuery(/blockType=i18n/)
    .type('javascript/auto')
    .use('i18n')
    .loader('@kazupon/vue-i18n-loader')
    .end()

  chainableConfig
    .plugin('dotenv')
    .use(require('dotenv-webpack'), [
      {
        path: `config/env/${env}`,
        expand: true,
      },
    ])

  chainableConfig
    .plugin('script-ext')
    .use(require('script-ext-html-webpack-plugin'), [
      {
        module: [
          'chunk-vendors',
          'app',
        ],
      },
    ])
    .after('html')

  chainableConfig
    .plugin('svg-spritemap')
    .use(require('svg-spritemap-webpack-plugin'), [
      [
        path.resolve('src', 'assets', 'icons', '*.svg'),
      ], {
        output: {
          filename: `${buildHash}/icon-sprite.svg`,
          svgo: {
            cleanupAttrs: true,
            cleanupNumericValues: true,
            cleanupListOfValues: true,
            convertPathData: true,
            convertStyleToAttrs: true,
            convertTransform: true,
            removeComments: true,
            removeDimensions: true,
            removeEmptyAttrs: true,
            removeEmptyText: true,
            removeEmptyContainers: true,
            removeEditorsNSData: true,
            removeHiddenElems: true,
            removeNonInheritableGroupAttrs: true,
            removeUselessDefs: true,
            removeUnknownsAndDefaults: true,
            removeUnusedNS: true,
            removeUselessStrokeAndFill: true,
          },
        },
        sprite: {
          prefix: `pu-icon-`,
          gutter: 2,
          generate: {
            symbol: true,
            title: false,
          },
        },
      },
    ])

  if (process.env.VUE_CLI_STORYBOOK) {
    const out = path.resolve('config/storybook/preview-head.html')
    chainableConfig
      .plugin('storybook-include-html')
      .use(require('./plugins/storybook-include-assets-plugin'), [
        {
          out,
          assets: [
            'icon-sprite.svg',
          ],
        },
      ])
      .after('svg-spritemap')
  } else {
    chainableConfig
      .plugin('inline-svg-spritemap')
      .use(require('./plugins/inline-spritemap-plugin'), [{
        assets: [
          `${buildHash}/icon-sprite.svg`,
        ],
      }])
      .after('svg-spritemap')
  }

  chainableConfig
    .plugin('copy-asset-images')
    .use(require('copy-webpack-plugin'), [[
      {
        from: 'src/assets/images/**/*',
        to: `${buildHash}/images/[name].[ext]`,
        toType: 'template',
      },
    ]])
}
