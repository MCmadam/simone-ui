module.exports = (cb) => {
  const args = require('minimist')(process.argv.slice(3))
  const buildHash = require('./build-hash')(args)

  delete args._
  const buildOptions = {
    ...args,
    assetsDir: buildHash,
    buildHash,
  }

  return cb(buildOptions)
}
