const path = require('path')

const createHash = (str) => {
  let hash = 0
  let i
  let chr

  for (i = 0; i < str.length; i += 1) {
    chr = str.charCodeAt(i)
    hash = ((hash << 5) - hash) + chr
    hash |= 0
  }

  if (hash < 1) {
    hash = hash + 1
  }

  return String(Math.abs(hash))
}

module.exports = () => {
  if (!process.env.VUE_APP_BUILDHASH) {
    const pkg = require(path.resolve('package.json'))
    const pkgVersion = pkg.version.split('.').join('')
    const randomString = (Math.random() + 1).toString().split('.').join('')
    const hash = createHash(randomString)
    const buildHash = `${hash.substring(0, 7)}${pkgVersion}`
    process.env.VUE_APP_BUILDHASH = buildHash
  }

  return process.env.VUE_APP_BUILDHASH
}
