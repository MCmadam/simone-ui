module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  reporters: [
    'default',
    [
      './node_modules/jest-html-reporter',
      {
        pageTitle: 'Unit test report',
      },
    ],
  ],
  collectCoverageFrom: [
    '**/src/**/*.{js,ts,vue}',
    '!**/util/test/**',
    '!**/router/routes/**',
    '!**/_global**',
    '!**/**old**',
    '!**/index.js',
    '!**/main.ts',
    '!**/*.(testspec).{js,ts}',
    '!**/*.snap.test.js',
    '!**/*.stories.{js,ts}',
  ],
  coverageThreshold: {
    global: {
      branches: 0,
      functions: 0,
      lines: 0,
      statements: 0,
    },
  },
  transformIgnorePatterns: ['/node_modules/(?!luxon)'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
    '^@mocks/(.*)$': '<rootDir>/__mocks__/$1',
  },
  testMatch: [
    '**/tests/unit/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.spec.(js|jsx|ts|tsx)',
    '**/src/**/*.(test|spec).(js|ts)',
  ],
}
