module.exports = {
  'as a super-admin I can invite an existing user to an organization': (
    browser
  ) => {},
  'as a super-admin I can invite someone to an organization by email': (
    browser
  ) => {},
  "when I'm not loggedIn I can see the solicit for an organization I have received via email with the actions to login and register": (
    browser
  ) => {},
  "when I'm loggedIn I can see the solicit for an organization I have received via email with the actions to accept and reject": (
    browser
  ) => {},
  "when I'm loggedIn I can see the solicit for an organization on my dashboard with the actions to accept and reject": (
    browser
  ) => {},
}
