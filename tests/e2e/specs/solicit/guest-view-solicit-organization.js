/**
 * As a guest (not logged in) I can see the solicit for an organization
 * I have received via email on the organization page
 */

module.exports = {
  'navigate to the organization page with solicit uuid as query param': (
    browser
  ) => {},
  'verify that the solicit has appeared': (browser) => {},
  'the solicit has a button that opens the auth modal starting the login flow': (
    browser
  ) => {},
  'the solicit has a button that opens the auth modal starting the register flow': (
    browser
  ) => {},
}
