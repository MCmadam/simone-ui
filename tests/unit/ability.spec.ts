import { createLocalVue, shallowMount } from '@vue/test-utils'
import abilityPlugin from '@/plugins/ability/plugin'
import ability from '@/mixins/ability'
import Ability from '@/lib/authorization/ability/pkg/ability'
import Vue from 'vue'

describe('ability', () => {
  let localVue
  let wrapper

  beforeEach(() => {
    wrapper = null
    localVue = createLocalVue()
    localVue.use(abilityPlugin)
  })

  describe('plugin', () => {
    describe('when the plugin is used by vue', () => {
      it('provides an instance of Ability to the root component which will be inherited by all children', () => {
        expect.assertions(1)

        const parent = Vue.extend({
          name: 'parent',
          template: '<div class="parentComponent"><slot /></div>',
        })

        wrapper = shallowMount(parent, {
          localVue,
          slots: {
            default: {
              name: 'child',
              template: '<div class="childComponent">childContent</div>',
            },
          },
        })

        const childWrapper = wrapper.find({ name: 'child' })
        expect(wrapper.vm.$ability).toBe(childWrapper.vm.$ability)
      })

      it("can be overwritten in a child component that provides it's own instance of Ability", () => {
        expect.assertions(4)

        const childAbility = new Ability([
          {
            actions: ['child'],
          },
        ])

        const childChildAbility = new Ability([
          {
            actions: ['childChild'],
          },
        ])

        wrapper = shallowMount(
          Vue.extend({
            name: 'child',
            ability: childAbility,
            template: '<div class="child"><slot /></div>',
          }),
          {
            localVue,
            parentComponent: {
              name: 'parent',
              template: '<div class="parent"><slot /></div>',
            },
            slots: {
              default: {
                name: 'childChild',
                ability: childChildAbility,
                template: '<div class="childChild">content</div>',
              },
            },
          }
        )

        const parentVM = wrapper.vm.$parent
        const childChildWrapper = wrapper.find({ name: 'childChild' })

        expect(wrapper.vm.$ability).toBe(childAbility)
        expect(childChildWrapper.vm.$ability).toBe(childChildAbility)

        expect(parentVM.$ability).not.toBe(wrapper.vm.$ability)
        expect(wrapper.vm.$ability).not.toBe(childChildWrapper.vm.$ability)
      })

      it('can use the local ability mixin to override inheriting the ability instance from parent', () => {})
    })

    describe('when an ability is updated on the root component', () => {
      it('automatically re-renders the root and all children that inherit from root', async () => {
        expect.assertions(4)

        const expected = 'I am tested'

        const mock = Vue.extend({
          name: 'parent',
          ability: new Ability(),
          template: `<div><span v-if="$can('test')" class="parent">${expected}</span><slot name="header" /><slot /></div>`,
        })

        wrapper = shallowMount(mock, {
          localVue,
          slots: {
            default: {
              name: 'child',
              template: `<div><span v-if="$can('test')" class="child">${expected}</span></div>`,
            },
            header: {
              name: 'headerSlot',
              template: `<div><span v-if="$can('test')" class="header">${expected}</span></div>`,
            },
          },
        })

        expect(wrapper.html()).not.toContain(expected)

        wrapper.vm.$ability.update([
          {
            actions: ['test'],
          },
        ])

        await wrapper.vm.$nextTick()

        expect(wrapper.find('span.parent').html()).toContain(expected)
        expect(wrapper.find({ name: 'child' }).html()).toContain(expected)
        expect(wrapper.find({ name: 'headerSlot' }).html()).toContain(expected)
      })
    })

    describe('when an ability is updated on a child component that inherits from root', () => {
      it('automatically re-renders the root and all children that inherit from root', async () => {
        expect.assertions(3)

        const expected = 'I am tested'

        const mock = Vue.extend({
          name: 'parent',
          ability: new Ability(),
          template: `<div><span v-if="$can('test')" class="parent">${expected}</span><slot /></div>`,
        })

        wrapper = shallowMount(mock, {
          localVue,
          slots: {
            default: {
              name: 'child',
              template: `<div><span v-if="$can('test')" class="child">${expected}</span></div>`,
            },
          },
        })

        expect(wrapper.html()).not.toContain(expected)

        const childWrapper = wrapper.find({ name: 'child' })

        childWrapper.vm.$ability.update([
          {
            actions: ['test'],
          },
        ])

        await wrapper.vm.$nextTick()

        expect(wrapper.find('span.parent').html()).toContain(expected)
        expect(childWrapper.html()).toContain(expected)
      })
    })

    describe('when an ability is updated on a child component that overrides the root', () => {
      it('does not re-render the root component', async () => {
        expect.assertions(3)

        const expected = 'I am tested'

        const mock = Vue.extend({
          name: 'parent',
          ability: new Ability(),
          template: `<div><span v-if="$can('test')" class="parent">${expected}</span><slot /></div>`,
        })

        wrapper = shallowMount(mock, {
          localVue,
          slots: {
            default: {
              name: 'child',
              ability: new Ability(),
              template: `<div><span v-if="$can('test')" class="child">${expected}</span></div>`,
            },
          },
        })

        expect(wrapper.html()).not.toContain(expected)

        const childWrapper = wrapper.find({ name: 'child' })

        childWrapper.vm.$ability.update([
          {
            actions: 'test',
          },
        ])

        await wrapper.vm.$nextTick()

        expect(wrapper.find('span.parent').exists()).toBe(false)
        expect(childWrapper.html()).toContain(expected)
      })
    })
  })

  describe('mixin (local)', () => {
    describe('when a component uses the local ability mixin', () => {
      it('overrides inheriting the ability from parent', async () => {
        expect.assertions(2)

        const parentAbility = new Ability()

        const mock = Vue.extend({
          name: 'parent',
          ability: parentAbility,
          template: '<div><slot /></div>',
        })

        wrapper = shallowMount(mock, {
          localVue,
          slots: {
            default: {
              name: 'child',
              mixins: [ability()],
              template:
                '<div><span v-if="$can(\'test\')">I has content</span></div>',
            },
          },
        })

        const childWrapper = wrapper.find({ name: 'child' })

        wrapper.vm.$ability.update([
          {
            actions: ['test'],
          },
        ])

        expect(childWrapper.html()).not.toContain('I has content')

        childWrapper.vm.$ability.update([
          {
            actions: ['test'],
          },
        ])

        await wrapper.vm.$nextTick()

        expect(childWrapper.html()).toContain('I has content')
      })

      it('creates a new instance of ability for each component instantiation by using ability: true', () => {
        expect.assertions(1)

        const mock = Vue.extend({
          name: 'mock',
          mixins: [ability()],
          template: '<div />',
        })

        wrapper = shallowMount(mock, {
          localVue,
        })
        const wrapper2 = shallowMount(mock, {
          localVue,
        })

        expect(wrapper.vm.$ability).not.toBe(wrapper2.vm.$ability)
      })
    })
  })
})
