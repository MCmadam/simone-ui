module.exports = require('./config/vue/extend-vue')((options) => {
  const { assetsDir } = options

  return {
    assetsDir,
    chainWebpack: require('./config/webpack/extend-webpack')(options),
    css: {
      loaderOptions: {
        sass: {
          prependData: '@import "~@/assets/scss/_entry.scss";',
        },
      },
    },
    devServer: {
      port: 9000,
    },
    pluginOptions: {
      storybook: {
        allowedPlugins: ['svg-spritemap', 'storybook-include-html'],
      },
    },
    publicPath: '/',
    transpileDependencies: [/luxon/, /@partup/],
  }
})
