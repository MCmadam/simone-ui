/* eslint-env node */

import { storiesOf } from '@storybook/vue'
import viewer from '@/components/util/story/viewer'

storiesOf('components/input/select', module)
  .addDecorator(viewer)
  .add('default', () => {
    return {
      data() {
        return {
          model: null,
          items: Array.from(Array(10)).map((_, i) => ({
            text: `item ${i}`,
            value: `value ${i}`,
          })),
        }
      },
      computed: {
        label() {
          return `model value: ${this.model}`
        },
      },
      template: `
        <flextory>
          <pu-label>{{label}}</pu-label>
          <pu-form-control>
            <pu-select
              :items="items"
            />
          </pu-form-control>
        </flextory>
      `,
    }
  })
