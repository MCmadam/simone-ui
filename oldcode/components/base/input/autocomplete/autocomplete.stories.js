/* eslint-env node */

import { storiesOf } from '@storybook/vue'

import viewer from '@/components/util/story/viewer'

storiesOf('inputs/autocomplete', module)
  .addDecorator(viewer)
  .add('default', () => {
    return {
      data() {
        return {
          selectedItems: [],
          source: Array.from(Array(5)).map((v, i) => ({
            name: `tag ${i}`,
          })),
        }
      },
      methods: {
        onSearch(text) {
          setTimeout(() => {
            this.source = Array.from(Array(5)).map((v, i) => ({ name: `${text} ${i}` }))
          }, 250)
        },
        onAdd(text, cb) {
          const newItem = {
            name: `newItem ${text}`,
          }

          this.selectedItems.push(newItem)
          this.source.push(newItem)
          // this.source.push({
          //   name: 'new ' + text,
          // })
        },
      },
      template: `
        <flextory>
          <pu-autocomplete
            v-model="selectedItems"
            label="Autocomplete label"
            :items="source"
            item-text="name"
            item-value="name"
            multiple
            @search="onSearch"
            @add="onAdd"
            />
        </flextory>
      `,
    }
  })
