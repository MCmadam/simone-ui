/* eslint-env node */

import {
  storiesOf,
} from '@storybook/vue'

import PuSelectList from './list.vue'

import viewer from '@/components/util/story/viewer'

storiesOf('inputs/select', module)
  .addDecorator(viewer)
  .add('list', () => {
    return {
      components: {
        PuSelectList,
      },
      data() {
        return {
          items: Array.from(Array(15)).map((i) => ({
            text: `item ${i}`,
          })),
        }
      },
      template: `
        <flextory>
          <pu-select-list
            :items="items"
          />
        </flextory>
      `,
    }
  })
