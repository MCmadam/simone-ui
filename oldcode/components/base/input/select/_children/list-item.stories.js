/* eslint-env node */

import {
  storiesOf,
} from '@storybook/vue'

import PuSelectListItem from './list-item.vue'

import viewer from '@/components/util/story/viewer'

storiesOf('inputs/select', module)
  .addDecorator(viewer)
  .add('list-item', () => {
    return {
      components: {
        PuSelectListItem,
      },
      template: `
        <flextory>
          <pu-select-list-item>
            some text
          </pu-select-list-item>
        </flextory>
      `,
    }
  })
