/* eslint-env node */

import {
  storiesOf,
} from '@storybook/vue'

import viewer from '@/components/util/story/viewer'

storiesOf('inputs/select', module)
  .addDecorator(viewer)
  .add('select', () => {
    return {
      data() {
        return {
          items: Array.from(Array(5)).map((v, i) => ({
            text: `item ${i}`,
          })),
          selectModel: null,
        }
      },
      template: `
        <flextory>
          <pu-select
            v-model="selectModel"
            :label="'select value: ' + selectModel"
            placeholder="placeholder"
            :items="items"
            hide-selected
            item-value="text"
          />
        </flextory>
      `,
    }
  })
  .add('select-multiple', () => {
    // const dropdownActive = boolean('dropdown active', false)
    return {
      data() {
        return {
          items: Array.from(Array(5)).map((v, i) => ({
            text: `item ${i}`,
          })),
          selectModel: null,
        }
      },
      computed: {
        modelValues() {
          return (this.selectModel || []).map((item) => item.text)
        },
      },
      template: `
        <flextory>
          <pu-select
            v-model="selectModel"
            :label="'select value: ' + modelValues"
            placeholder="placeholder"
            :items="items"
            multiple
            return-object

          />
        </flextory>
      `,
    }
  })
