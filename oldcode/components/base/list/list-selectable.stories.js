/* eslint-env node */

import {
  storiesOf,
} from '@storybook/vue'

import viewer from '@/components/util/story/viewer'

storiesOf('list', module)
  .addDecorator(viewer)
  .add('selectable', () => {
    return {
      data() {
        return {
          items: Array.from(Array(15)).map((v, i) => ({
            text: `item ${i}`,
          })),
          model: '',
        }
      },
      template: `
        <flextory>
          {{model}}
          <pu-list-selectable v-model="model">
            <pu-list-item
              v-for="item in items"
              :key="item.text"
            >{{item.text}}</pu-list-item>
          </pu-list-selectable>
        </flextory>
      `,
    }
  })
