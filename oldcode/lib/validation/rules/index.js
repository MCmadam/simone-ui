import {
  isArray,
  isString,
} from 'lodash'

export const isRequired = (
  errorMessage = 'this field is required'
) =>
  (val) => {
    return (isArray(val)
      ? !!val.length
      : !!val) || errorMessage
  }

export const maxLength = (
  amount = 0,
  errorMessage = `Dit veld mag maximaal ${amount} karakters bevatten`
) =>
  (val) => {
    if (val == null) {
      return undefined
    }
    if (
      isString(val)
      || isArray(val)
    ) {
      return val.length > amount
        ? errorMessage
        : true
    } else {
      throw new Error(`maxLength validator: ${Object.prototype.toString.call(val)} is not a string or array`)
    }
  }

export const minLength = (
  amount = 0,
  errorMessage = `Dit veld moet minimaal ${amount} karakters bevatten`
) =>
  (val) => {
    if (val == null) {
      return undefined
    }

    if (
      isString(val)
      || isArray(val)
    ) {
      return val.length > 0 && val.length < amount
        ? errorMessage
        : true
    } else {
      throw new Error(`minLength validator: ${Object.prototype.toString.call(val)} is not a string or array`)
    }
  }

export const isValidEmail = (
  errorMessage = 'Dit veld moet een geldig emailadres zijn'
) =>
  (string) => {
    if (string != null) {
      if (isString(string) && string.length > 0) {
        const isValid = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(string)
        return isValid || errorMessage
      }
    }
  }

export const isTruthy = (
  errorMessage = 'Dit veld moet geselecteerd zijn',
) =>
  (val) => {
    return !!val
  }
