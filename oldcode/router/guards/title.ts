import get from 'lodash/get'

export default (to, from, next) => {
  window.document.title = get(to, 'meta.title', 'Partup')
  next()
}
