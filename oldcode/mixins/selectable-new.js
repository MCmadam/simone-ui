import filter from 'lodash/filter'
import isUndefined from 'lodash/isUndefined'
import some from 'lodash/some'

import replace from '@/util/array/replace'
import comparable from './comparable'
import { valuableFactory } from './valuable'
import { lazyModelFactory } from './lazy-model'

export const selectableFactory = (event = 'select', prop = '_selected', value = 'value') => {
  /**
   * @mixin
   */
  return {
    name: 'selectable',

    mixins: [
      comparable,
      lazyModelFactory(event, prop),
      valuableFactory(value),
    ],

    props: {
      multiple: {
        type: Boolean,
        default: null,
      },
      selected: {
        type: Boolean,
        default: void 0,
      },
    },

    watch: {
      falseValue: {
        handler(newVal, oldVal) {
          this.updateModel(newVal, oldVal, true)
        },
      },
      selected: {
        handler(newVal) {
          this.select(newVal)
        },
        immediate: true,
      },
      trueValue: {
        handler(newVal, oldVal) {
          this.updateModel(newVal, oldVal)
        },
      },
    },

    computed: {
      isActive() {
        const trueValue = this.computedTrueValue
        const modelValue = this.internalValue

        if (this.isMultiple) {
          return Array.isArray(modelValue)
            ? some(modelValue, (item) => this.valueComparator(item, modelValue))
            : false
        }

        return trueValue
          ? this.valueComparator(modelValue, trueValue)
          : Boolean(modelValue)
      },
      isMultiple() {
        return this.multiple === true
          || (this.multiple === null && Array.isArray(this.internalValue))
      },
      hasFalseValue() {
        return !isUndefined(this.falseValue)
      },
    },

    methods: {
      /**
       * Programatically set the selected state of this component
       *
       * @method
       * @public
       * @param {Boolean} val force selected state if given
       */
      select(val) {
        const isActive = this.isActive

        if (!this.shouldUpdate(val)) {
          return
        }

        const trueValue = this.computedTrueValue
        let modelValue = this.internalValue

        if (this.isMultiple) {
          if (!Array.isArray(modelValue)) {
            modelValue = []
          }

          if (isActive) {
            if (this.hasFalseValue) {
              replace(modelValue, trueValue, this.falseValue)
            } else {
              modelValue = filter(modelValue, (item) => !this.valueComparator(item, trueValue))
            }
          } else {
            if (this.hasFalseValue) {
              replace(modelValue, this.falseValue, trueValue)
            } else {
              modelValue.push(trueValue)
            }
          }
        } else {
          if (isActive) {
            modelValue = this.hasFalseValue
              ? this.falseValue
              : modelValue ? null : !modelValue
          } else {
            modelValue = trueValue
              ? trueValue
              : !modelValue
          }
        }

        this.lazyValue = modelValue
      },

      onSelect() {
        this.select()
        this.$emit(event, this.lazyValue)
        this.$emit('selected', this.computedTrueValue)
      },

      shouldUpdate(isFalseValue = false) {
        return (this.isActive && isFalseValue) || (!this.isActive && !isFalseValue)
      },

      updateModel(newVal, oldVal, isFalseValue = false) {
        if (!this.shouldUpdate(isFalseValue)) {
          return
        }

        let modelValue = this.internalValue

        if (this.isMultiple) {
          replace(modelValue, oldVal, newVal)
        } else {
          modelValue = newVal
        }

        this.lazyValue = modelValue
      },
    },
  }
}

export default selectableFactory()
