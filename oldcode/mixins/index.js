import colorable from './colorable'
import comparable, { comparableFactory } from './comparable'
import focusable from './focusable'
import lazyModel, { lazyModelFactory } from './lazy-model'
import positionable from './positionable'
import selectable from './selectable' // , { selectableFactory }
import validatable from './validatable'
import valuable, { valuableFactory } from './valuable'
import transitionable from './transitionable'
import toggleable, { toggleableFactory } from './toggleable'

import {
  inject as registerableInject,
  provide as registerableProvide,
} from './registerable'

export {
  colorable,
  comparable,
  comparableFactory,
  focusable,
  lazyModel,
  lazyModelFactory,
  positionable,
  registerableInject,
  registerableProvide,
  selectable,
  // selectableFactory,
  validatable,
  valuable,
  valuableFactory,
  transitionable,
  toggleable,
  toggleableFactory,
}
