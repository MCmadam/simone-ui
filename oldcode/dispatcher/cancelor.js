/*
  THIS IS A WORK IN PROGRES!
*/
import REQUEST_STATUS from './request-status'

/* eslint-disable require-jsdoc */
/* eslint-disable no-invalid-this */
function Cancelor() {
  function cancel() {
    for (const c of this._current) {
      if (c.status === REQUEST_STATUS.sent) {
        c.cancelSource.cancel()
        c.status = REQUEST_STATUS.canceled
      }
    }

    return this
  }

  return cancel.bind(this)
}

export default Cancelor

// cancel
//   .conditions
//   .add('withParams', (params) => (req) => {
//     JSON.stringify(req.params) === JSON.stringify(params)
//   })
//   .add('withQuery', (query) => (req) => {

//   })

// const params = {}

// cancel
//   .custom((req) => {
//   })
//   .withParams(params)
