import Dispatcher from './dispatcher'

// @TODO: use some default cache

/* eslint-disable require-jsdoc */
class RequestContainer {
  constructor() {
    this._dispatchers = new Map()
  }

  add(id, options) {
    if (!this._dispatchers.has(id)) {
      this._dispatchers.set(id, new Dispatcher(options))
    }
  }

  get(id) {
    return this._dispatchers.get(id)
  }

  remove(id) {
    this._dispatchers.delete(id)
  }
}

export default new RequestContainer()
