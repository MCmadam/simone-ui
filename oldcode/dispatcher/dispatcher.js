import api from '@/api'
import Cancelor from './cancelor'
import CancelToken from 'axios/lib/cancel/CancelToken'
import logger from '@/util/logger'
import Regexp from 'path-to-regexp'
import REQUEST_STATUS from './request-status'

/* eslint-disable require-jsdoc */
export default class Dispatcher {
  constructor(options) {
    this._current = []
    this._history = []

    this.cancel = Cancelor.call(this)
    this.configure(options)
  }

  configure(options) {
    const {
      apiInstance = api,
      defaultInit,
      method,
      path,
      historySize = 10,
    } = options

    if (!method) {
      throw new Error(`request must have a method`)
    }
    if (!path) {
      throw new Error(`request must have a path`)
    }

    this._api = apiInstance
    this._defaultInit = defaultInit
    this._method = method
    this._path = path
    this._historySize = historySize

    return this
  }

  async exec(params, init = this._defaultInit) {
    let current

    try {
      const cancelSource = CancelToken.source()
      const path = this._parameterizePath(this._path, params)

      current = {
        id: this._genID(),
        cancelSource,
        init,
        params,
        status: REQUEST_STATUS.initialized,
      }

      this._enqueue(current)

      if (current.status === REQUEST_STATUS.canceled) {
        logger.debug('request canceled before invoked: ', current)
        return
      }

      current.status = REQUEST_STATUS.sent
      const response = await this._api[this._method](path, {
        ...init,
        cancelToken: cancelSource.token,
      })

      current.status = REQUEST_STATUS.received
      this._dequeue(current)

      return response
    } catch (error) {
      this._dequeue(current)
      throw error
    }
  }

  _enqueue(request) {
    this._current.unshift(request)
  }

  _dequeue(request) {
    let c
    if (request) {
      const i = this._current.findIndex(({ id }) => id === request.id)
      if (i > -1) {
        c = this._current.splice(i, 1)[0]
      }
    } else {
      const last = this._current[this._current.length - 1]
      if (last && (last.status !== REQUEST_STATUS.initialized || last.status !== REQUEST_STATUS.sent)) {
        c = this._current.pop()
      }
    }

    if (c) {
      this._history.unshift(c)
    }

    while (this._history.length > this._historySize) {
      this._history.pop()
    }
  }

  _parameterizePath(path, params) {
    if (!params || typeof params !== 'object') {
      return path
    }

    const filler = Regexp.compile(path)
    return filler(params, { pretty: true })
  }

  _genID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      // eslint-disable-next-line
      let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8)
      return v.toString(16)
    })
  }
}
