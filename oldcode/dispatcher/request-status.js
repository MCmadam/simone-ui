export default Object.freeze({
  initialized: 'initialized',
  sent: 'sent',
  received: 'received',
  canceled: 'canceled',
})
