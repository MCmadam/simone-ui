export default async () => {
  if (!window.fetch) {
    await import(/* webpackChunkName "polyfill-fetch" */ 'whatwg-fetch')
  }

  // #region luxon polyfills
  if (!String.prototype.repeat) {
    // @ts-ignore
    await import(
      /* webpackChunkName "polyfill-string-repeat" */ '@/polyfills/string/prototype/repeat'
    )
  }
  if (!Array.prototype.find) {
    // @ts-ignore
    await import(
      /* webpackChunkName "polyfill-array-find" */ '@/polyfills/array/prototype/find'
    )
  }
  if (!Array.prototype.findIndex) {
    // @ts-ignore
    await import(
      /* webpackChunkName "polyfill-array-findindex" */ '@/polyfills/array/prototype/findIndex'
    )
  }
  if (!Math.trunc) {
    // @ts-ignore
    await import(
      /* webpackChunkName "polyfill-math-trunc" */ '@/polyfills/math/trunc'
    )
  }
  if (!Math.sign) {
    // @ts-ignore
    await import(
      /* webpackChunkName: "polyfill-math-sign" */ '@/polyfills/math/sign'
    )
  }
  // #endregion
}
