// Source
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/trunc#Polyfill

Math.trunc = function(v) {
  return v < 0 ? Math.ceil(v) : Math.floor(v)
}
