
interface LayoutSectionBase {
  name: string;
}

type CombinedLayoutSection<Props, Methods> = Props & Methods & LayoutSectionBase

type LayoutSectionOptions<Props, Methods> = {
  props?: Props;
  methods?: Methods & ThisType<Props & Methods>;
}

type LayoutSection<Props, Methods> = object & CombinedLayoutSection<Props, Methods> & ThisType<CombinedLayoutSection<Props, Methods>>



const createSection = <Props, Methods>(options: LayoutSectionOptions<Props, Methods>): LayoutSection<Props, Methods> => {
  const instance = {
    name: 'trololol',
  }
  const props = options.props || {}
  const methods = options.methods || {}

  Object.assign(instance, {
    ...props,
    ...methods,
  })

  return instance as LayoutSection<Props, Methods>
}

const a = createSection({
  props: {
    expanded: true,
  },
  methods: {
    zegHallo() {

    },
  }
})
