/*
 * function unit tests
 * unit testing function utilities
 */

/* eslint-env jest */

import {
  invokeWithKey, filterAnd, filterOr,
} from './function'

describe('function tests', () => {
  describe('invokeWithKeys', () => {
    it('invokes a function with the given key', () => {
      const sut = {
        random: 'hello',
      }
      const fn = (key) => key

      expect(invokeWithKey('random')(fn)(sut)).toBe('hello')
    })
  })

  describe('filterAnd', () => {
    it('does not include an item when one function returns false', () => {
      expect.assertions(2)

      const sut = [1, 2]
      const sut2 = [
        () => true,
        () => false,
      ]

      expect(sut.filter(filterAnd(...sut2)).length).toBe(0)
      expect(sut.filter(filterAnd(...(sut2.reverse()))).length).toBe(0)
    })

    it('does include all items when all functions return true', () => {
      const sut = [1, 2]
      const sut2 = [
        () => true,
        () => true,
      ]

      expect(sut.filter(filterAnd(...sut2)).length).toBe(2)
    })
  })

  describe('filterOr', () => {
    it('does not include an item when all functions return false', () => {
      const sut = [1, 2]
      const sut2 = [
        () => false,
        () => false,
      ]

      expect(sut.filter(filterOr(...sut2)).length).toBe(0)
    })

    it('does include items when one of the functions returns true', () => {
      expect.assertions(2)

      const sut = [1, 2]
      const sut2 = [
        () => false,
        () => true,
      ]

      expect(sut.filter(filterOr(...sut2)).length).toBe(2)
      expect(sut.filter(filterOr(...(sut2.reverse()))).length).toBe(2)
    })
  })
})
