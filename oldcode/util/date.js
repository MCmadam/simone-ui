import { DateTime } from 'luxon'
import { filter } from 'lodash'
import { invokeWithKey, filterAnd, filterOr } from './function'

export const FORMATS = {
  default: 'dd\u200A/\u200ALL\u200A/\u200Ayyyy',
  message: 'dd\u200A/\u200ALL hh:mm',
}

export const parseDate = (date) => {
  if (!date || typeof date === 'boolean') {
    throw new TypeError(`parseDate 'date' argument is of type ${typeof date} which is not accepted`)
  }

  return DateTime.isDateTime(date)
    ? date
    : date instanceof Date
      ? DateTime.fromJSDate(date)
      : DateTime.fromJSDate(new Date(date))
}

export const isValidDate = (date) => {
  try {
    return parseDate(date).isValid
  } catch (error) {
    return false
  }
}

const endOf = (type) => (date) => {
  return parseDate(date).endOf(type)
}

export const endOfWeek = endOf('week')

export const isAfter = (afterDate) => (isDate) => {
  return parseDate(isDate) > parseDate(afterDate)
}
export const isBefore = (beforeDate) => (isDate) => {
  return parseDate(isDate) < parseDate(beforeDate)
}
export const isEqual = (onDate) => (isDate) => {
  return parseDate(isDate) === parseDate(onDate)
}

export const isInThisWeek = (date) => {
  const endOfThisWeek = endOfWeek(new Date())
  const parsed = parseDate(date)

  return isBefore(endOfThisWeek)(parsed) || isEqual(endOfThisWeek)(parsed)
}

export const areInThisWeek = (items, key = 'end_date') => {
  const endOfThisWeek = endOfWeek(new Date())
  const invokeWithDate = invokeWithKey(key)

  return filter(items,
    filterAnd(
      invokeWithDate(isValidDate),
      filterOr(
        invokeWithDate(isBefore(endOfThisWeek)),
        invokeWithDate(isEqual(endOfThisWeek)),
      )
    )
  )
}

export const areInNextWeek = (items, key = 'end_date') => {
  const endOfThisWeek = endOfWeek(new Date())
  const endOfNextWeek = endOfThisWeek.plus({ days: 7 })
  const invokeWithDate = invokeWithKey(key)

  return filter(items,
    filterAnd(
      invokeWithDate(isValidDate),
      filterOr(
        invokeWithDate(isEqual(endOfNextWeek)),
        filterAnd(
          invokeWithDate(isAfter(endOfThisWeek)),
          invokeWithDate(isBefore(endOfNextWeek)),
        )
      )
    )
  )
}

export const areAfterNextWeekOrNull = (items, key = 'end_date') => {
  const endOfNextWeek = endOfWeek(new Date()).plus({ days: 7 })
  const invokeWithDate = invokeWithKey(key)

  return filter(items,
    filterOr(
      invokeWithDate((x) => x == null),
      filterAnd(
        invokeWithDate(isValidDate),
        invokeWithDate(isAfter(endOfNextWeek))
      )
    )
  )
}
