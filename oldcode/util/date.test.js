import { DateTime } from 'luxon'
import { isValidDate, parseDate, endOfWeek, areAfterNextWeekOrNull, isAfter, isBefore, isOn } from './date'

/*
 * Date unit tests
 * testing functions to work with dates
 */

/* eslint-env jest */

describe('Date tests', () => {
  describe('isValidDate', () => {
    const validDates = [
      DateTime.fromJSDate(new Date()),
      new Date(),
      new Date().toISOString(),
      new Date().toDateString(),
      new Date().toLocaleDateString(),
      Date.now(),
    ]
    for (const date of validDates) {
      it(`returns true when the date argument: '${String(date)}' can be parsed by luxon`, () => {
        expect(isValidDate(date)).toBe(true)
      })
    }

    const invalidDates = [
      undefined,
      null,
      false,
      '',
      'putin',
    ]
    for (const date of invalidDates) {
      it(`returns false when the date argument: '${date}' cannot be parsed by luxon`, () => {
        expect(isValidDate(date)).toBe(false)
      })
    }

    it('does never throw', () => {

    })
  })

  describe('parseDate', () => {
    it('can create a DateTime instance from a JavaScript Date instance', () => {
      const sut = new Date()
      expect(parseDate(sut)).toBeInstanceOf(DateTime)
    })

    it('can create a DateTime instance from an ISO date string', () => {
      const sut = new Date().toISOString()
      expect(parseDate(sut)).toBeInstanceOf(DateTime)
    })

    it('returns the same object when already an instance of DateTime', () => {
      const sut = parseDate(new Date())
      expect(sut).toBe(sut)
    })

    it('throws a TypeError when null or undefined is given', () => {
      expect.assertions(2)

      const sut = [null, undefined]

      expect(() => {
        parseDate(sut[0])
      }).toThrowError(TypeError)
      expect(() => {
        parseDate(sut[1])
      }).toThrowError(TypeError)
    })
  })

  describe('isAfter', () => {
    it('returns a new function when the after date is given', () => {
      const sut = new Date()

      expect(isAfter(sut)).toBeInstanceOf(Function)
    })

    it('returns true when the isDate is after the afterDate', () => {
      const sutIsDate = new Date()
      const sutAfterDate = parseDate(new Date()).minus({ days: 1 })

      expect(isAfter(sutAfterDate)(sutIsDate)).toBe(true)
    })

    it('returns false when the isDate is before the afterDate', () => {
      const sutIsDate = new Date()
      const sutAfterDate = parseDate(new Date()).plus({ days: 1 })

      expect(isAfter(sutAfterDate)(sutIsDate)).toBe(false)
    })

    it('throws when one of the dates is not a valid date', () => {
      expect.assertions(2)

      const sut = [
        { is: new Date(), after: null },
        { is: null, after: new Date() },
      ]

      expect(() => {
        isAfter(sut[0].after)(sut[0].is)
      }).toThrowError(TypeError)

      expect(() => {
        isAfter(sut[1].after)(sut[1].is)
      }).toThrowError(TypeError)
    })
  })

  describe('isBefore', () => {
    it('returns a new function when the after date is given', () => {
      const sut = new Date()

      expect(isBefore(sut)).toBeInstanceOf(Function)
    })

    it('returns true when the isDate is after the afterDate', () => {
      const sutIsDate = new Date()
      const sutBeforeDate = parseDate(new Date()).plus({ days: 1 })

      expect(isBefore(sutBeforeDate)(sutIsDate)).toBe(true)
    })

    it('returns false when the isDate is before the afterDate', () => {
      const sutIsDate = new Date()
      const sutBeforeDate = parseDate(new Date()).minus({ days: 1 })

      expect(isBefore(sutBeforeDate)(sutIsDate)).toBe(false)
    })

    it('throws when one of the dates is not a valid date', () => {
      expect.assertions(2)

      const sut = [
        { is: new Date(), after: null },
        { is: null, after: new Date() },
      ]

      expect(() => {
        isBefore(sut[0].after)(sut[0].is)
      }).toThrowError(TypeError)

      expect(() => {
        isBefore(sut[1].after)(sut[1].is)
      }).toThrowError(TypeError)
    })
  })

  describe('areInThisWeek', () => {
    // @TODO: implement tests..
  })

  describe('areInNextWeek', () => {
    // @TODO: implement tests..
  })

  describe('areAfterNextWeekOrNull', () => {
    it('TODO does not throw, checks for validity', () => {

    })

    it('includes all items with an end_date that is null or undefined', () => {
      const sut = [
        { end_date: undefined },
        { end_date: null },
        { end_date: false },
        { end_date: new Date() },
      ]

      expect(areAfterNextWeekOrNull(sut).length).toBe(2)
    })

    it('TODO includes all items with an end_date that is after next week', () => {

    })

    it('TODO false when', () => {
      // @TODO: implement tests..
    })
  })
})
