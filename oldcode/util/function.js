import {
  get,
  noop,
  isFunction,
  isEqual,
} from 'lodash'

export const assert = (val, expected) => {
  return isEqual(isFunction(val) ? val() : val, expected)
}

export const invokeWithKey = (key) => (fn) => (obj) => {
  return fn(obj[key])
}

export const filterAnd = (...fns) => (...args) => {
  let isTrue = false

  for (const fn of fns) {
    isTrue = fn(...args)

    if (!isTrue) {
      return isTrue
    }
  }

  return Boolean(isTrue)
}

export const filterOr = (...fns) => (...args) => {
  let isTrue = false

  for (const fn of fns) {
    isTrue = fn(...args)

    if (isTrue) {
      return isTrue
    }
  }

  return Boolean(isTrue)
}
